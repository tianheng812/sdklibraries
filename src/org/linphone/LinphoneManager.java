package org.linphone;

import static android.media.AudioManager.MODE_RINGTONE;
import static android.media.AudioManager.STREAM_RING;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.Vibrator;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.linphone.compatibility.Compatibility;
import org.linphone.core.CallDirection;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneCallStats;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.core.LinphoneContent;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;
import org.linphone.core.LinphoneCoreFactoryImpl;
import org.linphone.core.LinphoneCoreListener;
import org.linphone.core.LinphoneEvent;
import org.linphone.core.LinphoneFriend;
import org.linphone.core.LinphoneInfoMessage;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.core.PayloadType;
import org.linphone.core.PresenceActivityType;
import org.linphone.core.PresenceModel;
import org.linphone.core.PublishState;
import org.linphone.core.Reason;
import org.linphone.core.SubscriptionState;
import org.linphone.core.TunnelConfig;
import org.linphone.core.LinphoneCall.State;
import org.linphone.mediastream.Log;
import org.linphone.mediastream.Version;
import org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfiguration;
import org.linphone.mediastream.video.capture.hwconf.Hacks;
import org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfiguration.AndroidCamera;
import org.linphone.tools.BooleanUtil;
import org.linphone.tools.StringUtil;

import com.sayee.sdk.KeepAliveReceiver;
import com.sayee.sdk.LinphoneService;
import com.sayee.sdk.activity.IncomingCallActivity;
import com.sayee.sdk.utils.SharedPreferencesUtil;
import com.sayee.sdk.utils.ToolsUtil;

public class LinphoneManager implements LinphoneCoreListener {
	private static final int LINPHONE_VOLUME_STREAM = 0;
	private static final int dbStep = 4;
	private static LinphoneManager instance;
	private static boolean sExited;
	private static boolean sLastProximitySensorValueNearby;
	private static Set<Activity> sProximityDependentActivities = new HashSet<Activity>();
	private static SensorEventListener sProximitySensorListener = new SensorEventListener() {
		public void onAccuracyChanged(Sensor paramAnonymousSensor,
				int paramAnonymousInt) {
		}

		public void onSensorChanged(SensorEvent paramAnonymousSensorEvent) {
			if (paramAnonymousSensorEvent.timestamp == 0L)
				return;
			sLastProximitySensorValueNearby = isProximitySensorNearby(paramAnonymousSensorEvent);
			proximityNearbyChanged();
		}
	};
	private static List<LinphoneSimpleListener> simpleListeners = new ArrayList<LinphoneSimpleListener>();
	private String basePath;
	private LinphoneSimpleListener.LinphoneOnComposingReceivedListener composingReceivedListener;
	private LinphoneSimpleListener.ConnectivityChangedListener connectivityListener;
	private boolean disableRinging = false;
	private LinphoneSimpleListener.LinphoneOnDTMFReceivedListener dtmfReceivedListener;
	private boolean isRinging;
	private String lastLcStatusMessage;
	private static boolean mAudioFocused;
	private AudioManager mAudioManager;
	private final String mChatDatabaseFile;
	private ConnectivityManager mConnectivityManager;
	// private final String mErrorToneFile;
	private Handler mHandler = new Handler();
	private PowerManager.WakeLock mIncallWakeLock;
	private BroadcastReceiver mKeepAliveReceiver = new KeepAliveReceiver();
	private final String mLPConfigXsd;
	private LinphoneCore mLc;
	public final String mLinphoneConfigFile;
	private final String mLinphoneFactoryConfigFile;
	private final String mLinphoneRootCaFile;
	private ListenerDispatcher mListenerDispatcher;
	private final String mPauseSoundFile;
	private PowerManager mPowerManager;
	private LinphonePreferences mPrefs;
	private Resources mR;
	private final String mRingSoundFile;
	private final String mRingbackSoundFile;
	private MediaPlayer mRingerPlayer;
	private Context mServiceContext;
	private Timer mTimer;
	private Vibrator mVibrator;
	private LinphoneSimpleListener.LinphoneOnNotifyReceivedListener notifyReceivedListener;
	private LinphoneSimpleListener.LinphoneOnRemoteProvisioningListener remoteProvisioningListener;
	private LinphoneCall ringingCall;
	private int savedMaxCallWhileGsmIncall;
	public String wizardLoginViewDomain = null;

	protected LinphoneManager(
			Context paramContext,
			LinphoneSimpleListener.LinphoneServiceListener paramLinphoneServiceListener) {
		sExited = false;
		this.mServiceContext = paramContext;
		this.mListenerDispatcher = new ListenerDispatcher(
				paramLinphoneServiceListener);
		this.basePath = paramContext.getFilesDir().getAbsolutePath();
		this.mLPConfigXsd = this.basePath + "/lpconfig.xsd";
		this.mLinphoneFactoryConfigFile = this.basePath + "/linphonerc";
		this.mLinphoneConfigFile = this.basePath + "/.linphonerc";
		this.mLinphoneRootCaFile = this.basePath + "/rootca.pem";
		this.mRingSoundFile = this.basePath + "/oldphone_mono.wav";
		this.mRingbackSoundFile = this.basePath + "/ringback.wav";
		this.mPauseSoundFile = this.basePath + "/toy_mono.wav";
		this.mChatDatabaseFile = this.basePath + "/linphone-history.db";
		// this.mErrorToneFile = this.basePath +"/error.wav";
		this.mPrefs = LinphonePreferences.instance();
		this.mAudioManager = ((AudioManager) paramContext
				.getSystemService("audio"));
		this.mVibrator = ((Vibrator) paramContext.getSystemService("vibrator"));
		this.mPowerManager = ((PowerManager) paramContext
				.getSystemService("power"));
		this.mConnectivityManager = ((ConnectivityManager) paramContext
				.getSystemService("connectivity"));
		this.mR = paramContext.getResources();
		
		
	}

	private void copyAssetsFromPackage() throws IOException {
		copyIfNotExist("oldphone_mono.wav", mRingSoundFile);
		copyIfNotExist("ringback.wav", mRingbackSoundFile);
		copyIfNotExist("toy_mono.wav", mPauseSoundFile);
		// copyIfNotExist("error.wav", mErrorToneFile);
		copyIfNotExist("linphonerc_default", mLinphoneConfigFile);
		copyFromPackage("linphonerc_factory", new File(
				mLinphoneFactoryConfigFile).getName());
		copyIfNotExist("lpconfig.xsd", mLPConfigXsd);
		copyIfNotExist("rootca.pem", mLinphoneRootCaFile);
	}

	public static void addListener(
			LinphoneSimpleListener paramLinphoneSimpleListener) {
		if (!simpleListeners.contains(paramLinphoneSimpleListener))
			simpleListeners.add(paramLinphoneSimpleListener);
	}

	private void allowSIPCalls() {
		if (savedMaxCallWhileGsmIncall == 0) {
			Log.w("SIP calls are already allowed as no GSM call known to be running");
			return;
		}
		mLc.setMaxCalls(savedMaxCallWhileGsmIncall);
		savedMaxCallWhileGsmIncall = 0;
	}

	private void checkRingTimeout() {
		if ((this.mLc.isIncall()) && (this.mLc.getCurrentCall() != null)) {
			int i = this.mPrefs.getOutgoingCallTimeout();
			long l = (System.currentTimeMillis() - this.mLc.getCurrentCall()
					.getCallLog().getTimestamp()) / 1000L;
			LinphoneCall.State state = mLc.getCurrentCall().getState();
			if ((i != 0)
					&& ((state == LinphoneCall.State.OutgoingRinging) || (state == LinphoneCall.State.OutgoingEarlyMedia))
					&& (l > i)) {
				Log.w("Ringing for " + l + "seconds, hang up");
				terminateCall();
			}
		}
	}

	public static final LinphoneManager createAndStart(
			Context paramContext,
			LinphoneSimpleListener.LinphoneServiceListener paramLinphoneServiceListener) {
		if (instance != null)
			return instance;
		instance = new LinphoneManager(paramContext,paramLinphoneServiceListener);
		instance.startLibLinphone(paramContext);
		boolean bool = ((TelephonyManager) paramContext.getSystemService("phone")).getCallState() == TelephonyManager.CALL_STATE_IDLE;
		setGsmIdle(bool);

		return instance;
	}

	public static void destroy() {
		if (instance == null)
			return;
		getInstance().changeStatusToOffline();
		sExited = true;
		instance.doDestroy();
		System.gc();
	}

	@TargetApi(11)
	private void doDestroy() {
		if (LinphoneService.isReady())
			ChatStorage.getInstance().close();
		// BluetoothManager.getInstance().destroy();
		try {
			if(mTimer!=null)
				mTimer.cancel();
			mLc.destroy();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			try {
				if (instance != null && instance.mKeepAliveReceiver != null) {
					mServiceContext
							.unregisterReceiver(instance.mKeepAliveReceiver);
				}
			} catch (Exception e) {

			}
			mLc = null;
			instance = null;
		}
	}

	public static String extractADisplayName(Resources r,
			LinphoneAddress address) {
		if (address == null)
			return "Unknown";

		final String displayName = address.getDisplayName();
		if (displayName != null) {
			return displayName;
		} else if (address.getUserName() != null) {
			return address.getUserName();
		} else {
			String rms = address.toString();
			if (rms != null && rms.length() > 1)
				return rms;

			return "Unknown";
		}
	}

	public static String extractIncomingRemoteName(Resources paramResources,
			LinphoneAddress paramLinphoneAddress) {
		return extractADisplayName(paramResources, paramLinphoneAddress);
	}

	public static final LinphoneManager getInstance() {
		if (instance != null)
			return instance;
		if (sExited) {
			/*throw new RuntimeException(
					"Linphone Manager was already destroyed. "
							+ "Better use getLcIfManagerNotDestroyed and check returned value");*/
			return null;
		}
		
		return null;

		/*throw new RuntimeException(
				"Linphone Manager should be created before accessed");*/

	}

	public static final LinphoneCore getLc() {
		return getInstance().mLc;
	}

	public static LinphoneCore getLcIfManagerNotDestroyedOrNull() {
		if (sExited || instance == null) {
			return null;
		}
		return getLc();
	}

	@SuppressWarnings("unchecked")
	private <T> List<T> getSimpleListeners(Class<T> paramClass) {
		ArrayList localArrayList = new ArrayList();
		Iterator<LinphoneSimpleListener> localIterator = simpleListeners
				.iterator();
		while (localIterator.hasNext()) {
			LinphoneSimpleListener localLinphoneSimpleListener = (LinphoneSimpleListener) localIterator
					.next();
			if (paramClass.isInstance(localLinphoneSimpleListener))
				localArrayList.add(localLinphoneSimpleListener);
		}
		return localArrayList;
	}

	private void initLiblinphone() throws LinphoneCoreException {

		//boolean isDebugLogEnabled = mPrefs.isDebugEnabled();
		LinphoneCoreFactory.instance().setDebugMode(false,
				"syaeeSdk");

		PreferencesMigrator prefMigrator = new PreferencesMigrator(
				mServiceContext);
		prefMigrator.migrateRemoteProvisioningUriIfNeeded();
		prefMigrator.migrateSharingServerUrlIfNeeded();
		prefMigrator.douseRandomPort();

		if (prefMigrator.isMigrationNeeded()) {
			prefMigrator.doMigration();
		}

		// Some devices could be using software AEC before
		// This will disable it in favor of hardware AEC if available
		if (prefMigrator.isEchoMigratioNeeded()) {
			Log.d("Echo canceller configuration need to be updated");
			prefMigrator.doEchoMigration();
			mPrefs.echoConfigurationUpdated();
		}

		mLc.setContext(mServiceContext);
		mLc.setZrtpSecretsCache(basePath + "/zrtp_secrets");
		if (mPrefs != null) {
			mPrefs.enableVideo(true);
			mPrefs.setInitiateVideoCall(true);
			mPrefs.setVideoPreview(true);
		}

		try {
			String versionName = mServiceContext.getPackageManager()
					.getPackageInfo(mServiceContext.getPackageName(), 0).versionName;
			if (versionName == null) {
				versionName = String
						.valueOf(mServiceContext.getPackageManager()
								.getPackageInfo(
										mServiceContext.getPackageName(), 0).versionCode);
			}
			mLc.setUserAgent("sayeeSdk", versionName);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		if (BooleanUtil.getInstance().isUse_linphonecore_ringing()) {
			disableRinging();
		} else {
			mLc.setRing(null);
		}
		mLc.setRootCA(mLinphoneRootCaFile);
		mLc.setPlayFile(mPauseSoundFile);
		mLc.setChatDatabasePath(mChatDatabaseFile);
		// mLc.setCallErrorTone(Reason.NotFound, mErrorToneFile);

		int availableCores = Runtime.getRuntime().availableProcessors();
		mLc.setCpuCount(availableCores);

		int migrationResult = getLc().migrateToMultiTransport();
		Log.d("Migration to multi transport result = " + migrationResult);

		if (BooleanUtil.getInstance().isEnable_push_id()) {
			Compatibility.initPushNotificationService(mServiceContext);
		}

		IntentFilter lFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
		lFilter.addAction(Intent.ACTION_SCREEN_OFF);
		mServiceContext.registerReceiver(mKeepAliveReceiver, lFilter);

		updateNetworkReachability(true);

		/*
		 * if (Version.sdkAboveOrEqual(Version.API11_HONEYCOMB_30)) {
		 * BluetoothManager.getInstance().initBluetooth(); }
		 */
		resetCameraFromPreferences();
		for (final PayloadType pt : mLc.getVideoCodecs()) {
			if (!pt.getMime().equals("VP8")) {
				if (!Version.hasFastCpuWithAsmOptim()&& pt.getMime().equals("H264")) {
					Log.w("CPU does not have asm optimisations available, disabling H264");
					continue;
				}

				if (pt.getMime().equals("H263")|| pt.getMime().equals("H263-1998")) {
					continue;
				}
			}
			//Log.i("llllllllllllllllll   "+mLc.isPayloadTypeEnabled(pt)+" "+pt.getMime() );
			mLc.enablePayloadType(pt, true);
		}

	}

	public static final boolean isInstanciated() {
		return instance != null;
	}

	private boolean isPresenceModelActivitySet() {
		LinphoneCore lc = getLcIfManagerNotDestroyedOrNull();
		if (isInstanciated() && lc != null) {
			return lc.getPresenceModel() != null
					&& lc.getPresenceModel().getActivity() != null;
		}
		return false;
	}

	public static Boolean isProximitySensorNearby(SensorEvent event) {
		float threshold = 4.001f; // <= 4 cm is near

		final float distanceInCm = event.values[0];
		final float maxDistance = event.sensor.getMaximumRange();
		Log.d("Proximity sensor report [" + distanceInCm
				+ "] , for max range [ " + maxDistance + "]");

		if (maxDistance <= threshold) {
			// Case binary 0/1 and short sensors
			threshold = maxDistance;
		}

		return distanceInCm < threshold;
	}

	private boolean isTunnelNeeded(NetworkInfo info) {
		if (info == null) {
			Log.i("No connectivity: tunnel should be disabled");
			return false;
		}

		String pref = mPrefs.getTunnelMode();

		if (StringUtil.getInstance().getTunnel_mode_entry_value_always()
				.equals(pref)) {
			return true;
		}

		if (info.getType() != ConnectivityManager.TYPE_WIFI
				&& StringUtil.getInstance()
						.getTunnel_mode_entry_value_3G_only().equals(pref)) {
			Log.i("need tunnel: 'no wifi' connection");
			return true;
		}

		return false;
	}

	private void manageTunnelServer(NetworkInfo info) {
		if (mLc == null)
			return;

		if (!mLc.isTunnelAvailable())
			return;
		Log.i("Managing tunnel");
		if (isTunnelNeeded(info)) {
			Log.i("Tunnel need to be activated");
			mLc.tunnelSetMode(LinphoneCore.TunnelMode.enable);
		} else {
			Log.i("Tunnel should not be used");
			String pref = mPrefs.getTunnelMode();
			mLc.tunnelSetMode(LinphoneCore.TunnelMode.disable);
			if (StringUtil.getInstance().getTunnel_mode_entry_value_auto()
					.equals(pref)) {
				mLc.tunnelSetMode(LinphoneCore.TunnelMode.auto);
			}
		}
	}

	private void preventSIPCalls() {
		if (savedMaxCallWhileGsmIncall != 0) {
			Log.w("SIP calls are already blocked due to GSM call running");
			return;
		}
		savedMaxCallWhileGsmIncall = mLc.getMaxCalls();
		mLc.setMaxCalls(0);
	}

	private static void proximityNearbyChanged() {
		Iterator<Activity> localIterator = sProximityDependentActivities
				.iterator();
		while (localIterator.hasNext())
			simulateProximitySensorNearby((Activity) localIterator.next(),
					sLastProximitySensorValueNearby);
	}

	public static boolean reinviteWithVideo() {
		return CallManager.getInstance().reinviteWithVideo();
	}

	public static void removeListener(
			LinphoneSimpleListener paramLinphoneSimpleListener) {
		simpleListeners.remove(paramLinphoneSimpleListener);
	}

	public void requestAudioFocus() {
		if (!mAudioFocused) {
			int res = mAudioManager.requestAudioFocus(null,
					AudioManager.STREAM_VOICE_CALL,
					AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
			Log.d("Audio focus requested: "
					+ (res == AudioManager.AUDIOFOCUS_REQUEST_GRANTED ? "Granted"
							: "Denied"));
			if (res == AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
				mAudioFocused = true;
		}
	}

	private void resetCameraFromPreferences() {
		// boolean bool = this.mPrefs.useFrontCam();
		int i = 0;
		AndroidCamera[] androidCamera = AndroidCameraConfiguration
				.retrieveCameras();
		if (androidCamera.length > 0) {
			for (AndroidCameraConfiguration.AndroidCamera localAndroidCamera : androidCamera)
				if (localAndroidCamera.frontFacing) {
					i = localAndroidCamera.id;
				}
			getLc().setVideoDevice(i);
		}
	}

	private void routeAudioToSpeakerHelper(boolean speakerOn) {
		// BluetoothManager.getInstance().disableBluetoothSCO();
		mLc.enableSpeaker(speakerOn);
	}

	public static void setGsmIdle(boolean gsmIdle) {
		LinphoneManager localLinphoneManager = instance;
		if (localLinphoneManager == null)
			return;
		if (gsmIdle) {
			localLinphoneManager.allowSIPCalls();
		} else {
			localLinphoneManager.preventSIPCalls();
		}
	}

	private static void simulateProximitySensorNearby(Activity activity,
			boolean nearby) {
		final Window window = activity.getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		View view = ((ViewGroup) window.getDecorView().findViewById(
				android.R.id.content)).getChildAt(0);
		if (nearby) {
			params.screenBrightness = 0.1f;
			view.setVisibility(View.INVISIBLE);
			Compatibility.hideNavigationBar(activity);
		} else {
			params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
			view.setVisibility(View.VISIBLE);
			Compatibility.showNavigationBar(activity);
		}
		window.setAttributes(params);
	}

	private void startLibLinphone(Context c) {
		try {
			try {
				copyAssetsFromPackage();
			} catch (Exception e) {
				e.printStackTrace();
			}
			boolean isDebugLogEnabled = !BooleanUtil.getInstance()
					.isDisable_every_log();
			LinphoneCoreFactory.instance().setDebugMode(isDebugLogEnabled,
					"sayee");

			mLc = LinphoneCoreFactory.instance().createLinphoneCore(this,
					mLinphoneConfigFile, mLinphoneFactoryConfigFile, null, c);

			TimerTask local1 = new TimerTask() {
				public void run() {
					new Handler(Looper.getMainLooper()).post(new Runnable() {

						@Override
						public void run() {
							try {
								mLc.iterate();
								checkRingTimeout();
							} catch (Exception e) {

							}
						}
					});

				}
			};
			this.mTimer = new Timer("Linphone scheduler");
			this.mTimer.schedule(local1, 0L, 20L);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void startProximitySensorForActivity(Activity paramActivity) {
		if (sProximityDependentActivities.contains(paramActivity)) {
			Log.i("proximity sensor already active for "
					+ paramActivity.getLocalClassName());
			return;
		}
		if (sProximityDependentActivities.isEmpty()) {
			SensorManager localSensorManager = (SensorManager) paramActivity
					.getSystemService("sensor");
			Sensor localSensor = localSensorManager.getDefaultSensor(8);
			if (localSensor != null) {
				localSensorManager.registerListener(sProximitySensorListener,
						localSensor, 2);
				Log.i("Proximity sensor detected, registering");
			}
			sProximityDependentActivities.add(paramActivity);
		}
		if (sLastProximitySensorValueNearby)
			simulateProximitySensorNearby(paramActivity, true);
	}

	private void startRinging() {
		if (disableRinging) {
			// Enable speaker audio route, linphone library will do the ringing
			// itself automatically
			routeAudioToSpeaker();
			return;
		}

		if (SharedPreferencesUtil.isAllowRinging(getContext())) {
			routeAudioToSpeaker(); // Need to be able to ear the ringtone during
									// the early media
		}

		if (mAudioManager == null)
			return;

		if (Hacks.needGalaxySAudioHack()) {
			mAudioManager.setMode(MODE_RINGTONE);
		}

		try {
			if (SharedPreferencesUtil.isAllowVibrate(getContext())) {
				if ((mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE || mAudioManager
						.getRingerMode() == AudioManager.RINGER_MODE_NORMAL)
						&& mVibrator != null) {
					long[] patern = { 0, 1000, 1000 };
					mVibrator.vibrate(patern, 1);
				}
			}

			if (SharedPreferencesUtil.isAllowRinging(getContext())) {
				if (mRingerPlayer == null) {
					requestAudioFocus();
					mRingerPlayer = new MediaPlayer();
					mRingerPlayer.setAudioStreamType(STREAM_RING);

					String ringtone = LinphonePreferences
							.instance()
							.getRingtone(
									android.provider.Settings.System.DEFAULT_RINGTONE_URI
											.toString());
					try {
						if (ringtone.startsWith("content://")) {
							mRingerPlayer.setDataSource(mServiceContext,
									Uri.parse(ringtone));
						} else {
							FileInputStream fis = new FileInputStream(ringtone);
							mRingerPlayer.setDataSource(fis.getFD());
							fis.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
						Log.e(e, "Cannot set ringtone");
					}

					mRingerPlayer.prepare();
					mRingerPlayer.setLooping(true);
					mRingerPlayer.start();
				} else {
					Log.w("already ringing");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		isRinging = true;
	}

	public static void stopProximitySensorForActivity(Activity paramActivity) {
		sProximityDependentActivities.remove(paramActivity);
		simulateProximitySensorNearby(paramActivity, false);
		if (sProximityDependentActivities.isEmpty()) {
			((SensorManager) paramActivity.getSystemService("sensor"))
					.unregisterListener(sProximitySensorListener);
			sLastProximitySensorValueNearby = false;
		}
	}

	private void stopRinging() {
		if (this.mRingerPlayer != null) {
			this.mRingerPlayer.stop();
			this.mRingerPlayer.release();
			this.mRingerPlayer = null;
		}
		if (this.mVibrator != null)
			this.mVibrator.cancel();
		if (Hacks.needGalaxySAudioHack())
			this.mAudioManager.setMode(0);
		this.isRinging = false;
		routeAudioToReceiver();
		/*
		 * if (!BluetoothManager.getInstance().isBluetoothHeadsetAvailable()) {
		 * Log.d(new Object[] { "Stopped ringing, routing back to earpiece" });
		 * routeAudioToReceiver(); }
		 */
	}

	public boolean acceptCall(LinphoneCall paramLinphoneCall) {
		try {
			this.mLc.acceptCall(paramLinphoneCall);
			return true;
		} catch (LinphoneCoreException localLinphoneCoreException) {
			Log.e(localLinphoneCoreException, "Accept call failed");
		}
		return false;
	}

	public boolean acceptCallIfIncomingPending() throws LinphoneCoreException {
		if (this.mLc.isInComingInvitePending()) {
			this.mLc.acceptCall(this.mLc.getCurrentCall());
			return true;
		}
		return false;
	}

	public boolean acceptCallWithParams(LinphoneCall paramLinphoneCall,
			LinphoneCallParams paramLinphoneCallParams) {
		try {
			this.mLc.acceptCallWithParams(paramLinphoneCall,
					paramLinphoneCallParams);
			return true;
		} catch (LinphoneCoreException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void addConnectivityChangedListener(
			LinphoneSimpleListener.ConnectivityChangedListener connectivityListener) {
		this.connectivityListener = connectivityListener;
	}

	public boolean addVideo() {
		enableCamera(this.mLc.getCurrentCall(), true);
		return reinviteWithVideo();
	}

	public void adjustVolume(int i) {
		if (Build.VERSION.SDK_INT < 15) {
			int oldVolume = mAudioManager
					.getStreamVolume(LINPHONE_VOLUME_STREAM);
			int maxVolume = mAudioManager
					.getStreamMaxVolume(LINPHONE_VOLUME_STREAM);

			int nextVolume = oldVolume + i;
			if (nextVolume > maxVolume)
				nextVolume = maxVolume;
			if (nextVolume < 0)
				nextVolume = 0;

			mLc.setPlaybackGain((nextVolume - maxVolume) * dbStep);

		} else {
			mAudioManager.adjustStreamVolume(LINPHONE_VOLUME_STREAM,
					i < 0 ? AudioManager.ADJUST_LOWER
							: AudioManager.ADJUST_RAISE, 0);
		}
	}

	public void audioStateChanged(LinphoneSimpleListener.LinphoneOnAudioChangedListener.AudioState paramAudioState) {
		Iterator<LinphoneSimpleListener.LinphoneOnAudioChangedListener> localIterator = getSimpleListeners(
				LinphoneSimpleListener.LinphoneOnAudioChangedListener.class).iterator();
		while (localIterator.hasNext())
			((LinphoneSimpleListener.LinphoneOnAudioChangedListener) localIterator.next()).onAudioStateChanged(paramAudioState);
	}

	public void authInfoRequested(LinphoneCore paramLinphoneCore,String paramString1, String paramString2, String paramString3) {
	}

	public void byeReceived(LinphoneCore paramLinphoneCore, String paramString) {
	}

	public void callEncryptionChanged(LinphoneCore paramLinphoneCore,LinphoneCall paramLinphoneCall, boolean paramBoolean,String paramString) {
		if (mListenerDispatcher != null)
			this.mListenerDispatcher.onCallEncryptionChanged(paramLinphoneCall,
					paramBoolean, paramString);
	}

	public void callState(LinphoneCore paramLinphoneCore, LinphoneCall call,
			LinphoneCall.State state, String message) {
		Log.i("----------------------", "-----new state [" + state+ "]----------");

		if (mListenerDispatcher != null)
			mListenerDispatcher.onCallStateChanged(call, state, message);
		
		if (state == LinphoneCall.State.Error&&!ToolsUtil.isRegistration()) {
			if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null)
				LinphoneManager.getLc().refreshRegisters();
			return;
		}

		if (state == LinphoneCall.State.OutgoingProgress || state == LinphoneCall.State.IncomingReceived) {
			if (!LinphoneUtils.isHasPermission(getContext())) {
				if(mLc!=null && mLc.isIncall()){
					Log.i(" ----------------在通话中");
					return;
				}else{
					Log.i(" --------------不在通话中");
					if(mLc!=null) mLc.terminateAllCalls();
					return;
				}
			}
		}

		
	 if (state == LinphoneCall.State.IncomingReceived || state == LinphoneCall.State.CallIncomingEarlyMedia) {
		 Context activity = getContext();
			if (mLc.getCallsNb() == 1) {
				ringingCall = call;
				if(activity!=null&&SharedPreferencesUtil.getSipIsCall(activity))
					startRinging();
			}
		} else if (call == ringingCall && isRinging) {
			// previous state was ringing, so stop ringing
			stopRinging();
		}

		if (state == LinphoneCall.State.Connected) {
			if (mLc.getCallsNb() == 1) {
				requestAudioFocus();
				Compatibility.setAudioManagerInCallMode(mAudioManager);
			}

			if (Hacks.needSoftvolume()) {
				Log.w("Using soft volume audio hack");
				adjustVolume(0); // Synchronize
			}
		}

		if (state == LinphoneCall.State.CallReleased|| state == LinphoneCall.State.Error) {
			if (mLc.getCallsNb() == 0) {
				if (mAudioFocused) {
					mAudioFocused = false;
				}

				Context activity = getContext();
				if (activity != null) {
					TelephonyManager tm = (TelephonyManager) activity
							.getSystemService(Context.TELEPHONY_SERVICE);
					if (tm.getCallState() == TelephonyManager.CALL_STATE_IDLE) {
						mAudioManager.setMode(AudioManager.MODE_NORMAL);
						routeAudioToReceiver();
					}
				}
			}
		}
		
		if (state == LinphoneCall.State.OutgoingEarlyMedia) {
			Compatibility.setAudioManagerInCallMode(mAudioManager);
		}else if (state == LinphoneCall.State.CallEnd) {
			if (mLc.getCallsNb() == 0) {
				if (mIncallWakeLock != null && mIncallWakeLock.isHeld()) {
					mIncallWakeLock.release();
					Log.i("Last call ended: releasing incall (CPU only) wake lock");
				} else {
					Log.i("Last call ended: no incall (CPU only) wake lock were held");
				}
			}
		}else if (state == LinphoneCall.State.StreamsRunning) {
			if (mIncallWakeLock == null) {
				mIncallWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "incall");
			}
			if (!mIncallWakeLock.isHeld()) {
				Log.i("New call active : acquiring incall (CPU only) wake lock");
				mIncallWakeLock.acquire();
			} else {
				Log.i("New call active while incall (CPU only) wake lock already active");
			}
		}
		
	}

	public void callStatsUpdated(LinphoneCore paramLinphoneCore,
			LinphoneCall paramLinphoneCall,
			LinphoneCallStats paramLinphoneCallStats) {
	}

	public void changeStatusToOffline() {
		if ((isInstanciated())
				&& (isPresenceModelActivitySet())
				&& (getLc().getPresenceModel().getActivity().getType() != PresenceActivityType.Offline))
			getLc().getPresenceModel().getActivity()
					.setType(PresenceActivityType.Offline);
		if ((!isInstanciated()) || (isPresenceModelActivitySet()))
			return;
		PresenceModel localPresenceModel = LinphoneCoreFactoryImpl.instance()
				.createPresenceModel(PresenceActivityType.Offline, null);
		getLc().setPresenceModel(localPresenceModel);
	}

	public void changeStatusToOnThePhone() {
		if ((isInstanciated())
				&& (isPresenceModelActivitySet())
				&& (getLc().getPresenceModel().getActivity().getType() != PresenceActivityType.OnThePhone))
			getLc().getPresenceModel().getActivity()
					.setType(PresenceActivityType.OnThePhone);
		if ((!isInstanciated()) || (isPresenceModelActivitySet()))
			return;
		PresenceModel localPresenceModel = LinphoneCoreFactoryImpl.instance()
				.createPresenceModel(PresenceActivityType.OnThePhone, null);
		getLc().setPresenceModel(localPresenceModel);
	}

	public void changeStatusToOnline() {
		if ((isInstanciated())
				&& (getLcIfManagerNotDestroyedOrNull() != null)
				&& (isPresenceModelActivitySet())
				&& (getLc().getPresenceModel().getActivity().getType() != PresenceActivityType.Online))
			getLc().getPresenceModel().getActivity()
					.setType(PresenceActivityType.Online);
		if ((!isInstanciated()) || (getLcIfManagerNotDestroyedOrNull() == null)
				|| (isPresenceModelActivitySet()))
			return;
		PresenceModel localPresenceModel = LinphoneCoreFactoryImpl.instance()
				.createPresenceModel(PresenceActivityType.Online, null);
		getLc().setPresenceModel(localPresenceModel);
	}

	public void configuringStatus(LinphoneCore paramLinphoneCore,
			LinphoneCore.RemoteProvisioningState paramRemoteProvisioningState,
			String paramString) {
		Log.d("Remote provisioning status = "
				+ paramRemoteProvisioningState.toString() + " (" + paramString
				+ ")");
		if (this.remoteProvisioningListener != null)
			this.remoteProvisioningListener
					.onConfiguringStatus(paramRemoteProvisioningState);
		if ((paramRemoteProvisioningState == LinphoneCore.RemoteProvisioningState.ConfiguringSuccessful)
				&& (LinphonePreferences.instance()
						.isProvisioningLoginViewEnabled())) {
			LinphoneProxyConfig localLinphoneProxyConfig = paramLinphoneCore
					.createProxyConfig();
			try {
				this.wizardLoginViewDomain = LinphoneCoreFactory
						.instance()
						.createLinphoneAddress(
								localLinphoneProxyConfig.getIdentity())
						.getDomain();
			} catch (LinphoneCoreException e) {
				e.printStackTrace();
				this.wizardLoginViewDomain = null;
			}
		}
	}

	public void connectivityChanged(
			ConnectivityManager paramConnectivityManager, boolean paramBoolean) {
		NetworkInfo localNetworkInfo = paramConnectivityManager.getActiveNetworkInfo();
		updateNetworkReachability(false);
		if (connectivityListener != null)
			connectivityListener.onConnectivityChanged(mServiceContext,
					localNetworkInfo, paramConnectivityManager);
	}

	public void copyFromPackage(String fileName, String paramString)
			throws IOException {
		FileOutputStream localFileOutputStream = mServiceContext
				.openFileOutput(paramString, 0);
		InputStream localInputStream = mR.getAssets().open(fileName);
		byte[] arrayOfByte = new byte[8048];
		int i = -1;
		while ((i = localInputStream.read(arrayOfByte)) != -1) {
			localFileOutputStream.write(arrayOfByte, 0, i);
		}
		localFileOutputStream.flush();
		localFileOutputStream.close();
		localInputStream.close();
	}

	public void copyIfNotExist(String fileName, String paramString)
			throws IOException {
		File localFile = new File(paramString);
		if (!localFile.exists())
			copyFromPackage(fileName, localFile.getName());
	}

	public boolean detectAudioCodec(String paramString) {
		PayloadType[] arrayOfPayloadType = this.mLc.getAudioCodecs();
		int i = arrayOfPayloadType.length;
		for (int j = 0; j < i; j++)
			if (paramString.equals(arrayOfPayloadType[j].getMime()))
				return true;
		return false;
	}

	public boolean detectVideoCodec(String paramString) {
		PayloadType[] arrayOfPayloadType = this.mLc.getVideoCodecs();
		int i = arrayOfPayloadType.length;
		for (int j = 0; j < i; j++)
			if (paramString.equals(arrayOfPayloadType[j].getMime()))
				return true;
		return false;
	}

	public void disableRinging() {
		this.disableRinging = true;
	}

	public void displayMessage(LinphoneCore paramLinphoneCore,
			String paramString) {
	}

	public void displayStatus(LinphoneCore paramLinphoneCore, String paramString) {
		this.lastLcStatusMessage = paramString;
		this.mListenerDispatcher.onDisplayStatus(paramString);
	}

	public void displayWarning(LinphoneCore paramLinphoneCore,
			String paramString) {
	}

	public void dtmfReceived(LinphoneCore paramLinphoneCore,
			LinphoneCall paramLinphoneCall, int paramInt) {
		Log.i("--------------------   dtmfReceived   DTMF received: "
				+ paramInt);
		if (this.dtmfReceivedListener != null)
			this.dtmfReceivedListener.onDTMFReceived(paramLinphoneCall,
					paramInt);
	}

	public void ecCalibrationStatus(LinphoneCore paramLinphoneCore,
			LinphoneCore.EcCalibratorStatus paramEcCalibratorStatus,
			int paramInt, Object paramObject) {
		routeAudioToReceiver();
		((EcCalibrationListener) paramObject).onEcCalibrationStatus(
				paramEcCalibratorStatus, paramInt);
	}

	public void enableCamera(LinphoneCall paramLinphoneCall,
			boolean paramBoolean) {
		if (paramLinphoneCall != null) {
			paramLinphoneCall.enableCamera(paramBoolean);
		}
	}

	public void fileTransferProgressIndication(LinphoneCore paramLinphoneCore,
			LinphoneChatMessage paramLinphoneChatMessage,
			LinphoneContent paramLinphoneContent, int paramInt) {
	}

	public void fileTransferRecv(LinphoneCore paramLinphoneCore,
			LinphoneChatMessage paramLinphoneChatMessage,
			LinphoneContent paramLinphoneContent, byte[] paramArrayOfByte,
			int paramInt) {
	}

	public int fileTransferSend(LinphoneCore paramLinphoneCore,
			LinphoneChatMessage paramLinphoneChatMessage,
			LinphoneContent paramLinphoneContent, ByteBuffer paramByteBuffer,
			int paramInt) {
		return 0;
	}

	public Context getContext() {
		try {
			if (this.mServiceContext != null)
				return this.mServiceContext;
			if (LinphoneService.isReady()) {
				return LinphoneService.instance().getApplicationContext();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return LinphoneService.instance().getApplicationContext();

	}

	public String getLPConfigXsdPath() {
		return this.mLPConfigXsd;
	}

	public String getLastLcStatusMessage() {
		return this.lastLcStatusMessage;
	}

	public LinphoneCall getPendingIncomingCall() {
		LinphoneCall currentCall = this.mLc.getCurrentCall();
		if (currentCall == null)
			return null;
		LinphoneCall.State state = currentCall.getState();
		boolean incomingPending = currentCall.getDirection() == CallDirection.Incoming
				&& (state == LinphoneCall.State.IncomingReceived || state == LinphoneCall.State.CallIncomingEarlyMedia);
		return incomingPending ? currentCall : null;
	}

	@SuppressLint("InlinedApi")
	public String getUserAgent() throws PackageManager.NameNotFoundException {
		StringBuilder localStringBuilder = new StringBuilder();
		localStringBuilder.append("LinphoneAndroid/"
				+ mServiceContext.getPackageManager().getPackageInfo(
						mServiceContext.getPackageName(), 0).versionCode);
		localStringBuilder.append(" (");
		localStringBuilder.append("Linphone/" + getLc().getVersion() + "; ");
		localStringBuilder.append(Build.DEVICE + " " + Build.MODEL
				+ " Android/" + Build.VERSION.SDK_INT);
		localStringBuilder.append(")");
		return localStringBuilder.toString();
	}

	public void globalState(LinphoneCore paramLinphoneCore,
			LinphoneCore.GlobalState paramGlobalState, String paramString) {
		Log.i(new Object[] { "new state [", paramGlobalState, "]" });
		if (paramGlobalState == LinphoneCore.GlobalState.GlobalOn)
			this.mHandler.post(new Runnable() {
				public void run() {
					try {
						initLiblinphone();
					} catch (LinphoneCoreException e) {
						e.printStackTrace();
					}
				}
			});
		this.mListenerDispatcher.onGlobalStateChanged(paramGlobalState,
				paramString);
	}

	public void infoReceived(LinphoneCore paramLinphoneCore,
			LinphoneCall paramLinphoneCall,
			LinphoneInfoMessage paramLinphoneInfoMessage) {
	}

	public void initTunnelFromConf() {
		if (!mLc.isTunnelAvailable())
			return;

		NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
		mLc.tunnelCleanServers();
		TunnelConfig config = mPrefs.getTunnelConfig();
		if (config.getHost() != null) {
			mLc.tunnelAddServer(config);
			manageTunnelServer(info);
		}
	}

	public void isComposingReceived(LinphoneCore paramLinphoneCore,
			LinphoneChatRoom paramLinphoneChatRoom) {
		Log.i("--------------- Composing received for chatroom "
				+ paramLinphoneChatRoom.getPeerAddress().asStringUriOnly());
		if (this.composingReceivedListener != null)
			this.composingReceivedListener
					.onComposingReceived(paramLinphoneChatRoom);
	}

	public void messageReceived(LinphoneCore paramLinphoneCore,
			LinphoneChatRoom paramLinphoneChatRoom, LinphoneChatMessage message) {
	}

	public void newOutgoingCall(String to, String displayName) {
		LinphoneAddress lAddress;

		if (mLc.isIncall()) {
			if (mListenerDispatcher != null)
				mListenerDispatcher.tryingNewOutgoingCallButAlreadyInCall();
			return;
		}

		try {
			lAddress = mLc.interpretUrl(to);
			LinphoneProxyConfig lpc = mLc.getDefaultProxyConfig();
			if (BooleanUtil.getInstance().isForbid_self_call() && lpc != null
					&& lAddress.asStringUriOnly().equals(lpc.getIdentity())) {
				if (mListenerDispatcher != null)
					mListenerDispatcher
							.tryingNewOutgoingCallButWrongDestinationAddress();
				return;
			}
		} catch (LinphoneCoreException e) {
			if (mListenerDispatcher != null)
				mListenerDispatcher
						.tryingNewOutgoingCallButWrongDestinationAddress();
			return;
		}
		lAddress.setDisplayName(displayName);

		boolean isLowBandwidthConnection = !LinphoneUtils
				.isHightBandwidthConnection(LinphoneService.instance()
						.getApplicationContext());

		if (mLc.isNetworkReachable()) {
			try {
				if (Version.isVideoCapable()) {
					CallManager.getInstance().inviteAddress(lAddress, true,
							isLowBandwidthConnection);
				} else {
					CallManager.getInstance().inviteAddress(lAddress, false,
							isLowBandwidthConnection);
				}

			} catch (LinphoneCoreException e) {
				if (mListenerDispatcher != null)
					mListenerDispatcher
							.tryingNewOutgoingCallButCannotGetCallParameters();
				return;
			}
		} else {
			Log.d("Error: Network is unreachable");
		}
	}

	public void newOutgoingCall(AddressType address) {
		newOutgoingCall(address.getText().toString(),
				address.getDisplayedName());
	}

	public void newSubscriptionRequest(LinphoneCore paramLinphoneCore,
			LinphoneFriend paramLinphoneFriend, String paramString) {

	}

	public void notifyPresenceReceived(LinphoneCore paramLinphoneCore,
			LinphoneFriend paramLinphoneFriend) {

	}

	public void notifyReceived(LinphoneCore paramLinphoneCore,
			LinphoneCall paramLinphoneCall,
			LinphoneAddress paramLinphoneAddress, byte[] paramArrayOfByte) {

	}

	public void notifyReceived(LinphoneCore paramLinphoneCore,
			LinphoneEvent paramLinphoneEvent, String paramString,
			LinphoneContent paramLinphoneContent) {
		Log.d("------------Notify received for event " + paramString);
		if (paramLinphoneContent != null) {
			Log.d("-------------with content " + paramLinphoneContent.getType()
					+ "/" + paramLinphoneContent.getSubtype() + " data:"
					+ paramLinphoneContent.getDataAsString());
		}
		if (this.notifyReceivedListener != null)
			this.notifyReceivedListener.onNotifyReceived(paramLinphoneEvent,
					paramString, paramLinphoneContent);
	}

	public void playDtmf(ContentResolver paramContentResolver, char paramChar) {
		try {
			int i = Settings.System.getInt(paramContentResolver, "dtmf_tone");
			if (i == 0)
				return;
		} catch (Settings.SettingNotFoundException e) {
			e.printStackTrace();
			getLc().playDtmf(paramChar, -1);
		}
	}

	public void publishStateChanged(LinphoneCore paramLinphoneCore,
			LinphoneEvent paramLinphoneEvent, PublishState paramPublishState) {
	}

	private boolean isFrist=true;
	public void registrationState(LinphoneCore paramLinphoneCore,
			LinphoneProxyConfig paramLinphoneProxyConfig,
			LinphoneCore.RegistrationState state,
			String paramString) {
		Log.i("--------- 注册状态  ----- [" + state + "  ---  "+(state==LinphoneCore.RegistrationState.RegistrationOk)
				+ "]   "+paramLinphoneProxyConfig.getDomain());
		
		if (this.notifyReceivedListener != null)
			this.mListenerDispatcher.onRegistrationStateChanged(paramLinphoneProxyConfig, state,paramString);
		
		if(state==LinphoneCore.RegistrationState.RegistrationOk&&isFrist&&mLc!=null)
			for (final PayloadType pt : mLc.getVideoCodecs()) {
				if (pt!=null&&!pt.getMime().equals("VP8")) {
					try {
						mLc.enablePayloadType(pt, false);
					} catch (LinphoneCoreException e) {
						e.printStackTrace();
					}
				}
		}
	}

	public void routeAudioToReceiver() {
		routeAudioToSpeakerHelper(false);
	}

	public void routeAudioToSpeaker() {
		routeAudioToSpeakerHelper(true);
	}

	public void sendStaticImage(boolean send) {
		if (mLc.isIncall()) {
			enableCamera(mLc.getCurrentCall(), !send);
		}
	}

	public void setNotifyReceivedListener(
			LinphoneSimpleListener.LinphoneOnNotifyReceivedListener paramLinphoneOnNotifyReceivedListener) {
		this.notifyReceivedListener = paramLinphoneOnNotifyReceivedListener;
	}

	public void setOnComposingReceivedListener(
			LinphoneSimpleListener.LinphoneOnComposingReceivedListener paramLinphoneOnComposingReceivedListener) {
		this.composingReceivedListener = paramLinphoneOnComposingReceivedListener;
	}

	public void setOnDTMFReceivedListener(
			LinphoneSimpleListener.LinphoneOnDTMFReceivedListener paramLinphoneOnDTMFReceivedListener) {
		this.dtmfReceivedListener = paramLinphoneOnDTMFReceivedListener;
	}

	public void setOnRemoteProvisioningListener(
			LinphoneSimpleListener.LinphoneOnRemoteProvisioningListener paramLinphoneOnRemoteProvisioningListener) {
		this.remoteProvisioningListener = paramLinphoneOnRemoteProvisioningListener;
	}

	public void show(LinphoneCore paramLinphoneCore) {
	}

	public void startEcCalibration(
			EcCalibrationListener paramEcCalibrationListener)
			throws LinphoneCoreException {
		routeAudioToSpeaker();
		this.mAudioManager.setStreamVolume(0,
				mAudioManager.getStreamMaxVolume(0), 0);
		this.mLc.startEchoCalibration(paramEcCalibrationListener);
		this.mAudioManager.setStreamVolume(0, mAudioManager.getStreamVolume(0),
				0);
	}

	public void subscriptionStateChanged(LinphoneCore core,
			LinphoneEvent paramLinphoneEvent, SubscriptionState state) {
		Log.d("Subscription state changed to " + state + " event name is "
				+ paramLinphoneEvent.getEventName());
	}

	public void terminateCall() {
		if (this.mLc.isIncall())
			this.mLc.terminateCall(this.mLc.getCurrentCall());
	}

	public void textReceived(LinphoneCore paramLinphoneCore,
			LinphoneChatRoom paramLinphoneChatRoom,
			LinphoneAddress paramLinphoneAddress, String paramString) {
	}

	public boolean toggleEnableCamera() {
		if (mLc.isIncall()) {
			boolean enabled = !mLc.getCurrentCall().cameraEnabled();
			enableCamera(mLc.getCurrentCall(), enabled);
			return enabled;
		}
		return false;
	}

	public void transferState(LinphoneCore paramLinphoneCore,
			LinphoneCall paramLinphoneCall, LinphoneCall.State paramState) {
	}

	public void updateNetworkReachability(boolean isFirst) {
		if (mServiceContext == null)
			return;
		ConnectivityManager cm = (ConnectivityManager) mServiceContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo eventInfo = cm.getActiveNetworkInfo();
		if (eventInfo == null || eventInfo.getState() == NetworkInfo.State.DISCONNECTED) {
			Log.i("当前没有网络");
			if (mLc != null) {
				mLc.setNetworkReachable(false);
			}
			System.gc();
		} else if (eventInfo.getState() == NetworkInfo.State.CONNECTED) {
			System.gc();
			Log.i("网络来了");
			manageTunnelServer(eventInfo);
			if (mLc != null) {
				mLc.setNetworkReachable(true);
				if (!isFirst&&eventInfo!=null && (eventInfo.getType() == 0 || eventInfo.getType() == 1)) {
					ToolsUtil.refreshRegisters(getContext());
				}
			}
		}
	}

	public static abstract interface AddressType {
		public abstract String getDisplayedName();

		public abstract CharSequence getText();

		public abstract void setDisplayedName(String paramString);

		public abstract void setText(CharSequence paramCharSequence);
	}

	public static abstract interface EcCalibrationListener {
		public abstract void onEcCalibrationStatus(
				LinphoneCore.EcCalibratorStatus paramEcCalibratorStatus,
				int paramInt);
	}

	private class ListenerDispatcher implements
			LinphoneSimpleListener.LinphoneServiceListener {
		private LinphoneSimpleListener.LinphoneServiceListener serviceListener;

		public ListenerDispatcher(
				LinphoneSimpleListener.LinphoneServiceListener localObject) {
			this.serviceListener = localObject;
		}

		public void onCallEncryptionChanged(LinphoneCall paramLinphoneCall,
				boolean paramBoolean, String paramString) {
			if (this.serviceListener != null)
				this.serviceListener.onCallEncryptionChanged(paramLinphoneCall,
						paramBoolean, paramString);
			Iterator localIterator = LinphoneManager.this
					.getSimpleListeners(
							LinphoneSimpleListener.LinphoneOnCallEncryptionChangedListener.class)
					.iterator();
			while (localIterator.hasNext())
				((LinphoneSimpleListener.LinphoneOnCallEncryptionChangedListener) localIterator
						.next()).onCallEncryptionChanged(paramLinphoneCall,
						paramBoolean, paramString);
		}

		public void onCallStateChanged(LinphoneCall paramLinphoneCall,
				LinphoneCall.State paramState, String paramString) {
			if ((paramState == LinphoneCall.State.OutgoingInit)
					|| (paramState == LinphoneCall.State.IncomingReceived)) {
				boolean sendCamera = mLc.getConferenceSize() == 0;
				enableCamera(paramLinphoneCall, sendCamera);
			}

			if (serviceListener != null)
				serviceListener.onCallStateChanged(paramLinphoneCall,
						paramState, paramString);

			Iterator<LinphoneOnCallStateChangedListener> localIterator = LinphoneManager.this
					.getSimpleListeners(
							LinphoneSimpleListener.LinphoneOnCallStateChangedListener.class)
					.iterator();
			while (localIterator.hasNext()) {
				((LinphoneSimpleListener.LinphoneOnCallStateChangedListener) localIterator
						.next()).onCallStateChanged(paramLinphoneCall,
						paramState, paramString);
			}
		}

		public void onDisplayStatus(String paramString) {
			if (this.serviceListener != null)
				this.serviceListener.onDisplayStatus(paramString);
		}

		public void onGlobalStateChanged(
				LinphoneCore.GlobalState paramGlobalState, String paramString) {
			if (this.serviceListener != null)
				this.serviceListener.onGlobalStateChanged(paramGlobalState,
						paramString);
		}

		public void onRegistrationStateChanged(
				LinphoneProxyConfig paramLinphoneProxyConfig,
				LinphoneCore.RegistrationState paramRegistrationState,
				String paramString) {
			if (this.serviceListener != null)
				this.serviceListener.onRegistrationStateChanged(
						paramLinphoneProxyConfig, paramRegistrationState,
						paramString);
			Iterator localIterator = LinphoneManager.this
					.getSimpleListeners(
							LinphoneSimpleListener.LinphoneOnRegistrationStateChangedListener.class)
					.iterator();
			while (localIterator.hasNext())
				((LinphoneSimpleListener.LinphoneOnRegistrationStateChangedListener) localIterator
						.next()).onRegistrationStateChanged(
						paramLinphoneProxyConfig, paramRegistrationState,
						paramString);
		}

		public void tryingNewOutgoingCallButAlreadyInCall() {
			if (this.serviceListener != null)
				this.serviceListener.tryingNewOutgoingCallButAlreadyInCall();
		}

		public void tryingNewOutgoingCallButCannotGetCallParameters() {
			if (this.serviceListener != null)
				this.serviceListener
						.tryingNewOutgoingCallButCannotGetCallParameters();
		}

		public void tryingNewOutgoingCallButWrongDestinationAddress() {
			if (this.serviceListener != null)
				this.serviceListener
						.tryingNewOutgoingCallButWrongDestinationAddress();
		}

	}

	public static abstract interface NewOutgoingCallUiListener {

		public abstract void onAlreadyInCall();

		public abstract void onCannotGetCallParameters();

		public abstract void onWrongDestinationAddress();
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.LinphoneManager JD-Core Version: 0.6.2
 */
package org.linphone.gcm;

import android.content.Context;
import com.google.android.gcm.GCMBroadcastReceiver;

public class GCMReceiver extends GCMBroadcastReceiver
{
  protected String getGCMIntentServiceClassName(Context paramContext)
  {
    return "org.linphone.gcm.GCMService";
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.gcm.GCMReceiver
 * JD-Core Version:    0.6.2
 */
package org.linphone.gcm;

import android.content.Context;
import android.content.Intent;
import com.google.android.gcm.GCMBaseIntentService;
import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.mediastream.Log;

public class GCMService extends GCMBaseIntentService
{
  protected String[] getSenderIds(Context paramContext)
  {
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramContext.getString(2131296279);
    return arrayOfString;
  }

  protected void onError(Context paramContext, String paramString)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = ("Error while registering push notification : " + paramString);
    Log.e(arrayOfObject);
  }

  protected void onMessage(Context paramContext, Intent paramIntent)
  {
    Log.d(new Object[] { "Push notification received" });
    if ((LinphoneManager.isInstanciated()) && (LinphoneManager.getLc().getCallsNb() == 0))
    {
      LinphoneManager.getLc().setNetworkReachable(false);
      LinphoneManager.getLc().setNetworkReachable(true);
    }
  }

  protected void onRegistered(Context paramContext, String paramString)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = ("Registered push notification : " + paramString);
    Log.d(arrayOfObject);
    LinphonePreferences.instance().setPushNotificationRegistrationID(paramString);
  }

  protected void onUnregistered(Context paramContext, String paramString)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = ("Unregistered push notification : " + paramString);
    Log.w(arrayOfObject);
    LinphonePreferences.instance().setPushNotificationRegistrationID(null);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.gcm.GCMService
 * JD-Core Version:    0.6.2
 */
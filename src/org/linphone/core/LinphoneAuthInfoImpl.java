package org.linphone.core;

class LinphoneAuthInfoImpl
  implements LinphoneAuthInfo
{
  protected final long nativePtr;
  boolean ownPtr = false;

  protected LinphoneAuthInfoImpl(long paramLong)
  {
    this.nativePtr = paramLong;
    this.ownPtr = false;
  }

  protected LinphoneAuthInfoImpl(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    this(paramString1, null, paramString2, null, paramString3, paramString4);
  }

  protected LinphoneAuthInfoImpl(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    this.nativePtr = newLinphoneAuthInfo();
    setUsername(paramString1);
    setUserId(paramString2);
    setPassword(paramString3);
    setHa1(paramString4);
    setDomain(paramString6);
    setRealm(paramString5);
    this.ownPtr = true;
  }

  private native void delete(long paramLong);

  private native String getDomain(long paramLong);

  private native String getHa1(long paramLong);

  private native String getPassword(long paramLong);

  private native String getRealm(long paramLong);

  private native String getUserId(long paramLong);

  private native String getUsername(long paramLong);

  private native long newLinphoneAuthInfo();

  private native void setDomain(long paramLong, String paramString);

  private native void setHa1(long paramLong, String paramString);

  private native void setPassword(long paramLong, String paramString);

  private native void setRealm(long paramLong, String paramString);

  private native void setUserId(long paramLong, String paramString);

  private native void setUsername(long paramLong, String paramString);

  public LinphoneAuthInfo clone()
  {
    return LinphoneCoreFactory.instance().createAuthInfo(getUsername(), getUserId(), getPassword(), getHa1(), getRealm(), getDomain());
  }

  protected void finalize()
    throws Throwable
  {
    if (this.ownPtr)
      delete(this.nativePtr);
  }

  public String getDomain()
  {
    return getDomain(this.nativePtr);
  }

  public String getHa1()
  {
    return getHa1(this.nativePtr);
  }

  public String getPassword()
  {
    return getPassword(this.nativePtr);
  }

  public String getRealm()
  {
    return getRealm(this.nativePtr);
  }

  public String getUserId()
  {
    return getUserId(this.nativePtr);
  }

  public String getUsername()
  {
    return getUsername(this.nativePtr);
  }

  public void setDomain(String paramString)
  {
    setDomain(this.nativePtr, paramString);
  }

  public void setHa1(String paramString)
  {
    setHa1(this.nativePtr, paramString);
  }

  public void setPassword(String paramString)
  {
    setPassword(this.nativePtr, paramString);
  }

  public void setRealm(String paramString)
  {
    setRealm(this.nativePtr, paramString);
  }

  public void setUserId(String paramString)
  {
    setUserId(this.nativePtr, paramString);
  }

  public void setUsername(String paramString)
  {
    setUsername(this.nativePtr, paramString);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneAuthInfoImpl
 * JD-Core Version:    0.6.2
 */
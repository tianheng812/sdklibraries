package org.linphone.core;

public abstract interface ErrorInfo
{
  public abstract String getDetails();

  public abstract String getPhrase();

  public abstract int getProtocolCode();

  public abstract Reason getReason();
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.ErrorInfo
 * JD-Core Version:    0.6.2
 */
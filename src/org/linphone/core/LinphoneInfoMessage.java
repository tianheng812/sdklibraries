package org.linphone.core;

public abstract interface LinphoneInfoMessage
{
  public abstract void addHeader(String paramString1, String paramString2);

  public abstract LinphoneContent getContent();

  public abstract String getHeader(String paramString);

  public abstract void setContent(LinphoneContent paramLinphoneContent);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneInfoMessage
 * JD-Core Version:    0.6.2
 */
package org.linphone.core;

public class LinphonePlayerImpl implements LinphonePlayer {
	private long nativePtr = 0L;

	LinphonePlayerImpl(long paramLong) {
		this.nativePtr = paramLong;
	}

	private native void close(long paramLong);

	private native void destroy(long paramLong);

	private native int getCurrentPosition(long paramLong);

	private native int getDuration(long paramLong);

	private native int getState(long paramLong);

	private native int open(long paramLong, String paramString,
			LinphonePlayer.Listener paramListener);

	private native int pause(long paramLong);

	private native int seek(long paramLong, int paramInt);

	private native int start(long paramLong);

	public void close() {
		close(this.nativePtr);
	}

	protected void finalize() {
		destroy(this.nativePtr);
	}

	public int getCurrentPosition() {
		return getCurrentPosition(this.nativePtr);
	}

	public int getDuration() {
		return getDuration(this.nativePtr);
	}

	public LinphonePlayer.State getState() {
		return LinphonePlayer.State.fromValue(getState(this.nativePtr));
	}

	public int open(String paramString, LinphonePlayer.Listener paramListener) {
		return open(this.nativePtr, paramString, paramListener);
	}

	public int pause() {
		return pause(this.nativePtr);
	}

	public int seek(int paramInt) {
		return seek(this.nativePtr, paramInt);
	}

	public int start() {
		return start(this.nativePtr);
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphonePlayerImpl JD-Core Version: 0.6.2
 */
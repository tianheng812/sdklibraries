package org.linphone.core;

public class LinphoneChatMessageImpl implements LinphoneChatMessage {
	protected final long nativePtr;

	protected LinphoneChatMessageImpl(long paramLong) {
		this.nativePtr = paramLong;
	}

	private native void addCustomHeader(long paramLong, String paramString1,String paramString2);

	private native String getAppData(long paramLong);

	private native String getCustomHeader(long paramLong, String paramString);

	private native long getErrorInfo(long paramLong);

	private native String getExternalBodyUrl(long paramLong);

	private native Object getFileTransferInformation(long paramLong);

	private native long getFrom(long paramLong);

	private native long getPeerAddress(long paramLong);

	private native int getReason(long paramLong);

	private native int getStatus(long paramLong);

	private native int getStorageId(long paramLong);

	private native String getText(long paramLong);

	private native long getTime(long paramLong);

	private native long getTo(long paramLong);

	private native boolean isOutgoing(long paramLong);

	private native boolean isRead(long paramLong);

	private native void setAppData(long paramLong, String paramString);

	private native void setExternalBodyUrl(long paramLong, String paramString);

	private native void startFileDownload(long paramLong,LinphoneChatMessage.StateListener paramStateListener);

	private native void store(long paramLong);

	private native void unref(long paramLong);

	public void addCustomHeader(String paramString1, String paramString2) {
		addCustomHeader(this.nativePtr, paramString1, paramString2);
	}

	protected void finalize() throws Throwable {
		unref(this.nativePtr);
		super.finalize();
	}

	public String getAppData() {
		return getAppData(this.nativePtr);
	}

	public String getCustomHeader(String paramString) {
		return getCustomHeader(this.nativePtr, paramString);
	}

	public ErrorInfo getErrorInfo() {
		return new ErrorInfoImpl(getErrorInfo(this.nativePtr));
	}

	public String getExternalBodyUrl() {
		return getExternalBodyUrl(this.nativePtr);
	}

	public LinphoneContent getFileTransferInformation() {
		return (LinphoneContent) getFileTransferInformation(this.nativePtr);
	}

	public LinphoneAddress getFrom() {
		return new LinphoneAddressImpl(getFrom(this.nativePtr),
				LinphoneAddressImpl.WrapMode.FromConst);
	}

	public long getNativePtr() {
		return this.nativePtr;
	}

	public LinphoneAddress getPeerAddress() {
		return new LinphoneAddressImpl(getPeerAddress(this.nativePtr),
				LinphoneAddressImpl.WrapMode.FromConst);
	}

	public Reason getReason() {
		return Reason.fromInt(getReason(this.nativePtr));
	}

	public LinphoneChatMessage.State getStatus() {
		return LinphoneChatMessage.State.fromInt(getStatus(this.nativePtr));
	}

	public int getStorageId() {
		return getStorageId(this.nativePtr);
	}

	public String getText() {
		return getText(this.nativePtr);
	}

	public long getTime() {
		return 1000L * getTime(this.nativePtr);
	}

	public LinphoneAddress getTo() {
		return new LinphoneAddressImpl(getTo(this.nativePtr),
				LinphoneAddressImpl.WrapMode.FromConst);
	}

	public boolean isOutgoing() {
		return isOutgoing(this.nativePtr);
	}

	public boolean isRead() {
		return isRead(this.nativePtr);
	}

	public void setAppData(String paramString) {
		setAppData(this.nativePtr, paramString);
	}

	public void setExternalBodyUrl(String paramString) {
		setExternalBodyUrl(this.nativePtr, paramString);
	}

	public void startFileDownload(@SuppressWarnings("deprecation") LinphoneChatMessage.StateListener paramStateListener) {
		startFileDownload(this.nativePtr, paramStateListener);
	}

	public void store() {
		store(this.nativePtr);
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneChatMessageImpl JD-Core Version:
 * 0.6.2
 */
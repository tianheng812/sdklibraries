package org.linphone.core;

public abstract interface PayloadType
{
  public abstract String getMime();

  public abstract int getRate();

  public abstract String getRecvFmtp();

  public abstract String getSendFmtp();

  public abstract void setRecvFmtp(String paramString);

  public abstract void setSendFmtp(String paramString);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PayloadType
 * JD-Core Version:    0.6.2
 */
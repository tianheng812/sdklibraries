 package org.linphone.core;
  
 import java.util.Vector; 
  
 public abstract interface LinphoneCall 
 { 
   public abstract State getState(); 
    
   public abstract LinphoneAddress getRemoteAddress(); 
    
   public abstract CallDirection getDirection(); 
    
   public abstract LinphoneCallLog getCallLog(); 
    
   public abstract LinphoneCallStats getAudioStats(); 
    
   public abstract LinphoneCallStats getVideoStats(); 
    
   public abstract LinphoneCallParams getRemoteParams(); 
    
   public abstract LinphoneCallParams getCurrentParamsCopy(); 
    
   public abstract void enableCamera(boolean paramBoolean); 
    
   public abstract boolean cameraEnabled(); 
    
   public abstract void enableEchoCancellation(boolean paramBoolean); 
    
   public abstract boolean isEchoCancellationEnabled(); 
    
   public abstract void enableEchoLimiter(boolean paramBoolean); 
    
   public abstract boolean isEchoLimiterEnabled(); 
    
   public abstract LinphoneCall getReplacedCall(); 
    
   public abstract int getDuration(); 
    
   public abstract float getCurrentQuality(); 
    
   public abstract float getAverageQuality(); 
    
   public abstract String getAuthenticationToken(); 
    
   public abstract boolean isAuthenticationTokenVerified(); 
    
   public abstract void setAuthenticationTokenVerified(boolean paramBoolean); 
    
   public abstract boolean isInConference(); 
    
   public abstract boolean mediaInProgress(); 
    
   public abstract float getPlayVolume(); 
    
   public abstract String getRemoteUserAgent(); 
    
   public abstract String getRemoteContact(); 
    
   public abstract void takeSnapshot(String paramString);
   
   public abstract void zoomVideo(float paramFloat1, float paramFloat2, float paramFloat3); 
    
   public abstract void startRecording(); 
    
   public abstract void stopRecording(); 
    
   public abstract State getTransferState(); 
    
   public abstract void sendInfoMessage(LinphoneInfoMessage paramLinphoneInfoMessage); 
    
   public abstract LinphoneCall getTransfererCall(); 
    
   public abstract LinphoneCall getTransferTargetCall(); 
    
   public abstract Reason getReason(); 
    
   public abstract ErrorInfo getErrorInfo(); 
    
   public abstract void setUserData(Object paramObject); 
    
   public abstract Object getUserData(); 
    
   public abstract LinphonePlayer getPlayer(); 
    
   public static class State 
   { 
     private static Vector<State> values = new Vector<State>(); 
     private final int mValue; 
     private final String mStringValue; 
      
     public final int value() 
     { 
       return this.mValue; 
     } 
      
     public static final State Idle = new State(0, "Idle"); 
     public static final State IncomingReceived = new State(1, "IncomingReceived"); 
     public static final State OutgoingInit = new State(2, "OutgoingInit"); 
     public static final State OutgoingProgress = new State(3, "OutgoingProgress"); 
     public static final State OutgoingRinging = new State(4, "OutgoingRinging"); 
     public static final State OutgoingEarlyMedia = new State(5, "OutgoingEarlyMedia"); 
     public static final State Connected = new State(6, "Connected"); 
     public static final State StreamsRunning = new State(7, "StreamsRunning"); 
     public static final State Pausing = new State(8, "Pausing"); 
     public static final State Paused = new State(9, "Paused"); 
     public static final State Resuming = new State(10, "Resuming"); 
     public static final State Refered = new State(11, "Refered"); 
     public static final State Error = new State(12, "Error"); 
     public static final State CallEnd = new State(13, "CallEnd"); 
     public static final State PausedByRemote = new State(14, "PausedByRemote"); 
     public static final State CallUpdatedByRemote = new State(15, "UpdatedByRemote"); 
     public static final State CallIncomingEarlyMedia = new State(16, "IncomingEarlyMedia"); 
     public static final State CallUpdating = new State(17, "Updating"); 
     public static final State CallReleased = new State(18, "Released"); 
     public static final State CallEarlyUpdatedByRemote = new State(19, "EarlyUpdatedByRemote"); 
     public static final State CallEarlyUpdating = new State(20, "EarlyUpdating"); 
      
     private State(int value, String stringValue) 
     { 
       this.mValue = value; 
       values.addElement(this); 
       this.mStringValue = stringValue; 
     } 
      
     public static State fromInt(int value) 
     { 
       for (int i = 0; i < values.size(); i++) 
       { 
         State state = (State)values.elementAt(i); 
         if (state.mValue == value) { 
           return state; 
         } 
       } 
       throw new RuntimeException("state not found [" + value + "]"); 
     } 
      
     public String toString() 
     { 
       return this.mStringValue; 
     } 
   } 
 } 
 

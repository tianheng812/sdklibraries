package org.linphone.core;

public class LinphoneContentImpl
  implements LinphoneContent
{
  private byte[] mData;
  private String mEncoding;
  private int mExpectedSize;
  private String mName;
  private String mSubtype;
  private String mType;

  public LinphoneContentImpl(String paramString1, String paramString2, String paramString3, byte[] paramArrayOfByte, String paramString4, int paramInt)
  {
    this.mType = paramString2;
    this.mSubtype = paramString3;
    this.mData = paramArrayOfByte;
    this.mEncoding = paramString4;
    this.mName = paramString1;
    this.mExpectedSize = paramInt;
  }

  public LinphoneContentImpl(String paramString1, String paramString2, byte[] paramArrayOfByte, String paramString3)
  {
    this.mType = paramString1;
    this.mSubtype = paramString2;
    this.mData = paramArrayOfByte;
    this.mEncoding = paramString3;
    this.mName = null;
    this.mExpectedSize = 0;
  }

  public byte[] getData()
  {
    return this.mData;
  }

  public String getDataAsString()
  {
    if (this.mData != null)
      return new String(this.mData);
    return null;
  }

  public String getEncoding()
  {
    return this.mEncoding;
  }

  public int getExpectedSize()
  {
    return this.mExpectedSize;
  }

  public String getName()
  {
    return this.mName;
  }

  public int getRealSize()
  {
    if (this.mData != null)
      return this.mData.length;
    return 0;
  }

  public String getSubtype()
  {
    return this.mSubtype;
  }

  public String getType()
  {
    return this.mType;
  }

  public void setData(byte[] paramArrayOfByte)
  {
    this.mData = paramArrayOfByte;
  }

  public void setEncoding(String paramString)
  {
    this.mEncoding = paramString;
  }

  public void setExpectedSize(int paramInt)
  {
    this.mExpectedSize = paramInt;
  }

  public void setName(String paramString)
  {
    this.mName = paramString;
  }

  public void setStringData(String paramString)
  {
    if (paramString != null)
    {
      this.mData = paramString.getBytes();
      return;
    }
    this.mData = null;
  }

  public void setSubtype(String paramString)
  {
    this.mSubtype = paramString;
  }

  public void setType(String paramString)
  {
    this.mType = paramString;
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneContentImpl
 * JD-Core Version:    0.6.2
 */
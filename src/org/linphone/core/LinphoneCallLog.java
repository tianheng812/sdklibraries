package org.linphone.core;
  
 import java.util.Vector; 
  
 public abstract interface LinphoneCallLog 
 { 
   public abstract LinphoneAddress getFrom(); 
    
   public abstract LinphoneAddress getTo(); 
    
   public abstract CallDirection getDirection(); 
    
   public abstract CallStatus getStatus(); 
    
   public abstract String getStartDate(); 
    
   public abstract long getTimestamp(); 
    
   public abstract int getCallDuration(); 
    
   public abstract int getCallId(); 
    
   public static class CallStatus 
   { 
     private static Vector<CallStatus> values = new Vector(); 
     private final int mValue; 
     private final String mStringValue; 
     public static final CallStatus Success = new CallStatus(0, "Sucess"); 
     public static final CallStatus Aborted = new CallStatus(1, "Aborted"); 
     public static final CallStatus Missed = new CallStatus(2, "Missed"); 
     public static final CallStatus Declined = new CallStatus(3, "Declined"); 
      
     private CallStatus(int value, String stringValue) 
     { 
       this.mValue = value; 
       values.addElement(this); 
       this.mStringValue = stringValue; 
     } 
      
     public static CallStatus fromInt(int value) 
     { 
       for (int i = 0; i < values.size(); i++) 
       { 
         CallStatus state = (CallStatus)values.elementAt(i); 
         if (state.mValue == value) { 
           return state; 
         } 
       } 
       throw new RuntimeException("CallStatus not found [" + value + "]"); 
     } 
      
     public String toString() 
     { 
       return this.mStringValue; 
     } 
      
     public int toInt() 
     { 
       return this.mValue; 
     } 
   } 
 } 
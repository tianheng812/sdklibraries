package org.linphone.core;

public abstract interface LinphoneEvent
{
  public abstract void acceptSubscription();

  public abstract void addCustomHeader(String paramString1, String paramString2);

  public abstract void denySubscription(Reason paramReason);

  public abstract LinphoneCore getCore();

  public abstract String getCustomHeader(String paramString);

  public abstract ErrorInfo getErrorInfo();

  public abstract String getEventName();

  public abstract Reason getReason();

  public abstract SubscriptionDir getSubscriptionDir();

  public abstract SubscriptionState getSubscriptionState();

  public abstract Object getUserContext();

  public abstract void notify(LinphoneContent paramLinphoneContent);

  public abstract void sendPublish(LinphoneContent paramLinphoneContent);

  public abstract void sendSubscribe(LinphoneContent paramLinphoneContent);

  public abstract void setUserContext(Object paramObject);

  public abstract void terminate();

  public abstract void updatePublish(LinphoneContent paramLinphoneContent);

  public abstract void updateSubscribe(LinphoneContent paramLinphoneContent);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneEvent
 * JD-Core Version:    0.6.2
 */
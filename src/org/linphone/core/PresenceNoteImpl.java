package org.linphone.core;

public class PresenceNoteImpl
  implements PresenceNote
{
  private long mNativePtr;

  protected PresenceNoteImpl(long paramLong)
  {
    this.mNativePtr = paramLong;
  }

  protected PresenceNoteImpl(String paramString1, String paramString2)
  {
    this.mNativePtr = newPresenceNoteImpl(paramString1, paramString2);
  }

  private native String getContent(long paramLong);

  private native String getLang(long paramLong);

  private native long newPresenceNoteImpl(String paramString1, String paramString2);

  private native int setContent(long paramLong, String paramString);

  private native int setLang(long paramLong, String paramString);

  private native void unref(long paramLong);

  protected void finalize()
  {
    unref(this.mNativePtr);
  }

  public String getContent()
  {
    return getContent(this.mNativePtr);
  }

  public String getLang()
  {
    return getLang(this.mNativePtr);
  }

  public long getNativePtr()
  {
    return this.mNativePtr;
  }

  public int setContent(String paramString)
  {
    return setContent(this.mNativePtr, paramString);
  }

  public int setLang(String paramString)
  {
    return setLang(this.mNativePtr, paramString);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PresenceNoteImpl
 * JD-Core Version:    0.6.2
 */
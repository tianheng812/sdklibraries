package org.linphone.core;

public abstract interface PresenceModel
{
  public abstract int addActivity(PresenceActivity paramPresenceActivity);

  public abstract int addNote(String paramString1, String paramString2);

  public abstract int addPerson(PresencePerson paramPresencePerson);

  public abstract int addService(PresenceService paramPresenceService);

  public abstract int clearActivities();

  public abstract int clearNotes();

  public abstract int clearPersons();

  public abstract int clearServices();

  public abstract PresenceActivity getActivity();

  public abstract PresenceBasicStatus getBasicStatus();

  public abstract String getContact();

  public abstract long getNbActivities();

  public abstract long getNbPersons();

  public abstract long getNbServices();

  public abstract PresenceNote getNote(String paramString);

  public abstract PresenceActivity getNthActivity(long paramLong);

  public abstract PresencePerson getNthPerson(long paramLong);

  public abstract PresenceService getNthService(long paramLong);

  public abstract long getTimestamp();

  public abstract int setActivity(PresenceActivityType paramPresenceActivityType, String paramString);

  public abstract int setBasicStatus(PresenceBasicStatus paramPresenceBasicStatus);

  public abstract void setContact(String paramString);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PresenceModel
 * JD-Core Version:    0.6.2
 */
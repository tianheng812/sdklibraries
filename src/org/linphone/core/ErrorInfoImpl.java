package org.linphone.core;

public class ErrorInfoImpl implements ErrorInfo {
	private int mCode;
	private String mDetails;
	private String mPhrase;
	private Reason mReason;

	public ErrorInfoImpl(long paramLong) {
		this.mReason = Reason.fromInt(getReason(paramLong));
		this.mCode = getProtocolCode(paramLong);
		this.mPhrase = getPhrase(paramLong);
		this.mDetails = getDetails(paramLong);
	}

	private native String getDetails(long paramLong);

	private native String getPhrase(long paramLong);

	private native int getProtocolCode(long paramLong);

	private native int getReason(long paramLong);

	public String getDetails() {
		return this.mDetails;
	}

	public String getPhrase() {
		return this.mPhrase;
	}

	public int getProtocolCode() {
		return this.mCode;
	}

	public Reason getReason() {
		return this.mReason;
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.ErrorInfoImpl JD-Core Version: 0.6.2
 */
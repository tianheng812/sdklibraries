package org.linphone.core;

public abstract interface LinphoneCallParams
{
  public abstract void addCustomHeader(String paramString1, String paramString2);

  public abstract void enableLowBandwidth(boolean paramBoolean);

  public abstract String getCustomHeader(String paramString);

  public abstract LinphoneCore.MediaEncryption getMediaEncryption();

  public abstract int getPrivacy();

  public abstract VideoSize getReceivedVideoSize();

  public abstract VideoSize getSentVideoSize();

  public abstract String getSessionName();

  public abstract PayloadType getUsedAudioCodec();

  public abstract PayloadType getUsedVideoCodec();

  public abstract boolean getVideoEnabled();

  public abstract boolean isLowBandwidthEnabled();

  public abstract void setAudioBandwidth(int paramInt);

  public abstract void setMediaEnctyption(LinphoneCore.MediaEncryption paramMediaEncryption);

  public abstract void setPrivacy(int paramInt);

  public abstract void setRecordFile(String paramString);

  public abstract void setSessionName(String paramString);

  public abstract void setVideoEnabled(boolean paramBoolean);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneCallParams
 * JD-Core Version:    0.6.2
 */
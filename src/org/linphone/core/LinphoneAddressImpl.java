package org.linphone.core;

public class LinphoneAddressImpl implements LinphoneAddress {
	protected final long nativePtr;

	private LinphoneAddressImpl(long paramLong) {
		this(paramLong, WrapMode.FromConst);
	}

	protected LinphoneAddressImpl(long aNativePtr, WrapMode mode) {
		switch (mode) {
		case FromNew:
			nativePtr = aNativePtr;
			break;
		case FromConst:
			nativePtr = clone(aNativePtr);
			break;
		case FromExisting:
			nativePtr = ref(aNativePtr);
			break;
		default:
			nativePtr = 0;
		}
	}

	protected LinphoneAddressImpl(String identity) throws LinphoneCoreException {
		this.nativePtr = newLinphoneAddressImpl(identity, null);
		if (this.nativePtr == 0)
			throw new LinphoneCoreException(
					"Cannot create LinphoneAdress from [" + identity + "]");
	}

	protected LinphoneAddressImpl(String paramString1, String paramString2,
			String paramString3) {
		this.nativePtr = newLinphoneAddressImpl(null, paramString3);
		setUserName(paramString1);
		setDomain(paramString2);
	}

	private native long clone(long paramLong);

	private native String getDisplayName(long paramLong);

	private native String getDomain(long paramLong);

	private native int getTransport(long paramLong);

	private native String getUserName(long paramLong);

	private native long newLinphoneAddressImpl(String paramString1,
			String paramString2);

	private native long ref(long paramLong);

	private native void setDisplayName(long paramLong, String paramString);

	private native void setDomain(long paramLong, String paramString);

	private native void setTransport(long paramLong, int paramInt);

	private native void setUserName(long paramLong, String paramString);

	private native String toString(long paramLong);

	private native String toUri(long paramLong);

	private native void unref(long paramLong);

	public String asString() {
		return toString();
	}

	public String asStringUriOnly() {
		return toUri(this.nativePtr);
	}

	public void clean() {
		throw new RuntimeException("Not implemented");
	}

	protected void finalize() throws Throwable {
		if (this.nativePtr != 0L)
			unref(this.nativePtr);
	}

	public String getDisplayName() {
		return getDisplayName(this.nativePtr);
	}

	public String getDomain() {
		return getDomain(this.nativePtr);
	}

	public String getPort() {
		return String.valueOf(getPortInt());
	}

	public int getPortInt() {
		return getPortInt();
	}

	public LinphoneAddress.TransportType getTransport() {
		return LinphoneAddress.TransportType
				.fromInt(getTransport(this.nativePtr));
	}

	public String getUserName() {
		return getUserName(this.nativePtr);
	}

	public void setDisplayName(String paramString) {
		setDisplayName(this.nativePtr, paramString);
	}

	public void setDomain(String paramString) {
		setDomain(this.nativePtr, paramString);
	}

	public void setPort(String paramString) {
		throw new RuntimeException("Not implemented");
	}

	public void setPortInt(int paramInt) {
		throw new RuntimeException("Not implemented");
	}

	public void setTransport(LinphoneAddress.TransportType paramTransportType) {
		setTransport(this.nativePtr, paramTransportType.toInt());
	}

	public void setUserName(String paramString) {
		setUserName(this.nativePtr, paramString);
	}

	public String toString() {
		return toString(this.nativePtr);
	}

	public String toUri() {
		return toUri(this.nativePtr);
	}

	public static enum WrapMode {
		FromNew, FromConst, FromExisting
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneAddressImpl JD-Core Version: 0.6.2
 */
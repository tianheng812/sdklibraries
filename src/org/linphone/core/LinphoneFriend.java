package org.linphone.core;

import java.util.Vector;

public abstract interface LinphoneFriend {
	public abstract void setAddress(LinphoneAddress paramLinphoneAddress);

	public abstract LinphoneAddress getAddress();

	public abstract void setIncSubscribePolicy(
			SubscribePolicy paramSubscribePolicy);

	public abstract SubscribePolicy getIncSubscribePolicy();

	public abstract void enableSubscribes(boolean paramBoolean);

	public abstract boolean isSubscribesEnabled();

	public abstract OnlineStatus getStatus();

	public abstract PresenceModel getPresenceModel();

	public abstract void edit();

	public abstract void done();

	public abstract String toString();

	public abstract long getNativePtr();

	public static class SubscribePolicy {
		private static Vector<SubscribePolicy> values = new Vector();
		protected final int mValue;
		private final String mStringValue;
		public static final SubscribePolicy SPWait = new SubscribePolicy(0,
				"SPWait");
		public static final SubscribePolicy SPDeny = new SubscribePolicy(1,
				"SPDeny");
		public static final SubscribePolicy SPAccept = new SubscribePolicy(2,
				"SPAccept");

		private SubscribePolicy(int value, String stringValue) {
			this.mValue = value;
			values.addElement(this);
			this.mStringValue = stringValue;
		}

		public static SubscribePolicy fromInt(int value) {
			for (int i = 0; i < values.size(); i++) {
				SubscribePolicy policy = (SubscribePolicy) values.elementAt(i);
				if (policy.mValue == value) {
					return policy;
				}
			}
			throw new RuntimeException("Policy not found [" + value + "]");
		}

		public String toString() {
			return this.mStringValue;
		}
	}
}

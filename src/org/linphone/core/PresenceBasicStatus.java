package org.linphone.core;

public enum PresenceBasicStatus {
	Open(0), Closed(1), Invalid(2);
	protected final int mValue;

	private PresenceBasicStatus(int paramInt) {
		this.mValue = paramInt;
	}

	protected static PresenceBasicStatus fromInt(int paramInt) {
		switch (paramInt) {
		default:
			return Invalid;
		case 0:
			return Open;
		case 1:
			return Closed;
		}
	}

	public int toInt() {
		return this.mValue;
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.PresenceBasicStatus JD-Core Version: 0.6.2
 */
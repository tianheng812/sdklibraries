package org.linphone.core;

import org.linphone.LinphoneManager.EcCalibrationListener;
import org.linphone.mediastream.video.AndroidVideoWindowImpl;

import java.util.Vector;

public abstract interface LinphoneCore {
	public abstract void setContext(Object paramObject);

	public abstract void clearProxyConfigs();

	public abstract void addProxyConfig(
			LinphoneProxyConfig paramLinphoneProxyConfig)
			throws LinphoneCoreException;

	public abstract void removeProxyConfig(
			LinphoneProxyConfig paramLinphoneProxyConfig);

	public abstract void setDefaultProxyConfig(
			LinphoneProxyConfig paramLinphoneProxyConfig);

	public abstract LinphoneProxyConfig getDefaultProxyConfig();

	public abstract LinphoneAuthInfo[] getAuthInfosList();

	public abstract LinphoneAuthInfo findAuthInfo(String username, String realm, String domain);

	public abstract void removeAuthInfo(LinphoneAuthInfo paramLinphoneAuthInfo);

	public abstract void clearAuthInfos();

	public abstract void addAuthInfo(LinphoneAuthInfo paramLinphoneAuthInfo);

	public abstract LinphoneAddress interpretUrl(String paramString)
			throws LinphoneCoreException;

	public abstract LinphoneCall invite(String paramString)
			throws LinphoneCoreException;

	public abstract LinphoneCall invite(LinphoneAddress paramLinphoneAddress)
			throws LinphoneCoreException;

	public abstract void terminateCall(LinphoneCall paramLinphoneCall);

	public abstract void declineCall(LinphoneCall paramLinphoneCall,
			Reason paramReason);

	public abstract LinphoneCall getCurrentCall();

	public abstract LinphoneAddress getRemoteAddress();

	public abstract boolean isIncall();

	public abstract boolean isInComingInvitePending();

	public abstract void iterate();

	public abstract void acceptCall(LinphoneCall paramLinphoneCall)
			throws LinphoneCoreException;

	public abstract void acceptCallWithParams(LinphoneCall paramLinphoneCall,
			LinphoneCallParams paramLinphoneCallParams)
			throws LinphoneCoreException;

	public abstract void acceptCallUpdate(LinphoneCall paramLinphoneCall,
			LinphoneCallParams paramLinphoneCallParams)
			throws LinphoneCoreException;

	public abstract void deferCallUpdate(LinphoneCall paramLinphoneCall)
			throws LinphoneCoreException;

	public abstract LinphoneCallLog[] getCallLogs();

	public abstract void setNetworkReachable(boolean paramBoolean);

	public abstract boolean isNetworkReachable();

	public abstract void destroy();

	public abstract void setPlaybackGain(float paramFloat);

	public abstract float getPlaybackGain();

	public abstract void setPlayLevel(int paramInt);

	public abstract int getPlayLevel();

	public abstract void muteMic(boolean paramBoolean);

	public abstract boolean isMicMuted();

	public abstract void sendDtmf(char paramChar);

	public abstract void playDtmf(char paramChar, int paramInt);

	public abstract void stopDtmf();

	public abstract void clearCallLogs();

	public abstract PayloadType findPayloadType(String paramString,
			int paramInt1, int paramInt2);

	public abstract PayloadType findPayloadType(String paramString, int paramInt);

	public abstract PayloadType findPayloadType(String paramString);

	public abstract void enablePayloadType(PayloadType paramPayloadType,
			boolean paramBoolean) throws LinphoneCoreException;

	public abstract boolean isPayloadTypeEnabled(PayloadType paramPayloadType);

	public abstract boolean payloadTypeIsVbr(PayloadType paramPayloadType);

	public abstract void setPayloadTypeBitrate(PayloadType paramPayloadType,
			int paramInt);

	public abstract int getPayloadTypeBitrate(PayloadType paramPayloadType);

	public abstract void setPayloadTypeNumber(PayloadType paramPayloadType,
			int paramInt);

	public abstract int getPayloadTypeNumber(PayloadType paramPayloadType);

	public abstract void enableAdaptiveRateControl(boolean paramBoolean);

	public abstract boolean isAdaptiveRateControlEnabled();

	public abstract void setAdaptiveRateAlgorithm(
			AdaptiveRateAlgorithm paramAdaptiveRateAlgorithm);

	public abstract AdaptiveRateAlgorithm getAdaptiveRateAlgorithm();

	public abstract void enableEchoCancellation(boolean paramBoolean);

	public abstract boolean isEchoCancellationEnabled();

	public abstract boolean isEchoLimiterEnabled();

	public abstract void setSignalingTransportPorts(Transports paramTransports);

	public abstract Transports getSignalingTransportPorts();

	public abstract void setSipDscp(int paramInt);

	public abstract int getSipDscp();

	public abstract void enableSpeaker(boolean paramBoolean);

	public abstract boolean isSpeakerEnabled();

	public abstract void addFriend(LinphoneFriend paramLinphoneFriend)
			throws LinphoneCoreException;

	public abstract LinphoneFriend[] getFriendList();

	public abstract void setPresenceInfo(int paramInt, String paramString,
			OnlineStatus paramOnlineStatus);

	public abstract OnlineStatus getPresenceInfo();

	public abstract void setPresenceModel(PresenceModel paramPresenceModel);

	public abstract PresenceModel getPresenceModel();

	public abstract LinphoneChatRoom getOrCreateChatRoom(String paramString);

	public abstract void setVideoWindow(Object paramObject);

	public abstract void setPreviewWindow(Object paramObject);

	public abstract void setDeviceRotation(int paramInt);

	public abstract void setVideoDevice(int paramInt);

	public abstract int getVideoDevice();

	public abstract boolean isVideoSupported();

	public abstract void enableVideo(boolean paramBoolean1,
			boolean paramBoolean2);

	public abstract boolean isVideoEnabled();

	public abstract void setStunServer(String paramString);

	public abstract String getStunServer();

	public abstract void setFirewallPolicy(FirewallPolicy paramFirewallPolicy);

	public abstract FirewallPolicy getFirewallPolicy();

	public abstract LinphoneCall inviteAddressWithParams(
			LinphoneAddress paramLinphoneAddress,
			LinphoneCallParams paramLinphoneCallParams)
			throws LinphoneCoreException;

	public abstract int updateCall(LinphoneCall paramLinphoneCall,
			LinphoneCallParams paramLinphoneCallParams);

	public abstract LinphoneCallParams createDefaultCallParameters();

	public abstract void setRing(String paramString);

	public abstract String getRing();

	public abstract void setRootCA(String paramString);

	public abstract void setRingback(String paramString);

	public abstract void setUploadBandwidth(int paramInt);

	public abstract void setDownloadBandwidth(int paramInt);

	public abstract void setDownloadPtime(int paramInt);

	public abstract void setUploadPtime(int paramInt);

	public abstract void setPreferredVideoSize(VideoSize paramVideoSize);

	public abstract void setPreferredVideoSizeByName(String paramString);

	public abstract VideoSize getPreferredVideoSize();

	public abstract void setPreferredFramerate(float paramFloat);

	public abstract float getPreferredFramerate();

	public abstract PayloadType[] getAudioCodecs();

	public abstract void setAudioCodecs(PayloadType[] paramArrayOfPayloadType);

	public abstract PayloadType[] getVideoCodecs();

	public abstract void setVideoCodecs(PayloadType[] paramArrayOfPayloadType);

	public abstract void enableKeepAlive(boolean paramBoolean);

	public abstract boolean isKeepAliveEnabled();

	public abstract void startEchoCalibration(
			EcCalibrationListener paramLinphoneCoreListener)
			throws LinphoneCoreException;

	public abstract boolean needsEchoCalibration();

	public abstract boolean needsEchoCanceler();

	public abstract void enableIpv6(boolean paramBoolean);

	public abstract boolean isIpv6Enabled();

	public abstract void adjustSoftwareVolume(int paramInt);

	public abstract boolean pauseCall(LinphoneCall paramLinphoneCall);

	public abstract boolean resumeCall(LinphoneCall paramLinphoneCall);

	public abstract boolean pauseAllCalls();

	public abstract void setZrtpSecretsCache(String paramString);

	public abstract void enableEchoLimiter(boolean paramBoolean);

	public abstract boolean isInConference();

	public abstract boolean enterConference();

	public abstract void leaveConference();

	public abstract void addToConference(LinphoneCall paramLinphoneCall);

	public abstract void removeFromConference(LinphoneCall paramLinphoneCall);

	public abstract void addAllToConference();

	public abstract void terminateConference();

	public abstract int getConferenceSize();

	public abstract void startConferenceRecording(String paramString);

	public abstract void stopConferenceRecording();

	public abstract void terminateAllCalls();

	public abstract LinphoneCall[] getCalls();

	public abstract int getCallsNb();

	public abstract void transferCall(LinphoneCall paramLinphoneCall,
			String paramString);

	public abstract void transferCallToAnother(LinphoneCall paramLinphoneCall1,
			LinphoneCall paramLinphoneCall2);

	public abstract LinphoneCall startReferedCall(
			LinphoneCall paramLinphoneCall,
			LinphoneCallParams paramLinphoneCallParams);

	public abstract LinphoneCall findCallFromUri(String paramString);

	public abstract int getMaxCalls();

	public abstract void setMaxCalls(int paramInt);

	public abstract boolean isMyself(String url);

	public abstract boolean soundResourcesLocked();

	public abstract boolean mediaEncryptionSupported(
			MediaEncryption paramMediaEncryption);

	public abstract void setMediaEncryption(MediaEncryption paramMediaEncryption);

	public abstract MediaEncryption getMediaEncryption();

	public abstract void setMediaEncryptionMandatory(boolean paramBoolean);

	public abstract boolean isMediaEncryptionMandatory();

	public abstract void setPlayFile(String paramString);

	public abstract void tunnelEnable(boolean paramBoolean);

	public abstract void tunnelSetMode(TunnelMode paramTunnelMode);

	public abstract TunnelMode tunnelGetMode();

	public abstract void tunnelEnableSip(boolean paramBoolean);

	public abstract boolean tunnelSipEnabled();

	public abstract void tunnelAutoDetect();

	public abstract void tunnelCleanServers();

	public abstract void tunnelSetHttpProxy(String paramString1, int paramInt,
			String paramString2, String paramString3);

	public abstract void tunnelAddServerAndMirror(String paramString,
			int paramInt1, int paramInt2, int paramInt3);

	public abstract void tunnelAddServer(TunnelConfig paramTunnelConfig);

	public abstract TunnelConfig[] tunnelGetServers();

	public abstract boolean isTunnelAvailable();

	public abstract LinphoneProxyConfig[] getProxyConfigList();

	public abstract void setVideoPolicy(boolean paramBoolean1,
			boolean paramBoolean2);

	public abstract boolean getVideoAutoInitiatePolicy();

	public abstract boolean getVideoAutoAcceptPolicy();

	public abstract void setStaticPicture(String paramString);

	public abstract void setUserAgent(String paramString1, String paramString2);

	public abstract void setCpuCount(int paramInt);

	public abstract void removeCallLog(LinphoneCallLog paramLinphoneCallLog);

	public abstract int getMissedCallsCount();

	public abstract void resetMissedCallsCount();

	public abstract void refreshRegisters();

	public abstract String getVersion();

	public abstract void removeFriend(LinphoneFriend paramLinphoneFriend);

	public abstract LinphoneFriend findFriendByAddress(String paramString);

	public abstract void setAudioPort(int paramInt);

	public abstract void setAudioPortRange(int paramInt1, int paramInt2);

	public abstract void setAudioDscp(int paramInt);

	public abstract int getAudioDscp();

	public abstract void setVideoPort(int paramInt);

	public abstract void setVideoPortRange(int paramInt1, int paramInt2);

	public abstract void setVideoDscp(int paramInt);

	public abstract int getVideoDscp();

	public abstract void setIncomingTimeout(int paramInt);

	public abstract void setInCallTimeout(int paramInt);

	public abstract void setMicrophoneGain(float paramFloat);

	public abstract void setPrimaryContact(String paramString);

	public abstract String getPrimaryContact();

	public abstract void setPrimaryContact(String paramString1,
			String paramString2);

	public abstract String getPrimaryContactUsername();

	public abstract String getPrimaryContactDisplayName();

	public abstract void setUseSipInfoForDtmfs(boolean paramBoolean);

	public abstract boolean getUseSipInfoForDtmfs();

	public abstract void setUseRfc2833ForDtmfs(boolean paramBoolean);

	public abstract boolean getUseRfc2833ForDtmfs();

	public abstract LpConfig getConfig();

	public abstract boolean upnpAvailable();

	public abstract UpnpState getUpnpState();

	public abstract String getUpnpExternalIpaddress();

	public abstract LinphoneInfoMessage createInfoMessage();

	public abstract LinphoneEvent subscribe(
			LinphoneAddress paramLinphoneAddress, String paramString,
			int paramInt, LinphoneContent paramLinphoneContent);

	public abstract LinphoneEvent createSubscribe(
			LinphoneAddress paramLinphoneAddress, String paramString,
			int paramInt);

	public abstract LinphoneEvent createPublish(
			LinphoneAddress paramLinphoneAddress, String paramString,
			int paramInt);

	public abstract LinphoneEvent publish(LinphoneAddress paramLinphoneAddress,
			String paramString, int paramInt,
			LinphoneContent paramLinphoneContent);

	public abstract void setChatDatabasePath(String paramString);

	public abstract LinphoneChatRoom[] getChatRooms();

	public abstract String[] getSupportedVideoSizes();

	public abstract int migrateToMultiTransport();

	public abstract boolean acceptEarlyMedia(LinphoneCall paramLinphoneCall);

	public abstract boolean acceptEarlyMediaWithParams(
			LinphoneCall paramLinphoneCall,
			LinphoneCallParams paramLinphoneCallParams);

	public abstract LinphoneProxyConfig createProxyConfig();

	public abstract LinphoneProxyConfig createProxyConfig(String paramString1,
			String paramString2, String paramString3, boolean paramBoolean)
			throws LinphoneCoreException;

	public abstract void setCallErrorTone(Reason paramReason, String paramString);

	public abstract void setTone(ToneID paramToneID, String paramString);

	public abstract void setMtu(int paramInt);

	public abstract int getMtu();

	public abstract void enableSdp200Ack(boolean paramBoolean);

	public abstract boolean isSdp200AckEnabled();

	public abstract void disableChat(Reason paramReason);

	public abstract void enableChat();

	public abstract boolean chatEnabled();

	public abstract void stopRinging();

	public abstract void setAudioJittcomp(int paramInt);

	public abstract void setVideoJittcomp(int paramInt);

	public abstract void setFileTransferServer(String paramString);

	public abstract String getFileTransferServer();

	public abstract LinphonePlayer createLocalPlayer(
			AndroidVideoWindowImpl paramAndroidVideoWindowImpl);

	public abstract void addListener(
			LinphoneCoreListener paramLinphoneCoreListener);

	public abstract void removeListener(
			LinphoneCoreListener paramLinphoneCoreListener);

	public abstract void setRemoteRingbackTone(String paramString);

	public abstract String getRemoteRingbackTone();

	public abstract void uploadLogCollection();

	public abstract void resetLogCollection();

	public abstract void setAudioMulticastAddr(String paramString)
			throws LinphoneCoreException;

	public abstract void setVideoMulticastAddr(String paramString)
			throws LinphoneCoreException;

	public abstract String getAudioMulticastAddr();

	public abstract String getVideoMulticastAddr();

	public abstract void setAudioMulticastTtl(int paramInt)
			throws LinphoneCoreException;

	public abstract void setVideoMulticastTtl(int paramInt)
			throws LinphoneCoreException;

	public abstract int getAudioMulticastTtl();

	public abstract int getVideoMulticastTtl();

	public abstract void enableAudioMulticast(boolean paramBoolean);

	public abstract boolean audioMulticastEnabled();

	public abstract void enableVideoMulticast(boolean paramBoolean);

	public abstract boolean videoMulticastEnabled();

	public abstract void enableDnsSrv(boolean paramBoolean);

	public abstract boolean dnsSrvEnabled();

	public static class GlobalState {
		private static Vector<GlobalState> values = new Vector();
		public static GlobalState GlobalOff = new GlobalState(0, "GlobalOff");
		public static GlobalState GlobalStartup = new GlobalState(1,
				"GlobalStartup");
		public static GlobalState GlobalOn = new GlobalState(2, "GlobalOn");
		public static GlobalState GlobalShutdown = new GlobalState(3,
				"GlobalShutdown");
		public static GlobalState GlobalConfiguring = new GlobalState(4,
				"GlobalConfiguring");
		private final int mValue;
		private final String mStringValue;

		private GlobalState(int value, String stringValue) {
			this.mValue = value;
			values.addElement(this);
			this.mStringValue = stringValue;
		}

		public static GlobalState fromInt(int value) {
			for (int i = 0; i < values.size(); i++) {
				GlobalState state = (GlobalState) values.elementAt(i);
				if (state.mValue == value) {
					return state;
				}
			}
			throw new RuntimeException("state not found [" + value + "]");
		}

		public String toString() {
			return this.mStringValue;
		}
	}

	public static class RemoteProvisioningState {
		private static Vector<RemoteProvisioningState> values = new Vector();
		public static RemoteProvisioningState ConfiguringSuccessful = new RemoteProvisioningState(
				0, "ConfiguringSuccessful");
		public static RemoteProvisioningState ConfiguringFailed = new RemoteProvisioningState(
				1, "ConfiguringFailed");
		public static RemoteProvisioningState ConfiguringSkipped = new RemoteProvisioningState(
				2, "ConfiguringSkipped");
		private final int mValue;
		private final String mStringValue;

		private RemoteProvisioningState(int value, String stringValue) {
			this.mValue = value;
			values.addElement(this);
			this.mStringValue = stringValue;
		}

		public static RemoteProvisioningState fromInt(int value) {
			for (int i = 0; i < values.size(); i++) {
				RemoteProvisioningState state = (RemoteProvisioningState) values
						.elementAt(i);
				if (state.mValue == value) {
					return state;
				}
			}
			throw new RuntimeException("state not found [" + value + "]");
		}

		public String toString() {
			return this.mStringValue;
		}
	}

	public static class RegistrationState {
		private static Vector<RegistrationState> values = new Vector();
		public static RegistrationState RegistrationNone = new RegistrationState(
				0, "RegistrationNone");
		public static RegistrationState RegistrationProgress = new RegistrationState(
				1, "RegistrationProgress");
		public static RegistrationState RegistrationOk = new RegistrationState(
				2, "RegistrationOk");
		public static RegistrationState RegistrationCleared = new RegistrationState(
				3, "RegistrationCleared");
		public static RegistrationState RegistrationFailed = new RegistrationState(
				4, "RegistrationFailed");
		private final int mValue;
		private final String mStringValue;

		private RegistrationState(int value, String stringValue) {
			this.mValue = value;
			values.addElement(this);
			this.mStringValue = stringValue;
		}

		public static RegistrationState fromInt(int value) {
			for (int i = 0; i < values.size(); i++) {
				RegistrationState state = (RegistrationState) values
						.elementAt(i);
				if (state.mValue == value) {
					return state;
				}
			}
			throw new RuntimeException("state not found [" + value + "]");
		}

		public String toString() {
			return this.mStringValue;
		}
	}

	public static class FirewallPolicy {
		private static Vector<FirewallPolicy> values = new Vector();
		public static FirewallPolicy NoFirewall = new FirewallPolicy(0,
				"NoFirewall");
		public static FirewallPolicy UseNatAddress = new FirewallPolicy(1,
				"UseNatAddress");
		public static FirewallPolicy UseStun = new FirewallPolicy(2, "UseStun");
		public static FirewallPolicy UseIce = new FirewallPolicy(3, "UseIce");
		public static FirewallPolicy UseUpnp = new FirewallPolicy(4, "UseUpnp");
		private final int mValue;
		private final String mStringValue;

		private FirewallPolicy(int value, String stringValue) {
			this.mValue = value;
			values.addElement(this);
			this.mStringValue = stringValue;
		}

		public static FirewallPolicy fromInt(int value) {
			for (int i = 0; i < values.size(); i++) {
				FirewallPolicy state = (FirewallPolicy) values.elementAt(i);
				if (state.mValue == value) {
					return state;
				}
			}
			throw new RuntimeException("state not found [" + value + "]");
		}

		public String toString() {
			return this.mStringValue;
		}

		public int value() {
			return this.mValue;
		}
	}

	public static class Transports {
		public int udp;
		public int tcp;
		public int tls;

		public Transports() {
		}

		public Transports(Transports t) {
			this.udp = t.udp;
			this.tcp = t.tcp;
			this.tls = t.tls;
		}

		public String toString() {
			return "udp[" + this.udp + "] tcp[" + this.tcp + "] tls["
					+ this.tls + "]";
		}
	}

	public static final class MediaEncryption {
		private static Vector<MediaEncryption> values = new Vector();
		public static final MediaEncryption None = new MediaEncryption(0,
				"None");
		public static final MediaEncryption SRTP = new MediaEncryption(1,
				"SRTP");
		public static final MediaEncryption ZRTP = new MediaEncryption(2,
				"ZRTP");
		public static final MediaEncryption DTLS = new MediaEncryption(3,
				"DTLS");
		protected final int mValue;
		private final String mStringValue;

		private MediaEncryption(int value, String stringValue) {
			this.mValue = value;
			values.addElement(this);
			this.mStringValue = stringValue;
		}

		public static MediaEncryption fromInt(int value) {
			for (int i = 0; i < values.size(); i++) {
				MediaEncryption menc = (MediaEncryption) values.elementAt(i);
				if (menc.mValue == value) {
					return menc;
				}
			}
			throw new RuntimeException("MediaEncryption not found [" + value
					+ "]");
		}

		public String toString() {
			return this.mStringValue;
		}
	}

	public static final class AdaptiveRateAlgorithm {
		private static Vector<AdaptiveRateAlgorithm> values = new Vector();
		public static final AdaptiveRateAlgorithm Simple = new AdaptiveRateAlgorithm(
				0, "Simple");
		public static final AdaptiveRateAlgorithm Stateful = new AdaptiveRateAlgorithm(
				1, "Stateful");
		protected final int mValue;
		private final String mStringValue;

		private AdaptiveRateAlgorithm(int value, String stringValue) {
			this.mValue = value;
			values.addElement(this);
			this.mStringValue = stringValue;
		}

		public static AdaptiveRateAlgorithm fromString(String value) {
			for (int i = 0; i < values.size(); i++) {
				AdaptiveRateAlgorithm alg = (AdaptiveRateAlgorithm) values
						.elementAt(i);
				if (alg.mStringValue.equalsIgnoreCase(value)) {
					return alg;
				}
			}
			throw new RuntimeException("AdaptiveRateAlgorithm not found ["
					+ value + "]");
		}

		public String toString() {
			return this.mStringValue;
		}
	}

	public static class EcCalibratorStatus {
		private static Vector<EcCalibratorStatus> values = new Vector();
		public static final int IN_PROGRESS_STATUS = 0;
		public static final int DONE_STATUS = 1;
		public static final int FAILED_STATUS = 2;
		public static final int DONE_NO_ECHO_STATUS = 3;
		public static EcCalibratorStatus InProgress = new EcCalibratorStatus(0,
				"InProgress");
		public static EcCalibratorStatus Done = new EcCalibratorStatus(1,
				"Done");
		public static EcCalibratorStatus Failed = new EcCalibratorStatus(2,
				"Failed");
		public static EcCalibratorStatus DoneNoEcho = new EcCalibratorStatus(3,
				"DoneNoEcho");
		private final int mValue;
		private final String mStringValue;

		private EcCalibratorStatus(int value, String stringValue) {
			this.mValue = value;
			values.addElement(this);
			this.mStringValue = stringValue;
		}

		public static EcCalibratorStatus fromInt(int value) {
			for (int i = 0; i < values.size(); i++) {
				EcCalibratorStatus status = (EcCalibratorStatus) values
						.elementAt(i);
				if (status.mValue == value) {
					return status;
				}
			}
			throw new RuntimeException("status not found [" + value + "]");
		}

		public String toString() {
			return this.mStringValue;
		}

		public int value() {
			return this.mValue;
		}
	}

	public static class UpnpState {
		private static Vector<UpnpState> values = new Vector();
		public static UpnpState Idle = new UpnpState(0, "Idle");
		public static UpnpState Pending = new UpnpState(1, "Pending");
		public static UpnpState Adding = new UpnpState(2, "Adding");
		public static UpnpState Removing = new UpnpState(3, "Removing");
		public static UpnpState NotAvailable = new UpnpState(4, "Not available");
		public static UpnpState Ok = new UpnpState(5, "Ok");
		public static UpnpState Ko = new UpnpState(6, "Ko");
		public static UpnpState Blacklisted = new UpnpState(7, "Blacklisted");
		protected final int mValue;
		private final String mStringValue;

		private UpnpState(int value, String stringValue) {
			this.mValue = value;
			values.addElement(this);
			this.mStringValue = stringValue;
		}

		public static UpnpState fromInt(int value) {
			for (int i = 0; i < values.size(); i++) {
				UpnpState mstate = (UpnpState) values.elementAt(i);
				if (mstate.mValue == value) {
					return mstate;
				}
			}
			throw new RuntimeException("UpnpState not found [" + value + "]");
		}

		public String toString() {
			return this.mStringValue;
		}
	}

	public static class LogCollectionUploadState {
		private static Vector<LogCollectionUploadState> values = new Vector();
		public static LogCollectionUploadState LogCollectionUploadStateInProgress = new LogCollectionUploadState(
				0, "LinphoneCoreLogCollectionUploadStateInProgress");
		public static LogCollectionUploadState LogCollectionUploadStateDelivered = new LogCollectionUploadState(
				1, "LinphoneCoreLogCollectionUploadStateDelivered");
		public static LogCollectionUploadState LogCollectionUploadStateNotDelivered = new LogCollectionUploadState(
				2, "LinphoneCoreLogCollectionUploadStateNotDelivered");
		private final int mValue;
		private final String mStringValue;

		private LogCollectionUploadState(int value, String stringValue) {
			this.mValue = value;
			values.addElement(this);
			this.mStringValue = stringValue;
		}

		public static LogCollectionUploadState fromInt(int value) {
			for (int i = 0; i < values.size(); i++) {
				LogCollectionUploadState state = (LogCollectionUploadState) values
						.elementAt(i);
				if (state.mValue == value) {
					return state;
				}
			}
			throw new RuntimeException("state not found [" + value + "]");
		}

		public String toString() {
			return this.mStringValue;
		}
	}

	public static enum TunnelMode {
		disable(0), enable(1), auto(2);

		private final int value;

		private TunnelMode(int value) {
			this.value = value;
		}

		public static int enumToInt(TunnelMode enum_mode) {
			return enum_mode.value;
		}

		public static TunnelMode intToEnum(int value) {
			switch (value) {
			case 0:
				return disable;
			case 1:
				return enable;
			case 2:
				return auto;
			}
			return disable;
		}
	}
	public abstract void setListener(LinphoneCoreListener listener);
}

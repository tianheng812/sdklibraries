package org.linphone.core;

public enum PublishState {
	None(0), Progress(1), Ok(2), Error(3), Expiring(4), Cleared(5);
	protected final int mValue;

	private PublishState(int paramInt) {
		this.mValue = paramInt;
	}

	protected static PublishState fromInt(int paramInt)
			throws LinphoneCoreException {
		switch (paramInt) {
		default:
			throw new LinphoneCoreException("Unhandled enum value " + paramInt
					+ " for PublishState");
		case 0:
			return None;
		case 1:
			return Progress;
		case 2:
			return Ok;
		case 3:
			return Error;
		case 4:
			return Expiring;
		case 5:
			return Cleared;
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.PublishState JD-Core Version: 0.6.2
 */
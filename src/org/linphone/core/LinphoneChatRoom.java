package org.linphone.core;

public abstract interface LinphoneChatRoom
{
  public abstract void cancelFileTransfer(LinphoneChatMessage paramLinphoneChatMessage);

  public abstract void compose();

  public abstract LinphoneChatMessage createFileTransferMessage(LinphoneContent paramLinphoneContent);

  public abstract LinphoneChatMessage createLinphoneChatMessage(String paramString);

  public abstract LinphoneChatMessage createLinphoneChatMessage(String paramString1, String paramString2, LinphoneChatMessage.State paramState, long paramLong, boolean paramBoolean1, boolean paramBoolean2);

  public abstract void deleteHistory();

  public abstract void deleteMessage(LinphoneChatMessage paramLinphoneChatMessage);

  public abstract void destroy();

  public abstract LinphoneCore getCore();

  public abstract LinphoneChatMessage[] getHistory();

  public abstract LinphoneChatMessage[] getHistory(int paramInt);

  public abstract LinphoneChatMessage[] getHistoryRange(int paramInt1, int paramInt2);

  public abstract int getHistorySize();

  public abstract LinphoneAddress getPeerAddress();

  public abstract int getUnreadMessagesCount();

  public abstract boolean isRemoteComposing();

  public abstract void markAsRead();

  public abstract void sendMessage(String paramString);
  
  public abstract void sendMessage(LinphoneChatMessage paramLinphoneChatMessage, LinphoneChatMessage.StateListener paramStateListener);

  public abstract void updateUrl(LinphoneChatMessage paramLinphoneChatMessage);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneChatRoom
 * JD-Core Version:    0.6.2
 */
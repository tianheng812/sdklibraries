package org.linphone.core;

public abstract interface PresenceNote
{
  public abstract String getContent();

  public abstract String getLang();

  public abstract long getNativePtr();

  public abstract int setContent(String paramString);

  public abstract int setLang(String paramString);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PresenceNote
 * JD-Core Version:    0.6.2
 */
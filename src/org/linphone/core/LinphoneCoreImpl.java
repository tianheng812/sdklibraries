package org.linphone.core;

import java.io.File;
import java.io.IOException;

import org.linphone.LinphoneManager.EcCalibrationListener;
import org.linphone.mediastream.Log;
import org.linphone.mediastream.Version;
import org.linphone.mediastream.video.AndroidVideoWindowImpl;
import org.linphone.mediastream.video.capture.hwconf.Hacks;

import android.content.Context;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.net.wifi.WifiManager.WifiLock;

public class LinphoneCoreImpl implements LinphoneCore {
	private final LinphoneCoreListener mListener;
	protected long nativePtr = 0L;
	private Context mContext = null;
	private AudioManager mAudioManager = null;
	private boolean mSpeakerEnabled = false;

	private native long newLinphoneCore(
			LinphoneCoreListener paramLinphoneCoreListener,
			String paramString1, String paramString2, Object paramObject);

	private native void iterate(long paramLong);

	private native LinphoneProxyConfig getDefaultProxyConfig(long paramLong);

	private native void setDefaultProxyConfig(long paramLong1, long paramLong2);

	private native int addProxyConfig(
			LinphoneProxyConfig paramLinphoneProxyConfig, long paramLong1,
			long paramLong2);

	private native void removeProxyConfig(long paramLong1, long paramLong2);

	private native void clearAuthInfos(long paramLong);

	private native void clearProxyConfigs(long paramLong);

	private native void addAuthInfo(long paramLong1, long paramLong2);

	private native void removeAuthInfo(long paramLong1, long paramLong2);

	private native Object invite(long paramLong, String paramString);

	private native void terminateCall(long paramLong1, long paramLong2);

	private native long getRemoteAddress(long paramLong);

	private native boolean isInCall(long paramLong);

	private native boolean isInComingInvitePending(long paramLong);

	private native void acceptCall(long paramLong1, long paramLong2);

	private native long getCallLog(long paramLong, int paramInt);

	private native int getNumberOfCallLogs(long paramLong);

	private native void delete(long paramLong);

	private native void setNetworkStateReachable(long paramLong,
			boolean paramBoolean);

	private native boolean isNetworkStateReachable(long paramLong);

	private native void setPlaybackGain(long paramLong, float paramFloat);

	private native float getPlaybackGain(long paramLong);

	private native void muteMic(long paramLong, boolean paramBoolean);

	private native long interpretUrl(long paramLong, String paramString);

	private native Object inviteAddress(long paramLong1, long paramLong2);

	private native Object inviteAddressWithParams(long paramLong1,
			long paramLong2, long paramLong3);

	private native void sendDtmf(long paramLong, char paramChar);

	private native void clearCallLogs(long paramLong);

	private native boolean isMicMuted(long paramLong);

	private native long findPayloadType(long paramLong, String paramString,
			int paramInt1, int paramInt2);

	private native int enablePayloadType(long paramLong1, long paramLong2,
			boolean paramBoolean);

	private native boolean isPayloadTypeEnabled(long paramLong1, long paramLong2);

	private native boolean payloadTypeIsVbr(long paramLong1, long paramLong2);

	private native void enableAdaptiveRateControl(long paramLong,
			boolean paramBoolean);

	private native boolean isAdaptiveRateControlEnabled(long paramLong);

	private native String getAdaptiveRateAlgorithm(long paramLong);

	private native void setAdaptiveRateAlgorithm(long paramLong,
			String paramString);

	private native void enableEchoCancellation(long paramLong,
			boolean paramBoolean);

	private native boolean isEchoCancellationEnabled(long paramLong);

	private native Object getCurrentCall(long paramLong);

	private native void playDtmf(long paramLong, char paramChar, int paramInt);

	private native void stopDtmf(long paramLong);

	private native void setVideoWindowId(long paramLong, Object paramObject);

	private native void setPreviewWindowId(long paramLong, Object paramObject);

	private native void setDeviceRotation(long paramLong, int paramInt);

	private native void addFriend(long paramLong1, long paramLong2);

	private native LinphoneFriend[] getFriendList(long paramLong);

	private native void setPresenceInfo(long paramLong, int paramInt1,
			String paramString, int paramInt2);

	private native int getPresenceInfo(long paramLong);

	private native void setPresenceModel(long paramLong1, long paramLong2);

	private native Object getPresenceModel(long paramLong);

	private native long getOrCreateChatRoom(long paramLong, String paramString);

	private native void enableVideo(long paramLong, boolean paramBoolean1,
			boolean paramBoolean2);

	private native boolean isVideoEnabled(long paramLong);

	private native boolean isVideoSupported(long paramLong);

	private native void setFirewallPolicy(long paramLong, int paramInt);

	private native int getFirewallPolicy(long paramLong);

	private native void setStunServer(long paramLong, String paramString);

	private native String getStunServer(long paramLong);

	private native long createDefaultCallParams(long paramLong);

	private native int updateCall(long paramLong1, long paramLong2,
			long paramLong3);

	private native void setUploadBandwidth(long paramLong, int paramInt);

	private native void setDownloadBandwidth(long paramLong, int paramInt);

	private native void setPreferredVideoSize(long paramLong, int paramInt1,
			int paramInt2);

	private native void setPreferredVideoSizeByName(long paramLong,
			String paramString);

	private native int[] getPreferredVideoSize(long paramLong);

	private native void setRing(long paramLong, String paramString);

	private native String getRing(long paramLong);

	private native void setRootCA(long paramLong, String paramString);

	private native void setRingback(long paramLong, String paramString);

	private native long[] listVideoPayloadTypes(long paramLong);

	private native void setVideoCodecs(long paramLong, long[] paramArrayOfLong);

	private native LinphoneProxyConfig[] getProxyConfigList(long paramLong);

	private native long[] getAuthInfosList(long paramLong);

	private native long findAuthInfos(long paramLong, String paramString1,
			String paramString2, String paramString3);

	private native long[] listAudioPayloadTypes(long paramLong);

	private native void setAudioCodecs(long paramLong, long[] paramArrayOfLong);

	private native void enableKeepAlive(long paramLong, boolean paramBoolean);

	private native boolean isKeepAliveEnabled(long paramLong);

	private native int startEchoCalibration(long paramLong, Object paramObject);

	private native int getSignalingTransportPort(long paramLong, int paramInt);

	private native void setSignalingTransportPorts(long paramLong,
			int paramInt1, int paramInt2, int paramInt3);

	private native void enableIpv6(long paramLong, boolean paramBoolean);

	private native boolean isIpv6Enabled(long paramLong);

	private native int pauseCall(long paramLong1, long paramLong2);

	private native int pauseAllCalls(long paramLong);

	private native int resumeCall(long paramLong1, long paramLong2);

	private native void setUploadPtime(long paramLong, int paramInt);

	private native void setDownloadPtime(long paramLong, int paramInt);

	private native void setZrtpSecretsCache(long paramLong, String paramString);

	private native void enableEchoLimiter(long paramLong, boolean paramBoolean);

	private native int setVideoDevice(long paramLong, int paramInt);

	private native int getVideoDevice(long paramLong);

	private native int getMediaEncryption(long paramLong);

	private native void setMediaEncryption(long paramLong, int paramInt);

	private native boolean isMediaEncryptionMandatory(long paramLong);

	private native void setMediaEncryptionMandatory(long paramLong,
			boolean paramBoolean);

	private native void removeCallLog(long paramLong1, long paramLong2);

	private native int getMissedCallsCount(long paramLong);

	private native void resetMissedCallsCount(long paramLong);

	private native String getVersion(long paramLong);

	private native void setAudioPort(long paramLong, int paramInt);

	private native void setVideoPort(long paramLong, int paramInt);

	private native void setAudioPortRange(long paramLong, int paramInt1,
			int paramInt2);

	private native void setVideoPortRange(long paramLong, int paramInt1,
			int paramInt2);

	private native void setIncomingTimeout(long paramLong, int paramInt);

	private native void setInCallTimeout(long paramLong, int paramInt);

	private native void setPrimaryContact2(long paramLong, String paramString);

	private native String getPrimaryContact(long paramLong);

	private native void setPrimaryContact(long paramLong, String paramString1,
			String paramString2);

	private native String getPrimaryContactUsername(long paramLong);

	private native String getPrimaryContactDisplayName(long paramLong);

	private native void setChatDatabasePath(long paramLong, String paramString);

	private native long[] getChatRooms(long paramLong);

	private native int migrateToMultiTransport(long paramLong);

	private native void setCallErrorTone(long paramLong, int paramInt,
			String paramString);

	private native void enableSdp200Ack(long paramLong, boolean paramBoolean);

	private native boolean isSdp200AckEnabled(long paramLong);

	private native void stopRinging(long paramLong);

	private static native void setAndroidPowerManager(Object paramObject);

	private native void setAndroidWifiLock(long paramLong, Object paramObject);

	private native void setAndroidMulticastLock(long paramLong,
			Object paramObject);

	LinphoneCoreImpl(LinphoneCoreListener listener, File userConfig,
			File factoryConfig, Object userdata) throws IOException {
		this.mListener = listener;
		String user = userConfig == null ? null : userConfig.getCanonicalPath();
		String factory = factoryConfig == null ? null : factoryConfig
				.getCanonicalPath();
		this.nativePtr = newLinphoneCore(listener, user, factory, userdata);
	}

	LinphoneCoreImpl(LinphoneCoreListener listener) throws IOException {
		this.mListener = listener;
		this.nativePtr = newLinphoneCore(listener, null, null, null);
	}
	
	public void setListener(LinphoneCoreListener listener){
		this.nativePtr = newLinphoneCore(listener, null, null, null);
	}
	

	protected void finalize() throws Throwable {
		if (this.nativePtr != 0L) {
			destroy();
		}
	}

	private boolean contextInitialized() {
		if (this.mContext == null) {
			Log.e(new Object[] { "Context of LinphoneCore has not been initialized, call setContext() after creating LinphoneCore." });
			return false;
		}
		return true;
	}

	public void setContext(Object context) {
		this.mContext = ((Context) context);
		this.mAudioManager = ((AudioManager) this.mContext.getSystemService("audio"));
		setAndroidPowerManager(this.mContext.getSystemService("power"));
		if (Version.sdkAboveOrEqual(12)) {
			WifiManager wifiManager = (WifiManager) this.mContext.getSystemService("wifi");
			WifiLock lock = wifiManager.createWifiLock(3, "linphonecore ["+ this.nativePtr + "] wifi-lock");
			lock.setReferenceCounted(true);
			// setAndroidWifiLock(this.nativePtr, lock);
		}
		if (Version.sdkAboveOrEqual(14)) {
			WifiManager wifiManager = (WifiManager) this.mContext.getSystemService("wifi");
			MulticastLock lock = wifiManager.createMulticastLock("linphonecore [" + this.nativePtr+ "] multicast-lock");
			lock.setReferenceCounted(true);
			// setAndroidMulticastLock(this.nativePtr, lock);
		}
	}

	public synchronized void addAuthInfo(LinphoneAuthInfo info) {
		isValid();
		addAuthInfo(this.nativePtr, ((LinphoneAuthInfoImpl) info).nativePtr);
	}

	public synchronized void removeAuthInfo(LinphoneAuthInfo info) {
		isValid();
		removeAuthInfo(this.nativePtr, ((LinphoneAuthInfoImpl) info).nativePtr);
	}

	public synchronized LinphoneProxyConfig getDefaultProxyConfig() {
		isValid();
		return getDefaultProxyConfig(this.nativePtr);
	}

	public synchronized LinphoneCall invite(String uri) {
		isValid();
		return (LinphoneCall) invite(this.nativePtr, uri);
	}

	public synchronized void iterate() {
		isValid();
		iterate(this.nativePtr);
	}

	public synchronized void setDefaultProxyConfig(LinphoneProxyConfig proxyCfg) {
		isValid();
		long proxyPtr = proxyCfg != null ? ((LinphoneProxyConfigImpl) proxyCfg).nativePtr
				: 0L;
		setDefaultProxyConfig(this.nativePtr, proxyPtr);
	}

	public synchronized void addProxyConfig(LinphoneProxyConfig proxyCfg)
			throws LinphoneCoreException {
		isValid();
		if (addProxyConfig(proxyCfg, this.nativePtr,
				((LinphoneProxyConfigImpl) proxyCfg).nativePtr) != 0) {
			throw new LinphoneCoreException("bad proxy config");
		}
		((LinphoneProxyConfigImpl) proxyCfg).mCore = this;
	}

	public synchronized void removeProxyConfig(LinphoneProxyConfig proxyCfg) {
		isValid();
		removeProxyConfig(this.nativePtr,
				((LinphoneProxyConfigImpl) proxyCfg).nativePtr);
	}

	public synchronized void clearAuthInfos() {
		isValid();
		clearAuthInfos(this.nativePtr);
	}

	public synchronized void clearProxyConfigs() {
		isValid();
		clearProxyConfigs(this.nativePtr);
	}

	public synchronized void terminateCall(LinphoneCall aCall) {
		isValid();
		if (aCall != null) {
			terminateCall(this.nativePtr, ((LinphoneCallImpl) aCall).nativePtr);
		}
	}

	public synchronized LinphoneAddress getRemoteAddress() {
		isValid();
		long ptr = getRemoteAddress(this.nativePtr);
		if (ptr == 0L) {
			return null;
		}
		return new LinphoneAddressImpl(ptr,
				LinphoneAddressImpl.WrapMode.FromConst);
	}

	public synchronized boolean isIncall() {
		isValid();
		return isInCall(this.nativePtr);
	}

	public synchronized boolean isInComingInvitePending() {
		isValid();
		return isInComingInvitePending(this.nativePtr);
	}

	public synchronized void acceptCall(LinphoneCall aCall) {
		isValid();
		acceptCall(this.nativePtr, ((LinphoneCallImpl) aCall).nativePtr);
	}

	public synchronized LinphoneCallLog[] getCallLogs() {
		isValid();
		LinphoneCallLog[] logs = new LinphoneCallLog[getNumberOfCallLogs(this.nativePtr)];
		for (int i = 0; i < getNumberOfCallLogs(this.nativePtr); i++) {
			logs[i] = new LinphoneCallLogImpl(getCallLog(this.nativePtr, i));
		}
		return logs;
	}

	public synchronized void destroy() {
		setAndroidPowerManager(null);
		delete(this.nativePtr);
		this.nativePtr = 0L;
	}

	private void isValid() {
		if (this.nativePtr == 0L) {
			throw new RuntimeException("object already destroyed");
		}
	}

	public synchronized void setNetworkReachable(boolean isReachable) {
		setNetworkStateReachable(this.nativePtr, isReachable);
	}

	public synchronized void setPlaybackGain(float gain) {
		setPlaybackGain(this.nativePtr, gain);
	}

	public synchronized float getPlaybackGain() {
		return getPlaybackGain(this.nativePtr);
	}

	public synchronized void muteMic(boolean isMuted) {
		muteMic(this.nativePtr, isMuted);
	}

	public synchronized LinphoneAddress interpretUrl(String destination)
			throws LinphoneCoreException {
		long lAddress = interpretUrl(this.nativePtr, destination);
		if (lAddress != 0L) {
			return new LinphoneAddressImpl(lAddress,
					LinphoneAddressImpl.WrapMode.FromNew);
		}
		throw new LinphoneCoreException("Cannot interpret [" + destination
				+ "]");
	}

	public synchronized LinphoneCall invite(LinphoneAddress to)
			throws LinphoneCoreException {
		LinphoneCall call = (LinphoneCall) inviteAddress(this.nativePtr,
				((LinphoneAddressImpl) to).nativePtr);
		if (call != null) {
			return call;
		}
		throw new LinphoneCoreException("Unable to invite address "
				+ to.asString());
	}

	public synchronized void sendDtmf(char number) {
		sendDtmf(this.nativePtr, number);
	}

	public synchronized void clearCallLogs() {
		clearCallLogs(this.nativePtr);
	}

	public synchronized boolean isMicMuted() {
		return isMicMuted(this.nativePtr);
	}

	public synchronized PayloadType findPayloadType(String mime, int clockRate,
			int channels) {
		isValid();
		long playLoadType = findPayloadType(this.nativePtr, mime, clockRate,
				channels);
		if (playLoadType == 0L) {
			return null;
		}
		return new PayloadTypeImpl(playLoadType);
	}

	public synchronized void enablePayloadType(PayloadType pt, boolean enable)
			throws LinphoneCoreException {
		isValid();
		if (enablePayloadType(this.nativePtr, ((PayloadTypeImpl) pt).nativePtr,
				enable) != 0) {
			throw new LinphoneCoreException("cannot enable payload type [" + pt
					+ "]");
		}
	}

	public synchronized boolean isPayloadTypeEnabled(PayloadType pt) {
		isValid();
		return isPayloadTypeEnabled(this.nativePtr,
				((PayloadTypeImpl) pt).nativePtr);
	}

	public synchronized boolean payloadTypeIsVbr(PayloadType pt) {
		isValid();
		return payloadTypeIsVbr(this.nativePtr,
				((PayloadTypeImpl) pt).nativePtr);
	}

	public synchronized void enableEchoCancellation(boolean enable) {
		isValid();
		enableEchoCancellation(this.nativePtr, enable);
	}

	public synchronized boolean isEchoCancellationEnabled() {
		isValid();
		return isEchoCancellationEnabled(this.nativePtr);
	}

	public synchronized LinphoneCall getCurrentCall() {
		isValid();
		return (LinphoneCall) getCurrentCall(this.nativePtr);
	}

	public int getPlayLevel() {
		return 0;
	}

	public void setPlayLevel(int level) {
	}

	private void applyAudioHacks() {
		if (Hacks.needGalaxySAudioHack()) {
			setMicrophoneGain(-9.0F);
		}
	}

	private void setAudioModeIncallForGalaxyS() {
		if (!contextInitialized()) {
			return;
		}
		this.mAudioManager.setMode(2);
	}

	public void routeAudioToSpeakerHelper(boolean speakerOn) {
		if (!contextInitialized()) {
			return;
		}
		if (Hacks.needGalaxySAudioHack()) {
			setAudioModeIncallForGalaxyS();
		}
		this.mAudioManager.setSpeakerphoneOn(speakerOn);
	}

	private native void forceSpeakerState(long paramLong, boolean paramBoolean);

	public void enableSpeaker(boolean value) {
		LinphoneCall call = getCurrentCall();
		this.mSpeakerEnabled = value;
		applyAudioHacks();
		if ((call != null)
				&& (call.getState() == LinphoneCall.State.StreamsRunning)
				&& (Hacks.needGalaxySAudioHack())) {
			Log.d(new Object[] { "Hack to have speaker=",
					Boolean.valueOf(value), " while on call" });
			forceSpeakerState(this.nativePtr, value);
		} else {
			routeAudioToSpeakerHelper(value);
		}
	}

	public boolean isSpeakerEnabled() {
		return this.mSpeakerEnabled;
	}

	public synchronized void playDtmf(char number, int duration) {
		playDtmf(this.nativePtr, number, duration);
	}

	public synchronized void stopDtmf() {
		stopDtmf(this.nativePtr);
	}

	public synchronized void addFriend(LinphoneFriend lf)
			throws LinphoneCoreException {
		addFriend(this.nativePtr, ((LinphoneFriendImpl) lf).nativePtr);
	}

	public synchronized LinphoneFriend[] getFriendList() {
		return getFriendList(this.nativePtr);
	}

	public synchronized void setPresenceInfo(int minutes_away,
			String alternative_contact, OnlineStatus status) {
		setPresenceInfo(this.nativePtr, minutes_away, alternative_contact,
				status.mValue);
	}

	public synchronized OnlineStatus getPresenceInfo() {
		return OnlineStatus.fromInt(getPresenceInfo(this.nativePtr));
	}

	public synchronized void setPresenceModel(PresenceModel presence) {
		setPresenceModel(this.nativePtr,
				((PresenceModelImpl) presence).getNativePtr());
	}

	public synchronized PresenceModel getPresenceModel() {
		return (PresenceModel) getPresenceModel(this.nativePtr);
	}

	public synchronized LinphoneChatRoom getOrCreateChatRoom(String to) {
		return new LinphoneChatRoomImpl(getOrCreateChatRoom(this.nativePtr, to));
	}

	public synchronized void setPreviewWindow(Object w) {
		setPreviewWindowId(this.nativePtr, w);
	}

	public synchronized void setVideoWindow(Object w) {
		setVideoWindowId(this.nativePtr, w);
	}

	public synchronized void setDeviceRotation(int rotation) {
		setDeviceRotation(this.nativePtr, rotation);
	}

	public synchronized void enableVideo(boolean vcap_enabled,
			boolean display_enabled) {
		enableVideo(this.nativePtr, vcap_enabled, display_enabled);
	}

	public synchronized boolean isVideoEnabled() {
		return isVideoEnabled(this.nativePtr);
	}

	public synchronized boolean isVideoSupported() {
		return isVideoSupported(this.nativePtr);
	}

	public synchronized FirewallPolicy getFirewallPolicy() {
		return FirewallPolicy.fromInt(getFirewallPolicy(this.nativePtr));
	}

	public synchronized String getStunServer() {
		return getStunServer(this.nativePtr);
	}

	public synchronized void setFirewallPolicy(FirewallPolicy pol) {
		setFirewallPolicy(this.nativePtr, pol.value());
	}

	public synchronized void setStunServer(String stunServer) {
		setStunServer(this.nativePtr, stunServer);
	}

	public synchronized LinphoneCallParams createDefaultCallParameters() {
		return new LinphoneCallParamsImpl(
				createDefaultCallParams(this.nativePtr));
	}

	public synchronized LinphoneCall inviteAddressWithParams(
			LinphoneAddress to, LinphoneCallParams params)
			throws LinphoneCoreException {
		long ptrDestination = ((LinphoneAddressImpl) to).nativePtr;
		long ptrParams = ((LinphoneCallParamsImpl) params).nativePtr;

		LinphoneCall call = (LinphoneCall) inviteAddressWithParams(
				this.nativePtr, ptrDestination, ptrParams);
		if (call != null) {
			return call;
		}
		throw new LinphoneCoreException("Unable to invite with params "
				+ to.asString());
	}

	public synchronized int updateCall(LinphoneCall call,
			LinphoneCallParams params) {
		long ptrCall = ((LinphoneCallImpl) call).nativePtr;
		long ptrParams = params != null ? ((LinphoneCallParamsImpl) params).nativePtr
				: 0L;

		return updateCall(this.nativePtr, ptrCall, ptrParams);
	}

	public synchronized void setUploadBandwidth(int bw) {
		setUploadBandwidth(this.nativePtr, bw);
	}

	public synchronized void setDownloadBandwidth(int bw) {
		setDownloadBandwidth(this.nativePtr, bw);
	}

	public synchronized void setPreferredVideoSize(VideoSize vSize) {
		setPreferredVideoSize(this.nativePtr, vSize.width, vSize.height);
	}

	public synchronized void setPreferredVideoSizeByName(String name) {
		setPreferredVideoSizeByName(this.nativePtr, name);
	}

	public synchronized VideoSize getPreferredVideoSize() {
		int[] nativeSize = getPreferredVideoSize(this.nativePtr);

		VideoSize vSize = new VideoSize();
		vSize.width = nativeSize[0];
		vSize.height = nativeSize[1];
		return vSize;
	}

	public synchronized void setRing(String path) {
		setRing(this.nativePtr, path);
	}

	public synchronized String getRing() {
		return getRing(this.nativePtr);
	}

	public synchronized void setRootCA(String path) {
		setRootCA(this.nativePtr, path);
	}

	public synchronized void setRingback(String path) {
		setRingback(this.nativePtr, path);
	}

	public synchronized LinphoneProxyConfig[] getProxyConfigList() {
		return getProxyConfigList(this.nativePtr);
	}

	public synchronized PayloadType[] getVideoCodecs() {
		long[] typesPtr = listVideoPayloadTypes(this.nativePtr);
		if (typesPtr == null) {
			return null;
		}
		PayloadType[] codecs = new PayloadType[typesPtr.length];
		for (int i = 0; i < codecs.length; i++) {
			codecs[i] = new PayloadTypeImpl(typesPtr[i]);
		}
		return codecs;
	}

	public synchronized void setVideoCodecs(PayloadType[] codecs) {
		long[] typesPtr = new long[codecs.length];
		for (int i = 0; i < codecs.length; i++) {
			typesPtr[i] = ((PayloadTypeImpl) codecs[i]).nativePtr;
		}
		setVideoCodecs(this.nativePtr, typesPtr);
	}

	public synchronized PayloadType[] getAudioCodecs() {
		long[] typesPtr = listAudioPayloadTypes(this.nativePtr);
		if (typesPtr == null) {
			return null;
		}
		PayloadType[] codecs = new PayloadType[typesPtr.length];
		for (int i = 0; i < codecs.length; i++) {
			codecs[i] = new PayloadTypeImpl(typesPtr[i]);
		}
		return codecs;
	}

	public synchronized void setAudioCodecs(PayloadType[] codecs) {
		long[] typesPtr = new long[codecs.length];
		for (int i = 0; i < codecs.length; i++) {
			typesPtr[i] = ((PayloadTypeImpl) codecs[i]).nativePtr;
		}
		setAudioCodecs(this.nativePtr, typesPtr);
	}

	public synchronized boolean isNetworkReachable() {
		return isNetworkStateReachable(this.nativePtr);
	}

	public synchronized void enableKeepAlive(boolean enable) {
		enableKeepAlive(this.nativePtr, enable);
	}

	public synchronized boolean isKeepAliveEnabled() {
		return isKeepAliveEnabled(this.nativePtr);
	}

	public synchronized void startEchoCalibration(LinphoneCoreListener listener)
			throws LinphoneCoreException {
		startEchoCalibration(this.nativePtr, listener);
	}

	public synchronized Transports getSignalingTransportPorts() {
		Transports transports = new Transports();
		transports.udp = getSignalingTransportPort(this.nativePtr, 0);
		transports.tcp = getSignalingTransportPort(this.nativePtr, 1);
		transports.tls = getSignalingTransportPort(this.nativePtr, 3);

		return transports;
	}

	public synchronized void setSignalingTransportPorts(Transports transports) {
		setSignalingTransportPorts(this.nativePtr, transports.udp,
				transports.tcp, transports.tls);
	}

	public synchronized void enableIpv6(boolean enable) {
		enableIpv6(this.nativePtr, enable);
	}

	public synchronized boolean isIpv6Enabled() {
		return isIpv6Enabled(this.nativePtr);
	}

	public synchronized void adjustSoftwareVolume(int i) {
	}

	public synchronized boolean pauseCall(LinphoneCall call) {
		return 0 == pauseCall(this.nativePtr,
				((LinphoneCallImpl) call).nativePtr);
	}

	public synchronized boolean resumeCall(LinphoneCall call) {
		return 0 == resumeCall(this.nativePtr,
				((LinphoneCallImpl) call).nativePtr);
	}

	public synchronized boolean pauseAllCalls() {
		return 0 == pauseAllCalls(this.nativePtr);
	}

	public synchronized void setDownloadPtime(int ptime) {
		setDownloadPtime(this.nativePtr, ptime);
	}

	public synchronized void setUploadPtime(int ptime) {
		setUploadPtime(this.nativePtr, ptime);
	}

	public synchronized void setZrtpSecretsCache(String file) {
		setZrtpSecretsCache(this.nativePtr, file);
	}

	public synchronized void enableEchoLimiter(boolean val) {
		enableEchoLimiter(this.nativePtr, val);
	}

	public synchronized void setVideoDevice(int id) {
		Log.i(new Object[] { "Setting camera id :", Integer.valueOf(id) });
		if (setVideoDevice(this.nativePtr, id) != 0) {
			Log.e(new Object[] { "Failed to set video device to id:",
					Integer.valueOf(id) });
		}
	}

	public synchronized int getVideoDevice() {
		return getVideoDevice(this.nativePtr);
	}

	private native void leaveConference(long paramLong);

	public synchronized void leaveConference() {
		leaveConference(this.nativePtr);
	}

	private native boolean enterConference(long paramLong);

	public synchronized boolean enterConference() {
		return enterConference(this.nativePtr);
	}

	private native boolean isInConference(long paramLong);

	public synchronized boolean isInConference() {
		return isInConference(this.nativePtr);
	}

	private native void terminateConference(long paramLong);

	public synchronized void terminateConference() {
		terminateConference(this.nativePtr);
	}

	private native int getConferenceSize(long paramLong);

	public synchronized int getConferenceSize() {
		return getConferenceSize(this.nativePtr);
	}

	private native int getCallsNb(long paramLong);

	public synchronized int getCallsNb() {
		return getCallsNb(this.nativePtr);
	}

	private native void terminateAllCalls(long paramLong);

	public synchronized void terminateAllCalls() {
		terminateAllCalls(this.nativePtr);
	}

	private native Object getCall(long paramLong, int paramInt);

	public synchronized LinphoneCall[] getCalls() {
		int size = getCallsNb(this.nativePtr);
		LinphoneCall[] calls = new LinphoneCall[size];
		for (int i = 0; i < size; i++) {
			calls[i] = ((LinphoneCall) getCall(this.nativePtr, i));
		}
		return calls;
	}

	private native void addAllToConference(long paramLong);

	public synchronized void addAllToConference() {
		addAllToConference(this.nativePtr);
	}

	private native void addToConference(long paramLong1, long paramLong2);

	public synchronized void addToConference(LinphoneCall call) {
		addToConference(this.nativePtr, getCallPtr(call));
	}

	private native void removeFromConference(long paramLong1, long paramLong2);

	public synchronized void removeFromConference(LinphoneCall call) {
		removeFromConference(this.nativePtr, getCallPtr(call));
	}

	private long getCallPtr(LinphoneCall call) {
		return ((LinphoneCallImpl) call).nativePtr;
	}

	private long getCallParamsPtr(LinphoneCallParams callParams) {
		return ((LinphoneCallParamsImpl) callParams).nativePtr;
	}

	private native int transferCall(long paramLong1, long paramLong2,
			String paramString);

	public synchronized void transferCall(LinphoneCall call, String referTo) {
		transferCall(this.nativePtr, getCallPtr(call), referTo);
	}

	private native int transferCallToAnother(long paramLong1, long paramLong2,
			long paramLong3);

	public synchronized void transferCallToAnother(LinphoneCall call,
			LinphoneCall dest) {
		transferCallToAnother(this.nativePtr, getCallPtr(call),
				getCallPtr(dest));
	}

	private native Object findCallFromUri(long paramLong, String paramString);

	public synchronized LinphoneCall findCallFromUri(String uri) {
		return (LinphoneCall) findCallFromUri(this.nativePtr, uri);
	}

	public synchronized MediaEncryption getMediaEncryption() {
		return MediaEncryption.fromInt(getMediaEncryption(this.nativePtr));
	}

	public synchronized boolean isMediaEncryptionMandatory() {
		return isMediaEncryptionMandatory(this.nativePtr);
	}

	public synchronized void setMediaEncryption(MediaEncryption menc) {
		setMediaEncryption(this.nativePtr, menc.mValue);
	}

	public synchronized void setMediaEncryptionMandatory(boolean yesno) {
		setMediaEncryptionMandatory(this.nativePtr, yesno);
	}

	private native int getMaxCalls(long paramLong);

	public synchronized int getMaxCalls() {
		return getMaxCalls(this.nativePtr);
	}

	public boolean isMyself(String uri) {
		LinphoneProxyConfig lpc = getDefaultProxyConfig();
		if (lpc == null) {
			return false;
		}
		return uri.equals(lpc.getIdentity());
	}

	private native boolean soundResourcesLocked(long paramLong);

	public synchronized boolean soundResourcesLocked() {
		return soundResourcesLocked(this.nativePtr);
	}

	private native void setMaxCalls(long paramLong, int paramInt);

	public synchronized void setMaxCalls(int max) {
		setMaxCalls(this.nativePtr, max);
	}

	private native boolean isEchoLimiterEnabled(long paramLong);

	public synchronized boolean isEchoLimiterEnabled() {
		return isEchoLimiterEnabled(this.nativePtr);
	}

	private native boolean mediaEncryptionSupported(long paramLong, int paramInt);

	public synchronized boolean mediaEncryptionSupported(MediaEncryption menc) {
		return mediaEncryptionSupported(this.nativePtr, menc.mValue);
	}

	private native void setPlayFile(long paramLong, String paramString);

	public synchronized void setPlayFile(String path) {
		setPlayFile(this.nativePtr, path);
	}

	private native void tunnelAddServerAndMirror(long paramLong,
			String paramString, int paramInt1, int paramInt2, int paramInt3);

	public synchronized void tunnelAddServerAndMirror(String host, int port,
			int mirror, int ms) {
		tunnelAddServerAndMirror(this.nativePtr, host, port, mirror, ms);
	}

	private native void tunnelAddServer(long paramLong,
			TunnelConfig paramTunnelConfig);

	public synchronized void tunnelAddServer(TunnelConfig config) {
		tunnelAddServer(this.nativePtr, config);
	}

	private final native TunnelConfig[] tunnelGetServers(long paramLong);

	public final synchronized TunnelConfig[] tunnelGetServers() {
		return tunnelGetServers(this.nativePtr);
	}

	private native void tunnelAutoDetect(long paramLong);

	public synchronized void tunnelAutoDetect() {
		tunnelAutoDetect(this.nativePtr);
	}

	private native void tunnelCleanServers(long paramLong);

	public synchronized void tunnelCleanServers() {
		tunnelCleanServers(this.nativePtr);
	}

	private native void tunnelEnable(long paramLong, boolean paramBoolean);

	public synchronized void tunnelEnable(boolean enable) {
		tunnelEnable(this.nativePtr, enable);
	}

	private native void tunnelSetMode(long paramLong, int paramInt);

	public synchronized void tunnelSetMode(TunnelMode mode) {
		tunnelSetMode(this.nativePtr, TunnelMode.enumToInt(mode));
	}

	private native int tunnelGetMode(long paramLong);

	public synchronized TunnelMode tunnelGetMode() {
		return TunnelMode.intToEnum(tunnelGetMode(this.nativePtr));
	}

	private native void tunnelEnableSip(long paramLong, boolean paramBoolean);

	public void tunnelEnableSip(boolean enable) {
		tunnelEnableSip(this.nativePtr, enable);
	}

	private native boolean tunnelSipEnabled(long paramLong);

	public boolean tunnelSipEnabled() {
		return tunnelSipEnabled(this.nativePtr);
	}

	public native boolean isTunnelAvailable();

	private native void acceptCallWithParams(long paramLong1, long paramLong2,
			long paramLong3);

	public synchronized void acceptCallWithParams(LinphoneCall aCall,
			LinphoneCallParams params) throws LinphoneCoreException {
		acceptCallWithParams(this.nativePtr, getCallPtr(aCall),
				getCallParamsPtr(params));
	}

	private native void acceptCallUpdate(long paramLong1, long paramLong2,
			long paramLong3);

	public synchronized void acceptCallUpdate(LinphoneCall aCall,
			LinphoneCallParams params) throws LinphoneCoreException {
		acceptCallUpdate(this.nativePtr, getCallPtr(aCall),
				getCallParamsPtr(params));
	}

	private native void deferCallUpdate(long paramLong1, long paramLong2);

	public synchronized void deferCallUpdate(LinphoneCall aCall)
			throws LinphoneCoreException {
		deferCallUpdate(this.nativePtr, getCallPtr(aCall));
	}

	private native void setVideoPolicy(long paramLong, boolean paramBoolean1,
			boolean paramBoolean2);

	public synchronized void setVideoPolicy(boolean autoInitiate,
			boolean autoAccept) {
		setVideoPolicy(this.nativePtr, autoInitiate, autoAccept);
	}

	private native boolean getVideoAutoInitiatePolicy(long paramLong);

	public synchronized boolean getVideoAutoInitiatePolicy() {
		return getVideoAutoInitiatePolicy(this.nativePtr);
	}

	private native boolean getVideoAutoAcceptPolicy(long paramLong);

	public synchronized boolean getVideoAutoAcceptPolicy() {
		return getVideoAutoAcceptPolicy(this.nativePtr);
	}

	private native void setStaticPicture(long paramLong, String paramString);

	public synchronized void setStaticPicture(String path) {
		setStaticPicture(this.nativePtr, path);
	}

	private native void setUserAgent(long paramLong, String paramString1,
			String paramString2);

	public synchronized void setUserAgent(String name, String version) {
		setUserAgent(this.nativePtr, name, version);
	}

	private native void setCpuCountNative(int paramInt);

	public synchronized void setCpuCount(int count) {
		setCpuCountNative(count);
	}

	public synchronized int getMissedCallsCount() {
		return getMissedCallsCount(this.nativePtr);
	}

	public synchronized void removeCallLog(LinphoneCallLog log) {
		removeCallLog(this.nativePtr,
				((LinphoneCallLogImpl) log).getNativePtr());
	}

	public synchronized void resetMissedCallsCount() {
		resetMissedCallsCount(this.nativePtr);
	}

	private native void tunnelSetHttpProxy(long paramLong, String paramString1,
			int paramInt, String paramString2, String paramString3);

	public synchronized void tunnelSetHttpProxy(String proxy_host, int port,
			String username, String password) {
		tunnelSetHttpProxy(this.nativePtr, proxy_host, port, username, password);
	}

	private native void refreshRegisters(long paramLong);

	public synchronized void refreshRegisters() {
		refreshRegisters(this.nativePtr);
	}

	public String getVersion() {
		return getVersion(this.nativePtr);
	}

	static int FIND_PAYLOAD_IGNORE_RATE = -1;
	static int FIND_PAYLOAD_IGNORE_CHANNELS = -1;

	public synchronized PayloadType findPayloadType(String mime, int clockRate) {
		return findPayloadType(mime, clockRate, FIND_PAYLOAD_IGNORE_CHANNELS);
	}

	private native void removeFriend(long paramLong1, long paramLong2);

	public synchronized void removeFriend(LinphoneFriend lf) {
		removeFriend(this.nativePtr, lf.getNativePtr());
	}

	private native LinphoneFriend getFriendByAddress(long paramLong,
			String paramString);

	public synchronized LinphoneFriend findFriendByAddress(String sipUri) {
		return getFriendByAddress(this.nativePtr, sipUri);
	}

	public synchronized void setAudioPort(int port) {
		setAudioPort(this.nativePtr, port);
	}

	public synchronized void setVideoPort(int port) {
		setVideoPort(this.nativePtr, port);
	}

	public synchronized void setAudioPortRange(int minPort, int maxPort) {
		setAudioPortRange(this.nativePtr, minPort, maxPort);
	}

	public synchronized void setVideoPortRange(int minPort, int maxPort) {
		setVideoPortRange(this.nativePtr, minPort, maxPort);
	}

	public synchronized void setIncomingTimeout(int timeout) {
		setIncomingTimeout(this.nativePtr, timeout);
	}

	public synchronized void setInCallTimeout(int timeout) {
		setInCallTimeout(this.nativePtr, timeout);
	}

	private native void setMicrophoneGain(long paramLong, float paramFloat);

	public synchronized void setMicrophoneGain(float gain) {
		setMicrophoneGain(this.nativePtr, gain);
	}

	public synchronized void setPrimaryContact(String address) {
		setPrimaryContact2(this.nativePtr, address);
	}

	public synchronized String getPrimaryContact() {
		return getPrimaryContact(this.nativePtr);
	}

	public synchronized void setPrimaryContact(String displayName,
			String username) {
		setPrimaryContact(this.nativePtr, displayName, username);
	}

	public synchronized String getPrimaryContactUsername() {
		return getPrimaryContactUsername(this.nativePtr);
	}

	public synchronized String getPrimaryContactDisplayName() {
		return getPrimaryContactDisplayName(this.nativePtr);
	}

	private native void setUseSipInfoForDtmfs(long paramLong,
			boolean paramBoolean);

	public synchronized void setUseSipInfoForDtmfs(boolean use) {
		setUseSipInfoForDtmfs(this.nativePtr, use);
	}

	private native boolean getUseSipInfoForDtmfs(long paramLong);

	public synchronized boolean getUseSipInfoForDtmfs() {
		return getUseSipInfoForDtmfs(this.nativePtr);
	}

	private native void setUseRfc2833ForDtmfs(long paramLong,
			boolean paramBoolean);

	public synchronized void setUseRfc2833ForDtmfs(boolean use) {
		setUseRfc2833ForDtmfs(this.nativePtr, use);
	}

	private native boolean getUseRfc2833ForDtmfs(long paramLong);

	public synchronized boolean getUseRfc2833ForDtmfs() {
		return getUseRfc2833ForDtmfs(this.nativePtr);
	}

	private native long getConfig(long paramLong);

	public synchronized LpConfig getConfig() {
		long configPtr = getConfig(this.nativePtr);
		return new LpConfigImpl(configPtr);
	}

	private native boolean needsEchoCalibration(long paramLong);

	public synchronized boolean needsEchoCalibration() {
		return needsEchoCalibration(this.nativePtr);
	}

	private native boolean needsEchoCanceler(long paramLong);

	public synchronized boolean needsEchoCanceler() {
		return needsEchoCanceler(this.nativePtr);
	}

	private native void declineCall(long paramLong1, long paramLong2,
			int paramInt);

	public synchronized void declineCall(LinphoneCall aCall, Reason reason) {
		declineCall(this.nativePtr, ((LinphoneCallImpl) aCall).nativePtr,
				reason.mValue);
	}

	private native boolean upnpAvailable(long paramLong);

	public synchronized boolean upnpAvailable() {
		return upnpAvailable(this.nativePtr);
	}

	private native int getUpnpState(long paramLong);

	public synchronized UpnpState getUpnpState() {
		return UpnpState.fromInt(getUpnpState(this.nativePtr));
	}

	private native String getUpnpExternalIpaddress(long paramLong);

	public synchronized String getUpnpExternalIpaddress() {
		return getUpnpExternalIpaddress(this.nativePtr);
	}

	private native int startConferenceRecording(long paramLong,
			String paramString);

	public synchronized void startConferenceRecording(String path) {
		startConferenceRecording(this.nativePtr, path);
	}

	private native int stopConferenceRecording(long paramLong);

	public synchronized void stopConferenceRecording() {
		stopConferenceRecording(this.nativePtr);
	}

	public synchronized PayloadType findPayloadType(String mime) {
		return findPayloadType(mime, FIND_PAYLOAD_IGNORE_RATE);
	}

	private native void setSipDscp(long paramLong, int paramInt);

	public synchronized void setSipDscp(int dscp) {
		setSipDscp(this.nativePtr, dscp);
	}

	private native int getSipDscp(long paramLong);

	public synchronized int getSipDscp() {
		return getSipDscp(this.nativePtr);
	}

	private native void setAudioDscp(long paramLong, int paramInt);

	public synchronized void setAudioDscp(int dscp) {
		setAudioDscp(this.nativePtr, dscp);
	}

	private native int getAudioDscp(long paramLong);

	public synchronized int getAudioDscp() {
		return getAudioDscp(this.nativePtr);
	}

	private native void setVideoDscp(long paramLong, int paramInt);

	public synchronized void setVideoDscp(int dscp) {
		setVideoDscp(this.nativePtr, dscp);
	}

	private native int getVideoDscp(long paramLong);

	public synchronized int getVideoDscp() {
		return getVideoDscp(this.nativePtr);
	}

	private native long createInfoMessage(long paramLong);

	public synchronized LinphoneInfoMessage createInfoMessage() {
		return new LinphoneInfoMessageImpl(createInfoMessage(this.nativePtr));
	}

	private native Object subscribe(long paramLong1, long paramLong2,
			String paramString1, int paramInt, String paramString2,
			String paramString3, byte[] paramArrayOfByte, String paramString4);

	public synchronized LinphoneEvent subscribe(LinphoneAddress resource,
			String eventname, int expires, LinphoneContent content) {
		return (LinphoneEvent) subscribe(this.nativePtr,
				((LinphoneAddressImpl) resource).nativePtr, eventname, expires,
				content != null ? content.getType() : null,
				content != null ? content.getSubtype() : null,
				content != null ? content.getData() : null,
				content != null ? content.getEncoding() : null);
	}

	private native Object publish(long paramLong1, long paramLong2,
			String paramString1, int paramInt, String paramString2,
			String paramString3, byte[] paramArrayOfByte, String paramString4);

	public synchronized LinphoneEvent publish(LinphoneAddress resource,
			String eventname, int expires, LinphoneContent content) {
		return (LinphoneEvent) publish(this.nativePtr,
				((LinphoneAddressImpl) resource).nativePtr, eventname, expires,
				content != null ? content.getType() : null,
				content != null ? content.getSubtype() : null,
				content != null ? content.getData() : null,
				content != null ? content.getEncoding() : null);
	}

	private native Object createSubscribe(long paramLong1, long paramLong2,
			String paramString, int paramInt);

	public synchronized LinphoneEvent createSubscribe(LinphoneAddress resource,
			String event, int expires) {
		return (LinphoneEvent) createSubscribe(this.nativePtr,
				((LinphoneAddressImpl) resource).nativePtr, event, expires);
	}

	private native Object createPublish(long paramLong1, long paramLong2,
			String paramString, int paramInt);

	public synchronized LinphoneEvent createPublish(LinphoneAddress resource,
			String event, int expires) {
		return (LinphoneEvent) createPublish(this.nativePtr,
				((LinphoneAddressImpl) resource).nativePtr, event, expires);
	}

	public synchronized void setChatDatabasePath(String path) {
		setChatDatabasePath(this.nativePtr, path);
	}

	public synchronized LinphoneChatRoom[] getChatRooms() {
		long[] typesPtr = getChatRooms(this.nativePtr);
		if (typesPtr == null) {
			return null;
		}
		LinphoneChatRoom[] proxies = new LinphoneChatRoom[typesPtr.length];
		for (int i = 0; i < proxies.length; i++) {
			proxies[i] = new LinphoneChatRoomImpl(typesPtr[i]);
		}
		return proxies;
	}

	public synchronized LinphoneAuthInfo[] getAuthInfosList() {
		long[] typesPtr = getAuthInfosList(this.nativePtr);
		if (typesPtr == null) {
			return null;
		}
		LinphoneAuthInfo[] authInfos = new LinphoneAuthInfo[typesPtr.length];
		for (int i = 0; i < authInfos.length; i++) {
			authInfos[i] = new LinphoneAuthInfoImpl(typesPtr[i]);
		}
		return authInfos;
	}

	public synchronized LinphoneAuthInfo findAuthInfo(String username,
			String realm, String domain) {
		long ptr = findAuthInfos(this.nativePtr, username, realm, domain);
		if (ptr == 0L) {
			return null;
		}
		return new LinphoneAuthInfoImpl(ptr);
	}

	private native LinphoneCall startReferedCall(long paramLong1,
			long paramLong2, long paramLong3);

	public synchronized LinphoneCall startReferedCall(LinphoneCall call,
			LinphoneCallParams params) {
		long ptrParams = ((LinphoneCallParamsImpl) params).nativePtr;
		return startReferedCall(this.nativePtr, getCallPtr(call), ptrParams);
	}

	private native String[] listSupportedVideoResolutions(long paramLong);

	public synchronized String[] getSupportedVideoSizes() {
		return listSupportedVideoResolutions(this.nativePtr);
	}

	public synchronized int migrateToMultiTransport() {
		return migrateToMultiTransport(this.nativePtr);
	}

	private native boolean acceptEarlyMedia(long paramLong1, long paramLong2);

	public synchronized boolean acceptEarlyMedia(LinphoneCall call) {
		return acceptEarlyMedia(this.nativePtr, getCallPtr(call));
	}

	private native boolean acceptEarlyMediaWithParams(long paramLong1,
			long paramLong2, long paramLong3);

	public synchronized boolean acceptEarlyMediaWithParams(LinphoneCall call,
			LinphoneCallParams params) {
		long ptrParams = params != null ? ((LinphoneCallParamsImpl) params).nativePtr
				: 0L;
		return acceptEarlyMediaWithParams(this.nativePtr, getCallPtr(call),
				ptrParams);
	}

	public synchronized LinphoneProxyConfig createProxyConfig() {
		return new LinphoneProxyConfigImpl(this);
	}

	public synchronized LinphoneProxyConfig createProxyConfig(String identity,
			String proxy, String route, boolean enableRegister)
			throws LinphoneCoreException {
		isValid();
		try {
			return new LinphoneProxyConfigImpl(this, identity, proxy, route,
					enableRegister);
		} catch (LinphoneCoreException e) {
		}
		return null;
	}

	public synchronized void setCallErrorTone(Reason reason, String path) {
		setCallErrorTone(this.nativePtr, reason.mValue, path);
	}

	private native void setMtu(long paramLong, int paramInt);

	public synchronized void setMtu(int mtu) {
		setMtu(this.nativePtr, mtu);
	}

	private native int getMtu(long paramLong);

	public synchronized int getMtu() {
		return getMtu(this.nativePtr);
	}

	public synchronized void enableSdp200Ack(boolean enable) {
		enableSdp200Ack(this.nativePtr, enable);
	}

	public synchronized boolean isSdp200AckEnabled() {
		return isSdp200AckEnabled(this.nativePtr);
	}

	private native void setTone(long paramLong, int paramInt, String paramString);

	public synchronized void setTone(ToneID id, String wavfile) {
		setTone(this.nativePtr, id.mValue, wavfile);
	}

	private native void disableChat(long paramLong, int paramInt);

	public synchronized void disableChat(Reason denycode) {
		disableChat(this.nativePtr, denycode.mValue);
	}

	private native void enableChat(long paramLong);

	public synchronized void enableChat() {
		enableChat(this.nativePtr);
	}

	private native boolean chatEnabled(long paramLong);

	public synchronized boolean chatEnabled() {
		return chatEnabled(this.nativePtr);
	}

	public synchronized void stopRinging() {
		stopRinging(this.nativePtr);
	}

	private native void setPayloadTypeBitrate(long paramLong1, long paramLong2,
			int paramInt);

	public synchronized void setPayloadTypeBitrate(PayloadType pt, int bitrate) {
		setPayloadTypeBitrate(this.nativePtr, ((PayloadTypeImpl) pt).nativePtr,
				bitrate);
	}

	private native int getPayloadTypeBitrate(long paramLong1, long paramLong2);

	public synchronized int getPayloadTypeBitrate(PayloadType pt) {
		return getPayloadTypeBitrate(this.nativePtr,
				((PayloadTypeImpl) pt).nativePtr);
	}

	private native void setPayloadTypeNumber(long paramLong1, long paramLong2,
			int paramInt);

	public synchronized void setPayloadTypeNumber(PayloadType pt, int number) {
		setPayloadTypeNumber(this.nativePtr, ((PayloadTypeImpl) pt).nativePtr,
				number);
	}

	private native int getPayloadTypeNumber(long paramLong1, long paramLong2);

	public synchronized int getPayloadTypeNumber(PayloadType pt) {
		return getPayloadTypeNumber(this.nativePtr,
				((PayloadTypeImpl) pt).nativePtr);
	}

	public synchronized void enableAdaptiveRateControl(boolean enable) {
		enableAdaptiveRateControl(this.nativePtr, enable);
	}

	public synchronized boolean isAdaptiveRateControlEnabled() {
		return isAdaptiveRateControlEnabled(this.nativePtr);
	}

	public synchronized AdaptiveRateAlgorithm getAdaptiveRateAlgorithm() {
		return AdaptiveRateAlgorithm
				.fromString(getAdaptiveRateAlgorithm(this.nativePtr));
	}

	public synchronized void setAdaptiveRateAlgorithm(AdaptiveRateAlgorithm alg) {
		setAdaptiveRateAlgorithm(this.nativePtr, alg.toString());
	}

	private native void setAudioJittcomp(long paramLong, int paramInt);

	public synchronized void setAudioJittcomp(int value) {
		setAudioJittcomp(this.nativePtr, value);
	}

	private native void setVideoJittcomp(long paramLong, int paramInt);

	public synchronized void setVideoJittcomp(int value) {
		setVideoJittcomp(this.nativePtr, value);
	}

	private native void setFileTransferServer(long paramLong, String paramString);

	public synchronized void setFileTransferServer(String serverUrl) {
		setFileTransferServer(this.nativePtr, serverUrl);
	}

	private native String getFileTransferServer(long paramLong);

	public synchronized String getFileTransferServer() {
		return getFileTransferServer(this.nativePtr);
	}

	private native long createLocalPlayer(long paramLong,
			AndroidVideoWindowImpl paramAndroidVideoWindowImpl);

	public synchronized LinphonePlayer createLocalPlayer(
			AndroidVideoWindowImpl window) {
		long playerPtr = createLocalPlayer(this.nativePtr, window);
		if (playerPtr != 0L) {
			return new LinphonePlayerImpl(playerPtr);
		}
		return null;
	}

	private native void addListener(long paramLong,
			LinphoneCoreListener paramLinphoneCoreListener);

	public void addListener(LinphoneCoreListener listener) {
		addListener(this.nativePtr, listener);
	}

	private native void removeListener(long paramLong,
			LinphoneCoreListener paramLinphoneCoreListener);

	public void removeListener(LinphoneCoreListener listener) {
		removeListener(this.nativePtr, listener);
	}

	private native void setRemoteRingbackTone(long paramLong, String paramString);

	public void setRemoteRingbackTone(String file) {
		setRemoteRingbackTone(this.nativePtr, file);
	}

	private native String getRemoteRingbackTone(long paramLong);

	public String getRemoteRingbackTone() {
		return getRemoteRingbackTone(this.nativePtr);
	}

	private native void uploadLogCollection(long paramLong);

	public void uploadLogCollection() {
		uploadLogCollection(this.nativePtr);
	}

	public native void resetLogCollection();

	private native void setPreferredFramerate(long paramLong, float paramFloat);

	public void setPreferredFramerate(float fps) {
		setPreferredFramerate(this.nativePtr, fps);
	}

	private native float getPreferredFramerate(long paramLong);

	public float getPreferredFramerate() {
		return getPreferredFramerate(this.nativePtr);
	}

	private native int setAudioMulticastAddr(long paramLong, String paramString);

	public void setAudioMulticastAddr(String ip) throws LinphoneCoreException {
		if (setAudioMulticastAddr(this.nativePtr, ip) != 0) {
			throw new LinphoneCoreException("bad ip address [" + ip + "]");
		}
	}

	private native int setVideoMulticastAddr(long paramLong, String paramString);

	public void setVideoMulticastAddr(String ip) throws LinphoneCoreException {
		if (setVideoMulticastAddr(this.nativePtr, ip) != 0) {
			throw new LinphoneCoreException("bad ip address [" + ip + "]");
		}
	}

	private native String getAudioMulticastAddr(long paramLong);

	public String getAudioMulticastAddr() {
		return getAudioMulticastAddr(this.nativePtr);
	}

	private native String getVideoMulticastAddr(long paramLong);

	public String getVideoMulticastAddr() {
		return getVideoMulticastAddr(this.nativePtr);
	}

	private native int setAudioMulticastTtl(long paramLong, int paramInt);

	public void setAudioMulticastTtl(int ttl) throws LinphoneCoreException {
		if (setAudioMulticastTtl(this.nativePtr, ttl) != 0) {
			throw new LinphoneCoreException("bad ttl value [" + ttl + "]");
		}
	}

	private native int setVideoMulticastTtl(long paramLong, int paramInt);

	public void setVideoMulticastTtl(int ttl) throws LinphoneCoreException {
		if (setVideoMulticastTtl(this.nativePtr, ttl) != 0) {
			throw new LinphoneCoreException("bad ttl value [" + ttl + "]");
		}
	}

	private native int getAudioMulticastTtl(long paramLong);

	public int getAudioMulticastTtl() {
		return getAudioMulticastTtl(this.nativePtr);
	}

	private native int getVideoMulticastTtl(long paramLong);

	public int getVideoMulticastTtl() {
		return getVideoMulticastTtl(this.nativePtr);
	}

	private native void enableAudioMulticast(long paramLong,
			boolean paramBoolean);

	public void enableAudioMulticast(boolean yesno) {
		enableAudioMulticast(this.nativePtr, yesno);
	}

	private native boolean audioMulticastEnabled(long paramLong);

	public boolean audioMulticastEnabled() {
		return audioMulticastEnabled(this.nativePtr);
	}

	private native void enableVideoMulticast(long paramLong,
			boolean paramBoolean);

	public void enableVideoMulticast(boolean yesno) {
		enableVideoMulticast(this.nativePtr, yesno);
	}

	private native boolean videoMulticastEnabled(long paramLong);

	public boolean videoMulticastEnabled() {
		return videoMulticastEnabled(this.nativePtr);
	}

	private native void enableDnsSrv(long paramLong, boolean paramBoolean);

	public void enableDnsSrv(boolean yesno) {
		enableDnsSrv(this.nativePtr, yesno);
	}

	private native boolean dnsSrvEnabled(long paramLong);

	public boolean dnsSrvEnabled() {
		return dnsSrvEnabled(this.nativePtr);
	}

	@Override
	public void startEchoCalibration(
			EcCalibrationListener paramLinphoneCoreListener)
			throws LinphoneCoreException {
		startEchoCalibration(this.nativePtr, paramLinphoneCoreListener);

	}

}
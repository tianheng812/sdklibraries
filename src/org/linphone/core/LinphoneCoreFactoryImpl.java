package org.linphone.core;

import android.util.Log;
import java.io.File;
import java.io.IOException;
import org.linphone.mediastream.MediastreamerAndroidContext;
import org.linphone.mediastream.Version;

public class LinphoneCoreFactoryImpl extends LinphoneCoreFactory {
	static {
		String str = "armeabi";
		if (Version.isX86()) {
			str = "x86";
			loadOptionalLibrary("avutil-linphone-x86");
			loadOptionalLibrary("swscale-linphone-x86");
			loadOptionalLibrary("avcodec-linphone-x86");
		}else if (Version.isArmv7()){
			str = "armeabi-v7a";
			loadOptionalLibrary("avutil-linphone-arm");
			loadOptionalLibrary("swscale-linphone-arm");
			loadOptionalLibrary("avcodec-linphone-arm");
		}
		
		
		System.loadLibrary("linphone-" + str);
		
		Version.dumpCapabilities();

	}

	private native void _setLogHandler(Object paramObject);

	public static boolean isArmv7() {
		return System.getProperty("os.arch").contains("armv7");
	}

	private static boolean loadOptionalLibrary(String paramString) {
		try {
			System.loadLibrary(paramString);
			return true;
		} catch (Throwable e) {
			Log.w("Unable to load optional library lib", paramString);
			e.printStackTrace();
		}
		return false;
	}

	public LinphoneAuthInfo createAuthInfo(String paramString1,
			String paramString2, String paramString3, String paramString4) {
		return new LinphoneAuthInfoImpl(paramString1, paramString2,
				paramString3, paramString4);
	}

	public LinphoneAuthInfo createAuthInfo(String paramString1,
			String paramString2, String paramString3, String paramString4,
			String paramString5, String paramString6) {
		return new LinphoneAuthInfoImpl(paramString1, paramString2,
				paramString3, paramString4, paramString5, paramString6);
	}

	public LinphoneAddress createLinphoneAddress(String paramString)
			throws LinphoneCoreException {
		return new LinphoneAddressImpl(paramString);
	}

	public LinphoneAddress createLinphoneAddress(String paramString1,
			String paramString2, String paramString3) {
		return new LinphoneAddressImpl(paramString1, paramString2, paramString3);
	}

	public LinphoneContent createLinphoneContent(String paramString1,
			String paramString2, String paramString3) {
		return new LinphoneContentImpl(paramString1, paramString2,
				paramString3.getBytes(), null);
	}

	public LinphoneContent createLinphoneContent(String paramString1,
			String paramString2, byte[] paramArrayOfByte, String paramString3) {
		return new LinphoneContentImpl(paramString1, paramString2,
				paramArrayOfByte, paramString3);
	}

	public LinphoneCore createLinphoneCore(
			LinphoneCoreListener paramLinphoneCoreListener, Object paramObject)
			throws LinphoneCoreException {
		try {
			MediastreamerAndroidContext.setContext(paramObject);
			LinphoneCoreImpl localLinphoneCoreImpl = new LinphoneCoreImpl(
					paramLinphoneCoreListener);
			return localLinphoneCoreImpl;
		} catch (IOException e) {
			e.printStackTrace();
			throw new LinphoneCoreException("Cannot create LinphoneCore",e);
		}
	}

	@Override
	public LinphoneCore createLinphoneCore(LinphoneCoreListener listener,
			String userConfig, String factoryConfig, Object userdata, Object context)
			throws LinphoneCoreException {
		try {
			MediastreamerAndroidContext.setContext(context);
			File user = userConfig == null ? null : new File(userConfig);
			File factory = factoryConfig == null ? null : new File(factoryConfig);
			LinphoneCore lc = new LinphoneCoreImpl(listener, user, factory, userdata);
			if(context!=null) lc.setContext(context);
			return lc;
		} catch (IOException e) {
			e.printStackTrace();
			throw new LinphoneCoreException("Cannot create LinphoneCore",e);
		}
	}

	public LinphoneFriend createLinphoneFriend() {
		return createLinphoneFriend(null);
	}

	public LinphoneFriend createLinphoneFriend(String paramString) {
		return new LinphoneFriendImpl(paramString);
	}

	public LpConfig createLpConfig(String paramString) {
		return new LpConfigImpl(paramString);
	}

	public PresenceActivity createPresenceActivity(
			PresenceActivityType paramPresenceActivityType, String paramString) {
		return new PresenceActivityImpl(paramPresenceActivityType, paramString);
	}

	public PresenceModel createPresenceModel() {
		return new PresenceModelImpl();
	}

	public PresenceModel createPresenceModel(
			PresenceActivityType paramPresenceActivityType, String paramString) {
		return new PresenceModelImpl(paramPresenceActivityType, paramString);
	}

	public PresenceModel createPresenceModel(
			PresenceActivityType paramPresenceActivityType,
			String paramString1, String paramString2, String paramString3) {
		return new PresenceModelImpl(paramPresenceActivityType, paramString1,
				paramString2, paramString3);
	}

	public PresenceService createPresenceService(String paramString1,
			PresenceBasicStatus paramPresenceBasicStatus, String paramString2) {
		return new PresenceServiceImpl(paramString1, paramPresenceBasicStatus,
				paramString2);
	}

	public native void setDebugMode(boolean paramBoolean, String paramString);

	public void setLogHandler(LinphoneLogHandler paramLinphoneLogHandler) {
		_setLogHandler(paramLinphoneLogHandler);
	}


}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneCoreFactoryImpl JD-Core Version:
 * 0.6.2
 */
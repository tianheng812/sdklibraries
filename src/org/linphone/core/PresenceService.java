package org.linphone.core;

public abstract interface PresenceService
{
  public abstract int addNote(PresenceNote paramPresenceNote);

  public abstract int clearNotes();

  public abstract PresenceBasicStatus getBasicStatus();

  public abstract String getContact();

  public abstract String getId();

  public abstract long getNativePtr();

  public abstract long getNbNotes();

  public abstract PresenceNote getNthNote(long paramLong);

  public abstract int setBasicStatus(PresenceBasicStatus paramPresenceBasicStatus);

  public abstract int setContact(String paramString);

  public abstract int setId(String paramString);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PresenceService
 * JD-Core Version:    0.6.2
 */
package org.linphone.core;

public abstract interface PresenceActivity
{
  public abstract String getDescription();

  public abstract long getNativePtr();

  public abstract PresenceActivityType getType();

  public abstract int setDescription(String paramString);

  public abstract int setType(PresenceActivityType paramPresenceActivityType);

  public abstract String toString();
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PresenceActivity
 * JD-Core Version:    0.6.2
 */
package org.linphone.core;

public abstract interface LpConfig
{
  public abstract boolean getBool(String paramString1, String paramString2, boolean paramBoolean);

  public abstract float getFloat(String paramString1, String paramString2, float paramFloat);

  public abstract int getInt(String paramString1, String paramString2, int paramInt);

  public abstract int[] getIntRange(String paramString1, String paramString2, int paramInt1, int paramInt2);

  public abstract String getString(String paramString1, String paramString2, String paramString3);

  public abstract void setBool(String paramString1, String paramString2, boolean paramBoolean);

  public abstract void setFloat(String paramString1, String paramString2, float paramFloat);

  public abstract void setInt(String paramString1, String paramString2, int paramInt);

  public abstract void setIntRange(String paramString1, String paramString2, int paramInt1, int paramInt2);

  public abstract void setString(String paramString1, String paramString2, String paramString3);

  public abstract void sync();
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LpConfig
 * JD-Core Version:    0.6.2
 */
package org.linphone.core;

public class PresenceModelImpl
  implements PresenceModel
{
  private long mNativePtr;

  protected PresenceModelImpl()
  {
    this.mNativePtr = newPresenceModelImpl();
  }

  protected PresenceModelImpl(long paramLong)
  {
    this.mNativePtr = paramLong;
  }

  protected PresenceModelImpl(PresenceActivityType paramPresenceActivityType, String paramString)
  {
    this.mNativePtr = newPresenceModelImpl(paramPresenceActivityType.toInt(), paramString);
  }

  protected PresenceModelImpl(PresenceActivityType paramPresenceActivityType, String paramString1, String paramString2, String paramString3)
  {
    this.mNativePtr = newPresenceModelImpl(paramPresenceActivityType.toInt(), paramString1, paramString2, paramString3);
  }

  private native int addActivity(long paramLong1, long paramLong2);

  private native int addNote(long paramLong, String paramString1, String paramString2);

  private native int addPerson(long paramLong1, long paramLong2);

  private native int addService(long paramLong1, long paramLong2);

  private native int clearActivities(long paramLong);

  private native int clearNotes(long paramLong);

  private native int clearPersons(long paramLong);

  private native int clearServices(long paramLong);

  private native Object getActivity(long paramLong);

  private native int getBasicStatus(long paramLong);

  private native String getContact(long paramLong);

  private native long getNbActivities(long paramLong);

  private native long getNbPersons(long paramLong);

  private native long getNbServices(long paramLong);

  private native Object getNote(long paramLong, String paramString);

  private native Object getNthActivity(long paramLong1, long paramLong2);

  private native Object getNthPerson(long paramLong1, long paramLong2);

  private native Object getNthService(long paramLong1, long paramLong2);

  private native long getTimestamp(long paramLong);

  private native long newPresenceModelImpl();

  private native long newPresenceModelImpl(int paramInt, String paramString);

  private native long newPresenceModelImpl(int paramInt, String paramString1, String paramString2, String paramString3);

  private native int setActivity(long paramLong, int paramInt, String paramString);

  private native int setBasicStatus(long paramLong, int paramInt);

  private native void setContact(long paramLong, String paramString);

  private native void unref(long paramLong);

  public int addActivity(PresenceActivity paramPresenceActivity)
  {
    return addActivity(this.mNativePtr, paramPresenceActivity.getNativePtr());
  }

  public int addNote(String paramString1, String paramString2)
  {
    return addNote(this.mNativePtr, paramString1, paramString2);
  }

  public int addPerson(PresencePerson paramPresencePerson)
  {
    return addPerson(this.mNativePtr, paramPresencePerson.getNativePtr());
  }

  public int addService(PresenceService paramPresenceService)
  {
    return addService(this.mNativePtr, paramPresenceService.getNativePtr());
  }

  public int clearActivities()
  {
    return clearActivities(this.mNativePtr);
  }

  public int clearNotes()
  {
    return clearNotes(this.mNativePtr);
  }

  public int clearPersons()
  {
    return clearPersons(this.mNativePtr);
  }

  public int clearServices()
  {
    return clearServices(this.mNativePtr);
  }

  protected void finalize()
  {
    unref(this.mNativePtr);
  }

  public PresenceActivity getActivity()
  {
    return (PresenceActivity)getActivity(this.mNativePtr);
  }

  public PresenceBasicStatus getBasicStatus()
  {
    return PresenceBasicStatus.fromInt(getBasicStatus(this.mNativePtr));
  }

  public String getContact()
  {
    return getContact(this.mNativePtr);
  }

  public long getNativePtr()
  {
    return this.mNativePtr;
  }

  public long getNbActivities()
  {
    return getNbActivities(this.mNativePtr);
  }

  public long getNbPersons()
  {
    return getNbPersons(this.mNativePtr);
  }

  public long getNbServices()
  {
    return getNbServices(this.mNativePtr);
  }

  public PresenceNote getNote(String paramString)
  {
    return (PresenceNote)getNote(this.mNativePtr, paramString);
  }

  public PresenceActivity getNthActivity(long paramLong)
  {
    return (PresenceActivity)getNthActivity(this.mNativePtr, paramLong);
  }

  public PresencePerson getNthPerson(long paramLong)
  {
    return (PresencePerson)getNthPerson(this.mNativePtr, paramLong);
  }

  public PresenceService getNthService(long paramLong)
  {
    return (PresenceService)getNthService(this.mNativePtr, paramLong);
  }

  public long getTimestamp()
  {
    return getTimestamp(this.mNativePtr);
  }

  public int setActivity(PresenceActivityType paramPresenceActivityType, String paramString)
  {
    return setActivity(this.mNativePtr, paramPresenceActivityType.toInt(), paramString);
  }

  public int setBasicStatus(PresenceBasicStatus paramPresenceBasicStatus)
  {
    return setBasicStatus(this.mNativePtr, paramPresenceBasicStatus.toInt());
  }

  public void setContact(String paramString)
  {
    setContact(this.mNativePtr, paramString);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PresenceModelImpl
 * JD-Core Version:    0.6.2
 */
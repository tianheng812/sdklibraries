package org.linphone.core;

class LpConfigImpl
  implements LpConfig
{
  private final long nativePtr;
  boolean ownPtr = false;

  public LpConfigImpl(long paramLong)
  {
    this.nativePtr = paramLong;
  }

  public LpConfigImpl(String paramString)
  {
    this.nativePtr = newLpConfigImpl(paramString);
    this.ownPtr = true;
  }

  private native void delete(long paramLong);

  private native boolean getBool(long paramLong, String paramString1, String paramString2, boolean paramBoolean);

  private native float getFloat(long paramLong, String paramString1, String paramString2, float paramFloat);

  private native int getInt(long paramLong, String paramString1, String paramString2, int paramInt);

  private native int[] getIntRange(long paramLong, String paramString1, String paramString2, int paramInt1, int paramInt2);

  private native String getString(long paramLong, String paramString1, String paramString2, String paramString3);

  private native long newLpConfigImpl(String paramString);

  private native void setBool(long paramLong, String paramString1, String paramString2, boolean paramBoolean);

  private native void setFloat(long paramLong, String paramString1, String paramString2, float paramFloat);

  private native void setInt(long paramLong, String paramString1, String paramString2, int paramInt);

  private native void setIntRange(long paramLong, String paramString1, String paramString2, int paramInt1, int paramInt2);

  private native void setString(long paramLong, String paramString1, String paramString2, String paramString3);

  private native void sync(long paramLong);

  protected void finalize()
    throws Throwable
  {
    if (this.ownPtr)
      delete(this.nativePtr);
  }

  public boolean getBool(String paramString1, String paramString2, boolean paramBoolean)
  {
    return getBool(this.nativePtr, paramString1, paramString2, paramBoolean);
  }

  public float getFloat(String paramString1, String paramString2, float paramFloat)
  {
    return getFloat(this.nativePtr, paramString1, paramString2, paramFloat);
  }

  public int getInt(String paramString1, String paramString2, int paramInt)
  {
    return getInt(this.nativePtr, paramString1, paramString2, paramInt);
  }

  public int[] getIntRange(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    return getIntRange(this.nativePtr, paramString1, paramString2, paramInt1, paramInt2);
  }

  public String getString(String paramString1, String paramString2, String paramString3)
  {
    return getString(this.nativePtr, paramString1, paramString2, paramString3);
  }

  public void setBool(String paramString1, String paramString2, boolean paramBoolean)
  {
    setBool(this.nativePtr, paramString1, paramString2, paramBoolean);
  }

  public void setFloat(String paramString1, String paramString2, float paramFloat)
  {
    setFloat(this.nativePtr, paramString1, paramString2, paramFloat);
  }

  public void setInt(String paramString1, String paramString2, int paramInt)
  {
    setInt(this.nativePtr, paramString1, paramString2, paramInt);
  }

  public void setIntRange(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    setIntRange(this.nativePtr, paramString1, paramString2, paramInt1, paramInt2);
  }

  public void setString(String paramString1, String paramString2, String paramString3)
  {
    setString(this.nativePtr, paramString1, paramString2, paramString3);
  }

  public void sync()
  {
    sync(this.nativePtr);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LpConfigImpl
 * JD-Core Version:    0.6.2
 */
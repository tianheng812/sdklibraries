package org.linphone.core;

public abstract interface LinphoneContent
{
  public abstract byte[] getData();

  public abstract String getDataAsString();

  public abstract String getEncoding();

  public abstract int getExpectedSize();

  public abstract String getName();

  public abstract int getRealSize();

  public abstract String getSubtype();

  public abstract String getType();

  public abstract void setData(byte[] paramArrayOfByte);

  public abstract void setEncoding(String paramString);

  public abstract void setExpectedSize(int paramInt);

  public abstract void setName(String paramString);

  public abstract void setStringData(String paramString);

  public abstract void setSubtype(String paramString);

  public abstract void setType(String paramString);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneContent
 * JD-Core Version:    0.6.2
 */
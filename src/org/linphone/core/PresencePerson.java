package org.linphone.core;

public abstract interface PresencePerson
{
  public abstract int addActivitiesNote(PresenceNote paramPresenceNote);

  public abstract int addActivity(PresenceActivity paramPresenceActivity);

  public abstract int addNote(PresenceNote paramPresenceNote);

  public abstract int clearActivitesNotes();

  public abstract int clearActivities();

  public abstract int clearNotes();

  public abstract String getId();

  public abstract long getNativePtr();

  public abstract long getNbActivities();

  public abstract long getNbActivitiesNotes();

  public abstract long getNbNotes();

  public abstract PresenceNote getNthActivitiesNote(long paramLong);

  public abstract PresenceActivity getNthActivity(long paramLong);

  public abstract PresenceNote getNthNote(long paramLong);

  public abstract int setId(String paramString);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PresencePerson
 * JD-Core Version:    0.6.2
 */
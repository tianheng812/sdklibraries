package org.linphone.core;

public class TunnelConfig {
	private int delay = 1000;
	private String host = null;
	private int port = 443;
	private int remoteUdpMirrorPort = 12345;

	public int getDelay() {
		return this.delay;
	}

	public String getHost() {
		return this.host;
	}

	public int getPort() {
		return this.port;
	}

	public int getRemoteUdpMirrorPort() {
		return this.remoteUdpMirrorPort;
	}

	public void setDelay(int paramInt) {
		this.delay = paramInt;
	}

	public void setHost(String paramString) {
		this.host = paramString;
	}

	public void setPort(int paramInt) {
		this.port = paramInt;
	}

	public void setRemoteUdpMirrorPort(int paramInt) {
		this.remoteUdpMirrorPort = paramInt;
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.TunnelConfig JD-Core Version: 0.6.2
 */
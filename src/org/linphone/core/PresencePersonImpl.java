package org.linphone.core;

public class PresencePersonImpl
  implements PresencePerson
{
  private long mNativePtr;

  protected PresencePersonImpl(long paramLong)
  {
    this.mNativePtr = paramLong;
  }

  protected PresencePersonImpl(String paramString)
  {
    this.mNativePtr = newPresencePersonImpl(paramString);
  }

  private native int addActivitiesNote(long paramLong1, long paramLong2);

  private native int addActivity(long paramLong1, long paramLong2);

  private native int addNote(long paramLong1, long paramLong2);

  private native int clearActivitesNotes(long paramLong);

  private native int clearActivities(long paramLong);

  private native int clearNotes(long paramLong);

  private native String getId(long paramLong);

  private native long getNbActivities(long paramLong);

  private native long getNbActivitiesNotes(long paramLong);

  private native long getNbNotes(long paramLong);

  private native Object getNthActivitiesNote(long paramLong1, long paramLong2);

  private native Object getNthActivity(long paramLong1, long paramLong2);

  private native Object getNthNote(long paramLong1, long paramLong2);

  private native long newPresencePersonImpl(String paramString);

  private native int setId(long paramLong, String paramString);

  private native void unref(long paramLong);

  public int addActivitiesNote(PresenceNote paramPresenceNote)
  {
    return addActivitiesNote(this.mNativePtr, paramPresenceNote.getNativePtr());
  }

  public int addActivity(PresenceActivity paramPresenceActivity)
  {
    return addActivity(this.mNativePtr, paramPresenceActivity.getNativePtr());
  }

  public int addNote(PresenceNote paramPresenceNote)
  {
    return addNote(this.mNativePtr, paramPresenceNote.getNativePtr());
  }

  public int clearActivitesNotes()
  {
    return clearActivitesNotes(this.mNativePtr);
  }

  public int clearActivities()
  {
    return clearActivities(this.mNativePtr);
  }

  public int clearNotes()
  {
    return clearNotes(this.mNativePtr);
  }

  protected void finalize()
  {
    unref(this.mNativePtr);
  }

  public String getId()
  {
    return getId(this.mNativePtr);
  }

  public long getNativePtr()
  {
    return this.mNativePtr;
  }

  public long getNbActivities()
  {
    return getNbActivities(this.mNativePtr);
  }

  public long getNbActivitiesNotes()
  {
    return getNbActivitiesNotes(this.mNativePtr);
  }

  public long getNbNotes()
  {
    return getNbNotes(this.mNativePtr);
  }

  public PresenceNote getNthActivitiesNote(long paramLong)
  {
    return (PresenceNote)getNthActivitiesNote(this.mNativePtr, paramLong);
  }

  public PresenceActivity getNthActivity(long paramLong)
  {
    return (PresenceActivity)getNthActivity(this.mNativePtr, paramLong);
  }

  public PresenceNote getNthNote(long paramLong)
  {
    return (PresenceNote)getNthNote(this.mNativePtr, paramLong);
  }

  public int setId(String paramString)
  {
    return setId(this.mNativePtr, paramString);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PresencePersonImpl
 * JD-Core Version:    0.6.2
 */
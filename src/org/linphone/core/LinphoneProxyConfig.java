package org.linphone.core;

public abstract interface LinphoneProxyConfig
{
  public abstract boolean avpfEnabled();

  public abstract void done();

  public abstract LinphoneProxyConfig edit();

  public abstract void enableAvpf(boolean paramBoolean);

  public abstract void enablePublish(boolean paramBoolean);

  public abstract void enableQualityReporting(boolean paramBoolean);

  public abstract LinphoneProxyConfig enableRegister(boolean paramBoolean);

  public abstract int getAvpfRRInterval();

  public abstract String getContactParameters();

  public abstract String getContactUriParameters();

  public abstract boolean getDialEscapePlus();

  public abstract String getDialPrefix();

  public abstract String getDomain();

  public abstract Reason getError();

  public abstract ErrorInfo getErrorInfo();

  public abstract int getExpires();

  public abstract String getIdentity();

  public abstract int getPrivacy();

  public abstract String getProxy();

  public abstract int getPublishExpires();

  public abstract String getQualityReportingCollector();

  public abstract int getQualityReportingInterval();

  public abstract String getRealm();

  public abstract String getRoute();

  public abstract LinphoneCore.RegistrationState getState();

  public abstract Object getUserData();

  public abstract boolean isRegistered();

  public abstract int lookupCCCFromE164(String paramString);

  public abstract int lookupCCCFromIso(String paramString);

  public abstract String normalizePhoneNumber(String paramString);

  public abstract boolean publishEnabled();

  public abstract boolean qualityReportingEnabled();

  public abstract boolean registerEnabled();

  public abstract void setAvpfRRInterval(int paramInt);

  public abstract void setContactParameters(String paramString);

  public abstract void setContactUriParameters(String paramString);

  public abstract void setDialEscapePlus(boolean paramBoolean);

  public abstract void setDialPrefix(String paramString);

  public abstract void setExpires(int paramInt);

  public abstract void setIdentity(String paramString)
    throws LinphoneCoreException;

  public abstract void setPrivacy(int paramInt);

  public abstract void setProxy(String paramString)
    throws LinphoneCoreException;

  public abstract void setPublishExpires(int paramInt);

  public abstract void setQualityReportingCollector(String paramString);

  public abstract void setQualityReportingInterval(int paramInt);

  public abstract void setRealm(String paramString);

  public abstract void setRoute(String paramString)
    throws LinphoneCoreException;

  public abstract void setUserData(Object paramObject);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneProxyConfig
 * JD-Core Version:    0.6.2
 */
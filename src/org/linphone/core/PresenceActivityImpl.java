package org.linphone.core;

public class PresenceActivityImpl
  implements PresenceActivity
{
  private long mNativePtr;

  protected PresenceActivityImpl(long paramLong)
  {
    this.mNativePtr = paramLong;
  }

  protected PresenceActivityImpl(PresenceActivityType paramPresenceActivityType, String paramString)
  {
    this.mNativePtr = newPresenceActivityImpl(paramPresenceActivityType.toInt(), paramString);
  }

  private native String getDescription(long paramLong);

  private native int getType(long paramLong);

  private native long newPresenceActivityImpl(int paramInt, String paramString);

  private native int setDescription(long paramLong, String paramString);

  private native int setType(long paramLong, int paramInt);

  private native String toString(long paramLong);

  private native void unref(long paramLong);

  protected void finalize()
  {
    unref(this.mNativePtr);
  }

  public String getDescription()
  {
    return getDescription(this.mNativePtr);
  }

  public long getNativePtr()
  {
    return this.mNativePtr;
  }

  public PresenceActivityType getType()
  {
    return PresenceActivityType.fromInt(getType(this.mNativePtr));
  }

  public int setDescription(String paramString)
  {
    return setDescription(this.mNativePtr, paramString);
  }

  public int setType(PresenceActivityType paramPresenceActivityType)
  {
    return setType(this.mNativePtr, paramPresenceActivityType.toInt());
  }

  public String toString()
  {
    return toString(this.mNativePtr);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PresenceActivityImpl
 * JD-Core Version:    0.6.2
 */
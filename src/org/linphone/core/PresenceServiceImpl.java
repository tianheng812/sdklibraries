package org.linphone.core;

public class PresenceServiceImpl
  implements PresenceService
{
  private long mNativePtr;

  protected PresenceServiceImpl(long paramLong)
  {
    this.mNativePtr = paramLong;
  }

  protected PresenceServiceImpl(String paramString1, PresenceBasicStatus paramPresenceBasicStatus, String paramString2)
  {
    this.mNativePtr = newPresenceServiceImpl(paramString1, paramPresenceBasicStatus.toInt(), paramString2);
  }

  private native int addNote(long paramLong1, long paramLong2);

  private native int clearNotes(long paramLong);

  private native int getBasicStatus(long paramLong);

  private native String getContact(long paramLong);

  private native String getId(long paramLong);

  private native long getNbNotes(long paramLong);

  private native Object getNthNote(long paramLong1, long paramLong2);

  private native long newPresenceServiceImpl(String paramString1, int paramInt, String paramString2);

  private native int setBasicStatus(long paramLong, int paramInt);

  private native int setContact(long paramLong, String paramString);

  private native int setId(long paramLong, String paramString);

  private native void unref(long paramLong);

  public int addNote(PresenceNote paramPresenceNote)
  {
    return addNote(this.mNativePtr, paramPresenceNote.getNativePtr());
  }

  public int clearNotes()
  {
    return clearNotes(this.mNativePtr);
  }

  protected void finalize()
  {
    unref(this.mNativePtr);
  }

  public PresenceBasicStatus getBasicStatus()
  {
    return PresenceBasicStatus.fromInt(getBasicStatus(this.mNativePtr));
  }

  public String getContact()
  {
    return getContact(this.mNativePtr);
  }

  public String getId()
  {
    return getId(this.mNativePtr);
  }

  public long getNativePtr()
  {
    return this.mNativePtr;
  }

  public long getNbNotes()
  {
    return getNbNotes(this.mNativePtr);
  }

  public PresenceNote getNthNote(long paramLong)
  {
    return (PresenceNote)getNthNote(this.mNativePtr, paramLong);
  }

  public int setBasicStatus(PresenceBasicStatus paramPresenceBasicStatus)
  {
    return setBasicStatus(this.mNativePtr, paramPresenceBasicStatus.toInt());
  }

  public int setContact(String paramString)
  {
    return setContact(this.mNativePtr, paramString);
  }

  public int setId(String paramString)
  {
    return setId(this.mNativePtr, paramString);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PresenceServiceImpl
 * JD-Core Version:    0.6.2
 */
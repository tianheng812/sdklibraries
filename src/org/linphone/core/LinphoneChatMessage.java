package org.linphone.core;
 import java.util.Vector; 
  
 public abstract interface LinphoneChatMessage 
 { 
   public abstract String getText(); 
    
   public abstract LinphoneAddress getPeerAddress(); 
    
   public abstract LinphoneAddress getFrom(); 
    
   public abstract LinphoneAddress getTo(); 
    
   public abstract String getExternalBodyUrl(); 
    
   public abstract void setExternalBodyUrl(String paramString); 
    
   public abstract void addCustomHeader(String paramString1, String paramString2); 
    
   public abstract String getCustomHeader(String paramString); 
    
   public abstract long getTime(); 
    
   public abstract State getStatus(); 
    
   public abstract boolean isRead(); 
    
   public abstract boolean isOutgoing(); 
    
   public abstract void store(); 
    
   public abstract int getStorageId(); 
    
   public abstract Reason getReason(); 
    
   public abstract ErrorInfo getErrorInfo(); 
    
   public abstract LinphoneContent getFileTransferInformation(); 
    
   public abstract void setAppData(String paramString); 
    
   public abstract String getAppData(); 
    
   public static class State 
   { 
     private static Vector<State> values = new Vector<State>(); 
     private final int mValue; 
     private final String mStringValue; 
      
     public final int value() 
     { 
       return this.mValue; 
     } 
      
     public static final State Idle = new State(0, "Idle"); 
     public static final State InProgress = new State(1, "InProgress"); 
     public static final State Delivered = new State(2, "Delivered"); 
     public static final State NotDelivered = new State(3, "NotDelivered"); 
     public static final State FileTransferError = new State(4, "FileTransferError"); 
     public static final State FileTransferDone = new State(5, "FileTransferDone"); 
      
     private State(int value, String stringValue) 
     { 
       this.mValue = value; 
       values.addElement(this); 
       this.mStringValue = stringValue; 
     } 
      
     public static State fromInt(int value) 
     { 
       for (int i = 0; i < values.size(); i++) 
       { 
         State state = (State)values.elementAt(i); 
         if (state.mValue == value) { 
           return state; 
         } 
       } 
       throw new RuntimeException("state not found [" + value + "]"); 
     } 
      
     public String toString() 
     { 
       return this.mStringValue; 
     } 
      
     public int toInt() 
     { 
       return this.mValue; 
     } 
   } 
    
   public static abstract interface LinphoneChatMessageListener 
   { 
     public abstract void onLinphoneChatMessageStateChanged(LinphoneChatMessage paramLinphoneChatMessage, State paramState); 
      
     public abstract void onLinphoneChatMessageFileTransferReceived(LinphoneChatMessage paramLinphoneChatMessage, LinphoneContent paramLinphoneContent, LinphoneBuffer paramLinphoneBuffer); 
      
     public abstract void onLinphoneChatMessageFileTransferSent(LinphoneChatMessage paramLinphoneChatMessage, LinphoneContent paramLinphoneContent, int paramInt1, int paramInt2, LinphoneBuffer paramLinphoneBuffer); 
      
     public abstract void onLinphoneChatMessageFileTransferProgressChanged(LinphoneChatMessage paramLinphoneChatMessage, LinphoneContent paramLinphoneContent, int paramInt1, int paramInt2); 
   } 
    
   @Deprecated 
   public static abstract interface StateListener 
   { 
     public abstract void onLinphoneChatMessageStateChanged(LinphoneChatMessage paramLinphoneChatMessage, State paramState); 
   } 
 } 

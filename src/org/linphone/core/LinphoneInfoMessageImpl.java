package org.linphone.core;

public class LinphoneInfoMessageImpl  implements LinphoneInfoMessage
{
  private LinphoneContent mContent;
  protected long nativePtr;

  public LinphoneInfoMessageImpl(long paramLong)
  {
    this.nativePtr = paramLong;
    this.mContent = ((LinphoneContent)getContent(this.nativePtr));
  }

  private native void addHeader(long paramLong, String paramString1, String paramString2);

  private native void delete(long paramLong);

  private native Object getContent(long paramLong);

  private native String getHeader(long paramLong, String paramString);

  private native void setContent(long paramLong, String paramString1, String paramString2, String paramString3);

  public void addHeader(String paramString1, String paramString2)
  {
    addHeader(this.nativePtr, paramString1, paramString2);
  }

  protected void finalize()
  {
    delete(this.nativePtr);
  }

  public LinphoneContent getContent()
  {
    return this.mContent;
  }

  public String getHeader(String paramString)
  {
    return getHeader(this.nativePtr, paramString);
  }

  public void setContent(LinphoneContent paramLinphoneContent)
  {
    this.mContent = paramLinphoneContent;
    setContent(this.nativePtr, this.mContent.getType(), this.mContent.getSubtype(), this.mContent.getDataAsString());
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneInfoMessageImpl
 * JD-Core Version:    0.6.2
 */
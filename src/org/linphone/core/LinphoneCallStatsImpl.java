package org.linphone.core;

class LinphoneCallStatsImpl
  implements LinphoneCallStats
{
  private float downloadBandwidth;
  private int iceState;
  private float jitterBufferSize;
  private long latePacketsCumulativeNumber;
  private float localLateRate;
  private float localLossRate;
  private int mediaType;
  private long nativePtr;
  private float receiverInterarrivalJitter;
  private float receiverLossRate;
  private float roundTripDelay;
  private float senderInterarrivalJitter;
  private float senderLossRate;
  private float uploadBandwidth;

  protected LinphoneCallStatsImpl(long paramLong1, long paramLong2)
  {
    this.nativePtr = paramLong2;
    this.mediaType = getMediaType(paramLong2);
    this.iceState = getIceState(paramLong2);
    this.downloadBandwidth = getDownloadBandwidth(paramLong2);
    this.uploadBandwidth = getUploadBandwidth(paramLong2);
    this.senderLossRate = getSenderLossRate(paramLong2);
    this.receiverLossRate = getReceiverLossRate(paramLong2);
    this.senderInterarrivalJitter = getSenderInterarrivalJitter(paramLong2, paramLong1);
    this.receiverInterarrivalJitter = getReceiverInterarrivalJitter(paramLong2, paramLong1);
    this.roundTripDelay = getRoundTripDelay(paramLong2);
    this.latePacketsCumulativeNumber = getLatePacketsCumulativeNumber(paramLong2, paramLong1);
    this.jitterBufferSize = getJitterBufferSize(paramLong2);
  }

  private native float getDownloadBandwidth(long paramLong);

  private native int getIceState(long paramLong);

  private native float getJitterBufferSize(long paramLong);

  private native long getLatePacketsCumulativeNumber(long paramLong1, long paramLong2);

  private native float getLocalLateRate(long paramLong);

  private native float getLocalLossRate(long paramLong);

  private native int getMediaType(long paramLong);

  private native float getReceiverInterarrivalJitter(long paramLong1, long paramLong2);

  private native float getReceiverLossRate(long paramLong);

  private native float getRoundTripDelay(long paramLong);

  private native float getSenderInterarrivalJitter(long paramLong1, long paramLong2);

  private native float getSenderLossRate(long paramLong);

  private native float getUploadBandwidth(long paramLong);

  private native void updateStats(long paramLong, int paramInt);

  public float getDownloadBandwidth()
  {
    return this.downloadBandwidth;
  }

  public LinphoneCallStats.IceState getIceState()
  {
    return LinphoneCallStats.IceState.fromInt(this.iceState);
  }

  public float getJitterBufferSize()
  {
    return this.jitterBufferSize;
  }

  public long getLatePacketsCumulativeNumber()
  {
    return this.latePacketsCumulativeNumber;
  }

  public float getLocalLateRate()
  {
    return this.localLateRate;
  }

  public float getLocalLossRate()
  {
    return this.localLossRate;
  }

  public LinphoneCallStats.MediaType getMediaType()
  {
    return LinphoneCallStats.MediaType.fromInt(this.mediaType);
  }

  public float getReceiverInterarrivalJitter()
  {
    return this.receiverInterarrivalJitter;
  }

  public float getReceiverLossRate()
  {
    return this.receiverLossRate;
  }

  public float getRoundTripDelay()
  {
    return this.roundTripDelay;
  }

  public float getSenderInterarrivalJitter()
  {
    return this.senderInterarrivalJitter;
  }

  public float getSenderLossRate()
  {
    return this.senderLossRate;
  }

  public float getUploadBandwidth()
  {
    return this.uploadBandwidth;
  }

  protected void updateRealTimeStats(LinphoneCall paramLinphoneCall)
  {
    updateStats(((LinphoneCallImpl)paramLinphoneCall).nativePtr, this.mediaType);
    this.localLossRate = getLocalLossRate(this.nativePtr);
    this.localLateRate = getLocalLateRate(this.nativePtr);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneCallStatsImpl
 * JD-Core Version:    0.6.2
 */
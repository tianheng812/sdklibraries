package org.linphone.core;

public abstract class LinphoneCoreFactory {
	private static String factoryName = "org.linphone.core.LinphoneCoreFactoryImpl";
	static LinphoneCoreFactory theLinphoneCoreFactory;

	public static final LinphoneCoreFactory instance() {
		if (theLinphoneCoreFactory == null)
			try {
				theLinphoneCoreFactory = (LinphoneCoreFactory) Class.forName(factoryName).newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return theLinphoneCoreFactory;

	}

	public static void setFactoryClassName(String paramString) {
		factoryName = paramString;
	}

	public abstract LinphoneAuthInfo createAuthInfo(String paramString1,
			String paramString2, String paramString3, String paramString4);

	public abstract LinphoneAuthInfo createAuthInfo(String paramString1,
			String paramString2, String paramString3, String paramString4,
			String paramString5, String paramString6);

	public abstract LinphoneAddress createLinphoneAddress(String paramString)
			throws LinphoneCoreException;

	public abstract LinphoneAddress createLinphoneAddress(String paramString1,
			String paramString2, String paramString3);

	public abstract LinphoneContent createLinphoneContent(String paramString1,
			String paramString2, String paramString3);

	public abstract LinphoneContent createLinphoneContent(String paramString1,
			String paramString2, byte[] paramArrayOfByte, String paramString3);

	public abstract LinphoneCore createLinphoneCore(
			LinphoneCoreListener paramLinphoneCoreListener, Object paramObject)
			throws LinphoneCoreException;

	public abstract LinphoneCore createLinphoneCore(
			LinphoneCoreListener paramLinphoneCoreListener,
			String paramString1, String paramString2, Object paramObject1,
			Object paramObject2) throws LinphoneCoreException;

	public abstract LinphoneFriend createLinphoneFriend();

	public abstract LinphoneFriend createLinphoneFriend(String paramString);

	public abstract LpConfig createLpConfig(String paramString);

	public abstract PresenceActivity createPresenceActivity(
			PresenceActivityType paramPresenceActivityType, String paramString);

	public abstract PresenceModel createPresenceModel();

	public abstract PresenceModel createPresenceModel(
			PresenceActivityType paramPresenceActivityType, String paramString);

	public abstract PresenceModel createPresenceModel(
			PresenceActivityType paramPresenceActivityType,
			String paramString1, String paramString2, String paramString3);

	public abstract PresenceService createPresenceService(String paramString1,
			PresenceBasicStatus paramPresenceBasicStatus, String paramString2);

	public abstract void setDebugMode(boolean paramBoolean, String paramString);

	public abstract void setLogHandler(
			LinphoneLogHandler paramLinphoneLogHandler);

}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneCoreFactory JD-Core Version: 0.6.2
 */
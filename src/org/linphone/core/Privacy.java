package org.linphone.core;

public abstract interface Privacy
{
  public static final int CRITICAL = 16;
  public static final int DEFAULT = 32768;
  public static final int HEADER = 2;
  public static final int ID = 8;
  public static final int NONE = 0;
  public static final int SESSION = 4;
  public static final int USER = 1;
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.Privacy
 * JD-Core Version:    0.6.2
 */
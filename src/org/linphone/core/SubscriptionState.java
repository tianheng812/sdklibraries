package org.linphone.core;

public enum SubscriptionState {
	None(0), OutoingProgress(1), IncomingReceived(2), Pending(3), Active(4), Terminated(
			5), Error(6), Expiring(7);
	protected final int mValue;

	private SubscriptionState(int paramInt) {
		this.mValue = paramInt;
	}

	protected static SubscriptionState fromInt(int paramInt)
			throws LinphoneCoreException {
		switch (paramInt) {
		default:
			throw new LinphoneCoreException("Unhandled enum value " + paramInt
					+ " for SubscriptionState");
		case 0:
			return None;
		case 1:
			return OutoingProgress;
		case 2:
			return IncomingReceived;
		case 3:
			return Pending;
		case 4:
			return Active;
		case 5:
			return Terminated;
		case 6:
			return Error;
		case 7:
			return Expiring;
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.SubscriptionState JD-Core Version: 0.6.2
 */
package org.linphone.core;

import java.util.Vector;

public abstract interface LinphoneAddress {

	public static class TransportType {
		private static Vector<TransportType> values = new Vector();
		public static TransportType LinphoneTransportUdp = new TransportType(0,
				"LinphoneTransportUdp");
		public static TransportType LinphoneTransportTcp = new TransportType(1,
				"LinphoneTransportTcp");
		public static TransportType LinphoneTransportTls = new TransportType(2,
				"LinphoneTransportTls");
		private final String mStringValue;
		private final int mValue;

		private TransportType(int paramInt, String paramString) {
			mValue = paramInt;
			values.addElement(this);
			mStringValue = paramString;
		}

		public static TransportType fromInt(int paramInt) {
			for (int i = 0; i < values.size(); i++) {
				TransportType localTransportType = (TransportType) values
						.elementAt(i);
				if (localTransportType.mValue == paramInt)
					return localTransportType;
			}
			throw new RuntimeException("state not found [" + paramInt + "]");
		}

		public int toInt() {
			return this.mValue;
		}

		public String toString() {
			return this.mStringValue;
		}
	}

	public abstract String asString();

	public abstract String asStringUriOnly();

	public abstract void clean();

	public abstract String getDisplayName();

	public abstract String getDomain();

	public abstract String getPort();

	public abstract int getPortInt();

	public abstract TransportType getTransport();

	public abstract String getUserName();

	public abstract void setDisplayName(String paramString);

	public abstract void setDomain(String paramString);

	public abstract void setPort(String paramString);

	public abstract void setPortInt(int paramInt);

	public abstract void setTransport(TransportType paramTransportType);

	public abstract void setUserName(String paramString);

	public abstract String toString();

}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneAddress JD-Core Version: 0.6.2
 */
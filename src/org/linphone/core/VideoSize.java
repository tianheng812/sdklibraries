package org.linphone.core;

public final class VideoSize {
	public static final int QCIF = 0;
	public static final int CIF = 1;
	public static final int HVGA = 2;
	public static final int QVGA = 3;
	public int height;
	public int width;

	public VideoSize() {
	}

	public VideoSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public static final VideoSize createStandard(int code, boolean inverted) {
		switch (code) {
		case QCIF:
			return inverted? new VideoSize(144, 176) : new VideoSize(176, 144);
		case CIF:
			return inverted? new VideoSize(288, 352) : new VideoSize(352, 288);
		case HVGA:
			return inverted? new VideoSize(320,480) : new VideoSize(480, 320);
		case QVGA:
			return inverted? new VideoSize(240, 320) : new VideoSize(320, 240);
		default:
			return new VideoSize(); // Invalid one
		}
	}

	public VideoSize createInverted() {
		return new VideoSize(this.height, this.width);
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		VideoSize other = (VideoSize) obj;
		if (this.height != other.height) {
			return false;
		}
		if (this.width != other.width) {
			return false;
		}
		return true;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + height;
		result = prime * result + width;
		return result;
	}

	public boolean isPortrait() {
		return this.height >= this.width;
	}

	public boolean isValid() {
		return (this.width > 0) && (this.height > 0);
	}

	public String toDisplayableString() {
		return this.width + "x" + this.height;
	}

	public String toString() {
		return "width = " + this.width + " height = " + this.height;
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.VideoSize JD-Core Version: 0.6.2
 */
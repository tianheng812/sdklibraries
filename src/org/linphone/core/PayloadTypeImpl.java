package org.linphone.core;

class PayloadTypeImpl
  implements PayloadType
{
  protected final long nativePtr;

  protected PayloadTypeImpl(long paramLong)
  {
    this.nativePtr = paramLong;
  }

  private native String getMime(long paramLong);

  private native int getRate(long paramLong);

  private native String getRecvFmtp(long paramLong);

  private native String getSendFmtp(long paramLong);

  private native void setRecvFmtp(long paramLong, String paramString);

  private native void setSendFmtp(long paramLong, String paramString);

  private native String toString(long paramLong);

  public String getMime()
  {
    return getMime(this.nativePtr);
  }

  public int getRate()
  {
    return getRate(this.nativePtr);
  }

  public String getRecvFmtp()
  {
    return getRecvFmtp(this.nativePtr);
  }

  public String getSendFmtp()
  {
    return getSendFmtp(this.nativePtr);
  }

  public void setRecvFmtp(String paramString)
  {
    setRecvFmtp(this.nativePtr, paramString);
  }

  public void setSendFmtp(String paramString)
  {
    setSendFmtp(this.nativePtr, paramString);
  }

  public String toString()
  {
    return toString(this.nativePtr);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.PayloadTypeImpl
 * JD-Core Version:    0.6.2
 */
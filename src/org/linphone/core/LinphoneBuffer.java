package org.linphone.core;

public abstract interface LinphoneBuffer 
{ 
  public abstract byte[] getContent(); 
   
  public abstract void setContent(byte[] paramArrayOfByte); 
   
  public abstract int getSize(); 
   
  public abstract void setSize(int paramInt); 
} 

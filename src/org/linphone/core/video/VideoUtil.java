package org.linphone.core.video;

import android.hardware.Camera;
import android.hardware.Camera.Size;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.linphone.core.VideoSize;

final class VideoUtil
{
  public static List<VideoSize> createList(List<Camera.Size> paramList)
  {
    ArrayList localArrayList = new ArrayList(paramList.size());
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Camera.Size localSize = (Camera.Size)localIterator.next();
      localArrayList.add(new VideoSize(localSize.width, localSize.height));
    }
    return localArrayList;
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.video.VideoUtil
 * JD-Core Version:    0.6.2
 */
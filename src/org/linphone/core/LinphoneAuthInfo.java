package org.linphone.core;

public abstract interface LinphoneAuthInfo
{
  public abstract LinphoneAuthInfo clone();

  public abstract String getDomain();

  public abstract String getHa1();

  public abstract String getPassword();

  public abstract String getRealm();

  public abstract String getUserId();

  public abstract String getUsername();

  public abstract void setDomain(String paramString);

  public abstract void setHa1(String paramString);

  public abstract void setPassword(String paramString);

  public abstract void setRealm(String paramString);

  public abstract void setUserId(String paramString);

  public abstract void setUsername(String paramString);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneAuthInfo
 * JD-Core Version:    0.6.2
 */
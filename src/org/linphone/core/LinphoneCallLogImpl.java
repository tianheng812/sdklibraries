package org.linphone.core;

class LinphoneCallLogImpl implements LinphoneCallLog {
	protected final long nativePtr;

	LinphoneCallLogImpl(long paramLong) {
		this.nativePtr = paramLong;
	}

	private native int getCallDuration(long paramLong);

	private native int getCallId(long paramLong);

	private native long getFrom(long paramLong);

	private native String getStartDate(long paramLong);

	private native int getStatus(long paramLong);

	private native long getTimestamp(long paramLong);

	private native long getTo(long paramLong);

	private native boolean isIncoming(long paramLong);

	public int getCallDuration() {
		return getCallDuration(this.nativePtr);
	}

	public int getCallId() {
		return getCallId(this.nativePtr);
	}

	public CallDirection getDirection() {
		if (isIncoming(this.nativePtr))
			return CallDirection.Incoming;
		return CallDirection.Outgoing;
	}

	public LinphoneAddress getFrom() {
		return new LinphoneAddressImpl(getFrom(this.nativePtr),
				LinphoneAddressImpl.WrapMode.FromExisting);
	}

	public long getNativePtr() {
		return this.nativePtr;
	}

	public String getStartDate() {
		return getStartDate(this.nativePtr);
	}

	public LinphoneCallLog.CallStatus getStatus() {
		return LinphoneCallLog.CallStatus.fromInt(getStatus(this.nativePtr));
	}

	public long getTimestamp() {
		return 1000L * getTimestamp(this.nativePtr);
	}

	public LinphoneAddress getTo() {
		return new LinphoneAddressImpl(getTo(this.nativePtr),
				LinphoneAddressImpl.WrapMode.FromExisting);
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneCallLogImpl JD-Core Version: 0.6.2
 */
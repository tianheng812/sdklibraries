package org.linphone.core;

public abstract interface LinphonePlayer {
	public abstract void close();

	public abstract int getCurrentPosition();

	public abstract int getDuration();

	public abstract State getState();

	public abstract int open(String paramString, Listener paramListener);

	public abstract int pause();

	public abstract int seek(int paramInt);

	public abstract int start();

	public static abstract interface Listener {
		public abstract void endOfFile(LinphonePlayer paramLinphonePlayer);
	}

	public static enum State {
		closed, paused, playing;

		public static State fromValue(int paramInt) {
			if (paramInt == 0)
				return closed;
			if (paramInt == 1)
				return paused;
			if (paramInt == 2)
				return playing;
			return null;
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphonePlayer JD-Core Version: 0.6.2
 */
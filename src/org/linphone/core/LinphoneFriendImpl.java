package org.linphone.core;

import java.io.Serializable;

public class LinphoneFriendImpl implements LinphoneFriend, Serializable {
	protected final long nativePtr;
	boolean ownPtr = false;

	protected LinphoneFriendImpl() {
		this.nativePtr = newLinphoneFriend(null);
	}

	protected LinphoneFriendImpl(long paramLong) {
		this.nativePtr = paramLong;
		this.ownPtr = false;
	}

	protected LinphoneFriendImpl(String paramString) {
		this.nativePtr = newLinphoneFriend(paramString);
	}

	private native void delete(long paramLong);

	private native void done(long paramLong);

	private native void edit(long paramLong);

	private native void enableSubscribes(long paramLong, boolean paramBoolean);

	private native long getAddress(long paramLong);

	private native Object getCore(long paramLong);

	private native int getIncSubscribePolicy(long paramLong);

	private native Object getPresenceModel(long paramLong);

	private native int getStatus(long paramLong);

	private Object getSyncObject() {
		Object localObject = getCore(this.nativePtr);
		if (localObject != null)
			return localObject;
		return this;
	}

	private native boolean isSubscribesEnabled(long paramLong);

	private native long newLinphoneFriend(String paramString);

	private native void setAddress(long paramLong1, long paramLong2);

	private native void setIncSubscribePolicy(long paramLong, int paramInt);

	private native void setPresenceModel(long paramLong1, long paramLong2);

	public void done() {
		synchronized (getSyncObject()) {
			done(this.nativePtr);
			return;
		}
	}

	public void edit() {
		synchronized (getSyncObject()) {
			edit(this.nativePtr);
			return;
		}
	}

	public void enableSubscribes(boolean paramBoolean) {
		synchronized (getSyncObject()) {
			enableSubscribes(this.nativePtr, paramBoolean);
			return;
		}
	}

	protected void finalize() throws Throwable {
		if (this.ownPtr)
			delete(this.nativePtr);
	}

	public LinphoneAddress getAddress() {
		return new LinphoneAddressImpl(getAddress(this.nativePtr),
				LinphoneAddressImpl.WrapMode.FromConst);
	}

	public LinphoneFriend.SubscribePolicy getIncSubscribePolicy() {
		return LinphoneFriend.SubscribePolicy
				.fromInt(getIncSubscribePolicy(this.nativePtr));
	}

	public long getNativePtr() {
		return this.nativePtr;
	}

	public PresenceModel getPresenceModel() {
		return (PresenceModel) getPresenceModel(this.nativePtr);
	}

	public OnlineStatus getStatus() {
		return OnlineStatus.fromInt(getStatus(this.nativePtr));
	}

	public boolean isSubscribesEnabled() {
		return isSubscribesEnabled(this.nativePtr);
	}

	public void setAddress(LinphoneAddress paramLinphoneAddress) {
		setAddress(this.nativePtr,
				((LinphoneAddressImpl) paramLinphoneAddress).nativePtr);
	}

	public void setIncSubscribePolicy(
			LinphoneFriend.SubscribePolicy paramSubscribePolicy) {
		synchronized (getSyncObject()) {
			setIncSubscribePolicy(this.nativePtr, paramSubscribePolicy.mValue);
			return;
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneFriendImpl JD-Core Version: 0.6.2
 */
package org.linphone.core;

class LinphoneChatRoomImpl implements LinphoneChatRoom {
	protected final long nativePtr;

	protected LinphoneChatRoomImpl(long paramLong) {
		this.nativePtr = paramLong;
	}

	private native void cancelFileTransfer(long paramLong1, long paramLong2);

	private native void compose(long paramLong);

	private native long createFileTransferMessage(long paramLong,
			String paramString1, String paramString2, String paramString3,
			int paramInt);

	private native long createLinphoneChatMessage(long paramLong,
			String paramString);

	private native long createLinphoneChatMessage2(long paramLong1,
			String paramString1, String paramString2, int paramInt,
			long paramLong2, boolean paramBoolean1, boolean paramBoolean2);

	private native void deleteHistory(long paramLong);

	private native void deleteMessage(long paramLong1, long paramLong2);

	private native void destroy(long paramLong);

	private native Object getCore(long paramLong);

	private native long[] getHistory(long paramLong, int paramInt);

	private LinphoneChatMessage[] getHistoryPrivate(long[] typesPtr) {
		if (typesPtr == null) {
			return null;
		}
		LinphoneChatMessage[] messages = new LinphoneChatMessage[typesPtr.length];
		for (int i = 0; i < messages.length; i++) {
			messages[i] = new LinphoneChatMessageImpl(typesPtr[i]);
		}
		return messages;
	}

	private native long[] getHistoryRange(long paramLong, int paramInt1,
			int paramInt2);

	private native int getHistorySize(long paramLong);

	private native long getPeerAddress(long paramLong);

	private native int getUnreadMessagesCount(long paramLong);

	private native boolean isRemoteComposing(long paramLong);

	private native void markAsRead(long paramLong);

	private native void sendMessage(long paramLong, String paramString);

	private native void sendMessage2(long paramLong1, Object paramObject,
			long paramLong2,
			LinphoneChatMessage.StateListener paramStateListener);

	private native void updateUrl(long paramLong1, long paramLong2);

	public void cancelFileTransfer(LinphoneChatMessage paramLinphoneChatMessage) {
		synchronized (getCore()) {
			cancelFileTransfer(this.nativePtr,
					((LinphoneChatMessageImpl) paramLinphoneChatMessage)
							.getNativePtr());
		}
	}

	public void compose() {
		synchronized (getCore()) {
			compose(this.nativePtr);
		}
	}

	public LinphoneChatMessage createFileTransferMessage(
			LinphoneContent paramLinphoneContent) {
		synchronized (getCore()) {
			LinphoneChatMessageImpl localLinphoneChatMessageImpl = new LinphoneChatMessageImpl(
					createFileTransferMessage(this.nativePtr,
							paramLinphoneContent.getName(),
							paramLinphoneContent.getType(),
							paramLinphoneContent.getSubtype(),
							paramLinphoneContent.getRealSize()));
			return localLinphoneChatMessageImpl;
		}
	}

	public LinphoneChatMessage createLinphoneChatMessage(String paramString) {
		synchronized (getCore()) {
			LinphoneChatMessageImpl localLinphoneChatMessageImpl = new LinphoneChatMessageImpl(
					createLinphoneChatMessage(this.nativePtr, paramString));
			return localLinphoneChatMessageImpl;
		}
	}

	public LinphoneChatMessage createLinphoneChatMessage(String message,
			String url, LinphoneChatMessage.State state, long timestamp,
			boolean isRead, boolean isIncoming) {
		synchronized (getCore()) {
			return new LinphoneChatMessageImpl(createLinphoneChatMessage2(
					this.nativePtr, message, url, state.value(),
					timestamp / 1000L, isRead, isIncoming));
		}
	}

	public void deleteHistory() {
		synchronized (getCore()) {
			deleteHistory(this.nativePtr);
		}
	}

	public void deleteMessage(LinphoneChatMessage message) {
		synchronized (getCore()) {
			if (message != null)
				deleteMessage(this.nativePtr,
						((LinphoneChatMessageImpl) message).getNativePtr());
		}

	}

	public void destroy() {
		destroy(this.nativePtr);
	}

	public LinphoneCore getCore() {
		return (LinphoneCore) getCore(this.nativePtr);
	}

	public LinphoneChatMessage[] getHistory() {
		synchronized (getCore()) {
			LinphoneChatMessage[] arrayOfLinphoneChatMessage = getHistory(0);
			return arrayOfLinphoneChatMessage;
		}
	}

	public LinphoneChatMessage[] getHistory(int paramInt) {
		synchronized (getCore()) {
			LinphoneChatMessage[] arrayOfLinphoneChatMessage = getHistoryPrivate(getHistory(
					this.nativePtr, paramInt));
			return arrayOfLinphoneChatMessage;
		}
	}

	public LinphoneChatMessage[] getHistoryRange(int paramInt1, int paramInt2) {
		synchronized (getCore()) {
			LinphoneChatMessage[] arrayOfLinphoneChatMessage = getHistoryPrivate(getHistoryRange(
					this.nativePtr, paramInt1, paramInt2));
			return arrayOfLinphoneChatMessage;
		}
	}

	public int getHistorySize() {
		synchronized (getCore()) {
			int i = getHistorySize(this.nativePtr);
			return i;
		}
	}

	public LinphoneAddress getPeerAddress() {
		return new LinphoneAddressImpl(getPeerAddress(this.nativePtr),
				LinphoneAddressImpl.WrapMode.FromConst);
	}

	public int getUnreadMessagesCount() {
		synchronized (getCore()) {
			int i = getUnreadMessagesCount(this.nativePtr);
			return i;
		}
	}

	public boolean isRemoteComposing() {
		synchronized (getCore()) {
			boolean bool = isRemoteComposing(this.nativePtr);
			return bool;
		}
	}

	public void markAsRead() {
		synchronized (getCore()) {
			markAsRead(this.nativePtr);
		}

	}

	public void sendMessage(String paramString) {
		synchronized (getCore()) {
			sendMessage(this.nativePtr, paramString);
		}
	}

	public void sendMessage(LinphoneChatMessage message,
			LinphoneChatMessage.StateListener paramStateListener) {
		synchronized (getCore()) {
			sendMessage2(this.nativePtr, message,
					((LinphoneChatMessageImpl) message).getNativePtr(),
					paramStateListener);
		}
	}

	public void updateUrl(LinphoneChatMessage message) {
		synchronized (getCore()) {
			if (message != null)
				updateUrl(this.nativePtr,
						((LinphoneChatMessageImpl) message).getNativePtr());
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneChatRoomImpl JD-Core Version: 0.6.2
 */
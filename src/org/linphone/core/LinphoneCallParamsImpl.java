package org.linphone.core;

public class LinphoneCallParamsImpl
  implements LinphoneCallParams
{
  protected final long nativePtr;

  public LinphoneCallParamsImpl(long paramLong)
  {
    this.nativePtr = paramLong;
  }

  private native void addCustomHeader(long paramLong, String paramString1, String paramString2);

  private native void audioBandwidth(long paramLong, int paramInt);

  private native void destroy(long paramLong);

  private native void enableLowBandwidth(long paramLong, boolean paramBoolean);

  private native void enableVideo(long paramLong, boolean paramBoolean);

  private native String getCustomHeader(long paramLong, String paramString);

  private native int getMediaEncryption(long paramLong);

  private native int getPrivacy(long paramLong);

  private native int[] getReceivedVideoSize(long paramLong);

  private native int[] getSentVideoSize(long paramLong);

  private native String getSessionName(long paramLong);

  private native long getUsedAudioCodec(long paramLong);

  private native long getUsedVideoCodec(long paramLong);

  private native boolean getVideoEnabled(long paramLong);

  private native boolean isLowBandwidthEnabled(long paramLong);

  private native boolean localConferenceMode(long paramLong);

  private native void setMediaEncryption(long paramLong, int paramInt);

  private native void setPrivacy(long paramLong, int paramInt);

  private native void setRecordFile(long paramLong, String paramString);

  private native void setSessionName(long paramLong, String paramString);

  public void addCustomHeader(String paramString1, String paramString2)
  {
    addCustomHeader(this.nativePtr, paramString1, paramString2);
  }

  public void enableLowBandwidth(boolean paramBoolean)
  {
    enableLowBandwidth(this.nativePtr, paramBoolean);
  }

  protected void finalize()
    throws Throwable
  {
    destroy(this.nativePtr);
    super.finalize();
  }

  public String getCustomHeader(String paramString)
  {
    return getCustomHeader(this.nativePtr, paramString);
  }

  public LinphoneCore.MediaEncryption getMediaEncryption()
  {
    return LinphoneCore.MediaEncryption.fromInt(getMediaEncryption(this.nativePtr));
  }

  public int getPrivacy()
  {
    return getPrivacy(this.nativePtr);
  }

  public VideoSize getReceivedVideoSize()
  {
    int[] arrayOfInt = getReceivedVideoSize(this.nativePtr);
    VideoSize localVideoSize = new VideoSize();
    localVideoSize.width = arrayOfInt[0];
    localVideoSize.height = arrayOfInt[1];
    return localVideoSize;
  }

  public VideoSize getSentVideoSize()
  {
    int[] arrayOfInt = getSentVideoSize(this.nativePtr);
    VideoSize localVideoSize = new VideoSize();
    localVideoSize.width = arrayOfInt[0];
    localVideoSize.height = arrayOfInt[1];
    return localVideoSize;
  }

  public String getSessionName()
  {
    return getSessionName(this.nativePtr);
  }

  public PayloadType getUsedAudioCodec()
  {
    long l = getUsedAudioCodec(this.nativePtr);
    if (l == 0L)
      return null;
    return new PayloadTypeImpl(l);
  }

  public PayloadType getUsedVideoCodec()
  {
    long l = getUsedVideoCodec(this.nativePtr);
    if (l == 0L)
      return null;
    return new PayloadTypeImpl(l);
  }

  public boolean getVideoEnabled()
  {
    return getVideoEnabled(this.nativePtr);
  }

  public boolean isLowBandwidthEnabled()
  {
    return isLowBandwidthEnabled(this.nativePtr);
  }

  public boolean localConferenceMode()
  {
    return localConferenceMode(this.nativePtr);
  }

  public void setAudioBandwidth(int paramInt)
  {
    audioBandwidth(this.nativePtr, paramInt);
  }

  public void setMediaEnctyption(LinphoneCore.MediaEncryption paramMediaEncryption)
  {
    setMediaEncryption(this.nativePtr, paramMediaEncryption.mValue);
  }

  public void setPrivacy(int paramInt)
  {
    setPrivacy(this.nativePtr, paramInt);
  }

  public void setRecordFile(String paramString)
  {
    setRecordFile(this.nativePtr, paramString);
  }

  public void setSessionName(String paramString)
  {
    setSessionName(this.nativePtr, paramString);
  }

  public void setVideoEnabled(boolean paramBoolean)
  {
    enableVideo(this.nativePtr, paramBoolean);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneCallParamsImpl
 * JD-Core Version:    0.6.2
 */
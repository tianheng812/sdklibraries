package org.linphone.core;

class LinphoneCallImpl implements LinphoneCall {
	private LinphoneCallStats audioStats;
	protected final long nativePtr;
	boolean ownPtr = false;
	Object userData;
	private LinphoneCallStats videoStats;

	private LinphoneCallImpl(long paramLong) {
		this.nativePtr = paramLong;
	}

	private native boolean cameraEnabled(long paramLong);

	private native void enableCamera(long paramLong, boolean paramBoolean);

	private native void enableEchoCancellation(long paramLong,
			boolean paramBoolean);

	private native void enableEchoLimiter(long paramLong, boolean paramBoolean);

	private native void finalize(long paramLong);

	private native String getAuthenticationToken(long paramLong);

	private native float getAverageQuality(long paramLong);

	private native long getCallLog(long paramLong);

	private native long getCurrentParamsCopy(long paramLong);

	private native float getCurrentQuality(long paramLong);

	private native int getDuration(long paramLong);

	private native long getErrorInfo(long paramLong);

	private native float getPlayVolume(long paramLong);

	private native long getPlayer(long paramLong);

	private native long getRemoteAddress(long paramLong);

	private native String getRemoteContact(long paramLong);

	private native long getRemoteParams(long paramLong);

	private native String getRemoteUserAgent(long paramLong);

	private native Object getReplacedCall(long paramLong);

	private native int getState(long paramLong);

	private native int getTransferState(long paramLong);

	private native Object getTransferTargetCall(long paramLong);

	private native Object getTransfererCall(long paramLong);

	private native boolean isAuthenticationTokenVerified(long paramLong);

	private native boolean isEchoCancellationEnabled(long paramLong);

	private native boolean isEchoLimiterEnabled(long paramLong);

	private native boolean isIncoming(long paramLong);

	private native int sendInfoMessage(long paramLong1, long paramLong2);

	private native void setAuthenticationTokenVerified(long paramLong,
			boolean paramBoolean);

	private native void startRecording(long paramLong);

	private native void stopRecording(long paramLong);

	private native void takeSnapshot(long paramLong, String paramString);

	private native void zoomVideo(long paramLong, float paramFloat1,
			float paramFloat2, float paramFloat3);

	public boolean cameraEnabled() {
		return cameraEnabled(this.nativePtr);
	}

	public void enableCamera(boolean paramBoolean) {
		enableCamera(this.nativePtr, paramBoolean);
	}

	public void enableEchoCancellation(boolean paramBoolean) {
		enableEchoCancellation(this.nativePtr, paramBoolean);
	}

	public void enableEchoLimiter(boolean paramBoolean) {
		enableEchoLimiter(this.nativePtr, paramBoolean);
	}

	public boolean equals(Object call) {
		if (this == call) {
			return true;
		}
		if (call == null) {
			return false;
		}
		if (!(call instanceof LinphoneCallImpl)) {
			return false;
		}
		return this.nativePtr == ((LinphoneCallImpl) call).nativePtr;
	}

	protected void finalize() throws Throwable {
		finalize(this.nativePtr);
	}

	public LinphoneCallStats getAudioStats() {
		if (this.audioStats != null)
			((LinphoneCallStatsImpl) this.audioStats).updateRealTimeStats(this);
		return this.audioStats;
	}

	public String getAuthenticationToken() {
		return getAuthenticationToken(this.nativePtr);
	}

	public float getAverageQuality() {
		return getAverageQuality(this.nativePtr);
	}

	public LinphoneCallLog getCallLog() {
		long l = getCallLog(this.nativePtr);
		if (l != 0L)
			return new LinphoneCallLogImpl(l);
		return null;
	}

	public LinphoneCallParams getCurrentParamsCopy() {
		return new LinphoneCallParamsImpl(getCurrentParamsCopy(this.nativePtr));
	}

	public float getCurrentQuality() {
		return getCurrentQuality(this.nativePtr);
	}

	public CallDirection getDirection() {
		if (isIncoming(this.nativePtr))
			return CallDirection.Incoming;
		return CallDirection.Outgoing;
	}

	public int getDuration() {
		return getDuration(this.nativePtr);
	}

	public ErrorInfo getErrorInfo() {
		return new ErrorInfoImpl(getErrorInfo(this.nativePtr));
	}

	public float getPlayVolume() {
		return getPlayVolume(this.nativePtr);
	}

	public LinphonePlayer getPlayer() {
		return new LinphonePlayerImpl(getPlayer(this.nativePtr));
	}

	public Reason getReason() {
		return null;
	}

	public LinphoneAddress getRemoteAddress() {
		long l = getRemoteAddress(this.nativePtr);
		if (l != 0L)
			return new LinphoneAddressImpl(l,
					LinphoneAddressImpl.WrapMode.FromConst);
		return null;
	}

	public String getRemoteContact() {
		return getRemoteContact(this.nativePtr);
	}

	public LinphoneCallParams getRemoteParams() {
		long l = getRemoteParams(this.nativePtr);
		if (l == 0L)
			return null;
		return new LinphoneCallParamsImpl(l);
	}

	public String getRemoteUserAgent() {
		return getRemoteUserAgent(this.nativePtr);
	}

	public LinphoneCall getReplacedCall() {
		return (LinphoneCall) getReplacedCall(this.nativePtr);
	}

	public LinphoneCall.State getState() {
		return LinphoneCall.State.fromInt(getState(this.nativePtr));
	}

	public LinphoneCall.State getTransferState() {
		return LinphoneCall.State.fromInt(getTransferState(this.nativePtr));
	}

	public LinphoneCall getTransferTargetCall() {
		return (LinphoneCall) getTransferTargetCall(this.nativePtr);
	}

	public LinphoneCall getTransfererCall() {
		return (LinphoneCall) getTransfererCall(this.nativePtr);
	}

	public Object getUserData() {
		return this.userData;
	}

	public LinphoneCallStats getVideoStats() {
		if (this.videoStats != null)
			((LinphoneCallStatsImpl) this.videoStats).updateRealTimeStats(this);
		return this.videoStats;
	}

	public int hashCode() {
		return 527 + (int) (this.nativePtr ^ this.nativePtr >>> 32);
	}

	public boolean isAuthenticationTokenVerified() {
		return isAuthenticationTokenVerified(this.nativePtr);
	}

	public boolean isEchoCancellationEnabled() {
		return isEchoCancellationEnabled(this.nativePtr);
	}

	public boolean isEchoLimiterEnabled() {
		return isEchoLimiterEnabled(this.nativePtr);
	}

	public boolean isInConference() {
		return new LinphoneCallParamsImpl(getCurrentParamsCopy(this.nativePtr))
				.localConferenceMode();
	}

	public void sendInfoMessage(LinphoneInfoMessage paramLinphoneInfoMessage) {
		sendInfoMessage(this.nativePtr,
				((LinphoneInfoMessageImpl) paramLinphoneInfoMessage).nativePtr);
	}

	public void setAudioStats(LinphoneCallStats paramLinphoneCallStats) {
		this.audioStats = paramLinphoneCallStats;
	}

	public void setAuthenticationTokenVerified(boolean paramBoolean) {
		setAuthenticationTokenVerified(this.nativePtr, paramBoolean);
	}

	public void setUserData(Object paramObject) {
		this.userData = paramObject;
	}

	public void setVideoStats(LinphoneCallStats paramLinphoneCallStats) {
		this.videoStats = paramLinphoneCallStats;
	}

	public void startRecording() {
		startRecording(this.nativePtr);
	}

	public void stopRecording() {
		stopRecording(this.nativePtr);
	}

	public void takeSnapshot(String paramString) {
		takeSnapshot(this.nativePtr, paramString);
	}

	public String toString() {
		return "Call " + this.nativePtr;
	}

	public void zoomVideo(float paramFloat1, float paramFloat2,
			float paramFloat3) {
		zoomVideo(this.nativePtr, paramFloat1, paramFloat2, paramFloat3);
	}

	@Override
	public boolean mediaInProgress() {
		 return false;
	}

	/*@Override
	public void takePreviewSnapshot(String path) {
		// TODO Auto-generated method stub
		
	}*/
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneCallImpl JD-Core Version: 0.6.2
 */
package org.linphone.core;

public class LinphoneEventImpl implements LinphoneEvent {
	private long mNativePtr;
	private Object mUserContext;

	protected LinphoneEventImpl(long paramLong) {
		this.mNativePtr = paramLong;
	}

	private native int acceptSubscription(long paramLong);

	private native void addCustomHeader(long paramLong, String paramString1,
			String paramString2);

	private native int denySubscription(long paramLong, int paramInt);

	private native Object getCore(long paramLong);

	private native String getCustomHeader(long paramLong, String paramString);

	private native long getErrorInfo(long paramLong);

	private native String getEventName(long paramLong);

	private native int getReason(long paramLong);

	private native int getSubscriptionDir(long paramLong);

	private native int getSubscriptionState(long paramLong);

	private native int notify(long paramLong, String paramString1,
			String paramString2, byte[] paramArrayOfByte, String paramString3);

	private native void sendPublish(long paramLong, String paramString1,
			String paramString2, byte[] paramArrayOfByte, String paramString3);

	private native void sendSubscribe(long paramLong, String paramString1,
			String paramString2, byte[] paramArrayOfByte, String paramString3);

	private native int terminate(long paramLong);

	private native void unref(long paramLong);

	private native int updatePublish(long paramLong, String paramString1,
			String paramString2, byte[] paramArrayOfByte, String paramString3);

	private native int updateSubscribe(long paramLong, String paramString1,
			String paramString2, byte[] paramArrayOfByte, String paramString3);

	public void acceptSubscription() {
		synchronized (getCore()) {
			acceptSubscription(this.mNativePtr);
		}
	}

	public void addCustomHeader(String paramString1, String paramString2) {
		addCustomHeader(this.mNativePtr, paramString1, paramString2);
	}

	public void denySubscription(Reason paramReason) {
		synchronized (getCore()) {
			denySubscription(this.mNativePtr, paramReason.mValue);
		}
	}

	protected void finalize() {
		unref(this.mNativePtr);
	}

	public LinphoneCore getCore() {
		LinphoneCore localLinphoneCore = (LinphoneCore) getCore(this.mNativePtr);
		return localLinphoneCore;
	}

	public String getCustomHeader(String paramString) {
		return getCustomHeader(this.mNativePtr, paramString);
	}

	public ErrorInfo getErrorInfo() {
		return new ErrorInfoImpl(getErrorInfo(this.mNativePtr));
	}

	public String getEventName() {
		return getEventName(this.mNativePtr);
	}

	public Reason getReason() {
		return Reason.fromInt(getReason(this.mNativePtr));
	}

	public SubscriptionDir getSubscriptionDir() {
		try {
			return SubscriptionDir.fromInt(getSubscriptionDir(this.mNativePtr));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public SubscriptionState getSubscriptionState() {
		try {
			return SubscriptionState
					.fromInt(getSubscriptionState(this.mNativePtr));
		} catch (LinphoneCoreException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Object getUserContext() {
		return this.mUserContext;
	}

	public void notify(LinphoneContent paramLinphoneContent) {
		synchronized (getCore()) {
			notify(this.mNativePtr, paramLinphoneContent.getType(),
					paramLinphoneContent.getSubtype(),
					paramLinphoneContent.getData(),
					paramLinphoneContent.getEncoding());
		}
	}

	public void sendPublish(LinphoneContent body) {
		synchronized (getCore()) {
			if (body != null) {
				sendPublish(this.mNativePtr, body.getType(), body.getSubtype(),
						body.getData(), body.getEncoding());
			} else {
				sendPublish(this.mNativePtr, null, null, null, null);
			}
		}
	}

	public void sendSubscribe(LinphoneContent body) {
		synchronized (getCore()) {
			if (body != null) {
				sendSubscribe(this.mNativePtr, body.getType(),
						body.getSubtype(), body.getData(), body.getEncoding());
			} else {
				sendSubscribe(this.mNativePtr, null, null, null, null);
			}
		}
	}

	public void setUserContext(Object paramObject) {
		this.mUserContext = paramObject;
	}

	public void terminate() {
		synchronized (getCore()) {
			terminate(this.mNativePtr);
			return;
		}
	}

	public void updatePublish(LinphoneContent paramLinphoneContent) {
		synchronized (getCore()) {
			updatePublish(this.mNativePtr, paramLinphoneContent.getType(),
					paramLinphoneContent.getSubtype(),
					paramLinphoneContent.getData(),
					paramLinphoneContent.getEncoding());
		}
	}

	public void updateSubscribe(LinphoneContent paramLinphoneContent) {
		synchronized (getCore()) {
			updateSubscribe(this.mNativePtr, paramLinphoneContent.getType(),
					paramLinphoneContent.getSubtype(),
					paramLinphoneContent.getData(),
					paramLinphoneContent.getEncoding());
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneEventImpl JD-Core Version: 0.6.2
 */
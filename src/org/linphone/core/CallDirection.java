package org.linphone.core;

public class CallDirection
{
  public static CallDirection Incoming = new CallDirection("Callincoming");
  public static CallDirection Outgoing = new CallDirection("CallOutgoing");
  private String mStringValue;

  private CallDirection(String paramString)
  {
    this.mStringValue = paramString;
  }

  public String toString()
  {
    return this.mStringValue;
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.CallDirection
 * JD-Core Version:    0.6.2
 */
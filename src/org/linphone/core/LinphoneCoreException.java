package org.linphone.core;

public class LinphoneCoreException extends Exception
{
  public LinphoneCoreException()
  {
  }

  public LinphoneCoreException(String paramString)
  {
    super(paramString);
  }

  public LinphoneCoreException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public LinphoneCoreException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.LinphoneCoreException
 * JD-Core Version:    0.6.2
 */
package org.linphone.core;

public enum ToneID
{
	Undefined(0),  Busy(1),  CallWaiting(2),  CallOnHold(3),  CallLost(4); 
	
  protected final int mValue;

  

  private ToneID(int paramInt)
  {
    this.mValue = paramInt;
  }

  protected static ToneID fromInt(int paramInt)
    throws LinphoneCoreException
  {
    switch (paramInt)
    {
    default:
      throw new LinphoneCoreException("Unhandled enum value " + paramInt + " for LinphoneToneID");
    case 0:
      return Undefined;
    case 1:
      return Busy;
    case 2:
      return CallWaiting;
    case 3:
      return CallOnHold;
    case 4:
    }
    return CallLost;
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.core.ToneID
 * JD-Core Version:    0.6.2
 */
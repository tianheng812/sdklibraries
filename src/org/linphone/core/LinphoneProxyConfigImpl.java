package org.linphone.core;

class LinphoneProxyConfigImpl implements LinphoneProxyConfig {
	protected LinphoneCoreImpl mCore;
	protected final long nativePtr;
	Object userData;

	protected LinphoneProxyConfigImpl(LinphoneCoreImpl paramLinphoneCoreImpl) {
		this.mCore = paramLinphoneCoreImpl;
		this.nativePtr = createProxyConfig(paramLinphoneCoreImpl.nativePtr);
	}

	protected LinphoneProxyConfigImpl(LinphoneCoreImpl paramLinphoneCoreImpl,
			long paramLong) {
		this.mCore = paramLinphoneCoreImpl;
		this.nativePtr = paramLong;
	}

	protected LinphoneProxyConfigImpl(LinphoneCoreImpl paramLinphoneCoreImpl,
			String paramString1, String paramString2, String paramString3,
			boolean paramBoolean) throws LinphoneCoreException {
		this.mCore = paramLinphoneCoreImpl;
		this.nativePtr = createProxyConfig(paramLinphoneCoreImpl.nativePtr);
		setIdentity(paramString1);
		setProxy(paramString2);
		setRoute(paramString3);
		enableRegister(paramBoolean);
	}

	private native boolean avpfEnabled(long paramLong);

	private native long createProxyConfig(long paramLong);

	private native void done(long paramLong);

	private native void edit(long paramLong);

	private native void enableAvpf(long paramLong, boolean paramBoolean);

	private native void enablePublish(long paramLong, boolean paramBoolean);

	private native void enableQualityReporting(long paramLong,
			boolean paramBoolean);

	private native void enableRegister(long paramLong, boolean paramBoolean);

	private native void finalize(long paramLong);

	private native int getAvpfRRInterval(long paramLong);

	private native String getContactParameters(long paramLong);

	private native String getContactUriParameters(long paramLong);

	private native boolean getDialEscapePlus(long paramLong);

	private native String getDialPrefix(long paramLong);

	private native String getDomain(long paramLong);

	private native int getError(long paramLong);

	private native long getErrorInfo(long paramLong);

	private native int getExpires(long paramLong);

	private native String getIdentity(long paramLong);

	private native int getPrivacy(long paramLong);

	private native String getProxy(long paramLong);

	private native int getPublishExpires(long paramLong);

	private native String getQualityReportingCollector(long paramLong);

	private native int getQualityReportingInterval(long paramLong);

	private native String getRealm(long paramLong);

	private native String getRoute(long paramLong);

	private native int getState(long paramLong);

	private native boolean isRegisterEnabled(long paramLong);

	private native boolean isRegistered(long paramLong);

	private void isValid() {
		if (this.nativePtr == 0L)
			throw new RuntimeException("proxy config removed");
	}

	private native int lookupCCCFromE164(long paramLong, String paramString);

	private native int lookupCCCFromIso(long paramLong, String paramString);

	private native long newLinphoneProxyConfig();

	private native String normalizePhoneNumber(long paramLong,
			String paramString);

	private native boolean publishEnabled(long paramLong);

	private native boolean qualityReportingEnabled(long paramLong);

	private native void setAvpfRRInterval(long paramLong, int paramInt);

	private native void setContactParameters(long paramLong, String paramString);

	private native void setContactUriParameters(long paramLong,
			String paramString);

	private native void setDialEscapePlus(long paramLong, boolean paramBoolean);

	private native void setDialPrefix(long paramLong, String paramString);

	private native void setExpires(long paramLong, int paramInt);

	private native void setIdentity(long paramLong, String paramString);

	private native void setPrivacy(long paramLong, int paramInt);

	private native int setProxy(long paramLong, String paramString);

	private native void setPublishExpires(long paramLong, int paramInt);

	private native void setQualityReportingCollector(long paramLong,
			String paramString);

	private native void setQualityReportingInterval(long paramLong, int paramInt);

	private native void setRealm(long paramLong, String paramString);

	private native int setRoute(long paramLong, String paramString);

	public boolean avpfEnabled() {
		isValid();
		return avpfEnabled(this.nativePtr);
	}

	public void done() {
		isValid();
		Object mutex = this.mCore != null ? this.mCore : this;
		synchronized (mutex) {
			done(this.nativePtr);
		}
	}

	public LinphoneProxyConfig edit() {
		isValid();
		Object mutex = this.mCore != null ? this.mCore : this;
		synchronized (mutex) {
			edit(this.nativePtr);
		}
		return this;
	}

	public void enableAvpf(boolean paramBoolean) {
		isValid();
		enableAvpf(this.nativePtr, paramBoolean);
	}

	public void enablePublish(boolean paramBoolean) {
		isValid();
		enablePublish(this.nativePtr, paramBoolean);
	}

	public void enableQualityReporting(boolean paramBoolean) {
		isValid();
		enableQualityReporting(this.nativePtr, paramBoolean);
	}

	public LinphoneProxyConfig enableRegister(boolean paramBoolean) {
		isValid();
		enableRegister(this.nativePtr, paramBoolean);
		return this;
	}

	protected void finalize() throws Throwable {
		if (this.nativePtr != 0L)
			finalize(this.nativePtr);
		super.finalize();
	}

	public int getAvpfRRInterval() {
		isValid();
		return getAvpfRRInterval(this.nativePtr);
	}

	public String getContactParameters() {
		isValid();
		return getContactParameters(this.nativePtr);
	}

	public String getContactUriParameters() {
		isValid();
		return getContactUriParameters(this.nativePtr);
	}

	public boolean getDialEscapePlus() {
		isValid();
		return getDialEscapePlus(this.nativePtr);
	}

	public String getDialPrefix() {
		isValid();
		return getDialPrefix(this.nativePtr);
	}

	public String getDomain() {
		isValid();
		return getDomain(this.nativePtr);
	}

	public Reason getError() {
		isValid();
		return Reason.fromInt(getError(this.nativePtr));
	}

	public ErrorInfo getErrorInfo() {
		return new ErrorInfoImpl(getErrorInfo(this.nativePtr));
	}

	public int getExpires() {
		isValid();
		return getExpires(this.nativePtr);
	}

	public String getIdentity() {
		isValid();
		return getIdentity(this.nativePtr);
	}

	public int getPrivacy() {
		isValid();
		return getPrivacy(this.nativePtr);
	}

	public String getProxy() {
		isValid();
		return getProxy(this.nativePtr);
	}

	public int getPublishExpires() {
		isValid();
		return getPublishExpires(this.nativePtr);
	}

	public String getQualityReportingCollector() {
		isValid();
		return getQualityReportingCollector(this.nativePtr);
	}

	public int getQualityReportingInterval() {
		isValid();
		return getQualityReportingInterval(this.nativePtr);
	}

	public String getRealm() {
		isValid();
		return getRealm(this.nativePtr);
	}

	public String getRoute() {
		isValid();
		return getRoute(this.nativePtr);
	}

	public LinphoneCore.RegistrationState getState() {
		isValid();
		return LinphoneCore.RegistrationState.fromInt(getState(this.nativePtr));
	}

	public Object getUserData() {
		return this.userData;
	}

	public boolean isRegistered() {
		isValid();
		return isRegistered(this.nativePtr);
	}

	public int lookupCCCFromE164(String paramString) {
		isValid();
		return lookupCCCFromE164(this.nativePtr, paramString);
	}

	public int lookupCCCFromIso(String paramString) {
		isValid();
		return lookupCCCFromIso(this.nativePtr, paramString);
	}

	public String normalizePhoneNumber(String paramString) {
		isValid();
		return normalizePhoneNumber(this.nativePtr, paramString);
	}

	public boolean publishEnabled() {
		isValid();
		return publishEnabled(this.nativePtr);
	}

	public boolean qualityReportingEnabled() {
		isValid();
		return avpfEnabled(this.nativePtr);
	}

	public boolean registerEnabled() {
		isValid();
		return isRegisterEnabled(this.nativePtr);
	}

	public void setAvpfRRInterval(int paramInt) {
		isValid();
		setAvpfRRInterval(this.nativePtr, paramInt);
	}

	public void setContactParameters(String paramString) {
		isValid();
		setContactParameters(this.nativePtr, paramString);
	}

	public void setContactUriParameters(String paramString) {
		isValid();
		setContactUriParameters(this.nativePtr, paramString);
	}

	public void setDialEscapePlus(boolean paramBoolean) {
		isValid();
		setDialEscapePlus(this.nativePtr, paramBoolean);
	}

	public void setDialPrefix(String paramString) {
		isValid();
		setDialPrefix(this.nativePtr, paramString);
	}

	public void setExpires(int paramInt) {
		isValid();
		setExpires(this.nativePtr, paramInt);
	}

	public void setIdentity(String paramString) throws LinphoneCoreException {
		isValid();
		setIdentity(this.nativePtr, paramString);
	}

	public void setPrivacy(int paramInt) {
		isValid();
		setPrivacy(this.nativePtr, paramInt);
	}

	public void setProxy(String paramString) throws LinphoneCoreException {
		isValid();
		if (setProxy(this.nativePtr, paramString) != 0)
			throw new LinphoneCoreException("Bad proxy address [" + paramString
					+ "]");
	}

	public void setPublishExpires(int paramInt) {
		isValid();
		setPublishExpires(this.nativePtr, paramInt);
	}

	public void setQualityReportingCollector(String paramString) {
		isValid();
		setQualityReportingCollector(this.nativePtr, paramString);
	}

	public void setQualityReportingInterval(int paramInt) {
		isValid();
		setQualityReportingInterval(this.nativePtr, paramInt);
	}

	public void setRealm(String paramString) {
		isValid();
		setRealm(this.nativePtr, paramString);
	}

	public void setRoute(String paramString) throws LinphoneCoreException {
		isValid();
		if (setRoute(this.nativePtr, paramString) != 0)
			throw new LinphoneCoreException("cannot set route [" + paramString
					+ "]");
	}

	public void setUserData(Object paramObject) {
		this.userData = paramObject;
	}
	
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.LinphoneProxyConfigImpl JD-Core Version:
 * 0.6.2
 */
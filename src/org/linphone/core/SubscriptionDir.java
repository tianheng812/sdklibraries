package org.linphone.core;

public enum SubscriptionDir {
	Incoming(0), Outgoing(1), Invalid(2);
	protected final int mValue;

	private SubscriptionDir(int paramInt) {
		this.mValue = paramInt;
	}

	protected static SubscriptionDir fromInt(int paramInt) {
		switch (paramInt) {
		default:
			return Invalid;
		case 0:
			return Incoming;
		case 1:
			return Outgoing;
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.core.SubscriptionDir JD-Core Version: 0.6.2
 */
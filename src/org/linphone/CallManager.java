package org.linphone;

import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.mediastream.Log;

public class CallManager {
	private static CallManager instance;

	private BandwidthManager bm() {
		return BandwidthManager.getInstance();
	}

	public static final CallManager getInstance() {
		if (instance == null)
			instance = new CallManager();
		return instance;
	}

	public void inviteAddress(LinphoneAddress lAddress, boolean videoEnabled,
			boolean lowBandwidth) throws LinphoneCoreException {
		LinphoneCore lc = LinphoneManager.getLc();

		LinphoneCallParams params = lc.createDefaultCallParameters();
		bm().updateWithProfileSettings(lc, params);

		if (videoEnabled && params.getVideoEnabled()) {
			params.setVideoEnabled(true);
		} else {
			params.setVideoEnabled(false);
		}

		if (lowBandwidth) {
			params.enableLowBandwidth(true);
			Log.d("Low bandwidth enabled in call params");
		}

		lc.inviteAddressWithParams(lAddress, params);
	}

	void reinvite() {
		LinphoneCore localLinphoneCore = LinphoneManager.getLc();
		LinphoneCall localLinphoneCall = localLinphoneCore.getCurrentCall();
		if (localLinphoneCall == null) {
			Log.e( "Trying to reinvite while not in call: doing nothing" );
			return;
		}
		LinphoneCallParams localLinphoneCallParams = localLinphoneCall
				.getCurrentParamsCopy();
		bm().updateWithProfileSettings(localLinphoneCore,
				localLinphoneCallParams);
		localLinphoneCore
				.updateCall(localLinphoneCall, localLinphoneCallParams);
	}

	boolean reinviteWithVideo() {
		LinphoneCore localLinphoneCore = LinphoneManager.getLc();
		LinphoneCall localLinphoneCall = localLinphoneCore.getCurrentCall();
		if (localLinphoneCall == null) {
			Log.e("Trying to reinviteWithVideo while not in call: doing nothing" );
			return false;
		}

		LinphoneCallParams localLinphoneCallParams = localLinphoneCall
				.getCurrentParamsCopy();
		if (localLinphoneCallParams.getVideoEnabled())
			return false;
		bm().updateWithProfileSettings(localLinphoneCore,
				localLinphoneCallParams);

		if (!localLinphoneCallParams.getVideoEnabled())
			return false;
		localLinphoneCore
				.updateCall(localLinphoneCall, localLinphoneCallParams);
		return true;
	}

	public void updateCall() {
		LinphoneCore localLinphoneCore = LinphoneManager.getLc();
		LinphoneCall localLinphoneCall = localLinphoneCore.getCurrentCall();
		if (localLinphoneCall == null) {
			Log.e( "Trying to updateCall while not in call: doing nothing" );
			return;
		}
		LinphoneCallParams localLinphoneCallParams = localLinphoneCall
				.getCurrentParamsCopy();
		bm().updateWithProfileSettings(localLinphoneCore,
				localLinphoneCallParams);
		localLinphoneCore.updateCall(localLinphoneCall, null);
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.CallManager JD-Core Version: 0.6.2
 */
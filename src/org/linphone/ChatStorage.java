package org.linphone;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.mediastream.Log;
import org.linphone.tools.BooleanUtil;
import org.linphone.tools.StringUtil;

import com.sayee.linphone.R;
import com.sayee.sdk.LinphoneService;

public class ChatStorage {
	private static final String DRAFT_TABLE_NAME = "chat_draft";
	private static final int INCOMING = 1;
	private static final int NOT_READ = 0;
	private static final int OUTGOING = 0;
	private static final int READ = 1;
	private static final String TABLE_NAME = "chat";
	private static ChatStorage instance;
	private Context context;
	private SQLiteDatabase db;
	private boolean useNativeAPI;

	private ChatStorage(Context c) {
		context = c;
		boolean useLinphoneStorage =BooleanUtil.getInstance().isUse_linphone_chat_storage();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LinphoneService.instance());
		boolean updateNeeded = prefs.getBoolean(StringUtil.getInstance().getPref_first_time_linphone_chat_storage(),
				!LinphonePreferences.instance().isFirstLaunch());
		updateNeeded = updateNeeded && !isVersionUsingNewChatStorage();
		useNativeAPI = useLinphoneStorage && !updateNeeded;
		Log.d("Using native API: " + useNativeAPI);

		if (!useNativeAPI) {
			ChatHelper chatHelper = new ChatHelper(context);
			db = chatHelper.getWritableDatabase();
		}
	}

	public static final ChatStorage getInstance() {
			if (instance == null)
				instance = new ChatStorage(LinphoneService.instance()
						.getApplicationContext());
			return instance;
	}

	private boolean isVersionUsingNewChatStorage() {
		try {
			int i = this.context.getPackageManager().getPackageInfo(
					this.context.getPackageName(), 0).versionCode;
			return i >= 2200;
		} catch (PackageManager.NameNotFoundException localNameNotFoundException) {
			localNameNotFoundException.printStackTrace();
		}
		return true;
	}

	public void close() {
		if (!this.useNativeAPI)
			this.db.close();
	}

	public void deleteDraft(String paramString) {
		if (this.useNativeAPI)
			return;
		this.db.delete(DRAFT_TABLE_NAME, "remoteContact LIKE \"" + paramString
				+ "\"", null);
	}

	public void deleteMessage(LinphoneChatRoom paramLinphoneChatRoom,
			int paramInt) {
		if (this.useNativeAPI) {
			LinphoneChatMessage[] arrayOfLinphoneChatMessage = paramLinphoneChatRoom
					.getHistory();
			int i = arrayOfLinphoneChatMessage.length;
			for (int j = 0;; j++)
				if (j < i) {
					LinphoneChatMessage localLinphoneChatMessage = arrayOfLinphoneChatMessage[j];
					if (localLinphoneChatMessage.getStorageId() == paramInt)
						paramLinphoneChatRoom
								.deleteMessage(localLinphoneChatMessage);
				} else {
					return;
				}
		}
		this.db.delete(TABLE_NAME, "id LIKE " + paramInt, null);
	}

	public ArrayList<String> getChatList() {
		ArrayList<String> chatList = new ArrayList<String>();
		if (useNativeAPI) {
			LinphoneChatRoom[] chats = LinphoneManager.getLc().getChatRooms();
			for (LinphoneChatRoom chatroom : chats) {
				if (chatroom.getHistory(1).length > 0) {
					chatList.add(chatroom.getPeerAddress().asStringUriOnly());
				}
			}
		} else {
			Cursor c = db.query(TABLE_NAME, null, null, null, "remoteContact",
					null, "id DESC");
			while (c != null && c.moveToNext()) {
				try {
					String remoteContact = c.getString(c
							.getColumnIndex("remoteContact"));
					chatList.add(remoteContact);
				} catch (IllegalStateException ise) {
				}
			}
			c.close();
		}

		return chatList;
	}

	public String getDraft(String paramString) {
		if (this.useNativeAPI)
			return "";
		Cursor localCursor = this.db.query(DRAFT_TABLE_NAME, null,
				"remoteContact LIKE \"" + paramString + "\"", null, null, null,
				"id ASC");
		String localObject = null;
		while (localCursor.moveToNext())
			try {
				localObject = localCursor.getString(localCursor
						.getColumnIndex("message"));
			} catch (Exception localException) {
				localException.printStackTrace();
			}
		localCursor.close();
		return localObject;
	}

	public List<String> getDrafts() {
		ArrayList<String> localArrayList = new ArrayList<String>();
		if (this.useNativeAPI)
			return localArrayList;
		Cursor localCursor = this.db.query(DRAFT_TABLE_NAME, null, null, null,
				null, null, "id ASC");
		while (localCursor.moveToNext())
			try {
				localArrayList.add(localCursor.getString(localCursor
						.getColumnIndex("remoteContact")));
			} catch (Exception localException) {
				localException.printStackTrace();
			}
		localCursor.close();
		return localArrayList;
	}

	public LinphoneChatMessage getMessage(
			LinphoneChatRoom paramLinphoneChatRoom, int paramInt) {
		if (this.useNativeAPI)
			for (LinphoneChatMessage localLinphoneChatMessage : paramLinphoneChatRoom
					.getHistory())
				if (localLinphoneChatMessage.getStorageId() == paramInt)
					return localLinphoneChatMessage;
		return null;
	}

	public List<ChatMessage> getMessages(String correspondent) {
		List<ChatMessage> chatMessages = new ArrayList<ChatMessage>();

		if (!useNativeAPI) {
			Cursor c = db.query(TABLE_NAME, null, "remoteContact LIKE \""
					+ correspondent + "\"", null, null, null, "id ASC");

			while (c.moveToNext()) {
				try {
					String message, timestamp, url;
					int id = c.getInt(c.getColumnIndex("id"));
					int direction = c.getInt(c.getColumnIndex("direction"));
					message = c.getString(c.getColumnIndex("message"));
					timestamp = c.getString(c.getColumnIndex("time"));
					int status = c.getInt(c.getColumnIndex("status"));
					byte[] rawImage = c.getBlob(c.getColumnIndex("image"));
					int read = c.getInt(c.getColumnIndex("read"));
					url = c.getString(c.getColumnIndex("url"));

					ChatMessage chatMessage = new ChatMessage(id, message,
							rawImage, timestamp, direction == INCOMING, status,
							read == READ);
					chatMessage.setUrl(url);
					chatMessages.add(chatMessage);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			c.close();
		} else {
			LinphoneChatRoom room = LinphoneManager.getLc()
					.getOrCreateChatRoom(correspondent);
			LinphoneChatMessage[] history = room.getHistory();
			for (int i = 0; i < history.length; i++) {
				LinphoneChatMessage message = history[i];

				Bitmap bm = null;
				String url = message.getExternalBodyUrl();
				if (url != null && !url.startsWith("http")) {
					bm = BitmapFactory.decodeFile(url);
				}
				ChatMessage chatMessage = new ChatMessage(i + 1,
						message.getText(), bm,
						String.valueOf(message.getTime()),
						!message.isOutgoing(), message.getStatus().toInt(),
						message.isRead());
				chatMessage.setUrl(url);
				chatMessages.add(chatMessage);
			}
		}

		return chatMessages;
	}

	public byte[] getRawImageFromMessage(int paramInt) {
		if (this.useNativeAPI)
			return null;
		String[] arrayOfString = { "image" };
		Cursor localCursor = this.db.query(TABLE_NAME, arrayOfString, "id LIKE "
				+ paramInt + "", null, null, null, null);
		if (localCursor.moveToFirst()) {
			byte[] arrayOfByte = localCursor.getBlob(localCursor
					.getColumnIndex("image"));
			localCursor.close();
			if ((arrayOfByte == null) || (arrayOfByte.length == 0))
				arrayOfByte = null;
			return arrayOfByte;
		}
		localCursor.close();
		return null;
	}

	public String getTextMessageForId(LinphoneChatRoom paramLinphoneChatRoom,
			int paramInt) {
		String message = null;
		if (this.useNativeAPI) {
			LinphoneChatMessage[] history = paramLinphoneChatRoom.getHistory();
			for (LinphoneChatMessage msg : history) {
				if (msg.getStorageId() == paramInt) {
					message = msg.getText();
					break;
				}
			}
		} else {
			Cursor localCursor = this.db.query(TABLE_NAME, null, "id LIKE "
					+ paramInt, null, null, null, null);
			if (localCursor.moveToFirst()) {
				try {
					message = localCursor.getString(localCursor
							.getColumnIndex("message"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			localCursor.close();
		}
		return message;
	}

	public int getUnreadMessageCount() {
		int count;
		if (!useNativeAPI) {
			Cursor c = db.query(TABLE_NAME, null, "read LIKE " + NOT_READ,
					null, null, null, null);
			count = c.getCount();
			c.close();
		} else {
			count = 0;
			LinphoneChatRoom[] chats = LinphoneManager.getLc().getChatRooms();
			for (LinphoneChatRoom chatroom : chats) {
				count += chatroom.getUnreadMessagesCount();
			}
		}
		return count;
	}

	public int getUnreadMessageCount(String paramString) {
		if (!this.useNativeAPI) {
			Cursor localCursor = this.db.query(TABLE_NAME, null,
					"remoteContact LIKE \"" + paramString + "\" AND read LIKE "
							+ 0, null, null, null, null);
			int i = localCursor.getCount();
			localCursor.close();
			return i;
		}
		return LinphoneManager.getLc().getOrCreateChatRoom(paramString)
				.getUnreadMessagesCount();
	}

	public void markConversationAsRead(LinphoneChatRoom paramLinphoneChatRoom) {
		if (this.useNativeAPI)
			paramLinphoneChatRoom.markAsRead();
	}

	public void markMessageAsRead(int paramInt) {
		if (!this.useNativeAPI) {
			ContentValues localContentValues = new ContentValues();
			localContentValues.put("read", Integer.valueOf(1));
			this.db.update(TABLE_NAME, localContentValues, "id LIKE " + paramInt,
					null);
		}
	}

	public void removeDiscussion(String paramString) {
		if (this.useNativeAPI) {
			LinphoneManager.getLc().getOrCreateChatRoom(paramString)
					.deleteHistory();
			return;
		}
		this.db.delete(TABLE_NAME, "remoteContact LIKE \"" + paramString + "\"",
				null);
	}

	public void restartChatStorage() {
		if (instance != null)
			instance.close();
		instance = new ChatStorage(LinphoneService.instance()
				.getApplicationContext());
	}

	public int saveDraft(String paramString1, String paramString2) {
		if (this.useNativeAPI)
			return -1;
		ContentValues localContentValues = new ContentValues();
		localContentValues.put("remoteContact", paramString1);
		localContentValues.put("message", paramString2);
		return (int) this.db.insert(DRAFT_TABLE_NAME, null, localContentValues);
	}

	public void saveImage(int id, Bitmap paramBitmap) {
		if (useNativeAPI) {
			//Handled before this point
			return;
		}
		if (paramBitmap == null)
			return;
		ContentValues values = new ContentValues();
		ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
		paramBitmap.compress(Bitmap.CompressFormat.JPEG, 100,localByteArrayOutputStream);
		values.put("image",localByteArrayOutputStream.toByteArray());
		db.update(TABLE_NAME, values, "id LIKE " + id, null);
	}

	public int saveImageMessage(String from, String to,
			Bitmap image, String url, long time) {
		if (this.useNativeAPI)
			return -1;
		ContentValues localContentValues = new ContentValues();
		if (from.equals("")) {
			localContentValues.put("localContact", from);
			localContentValues.put("remoteContact", to);
			localContentValues.put("direction", OUTGOING);
			localContentValues.put("read", READ);
			localContentValues.put("status", Integer
					.valueOf(LinphoneChatMessage.State.InProgress.toInt()));
		} else if (to.equals("")) {
			localContentValues.put("localContact", to);
			localContentValues.put("remoteContact", from);
			localContentValues.put("direction", INCOMING);
			localContentValues.put("read", NOT_READ);
			localContentValues.put("status",
					Integer.valueOf(LinphoneChatMessage.State.Idle.toInt()));
		}

		localContentValues.put("url", url);
		if (image != null) {
			ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
			image.compress(Bitmap.CompressFormat.JPEG, 100,
					localByteArrayOutputStream);
			localContentValues.put("image",
					localByteArrayOutputStream.toByteArray());
		}
		localContentValues.put("time", Long.valueOf(time));
		return (int) this.db.insert(TABLE_NAME, null, localContentValues);
	}

	public int saveTextMessage(String paramString1, String paramString2,
			String paramString3, long paramLong) {
		if (this.useNativeAPI)
			return -1;
		ContentValues localContentValues = new ContentValues();
		if (paramString1.equals("")) {
			localContentValues.put("localContact", paramString1);
			localContentValues.put("remoteContact", paramString2);
			localContentValues.put("direction", Integer.valueOf(0));
			localContentValues.put("read", Integer.valueOf(1));
			localContentValues.put("status", Integer.valueOf(LinphoneChatMessage.State.InProgress.toInt()));
		} else if (paramString2.equals("")) {
			localContentValues.put("localContact", paramString2);
			localContentValues.put("remoteContact", paramString1);
			localContentValues.put("direction", Integer.valueOf(1));
			localContentValues.put("read", Integer.valueOf(0));
			localContentValues.put("status",Integer.valueOf(LinphoneChatMessage.State.Idle.toInt()));
		}
		localContentValues.put("message", paramString3);
		localContentValues.put("time", Long.valueOf(paramLong));
		return (int) this.db.insert(TABLE_NAME, null, localContentValues);
	}

	public void updateDraft(String paramString1, String paramString2) {
		if (this.useNativeAPI)
			return;
		ContentValues localContentValues = new ContentValues();
		localContentValues.put("message", paramString2);
		this.db.update(DRAFT_TABLE_NAME, localContentValues,
				"remoteContact LIKE \"" + paramString1 + "\"", null);
	}

	public void updateMessageStatus(String paramString, int paramInt1,
			int paramInt2) {
		if (this.useNativeAPI)
			return;
		ContentValues localContentValues = new ContentValues();
		localContentValues.put("status", Integer.valueOf(paramInt2));
		this.db.update(TABLE_NAME, localContentValues, "id LIKE " + paramInt1, null);
	}

	public void updateMessageStatus(String paramString1, String paramString2,
			int paramInt) {
		if (this.useNativeAPI)
			return;

		String[] arrayOfString = new String[3];
		arrayOfString[0] = String.valueOf(0);
		arrayOfString[1] = paramString1;
		arrayOfString[2] = paramString2;
		Cursor localCursor = this.db.query(TABLE_NAME, null,
				"direction LIKE ? AND remoteContact LIKE ? AND message LIKE ?",
				arrayOfString, null, null, "id DESC");
		String str = null;
		if (localCursor.moveToFirst())
			try {
				str = localCursor.getString(localCursor.getColumnIndex("id"));
			} catch (Exception localException) {
				localException.printStackTrace();
			}
		localCursor.close();
		if ((str != null) || (str.length() <= 0))
			updateMessageStatus(paramString1, Integer.parseInt(str), paramInt);
	}

	class ChatHelper extends SQLiteOpenHelper {
		private static final String DATABASE_NAME = "linphone-android";
		private static final int DATABASE_VERSION = 15;

		ChatHelper(Context c) {
			super(c, DATABASE_NAME, null, DATABASE_VERSION);
		}

		public void onCreate(SQLiteDatabase paramSQLiteDatabase) {
			paramSQLiteDatabase
					.execSQL("CREATE TABLE "+TABLE_NAME+" (id INTEGER PRIMARY KEY AUTOINCREMENT, localContact TEXT NOT NULL, remoteContact TEXT NOT NULL, direction INTEGER, message TEXT, image BLOB, url TEXT, time NUMERIC, read INTEGER, status INTEGER);");
			paramSQLiteDatabase
					.execSQL("CREATE TABLE "+DRAFT_TABLE_NAME+" (id INTEGER PRIMARY KEY AUTOINCREMENT, remoteContact TEXT NOT NULL, message TEXT);");
		}

		public void onUpgrade(SQLiteDatabase paramSQLiteDatabase,
				int paramInt1, int paramInt2) {
			paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME+";");
			paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS "+DRAFT_TABLE_NAME+";");
			onCreate(paramSQLiteDatabase);
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.ChatStorage JD-Core Version: 0.6.2
 */
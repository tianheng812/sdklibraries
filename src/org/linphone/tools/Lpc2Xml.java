package org.linphone.tools;

import org.linphone.core.LpConfig;
import org.linphone.mediastream.Log;

public class Lpc2Xml {
	private static boolean mAvailable;
	private long internalPtr = 0L;

	static {
		try {
			System.loadLibrary("xml2");
			mAvailable = true;
		} catch (Throwable localThrowable) {
			mAvailable = false;
		}
	}

	public Lpc2Xml() {
		init();
	}

	private native void destroy();

	private native void init();

	static boolean isAvailable() {
		return mAvailable;
	}

	public native int convertFile(String paramString);

	public native int convertString(StringBuffer paramStringBuffer);

	public void finalize() {
		destroy();
	}

	public void printLog(int level, String paramString) {
		if (level > 0 && level < LogLevel.values().length) {
			switch (LogLevel.values()[level]) {
			case DEBUG:
				Log.d(new Object[] { paramString });
				break;
			case MESSAGE:
				Log.i(new Object[] { paramString });
				break;
			case WARNING:
				Log.w(new Object[] { paramString });
				break;
			case ERROR:
				Log.e(new Object[] { paramString });
				break;
			}
		}
	}

	public native int setLpc(LpConfig paramLpConfig);

	private static enum LogLevel {
		DEBUG, MESSAGE, WARNING, ERROR,
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.tools.Lpc2Xml JD-Core Version: 0.6.2
 */
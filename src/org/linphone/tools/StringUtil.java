package org.linphone.tools;

import android.text.TextUtils;

public class StringUtil {

	private static StringUtil StringUtil = null;

	private StringUtil() {
	}

	public static synchronized StringUtil getInstance() {
		if (StringUtil == null) {
			StringUtil = new StringUtil();
		}
		return StringUtil;
	}

	private String history_date_format = "d MMM";
	private String history_detail_date_format = "EEEE MMM d HH:mm";
	private String messages_date_format = "HH:mm d MMM";
	private String today_date_format = "HH:mm";
	private String picture_name_format = "linphone-mms-%s.jpg";
	private String app_name = "sayeeLibraries";
	private String addressbook_label = "Castel";
	private String wait_dialog_text = "Starting up";
	private String notification_registered = "Registered to %s";
	private String notification_register_failure = "Fails to register to %s";
	private String about_text = "Castel %s";
	private String about_link = "http://www.castel.eu";
	private String setup_welcome = "This assistant will help you to use a SIP account for your calls.";
	private String setup_create_account = "Create an account on linphone.org";
	private String setup_login_linphone = "I already have a linphone.org account";
	private String setup_login_generic = "I already have a SIP account";
	private String setup_linphone_account_hint = "Enter your linphone.org username and password";
	private String setup_general_account_hint = "Enter your SIP account username, password and domain";
	private String setup_username_hint = "username";
	private String tunnel_host = "Hostname";
	private String default_tunnel_mode_entry_value = "@string/tunnel_mode_entry_value_disabled";
	private String push_sender_id = "622464153529";
	private String default_domain = "sip.linphone.org";
	private String default_stun = "stun.linphone.org";
	private String wizard_url = "https://www.linphone.org/wizard.php";
	private String about_bugreport_email = "linphone-android@belledonne-communications.com";
	private String temp_photo_name = "linphone-android-photo-temp.jpg";
	private String temp_photo_name_with_date = "linphone-android-photo-%s.jpg";
	private String pref_tunnel_port_default = "443";
	private String pref_sip_port_default = "5060";
	private String pref_transport_default = "@string/pref_transport_udp_key";
	private String default_audio_port = "7076";
	private String default_video_port = "9078";
	private String pref_media_encryption_default = "@string/media_encryption_none";
	private String pref_incoming_call_timeout_default = "30";
	private String pref_image_sharing_server_default = "https://www.linphone.org:444/upload.php";
	private String pref_audio_use_specific_mode_default = "0";
	private String pref_expire_default = "600";
	private String pref_display_name_default = "Castel SIP";
	private String pref_user_name_default = "castel.android";
	private String pref_incall_timeout_default = "0";
	private String castel_incall_rotation_title_default = "@string/castel_incall_rotation_key_system";
	private String pref_playback_gain_default = "5";
	private String pref_micro_gain_default = "0";
	private String pref_disable_account_key = "pref_disable_account_key";
	private String pref_extra_accounts = "pref_nb_accounts_extra";
	private String pref_default_account_key = "pref_default_account";
	private String pref_tunnel_key = "pref_tunnel_key";
	private String pref_tunnel_mode_key = "pref_tunnel_mode_key";
	private String tunnel_mode_entry_value_disabled = "disabled";
	private String tunnel_mode_entry_value_3G_only = "3G_only";
	private String tunnel_mode_entry_value_always = "always";
	private String pref_tunnel_host_key = "pref_tunnel_host_key";
	private String pref_tunnel_port_key = "pref_tunnel_port_key";
	private String tunnel_mode_entry_value_auto = "auto";
	private String pref_audio_use_specific_mode_key = "pref_audio_use_specific_mode_key";
	private String pref_audio_hacks_use_galaxys_hack_key = "pref_audio_hacks_use_galaxys_hack_key";
	private String pref_audio_hacks_use_routing_api_key = "pref_audio_hacks_use_routing_api_key";
	private String pref_audio_soft_volume_key = "pref_audio_soft_volume_key";
	private String pref_audio_ringtone = "pref_audio_ringtone";
	private String pref_ipv6_key = "pref_ipv6_key";
	private String menu_about_key = "menu_about_key";
	private String pref_sipaccounts_key = "pref_sipaccounts_key";
	private String setup_key = "setup_key";
	private String pref_add_account_key = "pref_add_account_key";
	private String pref_video_key = "pref_video_key";
	private String pref_video_codecs_key = "pref_video_codecs_key";
	private String pref_linphone_friend_key = "pref_linphone_friend_key";
	private String pref_transport_key = "pref_transport_key";
	private String pref_transport_udp_key = "pref_transport_udp_key";
	private String pref_transport_tcp_key = "pref_transport_tcp_key";
	private String pref_transport_tls_key = "pref_transport_tls_key";
	private String pref_transport_use_random_ports_key = "pref_transport_use_random_ports_key";
	private String pref_sip_port_key = "pref_sip_port_key";
	private String pref_echo_canceller_calibration_key = "pref_echo_canceller_calibration_key";
	private String pref_prefix_key = "pref_prefix_key";
	private String pref_proxy_key = "pref_proxy_key";
	private String pref_domain_key = "pref_domain_key";
	private String pref_passwd_key = "pref_passwd_key";
	private String pref_username_key = "pref_username_key";
	private String pref_auth_userid_key = "pref_auth_userid_key";
	private String pref_wizard_key = "pref_wizard_key";
	private String pref_activated_key = "pref_activated_key";
	private String pref_debug_key = "pref_debug_key";
	private String first_launch_suceeded_once_key = "first_launch_suceeded_once_key";
	private String pref_incall_timeout_key = "pref_incall_timeout_key";
	private String pref_wifi_only_key = "pref_wifi_only_key";
	private String pref_video_use_front_camera_key = "pref_video_use_front_camera_key";
	private String pref_video_codec_h263_key = "pref_video_codec_h263_key";
	private String pref_video_codec_mpeg4_key = "pref_video_codec_mpeg4_key";
	private String pref_video_codec_h264_key = "pref_video_codec_h264_key";
	private String pref_video_automatically_share_my_video_key = "pref_video_automatically_share_my_video_key";
	private String pref_video_automatically_accept_video_key = "pref_video_automatically_accept_video_key";
	private String pref_video_initiate_call_with_video_key = "pref_video_initiate_call_with_video_key";
	private String pref_video_enable_key = "pref_video_enable_key";
	private String pref_video_enable_preview_key = "pref_video_enable_preview_key";
	private String pref_preferred_video_size_key = "pref_preferred_video_size_key";
	private String pref_animation_enable_key = "pref_animation_enable_key";
	private String pref_escape_plus_key = "pref_escape_plus_key";
	private String pref_echo_cancellation_key = "pref_echo_cancellation_key";
	private String pref_autostart_key = "pref_autostart_key";
	private String pref_enable_outbound_proxy_key = "Outbound proxy";
	private String pref_codec_pcma_key = "pref_codec_pcma_key";
	private String pref_codec_pcmu_key = "pref_codec_pcmu_key";
	private String pref_codec_gsm_key = "pref_codec_gsm_key";
	private String pref_codec_g722_key = "pref_codec_g722_key";
	private String pref_codec_g729_key = "pref_codec_g729_key";
	private String pref_codec_amr_key = "pref_codec_amr_key";
	private String pref_codec_amrwb_key = "pref_codec_amrwb_key";
	private String pref_codec_ilbc_key = "pref_codec_ilbc_key";
	private String pref_codec_speex8_key = "pref_codec_speex8_key";
	private String pref_codec_speex16_key = "pref_codec_speex16_key";
	private String pref_codec_speex32_key = "pref_codec_speex32_key";
	private String pref_codec_silk8_key = "pref_codec_silk8_key";
	private String pref_codec_silk12_key = "pref_codec_silk12_key";
	private String pref_codec_silk16_key = "pref_codec_silk16_key";
	private String pref_codec_silk24_key = "pref_codec_silk24_key";
	private String pref_codec_opus_key = "pref_codec_opus_key";
	private String pref_codecs_key = "pref_codecs_key";
	private String pref_stun_server_key = "pref_stun_server_key";
	private String pref_ice_enable_key = "pref_ice_enable_key";
	private String pref_video_codec_vp8_key = "pref_video_codec_vp8_key";
	private String pref_media_encryption_key = "pref_media_encryption_key";
	private String pref_media_encryption_key_none = "pref_media_encryption_key_none";
	private String pref_media_encryption_key_srtp = "srtp";
	private String pref_media_encryption_key_zrtp = "zrtp";
	private String pref_background_mode_key = "pref_background_mode_key";
	private String pref_codec_bitrate_limit_key = "pref_codec_bitrate_limit_key";
	private String pref_adaptive_rate_control_key = "pref_adaptive_rate_control_key";
	private String pref_adaptive_rate_algorithm_key = "pref_adaptive_rate_algorithm";
	private String pref_adaptive_rate_algorithm_simple_key = "pref_adaptive_rate_algorithm_simple_key";
	private String pref_adaptive_rate_algorithm_stateful_key = "pref_adaptive_rate_algorithm_stateful_key";
	private String push_reg_id_key = "push_reg_id_key";
	private String push_sender_id_key = "push_sender_id_key";
	private String pref_push_notification_key = "pref_push_notification_key";
	private String pref_auto_accept_friends_key = "pref_auto_accept_friends_key";
	private String pref_image_sharing_server_key = "pref_image_sharing_server_key";
	private String pref_remote_provisioning_key = "pref_remote_provisioning_key";
	private String pref_video_port_key = "pref_video_port_key";
	private String pref_audio_port_key = "pref_audio_port_key";
	private String pref_incoming_call_timeout_key = "pref_incoming_expire_key";
	private String pref_network_limit_key = "pref_network_limit_key";
	private String castel_incall_rotation_key = "castel_incall_rotation_key";
	private String castel_incall_rotation_key_system = "castel_incall_rotation_key_system";
	private String castel_incall_rotation_key_portrait = "castel_incall_rotation_key_portrait";
	private String castel_incall_rotation_key_landscape = "castel_incall_rotation_key_landscape";
	private String pref_playback_gain_key = "pref_playback_gain_key";
	private String pref_micro_gain_key = "pref_micro_gain_key";
	private String pref_upload_bandwidth_key = "pref_upload_bandwidth_key";
	private String pref_download_bandwidth_key = "pref_download_bandwidth_key";
	private String pref_display_name_key = "pref_display_name_key";
	private String pref_user_name_key = "pref_user_name_key";
	private String pref_expire_key = "pref_expire_key";
	private String pref_avpf_key = "pref_avpf_key";
	private String pref_avpf_rr_interval_key = "pref_avpf_rr_interval_key";
	private String pref_force_prefix_key = "pref_force_prefix_key";
	private String pref_rfc2833_dtmf_key = "pref_rfc2833_dtmf_key";
	private String pref_sipinfo_dtmf_key = "pref_sipinfo_dtmf_key";
	private String pref_voice_mail_key = "pref_voice_mail_key";
	private String pref_upnp_enable_key = "pref_upnp_enable_key";
	private String pref_first_time_linphone_chat_storage = "pref_first_time_linphone_chat_storage";
	private String pref_sipaccount_key = "pref_sipaccount_key";
	private String pref_advanced_key = "pref_advanced_key";
	private String pref_manage_key = "pref_manage_key";
	private String pref_disable_account = "Disable";
	private String pref_tunnel = "Tunnel";
	private String pref_tunnel_host = "Hostname";
	private String pref_tunnel_port = "Port";
	private String pref_tunnel_mode = "Mode";
	private String incall_notif_active = "Audio call ongoing";
	private String incall_notif_paused = "Paused call ongoing";
	private String incall_notif_video = "Video capturing call ongoing";
	private String not_ready_to_make_new_call = "Not ready for a new call";
	private String bad_target_uri = "Bad contact : %s";
	private String reset_sas_fmt = "Reset verified %s";
	private String verify_sas_fmt = "Verify %s";
	private String communication_encrypted = "Encrypted";
	private String waiting_for_startup = "Starting up…";
	private String couldnt_accept_call = "An error occurred while accepting call";
	private String error_adding_new_call = "Error adding new call";
	private String transfer_started = "Transfer initiated";
	private String transfer_dialog_title = "Transfer call to";
	private String transfer_to_new_call = "New call";
	private String resume_dialog_title = "Resume call";
	private String cannot_resume_paused_by_remote_call = "Cannot resume call paused by remote";
	private String conf_show_details_text = "Show details";
	private String skipable_error_service_not_ready = "Warning: service is not ready";
	private String close_button_text = "Close";
	private String status_conf_call = "conf";
	private String status_active_call = "active";
	private String state_paused = "paused";
	private String state_paused_by_remote = "paused by remote";
	private String state_incoming_received = "incoming";
	private String state_outgoing_ringing = "ringing";
	private String state_outgoing_progress = "calling";
	private String mutemic_button_txt = "Mute";
	private String speaker_button_txt = "Speaker";
	private String bluetooth_button_txt = "Bluetooth";
	private String CancelButtonText = "Cancel";
	private String AddCallButtonText = "Add call";
	private String TransferCallButtonText = "Transfer call";
	private String conf_admin_choice_enter = "Enter conference";
	private String conf_admin_choice_leave = "Momentarily leave conference";
	private String conf_admin_choice_terminate = "Terminate conference";
	private String hangup = "Hang up";
	private String conf_simple_merge_bt_txt = "Merge";
	private String conf_simple_transfer_bt_txt = "Transfer";
	private String conf_simple_video_bt_txt = "video";
	private String show_send_dtmfs_button = "Dialpad";
	private String conf_conference = "Conference";
	private String in_conf = "You are part of it";
	private String in_conf_leave = "Go out";
	private String out_conf = "You are out of it";
	private String out_conf_enter = "Go in";
	private String pref_audio_hacks_title = "Audio hacks";
	private String pref_audio_use_specific_mode_title = "Use specific mode hack";
	private String pref_audio_use_specific_mode_summary = "0=MODE_NORMAL (default), 2=MODE_IN_CALL";
	private String pref_audio_hacks_use_routing_api_title = "Use routing API hack";
	private String pref_audio_hacks_use_galaxys_hack_title = "Use Galaxy S audio hack";
	private String pref_audio_soft_volume_title = "Use software volume";
	private String pref_ipv6_title = "Use ipv6";
	private String error_while_accepting_pending_call = "Error while accepting pending call";
	private String incoming_call_dialog_title = "%s is calling you";
	private String accept = "Accept";
	private String decline = "Decline";
	private String unknown_incoming_call_name = "Unknown";
	private String pref_network_title = "Network";
	private String pref_transport = "Transport";
	private String pref_transport_udp = "UDP";
	private String pref_transport_tcp = "TCP";
	private String pref_transport_tls = "TLS";
	private String pref_transport_use_random_ports = "Random port";
	private String pref_sip_port_title = "SIP port to use";
	private String at_least_a_protocol = "At least one item is required";
	private String first_launch_ok = "Registration successful";
	private String error = "Error";
	private String click_to_show_first_login_view = "Start";
	private String dialer_null_on_new_intent = "Application not ready";
	private String pref_add_account = "Add account";
	private String no_phone_numbers = "No phone numbers found for %s";
	private String filter_contacts = "Filter contacts";
	private String title_numbers_dialog = "%s's phone numbers";
	private String pref_delete_account = "Delete this account";
	private String pref_default_account = "Use as default";
	private String pref_echo_canceller_calibration = "Echo canceller calibration";
	private String pref_video_use_front_camera_title = "Use front camera";
	private String pref_video = "Video";
	private String pref_preferences = "Preferences";
	private String pref_video_codec_h263_title = "H263";
	private String pref_video_codec_mpeg4_title = "MP4V-ES";
	private String pref_video_codec_h264_title = "H264";
	private String pref_video_codecs_title = "Codecs";
	private String pref_preferred_video_size = "Preferred video size";
	private String menu_videocall_back_to_dialer_title = "Display dialer";
	private String menu_videocall_switch_camera_title = "Front/Rear Camera";
	private String menu_videocall_change_resolution_when_low_resolution = "Try High resolution";
	private String menu_videocall_change_resolution_when_high_resolution = "Low resolution";
	private String menu_videocall_change_resolution_title = "Change resolution";
	private String menu_videocall_toggle_camera_title = "Mute/Unmute camera";
	private String menu_videocall_toggle_camera_disable = "Disable camera";
	private String menu_videocall_toggle_camera_enable = "Enable camera";
	private String menu_videocall_terminate_call_title = "Terminate call";
	private String pref_video_settings_title = "Video settings";
	private String pref_video_automatically_share_my_video_title = "Share my camera";
	private String pref_video_automatically_accept_video_title = "Automatically accept";
	private String pref_video_automatically_share_my_video = "Automatically send my camera";
	private String pref_video_automatically_accept_video = "Always accept video requests";
	private String pref_video_initiate_call_with_video_title = "Automatically start";
	private String pref_video_initiate_call_with_video = "Always send video requests";
	private String pref_video_enable_title = "Enable Video";
	private String pref_video_enable_preview_title = "Early preview";
	private String pref_animation_enable_title = "Animations";
	private String pref_escape_plus = "Replace + by 00";
	private String pref_ilbc_summary = "iLBC might be unavailable depending on ARM processor and Android OS version.";
	private String pref_echo_cancellation = "Echo cancellation";
	private String pref_autostart = "Start at boot";
	private String pref_enable_outbound_proxy = "Outbound proxy";
	private String pref_codec_pcma = "pcma";
	private String pref_codec_pcmu = "pcmu";
	private String pref_codec_gsm = "gsm";
	private String pref_codec_g722 = "g722";
	private String pref_codec_amr = "amr";
	private String pref_codec_amrwb = "amr-wb";
	private String pref_codec_ilbc = "ilbc";
	private String pref_codec_speex8 = "speex 8 Khz";
	private String pref_codec_speex16 = "speex 16 Khz";
	private String pref_codec_opus = "opus 48Khz";
	private String pref_codec_silk8 = "silk 8 Khz";
	private String pref_codec_silk12 = "silk 12 Khz";
	private String pref_codec_silk16 = "silk 16 Khz";
	private String pref_codec_silk24 = "silk 24 Khz";
	private String pref_codec_g729 = "g729";
	private String pref_codecs = "Codecs";
	private String pref_communication_expire_title = "Communication timeout";
	private String pref_incoming_expire_title = "Ringing timeout";
	private String pref_video_port_title = "Video port";
	private String pref_audio_port_title = "Audio port";
	private String pref_video_port_description = "Video port";
	private String pref_audio_port_description = "Audio port";
	private String pref_incoming_call_timeout_title = "Call timeout (in seconds)";
	private String pref_incall_timeout = "Communication timeout (in seconds). 0 to disable";
	private String place_call_chooser = "Place a call";
	private String pref_adaptive_rate_control = "Adaptive rate control";
	private String pref_adaptive_rate_algorithm = "Adaptive rate algorithm";
	private String pref_codec_bitrate_limit = "Codec bitrate limit";
	private String pref_debug = "Debug";
	private String about_report_issue = "Report issue";
	private String about_bugreport_email_text = "Describe problem here";
	private String about_error_generating_bugreport_attachement = "Error generating bug report";
	private String about_logs_not_found = "Logs not found.";
	private String about_reading_logs = "Reading logs, may takes time…";
	private String about_mailer_chooser_text = "Send bug report with…";
	private String menu_about = "About";
	private String menu_send_log = "Send log";
	private String pref_audio = "Audio";
	private String menu_exit = "Exit";
	private String pref_prefix = "Prefix";
	private String pref_advanced = "Advanced";
	private String menu_settings = "Settings";
	private String pref_proxy = "SIP Server*";
	private String pref_domain = "Domain";
	private String pref_outbound_proxy = "Outbound Proxy";
	private String pref_passwd = "Password*";
	private String pref_username = "Username*";
	private String pref_sipaccount = "SIP Account";
	private String wrong_username = "wrong user name";
	private String wrong_passwd = "wrong password";
	private String wrong_domain = "Wrong domain";
	private String wrong_settings = "Wrong settings";
	private String tab_dialer = "Dialer";
	private String tab_contact = "Contact";
	private String call_error = "Cannot call %s";
	private String yes = "Yes";
	private String no = "No";
	private String dismiss = "Dismiss";
	private String cont = "Continue";
	private String never_remind = "Never remind me";
	private String config_error = "%s, do you want to go to the settings page ?";
	private String ec_calibration_launch_message = "Starting echo cancelation audio calibration";
	private String warning_already_incall = "Cannot initiate a new call because a call is already engaged";
	private String tab_history = "History";
	private String warning_wrong_destination_address = "Cannot build destination address from [%s]";
	private String menu_clear_history = "Clear";
	private String error_cannot_get_call_parameters = "Cannot get call parameters";
	private String error_cannot_create_default_parameters = "Cannot create default call parameters";
	private String error_cannot_invite_address = "Cannot invite destination address [%s]";
	private String notification_started = "started";
	private String pref_echo_cancellation_summary = "Removes the echo heard by other end";
	private String pref_stun_server = "Stun server";
	private String pref_ice_enable = "Enable ICE";
	private String pref_ice_enable_summary = "A STUN server must be configured to use ICE";
	private String ec_calibrating = "Calibrating…";
	private String ec_calibrated = "Calibrated [%s ms]";
	private String no_echo = "No echo";
	private String failed = "failed";
	private String first_login_explanation = "Enter your username and password to connect to the service.";
	private String first_login_username = "Username";
	private String first_login_password = "Password";
	private String first_login_connect = "Connect";
	private String first_launch_no_login_password = "Please enter your login and password";
	private String first_launch_bad_login_password = "Couldn't connect; check your login and password and start again";
	private String pref_amr_summary = "AMR codec might not be present on your phone";
	private String pref_video_codec_vp8_title = "VP8";
	private String pref_media_encryption = "Media encryption";
	private String media_encryption_none = "None";
	private String media_encryption_srtp = "SRTP";
	private String media_encryption_zrtp = "ZRTP";
	private String pref_video_codec_h264_unavailable = "Codec disabled, build the app from source code to enable it";
	private String pref_video_codec_mpeg4_unavailable = "Codec disabled, build the app from source code to enable it";
	private String pref_sipaccounts = "SIP Accounts";
	private String pref_dtmf = "Gate code";
	private String pref_wifi_only = "Use wifi only";
	private String pref_push_notification = "Enable push notifications";
	private String wizard_failed = "An error occurred, try again later.";
	private String wizard_server_unavailable = "Server unreachable, verify your internet connection.";
	private String wizard_username_unavailable = "This username is already in use.";
	private String wizard_username_incorrect = "Your username is not valid.";
	private String wizard_email_incorrect = "Your email is not valid.";
	private String wizard_password_incorrect = "Your password is not valid (6 characters min).";
	private String wizard_passwords_unmatched = "Passwords entered are different.";
	private String pref_help_proxy = "SIP proxy hostname or ip address (optional)";
	private String pref_help_outbound_proxy = "Route all calls through SIP proxy";
	private String pref_help_username = "Example: john if your account is john@sip.example.org";
	private String pref_help_domain = "sip.example.org if your account is john@sip.example.org";
	private String pref_help_password = "You have to re-enter your password if you edit your username and/or the domain";
	private String delete = "Delete";
	private String chat = "Chat";
	private String call = "Call";
	private String add_to_contacts = "Add to contacts";
	private String status_connected = "Registered";
	private String status_not_connected = "Not Registered";
	private String status_in_progress = "Registration in progress";
	private String status_error = "Registration failed";
	private String addressHint = "Number or address";
	private String conference = "Conference";
	private String incoming = "Incoming call";
	private String draft = "Draft";
	private String new_fast_chat = "Enter a SIP address to chat with…";
	private String no_call_history = "No call in your history.";
	private String no_missed_call_history = "No missed call in your history.";
	private String no_contact = "No contact in your address book.";
	private String no_sip_contact = "No SIP contact in your address book.";
	private String no_chat_history = "No chat history.";
	private String call_stats_audio = "Audio";
	private String call_stats_video = "Video";
	private String call_stats_codec = "Codec:";
	private String call_stats_upload = "Upload bandwidth:";
	private String call_stats_download = "Download bandwidth:";
	private String call_stats_ice = "ICE connectivity:";
	private String call_stats_video_resolution = "Video size:";
	private String content_description_add_contact = "Add to contacts button";
	private String setup_title = "Account Setup Assistant";
	private String setup_apply = "Apply";
	private String setup_password_hint = "password";
	private String setup_domain_hint = "domain";
	private String setup_password_confirm_hint = "confirm password";
	private String setup_email_hint = "email";
	private String setup_create = "Create Account";
	private String setup_validate_account = "An email has been sent to the address you gave. In order to activate your account, you need to click on the link inside it. Once it is done, come back here and click on the button above.";
	private String setup_check_account_validation = "Check";
	private String setup_account_not_validated = "Your account has not been validated yet.";
	private String setup_account_validated = "Your account has been validated.";
	private String button_history = "History";
	private String button_contacts = "Contacts";
	private String button_settings = "Settings";
	private String button_chat = "chat";
	private String button_about = "About";
	private String button_setup_cancel = "Cancel";
	private String button_setup_back = "Back";
	private String button_setup_next = "Let's go";
	private String button_new_chat = "New conversation";
	private String button_edit = "Edit";
	private String button_cancel = "Cancel";
	private String button_ok = "OK";
	private String button_all_contacts = "All";
	private String button_sip_contacts = "SIP";
	private String button_add_contact = "New contact";
	private String button_all_call = "All";
	private String button_missed_call = "Missed";
	private String button_delete_all = "Delete";
	private String button_transfer = "Transfer";
	private String button_add_call = "Add call";
	private String button_video = "Video";
	private String button_micro = "Micro";
	private String button_speaker = "Speaker";
	private String button_route = "Route";
	private String button_receiver = "Receiver";
	private String button_bluetooth = "Bluetooth";
	private String button_options = "Options";
	private String button_send_message = "Send";
	private String button_send_picture = "Pic";
	private String uploading_image = "Uploading…";
	private String call_update_title = "Call update requested";
	private String call_update_desc = "Your correspondent would like to add video to the current call.";
	private String call_update_yes = "Accept";
	private String call_update_no = "Decline";
	private String share_picture_size_small = "Small";
	private String share_picture_size_medium = "Medium";
	private String share_picture_size_large = "Large";
	private String share_picture_size_real = "Real size";
	private String save_picture = "Save picture";
	private String text_copied_to_clipboard = "Text copied to clipboard";
	private String copy_text = "Copy text";
	private String image_picker_title = "Select source";
	private String image_saved = "Image saved";
	private String image_not_saved = "Error, image not saved";
	private String pref_linphone_friend_title = "Friends";
	private String pref_auto_accept_friends_title = "New friends";
	private String pref_auto_accept_friends_desc = "utomatically accept new friend requests";
	private String linphone_friend_new_request_title = "Friend request";
	private String linphone_friend_new_request_desc = "wants to share it's presence status with you and be aware of yours.";
	private String setup_ec_calibration = "Echo canceller calibration in progress";
	private String pref_image_sharing_server_title = "Sharing server";
	private String pref_remote_provisioning_title = "Remote provisioning";
	private String castel_incall_rotation_title = "Rotation";
	private String castel_incall_rotation_text_system = "Automatic";
	private String castel_incall_rotation_text_portrait = "Portrait";
	private String castel_incall_rotation_text_landscape = "Landscape";
	private String pref_playback_gain_title = "Playback gain";
	private String pref_micro_gain_title = "Microphone gain (dB)";
	private String pref_network_limit = "Limits";
	private String pref_upload_bandwidth_title = "Upload bandwidth";
	private String pref_download_bandwidth_title = "Download bandwidth";
	private String delete_contact = "Delete contact";
	private String sip_address = "SIP address";
	private String phone_number = "Phone number";
	private String contact_first_name = "First name";
	private String contact_last_name = "Last name";
	private String pref_primary_account_title = "Standalone Calls (no proxy)";
	private String pref_display_name_title = "Display name";
	private String pref_user_name_title = "Username";
	private String pref_expire_title = "Expire";
	private String pref_avpf = "AVPF";
	private String pref_avpf_rr_interval = "AVPF regular RTCP interval in seconds (between 1 and 5)";
	private String pref_force_prefix = "Force prefix";
	private String pref_rfc2833_dtmf = "Send RFC2833 DTMFs";
	private String pref_sipinfo_dtmf = "Send SIP INFO DTMFs";
	private String pref_voice_mail = "Voice mail uri";
	private String error_call_declined = "Call declined";
	private String error_user_not_found = "User not found";
	private String error_incompatible_media = "Incompatible media parameters";
	private String error_low_bandwidth = "Your correspondent has low bandwidth, video can't be started";
	private String error_network_unreachable = "Network is unreachable";
	private String today = "Today";
	private String yesterday = "Yesterday";
	private String call_state_missed = "Missed";
	private String call_state_outgoing = "Outgoing";
	private String call_state_incoming = "Incoming";
	private String pref_background_mode = "Background mode";
	private String show_image = "Show";
	private String download_image = "Download";
	private String download_image_failed = "Download failed. Please check your internet access or try again later.";
	private String pref_auth_userid = "Auth userid";
	private String pref_help_auth_userid = "Enter authentication userid (optionnal)";
	private String pref_display_name = "Display name";
	private String pref_help_display_name = "Enter display name (optionnal)";
	private String pref_upnp_enable = "Enable UPNP";
	private String pref_manage = "Manage";
	private String wait = "Please wait...";
	private String importing_messages = "Updating messages database";
	private String default_account_flag = "Default account";
	private String setup_remote_provisioning = "Download provisioning";
	private String setup_remote_provisioning_hint = "This assistant will download an existing configuration.";
	private String setup_remote_provisioning_url_hint = "provisioning url";
	private String setup_remote_provisioning_login_hint = "The configuration you downloaded doesn't include your account. Please fill it in.";
	private String setup_confirm_username = "Your username will be %s (uppercase characters are not allowed). Do you accept ?";
	private String setup_title_assistant = "Account setup assistant";
	private String zrtp_accept = "Accept";
	private String zrtp_deny = "Deny";
	private String zrtp_help = "You should only accept if you have the same code (see above) as your correspondent";
	private String remote_composing = "Remote is writing...";
	private String unread_messages = "%i unread messages";
	private String retry = "Retry";
	private String remote_provisioning_failure = "Failed to download or apply remote provisioning profile...";
	private String remote_provisioning_again_title = "Remote provisioning";
	private String remote_provisioning_again_message = "Do you want to change the provisioning URI ?";
	private String pref_dtmf_sequence_title = "DTMF sequence";
	private String dtmf_sequence_not_set_failure = "No DTMF set in advanced preferences";
	private String dtmf_sequence = "DTMF sequence";

	public String getHistory_date_format() {
		return history_date_format;
	}

	public void setHistory_date_format(String history_date_format) {
		this.history_date_format = history_date_format;
	}

	public String getHistory_detail_date_format() {
		return history_detail_date_format;
	}

	public void setHistory_detail_date_format(String history_detail_date_format) {
		this.history_detail_date_format = history_detail_date_format;
	}

	public String getMessages_date_format() {
		return messages_date_format;
	}

	public void setMessages_date_format(String messages_date_format) {
		this.messages_date_format = messages_date_format;
	}

	public String getToday_date_format() {
		return today_date_format;
	}

	public void setToday_date_format(String today_date_format) {
		this.today_date_format = today_date_format;
	}

	public String getPicture_name_format() {
		return picture_name_format;
	}

	public void setPicture_name_format(String picture_name_format) {
		this.picture_name_format = picture_name_format;
	}

	public String getApp_name() {
		return app_name;
	}

	public void setApp_name(String app_name) {
		this.app_name = app_name;
	}

	public String getAddressbook_label() {
		return addressbook_label;
	}

	public void setAddressbook_label(String addressbook_label) {
		this.addressbook_label = addressbook_label;
	}

	public String getWait_dialog_text() {
		return wait_dialog_text;
	}

	public void setWait_dialog_text(String wait_dialog_text) {
		this.wait_dialog_text = wait_dialog_text;
	}

	public String getNotification_registered() {
		return notification_registered;
	}

	public void setNotification_registered(String notification_registered) {
		this.notification_registered = notification_registered;
	}

	public String getNotification_register_failure() {
		return notification_register_failure;
	}

	public void setNotification_register_failure(
			String notification_register_failure) {
		this.notification_register_failure = notification_register_failure;
	}

	public String getAbout_text() {
		return about_text;
	}

	public void setAbout_text(String about_text) {
		this.about_text = about_text;
	}

	public String getAbout_link() {
		return about_link;
	}

	public void setAbout_link(String about_link) {
		this.about_link = about_link;
	}

	public String getSetup_welcome() {
		return setup_welcome;
	}

	public void setSetup_welcome(String setup_welcome) {
		this.setup_welcome = setup_welcome;
	}

	public String getSetup_create_account() {
		return setup_create_account;
	}

	public void setSetup_create_account(String setup_create_account) {
		this.setup_create_account = setup_create_account;
	}

	public String getSetup_login_linphone() {
		return setup_login_linphone;
	}

	public void setSetup_login_linphone(String setup_login_linphone) {
		this.setup_login_linphone = setup_login_linphone;
	}

	public String getSetup_login_generic() {
		return setup_login_generic;
	}

	public void setSetup_login_generic(String setup_login_generic) {
		this.setup_login_generic = setup_login_generic;
	}

	public String getSetup_linphone_account_hint() {
		return setup_linphone_account_hint;
	}

	public void setSetup_linphone_account_hint(
			String setup_linphone_account_hint) {
		this.setup_linphone_account_hint = setup_linphone_account_hint;
	}

	public String getSetup_general_account_hint() {
		return setup_general_account_hint;
	}

	public void setSetup_general_account_hint(String setup_general_account_hint) {
		this.setup_general_account_hint = setup_general_account_hint;
	}

	public String getSetup_username_hint() {
		return setup_username_hint;
	}

	public void setSetup_username_hint(String setup_username_hint) {
		this.setup_username_hint = setup_username_hint;
	}

	public String getTunnel_host() {
		return tunnel_host;
	}

	public void setTunnel_host(String tunnel_host) {
		this.tunnel_host = tunnel_host;
	}

	public String getDefault_tunnel_mode_entry_value() {
		return default_tunnel_mode_entry_value;
	}

	public void setDefault_tunnel_mode_entry_value(
			String default_tunnel_mode_entry_value) {
		this.default_tunnel_mode_entry_value = default_tunnel_mode_entry_value;
	}

	public String getPush_sender_id() {
		return push_sender_id;
	}

	public void setPush_sender_id(String push_sender_id) {
		this.push_sender_id = push_sender_id;
	}

	public String getDefault_domain() {
		return default_domain;
	}

	public void setDefault_domain(String default_domain) {
		this.default_domain = default_domain;
	}

	public String getDefault_stun() {
		return default_stun;
	}

	public void setDefault_stun(String default_stun) {
		this.default_stun = default_stun;
	}

	public String getWizard_url() {
		return wizard_url;
	}

	public void setWizard_url(String wizard_url) {
		this.wizard_url = wizard_url;
	}

	public String getAbout_bugreport_email() {
		return about_bugreport_email;
	}

	public void setAbout_bugreport_email(String about_bugreport_email) {
		this.about_bugreport_email = about_bugreport_email;
	}

	public String getTemp_photo_name() {
		return temp_photo_name;
	}

	public void setTemp_photo_name(String temp_photo_name) {
		this.temp_photo_name = temp_photo_name;
	}

	public String getTemp_photo_name_with_date() {
		return temp_photo_name_with_date;
	}

	public void setTemp_photo_name_with_date(String temp_photo_name_with_date) {
		this.temp_photo_name_with_date = temp_photo_name_with_date;
	}

	public String getPref_tunnel_port_default() {
		return pref_tunnel_port_default;
	}

	public void setPref_tunnel_port_default(String pref_tunnel_port_default) {
		this.pref_tunnel_port_default = pref_tunnel_port_default;
	}

	public String getPref_sip_port_default() {
		return pref_sip_port_default;
	}

	public void setPref_sip_port_default(String pref_sip_port_default) {
		this.pref_sip_port_default = pref_sip_port_default;
	}

	public String getPref_transport_default() {
		return pref_transport_default;
	}

	public void setPref_transport_default(String pref_transport_default) {
		this.pref_transport_default = pref_transport_default;
	}

	public String getDefault_audio_port() {
		return default_audio_port;
	}

	public void setDefault_audio_port(String default_audio_port) {
		this.default_audio_port = default_audio_port;
	}

	public String getDefault_video_port() {
		return default_video_port;
	}

	public void setDefault_video_port(String default_video_port) {
		this.default_video_port = default_video_port;
	}

	public String getPref_media_encryption_default() {
		return pref_media_encryption_default;
	}

	public void setPref_media_encryption_default(
			String pref_media_encryption_default) {
		this.pref_media_encryption_default = pref_media_encryption_default;
	}

	public String getPref_incoming_call_timeout_default() {
		return pref_incoming_call_timeout_default;
	}

	public void setPref_incoming_call_timeout_default(
			String pref_incoming_call_timeout_default) {
		this.pref_incoming_call_timeout_default = pref_incoming_call_timeout_default;
	}

	public String getPref_image_sharing_server_default() {
		return pref_image_sharing_server_default;
	}

	public void setPref_image_sharing_server_default(
			String pref_image_sharing_server_default) {
		this.pref_image_sharing_server_default = pref_image_sharing_server_default;
	}

	public String getPref_audio_use_specific_mode_default() {
		return pref_audio_use_specific_mode_default;
	}

	public void setPref_audio_use_specific_mode_default(
			String pref_audio_use_specific_mode_default) {
		this.pref_audio_use_specific_mode_default = pref_audio_use_specific_mode_default;
	}

	public String getPref_expire_default() {
		return pref_expire_default;
	}

	public void setPref_expire_default(String pref_expire_default) {
		this.pref_expire_default = pref_expire_default;
	}

	public String getPref_display_name_default() {
		return pref_display_name_default;
	}

	public void setPref_display_name_default(String pref_display_name_default) {
		this.pref_display_name_default = pref_display_name_default;
	}

	public String getPref_user_name_default() {
		return pref_user_name_default;
	}

	public void setPref_user_name_default(String pref_user_name_default) {
		this.pref_user_name_default = pref_user_name_default;
	}

	public String getPref_incall_timeout_default() {
		return pref_incall_timeout_default;
	}

	public void setPref_incall_timeout_default(
			String pref_incall_timeout_default) {
		this.pref_incall_timeout_default = pref_incall_timeout_default;
	}

	public String getCastel_incall_rotation_title_default() {
		return castel_incall_rotation_title_default;
	}

	public void setCastel_incall_rotation_title_default(
			String castel_incall_rotation_title_default) {
		this.castel_incall_rotation_title_default = castel_incall_rotation_title_default;
	}

	public String getPref_playback_gain_default() {
		return pref_playback_gain_default;
	}

	public void setPref_playback_gain_default(String pref_playback_gain_default) {
		this.pref_playback_gain_default = pref_playback_gain_default;
	}

	public String getPref_micro_gain_default() {
		return pref_micro_gain_default;
	}

	public void setPref_micro_gain_default(String pref_micro_gain_default) {
		this.pref_micro_gain_default = pref_micro_gain_default;
	}

	public String getPref_disable_account_key() {
		return pref_disable_account_key;
	}

	public void setPref_disable_account_key(String pref_disable_account_key) {
		this.pref_disable_account_key = pref_disable_account_key;
	}

	public String getPref_extra_accounts() {
		return pref_extra_accounts;
	}

	public void setPref_extra_accounts(String pref_extra_accounts) {
		this.pref_extra_accounts = pref_extra_accounts;
	}

	public String getPref_default_account_key() {
		return pref_default_account_key;
	}

	public void setPref_default_account_key(String pref_default_account_key) {
		this.pref_default_account_key = pref_default_account_key;
	}

	public String getPref_tunnel_key() {
		return pref_tunnel_key;
	}

	public void setPref_tunnel_key(String pref_tunnel_key) {
		this.pref_tunnel_key = pref_tunnel_key;
	}

	public String getPref_tunnel_mode_key() {
		return pref_tunnel_mode_key;
	}

	public void setPref_tunnel_mode_key(String pref_tunnel_mode_key) {
		this.pref_tunnel_mode_key = pref_tunnel_mode_key;
	}

	public String getTunnel_mode_entry_value_disabled() {
		return tunnel_mode_entry_value_disabled;
	}

	public void setTunnel_mode_entry_value_disabled(
			String tunnel_mode_entry_value_disabled) {
		this.tunnel_mode_entry_value_disabled = tunnel_mode_entry_value_disabled;
	}

	public String getTunnel_mode_entry_value_3G_only() {
		return tunnel_mode_entry_value_3G_only;
	}

	public void setTunnel_mode_entry_value_3G_only(
			String tunnel_mode_entry_value_3G_only) {
		this.tunnel_mode_entry_value_3G_only = tunnel_mode_entry_value_3G_only;
	}

	public String getTunnel_mode_entry_value_always() {
		return tunnel_mode_entry_value_always;
	}

	public void setTunnel_mode_entry_value_always(
			String tunnel_mode_entry_value_always) {
		this.tunnel_mode_entry_value_always = tunnel_mode_entry_value_always;
	}

	public String getPref_tunnel_host_key() {
		return pref_tunnel_host_key;
	}

	public void setPref_tunnel_host_key(String pref_tunnel_host_key) {
		this.pref_tunnel_host_key = pref_tunnel_host_key;
	}

	public String getPref_tunnel_port_key() {
		return pref_tunnel_port_key;
	}

	public void setPref_tunnel_port_key(String pref_tunnel_port_key) {
		this.pref_tunnel_port_key = pref_tunnel_port_key;
	}

	public String getTunnel_mode_entry_value_auto() {
		return tunnel_mode_entry_value_auto;
	}

	public void setTunnel_mode_entry_value_auto(
			String tunnel_mode_entry_value_auto) {
		this.tunnel_mode_entry_value_auto = tunnel_mode_entry_value_auto;
	}

	public String getPref_audio_use_specific_mode_key() {
		return pref_audio_use_specific_mode_key;
	}

	public void setPref_audio_use_specific_mode_key(
			String pref_audio_use_specific_mode_key) {
		this.pref_audio_use_specific_mode_key = pref_audio_use_specific_mode_key;
	}

	public String getPref_audio_hacks_use_galaxys_hack_key() {
		return pref_audio_hacks_use_galaxys_hack_key;
	}

	public void setPref_audio_hacks_use_galaxys_hack_key(
			String pref_audio_hacks_use_galaxys_hack_key) {
		this.pref_audio_hacks_use_galaxys_hack_key = pref_audio_hacks_use_galaxys_hack_key;
	}

	public String getPref_audio_hacks_use_routing_api_key() {
		return pref_audio_hacks_use_routing_api_key;
	}

	public void setPref_audio_hacks_use_routing_api_key(
			String pref_audio_hacks_use_routing_api_key) {
		this.pref_audio_hacks_use_routing_api_key = pref_audio_hacks_use_routing_api_key;
	}

	public String getPref_audio_soft_volume_key() {
		return pref_audio_soft_volume_key;
	}

	public void setPref_audio_soft_volume_key(String pref_audio_soft_volume_key) {
		this.pref_audio_soft_volume_key = pref_audio_soft_volume_key;
	}

	public String getPref_audio_ringtone() {
		return pref_audio_ringtone;
	}

	public void setPref_audio_ringtone(String pref_audio_ringtone) {
		this.pref_audio_ringtone = pref_audio_ringtone;
	}

	public String getPref_ipv6_key() {
		return pref_ipv6_key;
	}

	public void setPref_ipv6_key(String pref_ipv6_key) {
		this.pref_ipv6_key = pref_ipv6_key;
	}

	public String getMenu_about_key() {
		return menu_about_key;
	}

	public void setMenu_about_key(String menu_about_key) {
		this.menu_about_key = menu_about_key;
	}

	public String getPref_sipaccounts_key() {
		return pref_sipaccounts_key;
	}

	public void setPref_sipaccounts_key(String pref_sipaccounts_key) {
		this.pref_sipaccounts_key = pref_sipaccounts_key;
	}

	public String getSetup_key() {
		return setup_key;
	}

	public void setSetup_key(String setup_key) {
		this.setup_key = setup_key;
	}

	public String getPref_add_account_key() {
		return pref_add_account_key;
	}

	public void setPref_add_account_key(String pref_add_account_key) {
		this.pref_add_account_key = pref_add_account_key;
	}

	public String getPref_video_key() {
		return pref_video_key;
	}

	public void setPref_video_key(String pref_video_key) {
		this.pref_video_key = pref_video_key;
	}

	public String getPref_video_codecs_key() {
		return pref_video_codecs_key;
	}

	public void setPref_video_codecs_key(String pref_video_codecs_key) {
		this.pref_video_codecs_key = pref_video_codecs_key;
	}

	public String getPref_linphone_friend_key() {
		return pref_linphone_friend_key;
	}

	public void setPref_linphone_friend_key(String pref_linphone_friend_key) {
		this.pref_linphone_friend_key = pref_linphone_friend_key;
	}

	public String getPref_transport_key() {
		return pref_transport_key;
	}

	public void setPref_transport_key(String pref_transport_key) {
		this.pref_transport_key = pref_transport_key;
	}

	public String getPref_transport_udp_key() {
		return pref_transport_udp_key;
	}

	public void setPref_transport_udp_key(String pref_transport_udp_key) {
		this.pref_transport_udp_key = pref_transport_udp_key;
	}

	public String getPref_transport_tcp_key() {
		return pref_transport_tcp_key;
	}

	public void setPref_transport_tcp_key(String pref_transport_tcp_key) {
		this.pref_transport_tcp_key = pref_transport_tcp_key;
	}

	public String getPref_transport_tls_key() {
		return pref_transport_tls_key;
	}

	public void setPref_transport_tls_key(String pref_transport_tls_key) {
		this.pref_transport_tls_key = pref_transport_tls_key;
	}

	public String getPref_transport_use_random_ports_key() {
		return pref_transport_use_random_ports_key;
	}

	public void setPref_transport_use_random_ports_key(
			String pref_transport_use_random_ports_key) {
		this.pref_transport_use_random_ports_key = pref_transport_use_random_ports_key;
	}

	public String getPref_sip_port_key() {
		return pref_sip_port_key;
	}

	public void setPref_sip_port_key(String pref_sip_port_key) {
		this.pref_sip_port_key = pref_sip_port_key;
	}

	public String getPref_echo_canceller_calibration_key() {
		return pref_echo_canceller_calibration_key;
	}

	public void setPref_echo_canceller_calibration_key(
			String pref_echo_canceller_calibration_key) {
		this.pref_echo_canceller_calibration_key = pref_echo_canceller_calibration_key;
	}

	public String getPref_prefix_key() {
		return pref_prefix_key;
	}

	public void setPref_prefix_key(String pref_prefix_key) {
		this.pref_prefix_key = pref_prefix_key;
	}

	public String getPref_proxy_key() {
		return pref_proxy_key;
	}

	public void setPref_proxy_key(String pref_proxy_key) {
		this.pref_proxy_key = pref_proxy_key;
	}

	public String getPref_domain_key() {
		return pref_domain_key;
	}

	public void setPref_domain_key(String pref_domain_key) {
		this.pref_domain_key = pref_domain_key;
	}

	public String getPref_passwd_key() {
		return pref_passwd_key;
	}

	public void setPref_passwd_key(String pref_passwd_key) {
		this.pref_passwd_key = pref_passwd_key;
	}

	public String getPref_username_key() {
		return pref_username_key;
	}

	public void setPref_username_key(String pref_username_key) {
		this.pref_username_key = pref_username_key;
	}

	public String getPref_auth_userid_key() {
		return pref_auth_userid_key;
	}

	public void setPref_auth_userid_key(String pref_auth_userid_key) {
		this.pref_auth_userid_key = pref_auth_userid_key;
	}

	public String getPref_wizard_key() {
		return pref_wizard_key;
	}

	public void setPref_wizard_key(String pref_wizard_key) {
		this.pref_wizard_key = pref_wizard_key;
	}

	public String getPref_activated_key() {
		return pref_activated_key;
	}

	public void setPref_activated_key(String pref_activated_key) {
		this.pref_activated_key = pref_activated_key;
	}

	public String getPref_debug_key() {
		return pref_debug_key;
	}

	public void setPref_debug_key(String pref_debug_key) {
		this.pref_debug_key = pref_debug_key;
	}

	public String getFirst_launch_suceeded_once_key() {
		return first_launch_suceeded_once_key;
	}

	public void setFirst_launch_suceeded_once_key(
			String first_launch_suceeded_once_key) {
		this.first_launch_suceeded_once_key = first_launch_suceeded_once_key;
	}

	public String getPref_incall_timeout_key() {
		return pref_incall_timeout_key;
	}

	public void setPref_incall_timeout_key(String pref_incall_timeout_key) {
		this.pref_incall_timeout_key = pref_incall_timeout_key;
	}

	public String getPref_wifi_only_key() {
		return pref_wifi_only_key;
	}

	public void setPref_wifi_only_key(String pref_wifi_only_key) {
		this.pref_wifi_only_key = pref_wifi_only_key;
	}

	public String getPref_video_use_front_camera_key() {
		return pref_video_use_front_camera_key;
	}

	public void setPref_video_use_front_camera_key(
			String pref_video_use_front_camera_key) {
		this.pref_video_use_front_camera_key = pref_video_use_front_camera_key;
	}

	public String getPref_video_codec_h263_key() {
		return pref_video_codec_h263_key;
	}

	public void setPref_video_codec_h263_key(String pref_video_codec_h263_key) {
		this.pref_video_codec_h263_key = pref_video_codec_h263_key;
	}

	public String getPref_video_codec_mpeg4_key() {
		return pref_video_codec_mpeg4_key;
	}

	public void setPref_video_codec_mpeg4_key(String pref_video_codec_mpeg4_key) {
		this.pref_video_codec_mpeg4_key = pref_video_codec_mpeg4_key;
	}

	public String getPref_video_codec_h264_key() {
		return pref_video_codec_h264_key;
	}

	public void setPref_video_codec_h264_key(String pref_video_codec_h264_key) {
		this.pref_video_codec_h264_key = pref_video_codec_h264_key;
	}

	public String getPref_video_automatically_share_my_video_key() {
		return pref_video_automatically_share_my_video_key;
	}

	public void setPref_video_automatically_share_my_video_key(
			String pref_video_automatically_share_my_video_key) {
		this.pref_video_automatically_share_my_video_key = pref_video_automatically_share_my_video_key;
	}

	public String getPref_video_automatically_accept_video_key() {
		return pref_video_automatically_accept_video_key;
	}

	public void setPref_video_automatically_accept_video_key(
			String pref_video_automatically_accept_video_key) {
		this.pref_video_automatically_accept_video_key = pref_video_automatically_accept_video_key;
	}

	public String getPref_video_initiate_call_with_video_key() {
		return pref_video_initiate_call_with_video_key;
	}

	public void setPref_video_initiate_call_with_video_key(
			String pref_video_initiate_call_with_video_key) {
		this.pref_video_initiate_call_with_video_key = pref_video_initiate_call_with_video_key;
	}

	public String getPref_video_enable_key() {
		return pref_video_enable_key;
	}

	public void setPref_video_enable_key(String pref_video_enable_key) {
		this.pref_video_enable_key = pref_video_enable_key;
	}

	public String getPref_video_enable_preview_key() {
		return pref_video_enable_preview_key;
	}

	public void setPref_video_enable_preview_key(
			String pref_video_enable_preview_key) {
		this.pref_video_enable_preview_key = pref_video_enable_preview_key;
	}

	public String getPref_preferred_video_size_key() {
		return pref_preferred_video_size_key;
	}

	public void setPref_preferred_video_size_key(
			String pref_preferred_video_size_key) {
		this.pref_preferred_video_size_key = pref_preferred_video_size_key;
	}

	public String getPref_animation_enable_key() {
		return pref_animation_enable_key;
	}

	public void setPref_animation_enable_key(String pref_animation_enable_key) {
		this.pref_animation_enable_key = pref_animation_enable_key;
	}

	public String getPref_escape_plus_key() {
		return pref_escape_plus_key;
	}

	public void setPref_escape_plus_key(String pref_escape_plus_key) {
		this.pref_escape_plus_key = pref_escape_plus_key;
	}

	public String getPref_echo_cancellation_key() {
		return pref_echo_cancellation_key;
	}

	public void setPref_echo_cancellation_key(String pref_echo_cancellation_key) {
		this.pref_echo_cancellation_key = pref_echo_cancellation_key;
	}

	public String getPref_autostart_key() {
		return pref_autostart_key;
	}

	public void setPref_autostart_key(String pref_autostart_key) {
		this.pref_autostart_key = pref_autostart_key;
	}

	public String getPref_enable_outbound_proxy_key() {
		return pref_enable_outbound_proxy_key;
	}

	public void setPref_enable_outbound_proxy_key(
			String pref_enable_outbound_proxy_key) {
		this.pref_enable_outbound_proxy_key = pref_enable_outbound_proxy_key;
	}

	public String getPref_codec_pcma_key() {
		return pref_codec_pcma_key;
	}

	public void setPref_codec_pcma_key(String pref_codec_pcma_key) {
		this.pref_codec_pcma_key = pref_codec_pcma_key;
	}

	public String getPref_codec_pcmu_key() {
		return pref_codec_pcmu_key;
	}

	public void setPref_codec_pcmu_key(String pref_codec_pcmu_key) {
		this.pref_codec_pcmu_key = pref_codec_pcmu_key;
	}

	public String getPref_codec_gsm_key() {
		return pref_codec_gsm_key;
	}

	public void setPref_codec_gsm_key(String pref_codec_gsm_key) {
		this.pref_codec_gsm_key = pref_codec_gsm_key;
	}

	public String getPref_codec_g722_key() {
		return pref_codec_g722_key;
	}

	public void setPref_codec_g722_key(String pref_codec_g722_key) {
		this.pref_codec_g722_key = pref_codec_g722_key;
	}

	public String getPref_codec_g729_key() {
		return pref_codec_g729_key;
	}

	public void setPref_codec_g729_key(String pref_codec_g729_key) {
		this.pref_codec_g729_key = pref_codec_g729_key;
	}

	public String getPref_codec_amr_key() {
		return pref_codec_amr_key;
	}

	public void setPref_codec_amr_key(String pref_codec_amr_key) {
		this.pref_codec_amr_key = pref_codec_amr_key;
	}

	public String getPref_codec_amrwb_key() {
		return pref_codec_amrwb_key;
	}

	public void setPref_codec_amrwb_key(String pref_codec_amrwb_key) {
		this.pref_codec_amrwb_key = pref_codec_amrwb_key;
	}

	public String getPref_codec_ilbc_key() {
		return pref_codec_ilbc_key;
	}

	public void setPref_codec_ilbc_key(String pref_codec_ilbc_key) {
		this.pref_codec_ilbc_key = pref_codec_ilbc_key;
	}

	public String getPref_codec_speex8_key() {
		return pref_codec_speex8_key;
	}

	public void setPref_codec_speex8_key(String pref_codec_speex8_key) {
		this.pref_codec_speex8_key = pref_codec_speex8_key;
	}

	public String getPref_codec_speex16_key() {
		return pref_codec_speex16_key;
	}

	public void setPref_codec_speex16_key(String pref_codec_speex16_key) {
		this.pref_codec_speex16_key = pref_codec_speex16_key;
	}

	public String getPref_codec_speex32_key() {
		return pref_codec_speex32_key;
	}

	public void setPref_codec_speex32_key(String pref_codec_speex32_key) {
		this.pref_codec_speex32_key = pref_codec_speex32_key;
	}

	public String getPref_codec_silk8_key() {
		return pref_codec_silk8_key;
	}

	public void setPref_codec_silk8_key(String pref_codec_silk8_key) {
		this.pref_codec_silk8_key = pref_codec_silk8_key;
	}

	public String getPref_codec_silk12_key() {
		return pref_codec_silk12_key;
	}

	public void setPref_codec_silk12_key(String pref_codec_silk12_key) {
		this.pref_codec_silk12_key = pref_codec_silk12_key;
	}

	public String getPref_codec_silk16_key() {
		return pref_codec_silk16_key;
	}

	public void setPref_codec_silk16_key(String pref_codec_silk16_key) {
		this.pref_codec_silk16_key = pref_codec_silk16_key;
	}

	public String getPref_codec_silk24_key() {
		return pref_codec_silk24_key;
	}

	public void setPref_codec_silk24_key(String pref_codec_silk24_key) {
		this.pref_codec_silk24_key = pref_codec_silk24_key;
	}

	public String getPref_codec_opus_key() {
		return pref_codec_opus_key;
	}

	public void setPref_codec_opus_key(String pref_codec_opus_key) {
		this.pref_codec_opus_key = pref_codec_opus_key;
	}

	public String getPref_codecs_key() {
		return pref_codecs_key;
	}

	public void setPref_codecs_key(String pref_codecs_key) {
		this.pref_codecs_key = pref_codecs_key;
	}

	public String getPref_stun_server_key() {
		return pref_stun_server_key;
	}

	public void setPref_stun_server_key(String pref_stun_server_key) {
		this.pref_stun_server_key = pref_stun_server_key;
	}

	public String getPref_ice_enable_key() {
		return pref_ice_enable_key;
	}

	public void setPref_ice_enable_key(String pref_ice_enable_key) {
		this.pref_ice_enable_key = pref_ice_enable_key;
	}

	public String getPref_video_codec_vp8_key() {
		return pref_video_codec_vp8_key;
	}

	public void setPref_video_codec_vp8_key(String pref_video_codec_vp8_key) {
		this.pref_video_codec_vp8_key = pref_video_codec_vp8_key;
	}

	public String getPref_media_encryption_key() {
		return pref_media_encryption_key;
	}

	public void setPref_media_encryption_key(String pref_media_encryption_key) {
		this.pref_media_encryption_key = pref_media_encryption_key;
	}

	public String getPref_media_encryption_key_none() {
		return pref_media_encryption_key_none;
	}

	public void setPref_media_encryption_key_none(
			String pref_media_encryption_key_none) {
		this.pref_media_encryption_key_none = pref_media_encryption_key_none;
	}

	public String getPref_media_encryption_key_srtp() {
		return pref_media_encryption_key_srtp;
	}

	public void setPref_media_encryption_key_srtp(
			String pref_media_encryption_key_srtp) {
		this.pref_media_encryption_key_srtp = pref_media_encryption_key_srtp;
	}

	public String getPref_media_encryption_key_zrtp() {
		return pref_media_encryption_key_zrtp;
	}

	public void setPref_media_encryption_key_zrtp(
			String pref_media_encryption_key_zrtp) {
		this.pref_media_encryption_key_zrtp = pref_media_encryption_key_zrtp;
	}

	public String getPref_background_mode_key() {
		return pref_background_mode_key;
	}

	public void setPref_background_mode_key(String pref_background_mode_key) {
		this.pref_background_mode_key = pref_background_mode_key;
	}

	public String getPref_codec_bitrate_limit_key() {
		return pref_codec_bitrate_limit_key;
	}

	public void setPref_codec_bitrate_limit_key(
			String pref_codec_bitrate_limit_key) {
		this.pref_codec_bitrate_limit_key = pref_codec_bitrate_limit_key;
	}

	public String getPref_adaptive_rate_control_key() {
		return pref_adaptive_rate_control_key;
	}

	public void setPref_adaptive_rate_control_key(
			String pref_adaptive_rate_control_key) {
		this.pref_adaptive_rate_control_key = pref_adaptive_rate_control_key;
	}

	public String getPref_adaptive_rate_algorithm_key() {
		return pref_adaptive_rate_algorithm_key;
	}

	public void setPref_adaptive_rate_algorithm_key(
			String pref_adaptive_rate_algorithm_key) {
		this.pref_adaptive_rate_algorithm_key = pref_adaptive_rate_algorithm_key;
	}

	public String getPref_adaptive_rate_algorithm_simple_key() {
		return pref_adaptive_rate_algorithm_simple_key;
	}

	public void setPref_adaptive_rate_algorithm_simple_key(
			String pref_adaptive_rate_algorithm_simple_key) {
		this.pref_adaptive_rate_algorithm_simple_key = pref_adaptive_rate_algorithm_simple_key;
	}

	public String getPref_adaptive_rate_algorithm_stateful_key() {
		return pref_adaptive_rate_algorithm_stateful_key;
	}

	public void setPref_adaptive_rate_algorithm_stateful_key(
			String pref_adaptive_rate_algorithm_stateful_key) {
		this.pref_adaptive_rate_algorithm_stateful_key = pref_adaptive_rate_algorithm_stateful_key;
	}

	public String getPush_reg_id_key() {
		return push_reg_id_key;
	}

	public void setPush_reg_id_key(String push_reg_id_key) {
		this.push_reg_id_key = push_reg_id_key;
	}

	public String getPush_sender_id_key() {
		return push_sender_id_key;
	}

	public void setPush_sender_id_key(String push_sender_id_key) {
		this.push_sender_id_key = push_sender_id_key;
	}

	public String getPref_push_notification_key() {
		return pref_push_notification_key;
	}

	public void setPref_push_notification_key(String pref_push_notification_key) {
		this.pref_push_notification_key = pref_push_notification_key;
	}

	public String getPref_auto_accept_friends_key() {
		return pref_auto_accept_friends_key;
	}

	public void setPref_auto_accept_friends_key(
			String pref_auto_accept_friends_key) {
		this.pref_auto_accept_friends_key = pref_auto_accept_friends_key;
	}

	public String getPref_image_sharing_server_key() {
		return pref_image_sharing_server_key;
	}

	public void setPref_image_sharing_server_key(
			String pref_image_sharing_server_key) {
		this.pref_image_sharing_server_key = pref_image_sharing_server_key;
	}

	public String getPref_remote_provisioning_key() {
		return pref_remote_provisioning_key;
	}

	public void setPref_remote_provisioning_key(
			String pref_remote_provisioning_key) {
		this.pref_remote_provisioning_key = pref_remote_provisioning_key;
	}

	public String getPref_video_port_key() {
		return pref_video_port_key;
	}

	public void setPref_video_port_key(String pref_video_port_key) {
		this.pref_video_port_key = pref_video_port_key;
	}

	public String getPref_audio_port_key() {
		return pref_audio_port_key;
	}

	public void setPref_audio_port_key(String pref_audio_port_key) {
		this.pref_audio_port_key = pref_audio_port_key;
	}

	public String getPref_incoming_call_timeout_key() {
		return pref_incoming_call_timeout_key;
	}

	public void setPref_incoming_call_timeout_key(
			String pref_incoming_call_timeout_key) {
		this.pref_incoming_call_timeout_key = pref_incoming_call_timeout_key;
	}

	public String getPref_network_limit_key() {
		return pref_network_limit_key;
	}

	public void setPref_network_limit_key(String pref_network_limit_key) {
		this.pref_network_limit_key = pref_network_limit_key;
	}

	public String getCastel_incall_rotation_key() {
		return castel_incall_rotation_key;
	}

	public void setCastel_incall_rotation_key(String castel_incall_rotation_key) {
		this.castel_incall_rotation_key = castel_incall_rotation_key;
	}

	public String getCastel_incall_rotation_key_system() {
		return castel_incall_rotation_key_system;
	}

	public void setCastel_incall_rotation_key_system(
			String castel_incall_rotation_key_system) {
		this.castel_incall_rotation_key_system = castel_incall_rotation_key_system;
	}

	public String getCastel_incall_rotation_key_portrait() {
		return castel_incall_rotation_key_portrait;
	}

	public void setCastel_incall_rotation_key_portrait(
			String castel_incall_rotation_key_portrait) {
		this.castel_incall_rotation_key_portrait = castel_incall_rotation_key_portrait;
	}

	public String getCastel_incall_rotation_key_landscape() {
		return castel_incall_rotation_key_landscape;
	}

	public void setCastel_incall_rotation_key_landscape(
			String castel_incall_rotation_key_landscape) {
		this.castel_incall_rotation_key_landscape = castel_incall_rotation_key_landscape;
	}

	public String getPref_playback_gain_key() {
		return pref_playback_gain_key;
	}

	public void setPref_playback_gain_key(String pref_playback_gain_key) {
		this.pref_playback_gain_key = pref_playback_gain_key;
	}

	public String getPref_micro_gain_key() {
		return pref_micro_gain_key;
	}

	public void setPref_micro_gain_key(String pref_micro_gain_key) {
		this.pref_micro_gain_key = pref_micro_gain_key;
	}

	public String getPref_upload_bandwidth_key() {
		return pref_upload_bandwidth_key;
	}

	public void setPref_upload_bandwidth_key(String pref_upload_bandwidth_key) {
		this.pref_upload_bandwidth_key = pref_upload_bandwidth_key;
	}

	public String getPref_download_bandwidth_key() {
		return pref_download_bandwidth_key;
	}

	public void setPref_download_bandwidth_key(
			String pref_download_bandwidth_key) {
		this.pref_download_bandwidth_key = pref_download_bandwidth_key;
	}

	public String getPref_display_name_key() {
		return pref_display_name_key;
	}

	public void setPref_display_name_key(String pref_display_name_key) {
		this.pref_display_name_key = pref_display_name_key;
	}

	public String getPref_user_name_key() {
		return pref_user_name_key;
	}

	public void setPref_user_name_key(String pref_user_name_key) {
		this.pref_user_name_key = pref_user_name_key;
	}

	public String getPref_expire_key() {
		return pref_expire_key;
	}

	public void setPref_expire_key(String pref_expire_key) {
		this.pref_expire_key = pref_expire_key;
	}

	public String getPref_avpf_key() {
		return pref_avpf_key;
	}

	public void setPref_avpf_key(String pref_avpf_key) {
		this.pref_avpf_key = pref_avpf_key;
	}

	public String getPref_avpf_rr_interval_key() {
		return pref_avpf_rr_interval_key;
	}

	public void setPref_avpf_rr_interval_key(String pref_avpf_rr_interval_key) {
		this.pref_avpf_rr_interval_key = pref_avpf_rr_interval_key;
	}

	public String getPref_force_prefix_key() {
		return pref_force_prefix_key;
	}

	public void setPref_force_prefix_key(String pref_force_prefix_key) {
		this.pref_force_prefix_key = pref_force_prefix_key;
	}

	public String getPref_rfc2833_dtmf_key() {
		return pref_rfc2833_dtmf_key;
	}

	public void setPref_rfc2833_dtmf_key(String pref_rfc2833_dtmf_key) {
		this.pref_rfc2833_dtmf_key = pref_rfc2833_dtmf_key;
	}

	public String getPref_sipinfo_dtmf_key() {
		return pref_sipinfo_dtmf_key;
	}

	public void setPref_sipinfo_dtmf_key(String pref_sipinfo_dtmf_key) {
		this.pref_sipinfo_dtmf_key = pref_sipinfo_dtmf_key;
	}

	public String getPref_voice_mail_key() {
		return pref_voice_mail_key;
	}

	public void setPref_voice_mail_key(String pref_voice_mail_key) {
		this.pref_voice_mail_key = pref_voice_mail_key;
	}

	public String getPref_upnp_enable_key() {
		return pref_upnp_enable_key;
	}

	public void setPref_upnp_enable_key(String pref_upnp_enable_key) {
		this.pref_upnp_enable_key = pref_upnp_enable_key;
	}

	public String getPref_first_time_linphone_chat_storage() {
		return pref_first_time_linphone_chat_storage;
	}

	public void setPref_first_time_linphone_chat_storage(
			String pref_first_time_linphone_chat_storage) {
		this.pref_first_time_linphone_chat_storage = pref_first_time_linphone_chat_storage;
	}

	public String getPref_sipaccount_key() {
		return pref_sipaccount_key;
	}

	public void setPref_sipaccount_key(String pref_sipaccount_key) {
		this.pref_sipaccount_key = pref_sipaccount_key;
	}

	public String getPref_advanced_key() {
		return pref_advanced_key;
	}

	public void setPref_advanced_key(String pref_advanced_key) {
		this.pref_advanced_key = pref_advanced_key;
	}

	public String getPref_manage_key() {
		return pref_manage_key;
	}

	public void setPref_manage_key(String pref_manage_key) {
		this.pref_manage_key = pref_manage_key;
	}

	public String getPref_disable_account() {
		return pref_disable_account;
	}

	public void setPref_disable_account(String pref_disable_account) {
		this.pref_disable_account = pref_disable_account;
	}

	public String getPref_tunnel() {
		return pref_tunnel;
	}

	public void setPref_tunnel(String pref_tunnel) {
		this.pref_tunnel = pref_tunnel;
	}

	public String getPref_tunnel_host() {
		return pref_tunnel_host;
	}

	public void setPref_tunnel_host(String pref_tunnel_host) {
		this.pref_tunnel_host = pref_tunnel_host;
	}

	public String getPref_tunnel_port() {
		return pref_tunnel_port;
	}

	public void setPref_tunnel_port(String pref_tunnel_port) {
		this.pref_tunnel_port = pref_tunnel_port;
	}

	public String getPref_tunnel_mode() {
		return pref_tunnel_mode;
	}

	public void setPref_tunnel_mode(String pref_tunnel_mode) {
		this.pref_tunnel_mode = pref_tunnel_mode;
	}

	public String getIncall_notif_active() {
		return incall_notif_active;
	}

	public void setIncall_notif_active(String incall_notif_active) {
		this.incall_notif_active = incall_notif_active;
	}

	public String getIncall_notif_paused() {
		return incall_notif_paused;
	}

	public void setIncall_notif_paused(String incall_notif_paused) {
		this.incall_notif_paused = incall_notif_paused;
	}

	public String getIncall_notif_video() {
		return incall_notif_video;
	}

	public void setIncall_notif_video(String incall_notif_video) {
		this.incall_notif_video = incall_notif_video;
	}

	public String getNot_ready_to_make_new_call() {
		return not_ready_to_make_new_call;
	}

	public void setNot_ready_to_make_new_call(String not_ready_to_make_new_call) {
		this.not_ready_to_make_new_call = not_ready_to_make_new_call;
	}

	public String getBad_target_uri() {
		return bad_target_uri;
	}

	public void setBad_target_uri(String bad_target_uri) {
		this.bad_target_uri = bad_target_uri;
	}

	public String getReset_sas_fmt() {
		return reset_sas_fmt;
	}

	public void setReset_sas_fmt(String reset_sas_fmt) {
		this.reset_sas_fmt = reset_sas_fmt;
	}

	public String getVerify_sas_fmt() {
		return verify_sas_fmt;
	}

	public void setVerify_sas_fmt(String verify_sas_fmt) {
		this.verify_sas_fmt = verify_sas_fmt;
	}

	public String getCommunication_encrypted() {
		return communication_encrypted;
	}

	public void setCommunication_encrypted(String communication_encrypted) {
		this.communication_encrypted = communication_encrypted;
	}

	public String getWaiting_for_startup() {
		return waiting_for_startup;
	}

	public void setWaiting_for_startup(String waiting_for_startup) {
		this.waiting_for_startup = waiting_for_startup;
	}

	public String getCouldnt_accept_call() {
		return couldnt_accept_call;
	}

	public void setCouldnt_accept_call(String couldnt_accept_call) {
		this.couldnt_accept_call = couldnt_accept_call;
	}

	public String getError_adding_new_call() {
		return error_adding_new_call;
	}

	public void setError_adding_new_call(String error_adding_new_call) {
		this.error_adding_new_call = error_adding_new_call;
	}

	public String getTransfer_started() {
		return transfer_started;
	}

	public void setTransfer_started(String transfer_started) {
		this.transfer_started = transfer_started;
	}

	public String getTransfer_dialog_title() {
		return transfer_dialog_title;
	}

	public void setTransfer_dialog_title(String transfer_dialog_title) {
		this.transfer_dialog_title = transfer_dialog_title;
	}

	public String getTransfer_to_new_call() {
		return transfer_to_new_call;
	}

	public void setTransfer_to_new_call(String transfer_to_new_call) {
		this.transfer_to_new_call = transfer_to_new_call;
	}

	public String getResume_dialog_title() {
		return resume_dialog_title;
	}

	public void setResume_dialog_title(String resume_dialog_title) {
		this.resume_dialog_title = resume_dialog_title;
	}

	public String getCannot_resume_paused_by_remote_call() {
		return cannot_resume_paused_by_remote_call;
	}

	public void setCannot_resume_paused_by_remote_call(
			String cannot_resume_paused_by_remote_call) {
		this.cannot_resume_paused_by_remote_call = cannot_resume_paused_by_remote_call;
	}

	public String getConf_show_details_text() {
		return conf_show_details_text;
	}

	public void setConf_show_details_text(String conf_show_details_text) {
		this.conf_show_details_text = conf_show_details_text;
	}

	public String getSkipable_error_service_not_ready() {
		return skipable_error_service_not_ready;
	}

	public void setSkipable_error_service_not_ready(
			String skipable_error_service_not_ready) {
		this.skipable_error_service_not_ready = skipable_error_service_not_ready;
	}

	public String getClose_button_text() {
		return close_button_text;
	}

	public void setClose_button_text(String close_button_text) {
		this.close_button_text = close_button_text;
	}

	public String getStatus_conf_call() {
		return status_conf_call;
	}

	public void setStatus_conf_call(String status_conf_call) {
		this.status_conf_call = status_conf_call;
	}

	public String getStatus_active_call() {
		return status_active_call;
	}

	public void setStatus_active_call(String status_active_call) {
		this.status_active_call = status_active_call;
	}

	public String getState_paused() {
		return state_paused;
	}

	public void setState_paused(String state_paused) {
		this.state_paused = state_paused;
	}

	public String getState_paused_by_remote() {
		return state_paused_by_remote;
	}

	public void setState_paused_by_remote(String state_paused_by_remote) {
		this.state_paused_by_remote = state_paused_by_remote;
	}

	public String getState_incoming_received() {
		return state_incoming_received;
	}

	public void setState_incoming_received(String state_incoming_received) {
		this.state_incoming_received = state_incoming_received;
	}

	public String getState_outgoing_ringing() {
		return state_outgoing_ringing;
	}

	public void setState_outgoing_ringing(String state_outgoing_ringing) {
		this.state_outgoing_ringing = state_outgoing_ringing;
	}

	public String getState_outgoing_progress() {
		return state_outgoing_progress;
	}

	public void setState_outgoing_progress(String state_outgoing_progress) {
		this.state_outgoing_progress = state_outgoing_progress;
	}

	public String getMutemic_button_txt() {
		return mutemic_button_txt;
	}

	public void setMutemic_button_txt(String mutemic_button_txt) {
		this.mutemic_button_txt = mutemic_button_txt;
	}

	public String getSpeaker_button_txt() {
		return speaker_button_txt;
	}

	public void setSpeaker_button_txt(String speaker_button_txt) {
		this.speaker_button_txt = speaker_button_txt;
	}

	public String getBluetooth_button_txt() {
		return bluetooth_button_txt;
	}

	public void setBluetooth_button_txt(String bluetooth_button_txt) {
		this.bluetooth_button_txt = bluetooth_button_txt;
	}

	public String getCancelButtonText() {
		return CancelButtonText;
	}

	public void setCancelButtonText(String cancelButtonText) {
		CancelButtonText = cancelButtonText;
	}

	public String getAddCallButtonText() {
		return AddCallButtonText;
	}

	public void setAddCallButtonText(String addCallButtonText) {
		AddCallButtonText = addCallButtonText;
	}

	public String getTransferCallButtonText() {
		return TransferCallButtonText;
	}

	public void setTransferCallButtonText(String transferCallButtonText) {
		TransferCallButtonText = transferCallButtonText;
	}

	public String getConf_admin_choice_enter() {
		return conf_admin_choice_enter;
	}

	public void setConf_admin_choice_enter(String conf_admin_choice_enter) {
		this.conf_admin_choice_enter = conf_admin_choice_enter;
	}

	public String getConf_admin_choice_leave() {
		return conf_admin_choice_leave;
	}

	public void setConf_admin_choice_leave(String conf_admin_choice_leave) {
		this.conf_admin_choice_leave = conf_admin_choice_leave;
	}

	public String getConf_admin_choice_terminate() {
		return conf_admin_choice_terminate;
	}

	public void setConf_admin_choice_terminate(
			String conf_admin_choice_terminate) {
		this.conf_admin_choice_terminate = conf_admin_choice_terminate;
	}

	public String getHangup() {
		return hangup;
	}

	public void setHangup(String hangup) {
		this.hangup = hangup;
	}

	public String getConf_simple_merge_bt_txt() {
		return conf_simple_merge_bt_txt;
	}

	public void setConf_simple_merge_bt_txt(String conf_simple_merge_bt_txt) {
		this.conf_simple_merge_bt_txt = conf_simple_merge_bt_txt;
	}

	public String getConf_simple_transfer_bt_txt() {
		return conf_simple_transfer_bt_txt;
	}

	public void setConf_simple_transfer_bt_txt(
			String conf_simple_transfer_bt_txt) {
		this.conf_simple_transfer_bt_txt = conf_simple_transfer_bt_txt;
	}

	public String getConf_simple_video_bt_txt() {
		return conf_simple_video_bt_txt;
	}

	public void setConf_simple_video_bt_txt(String conf_simple_video_bt_txt) {
		this.conf_simple_video_bt_txt = conf_simple_video_bt_txt;
	}

	public String getShow_send_dtmfs_button() {
		return show_send_dtmfs_button;
	}

	public void setShow_send_dtmfs_button(String show_send_dtmfs_button) {
		this.show_send_dtmfs_button = show_send_dtmfs_button;
	}

	public String getConf_conference() {
		return conf_conference;
	}

	public void setConf_conference(String conf_conference) {
		this.conf_conference = conf_conference;
	}

	public String getIn_conf() {
		return in_conf;
	}

	public void setIn_conf(String in_conf) {
		this.in_conf = in_conf;
	}

	public String getIn_conf_leave() {
		return in_conf_leave;
	}

	public void setIn_conf_leave(String in_conf_leave) {
		this.in_conf_leave = in_conf_leave;
	}

	public String getOut_conf() {
		return out_conf;
	}

	public void setOut_conf(String out_conf) {
		this.out_conf = out_conf;
	}

	public String getOut_conf_enter() {
		return out_conf_enter;
	}

	public void setOut_conf_enter(String out_conf_enter) {
		this.out_conf_enter = out_conf_enter;
	}

	public String getPref_audio_hacks_title() {
		return pref_audio_hacks_title;
	}

	public void setPref_audio_hacks_title(String pref_audio_hacks_title) {
		this.pref_audio_hacks_title = pref_audio_hacks_title;
	}

	public String getPref_audio_use_specific_mode_title() {
		return pref_audio_use_specific_mode_title;
	}

	public void setPref_audio_use_specific_mode_title(
			String pref_audio_use_specific_mode_title) {
		this.pref_audio_use_specific_mode_title = pref_audio_use_specific_mode_title;
	}

	public String getPref_audio_use_specific_mode_summary() {
		return pref_audio_use_specific_mode_summary;
	}

	public void setPref_audio_use_specific_mode_summary(
			String pref_audio_use_specific_mode_summary) {
		this.pref_audio_use_specific_mode_summary = pref_audio_use_specific_mode_summary;
	}

	public String getPref_audio_hacks_use_routing_api_title() {
		return pref_audio_hacks_use_routing_api_title;
	}

	public void setPref_audio_hacks_use_routing_api_title(
			String pref_audio_hacks_use_routing_api_title) {
		this.pref_audio_hacks_use_routing_api_title = pref_audio_hacks_use_routing_api_title;
	}

	public String getPref_audio_hacks_use_galaxys_hack_title() {
		return pref_audio_hacks_use_galaxys_hack_title;
	}

	public void setPref_audio_hacks_use_galaxys_hack_title(
			String pref_audio_hacks_use_galaxys_hack_title) {
		this.pref_audio_hacks_use_galaxys_hack_title = pref_audio_hacks_use_galaxys_hack_title;
	}

	public String getPref_audio_soft_volume_title() {
		return pref_audio_soft_volume_title;
	}

	public void setPref_audio_soft_volume_title(
			String pref_audio_soft_volume_title) {
		this.pref_audio_soft_volume_title = pref_audio_soft_volume_title;
	}

	public String getPref_ipv6_title() {
		return pref_ipv6_title;
	}

	public void setPref_ipv6_title(String pref_ipv6_title) {
		this.pref_ipv6_title = pref_ipv6_title;
	}

	public String getError_while_accepting_pending_call() {
		return error_while_accepting_pending_call;
	}

	public void setError_while_accepting_pending_call(
			String error_while_accepting_pending_call) {
		this.error_while_accepting_pending_call = error_while_accepting_pending_call;
	}

	public String getIncoming_call_dialog_title() {
		return incoming_call_dialog_title;
	}

	public void setIncoming_call_dialog_title(String incoming_call_dialog_title) {
		this.incoming_call_dialog_title = incoming_call_dialog_title;
	}

	public String getAccept() {
		return accept;
	}

	public void setAccept(String accept) {
		this.accept = accept;
	}

	public String getDecline() {
		return decline;
	}

	public void setDecline(String decline) {
		this.decline = decline;
	}

	public String getUnknown_incoming_call_name() {
		return unknown_incoming_call_name;
	}

	public void setUnknown_incoming_call_name(String unknown_incoming_call_name) {
		this.unknown_incoming_call_name = unknown_incoming_call_name;
	}

	public String getPref_network_title() {
		return pref_network_title;
	}

	public void setPref_network_title(String pref_network_title) {
		this.pref_network_title = pref_network_title;
	}

	public String getPref_transport() {
		return pref_transport;
	}

	public void setPref_transport(String pref_transport) {
		this.pref_transport = pref_transport;
	}

	public String getPref_transport_udp() {
		return pref_transport_udp;
	}

	public void setPref_transport_udp(String pref_transport_udp) {
		this.pref_transport_udp = pref_transport_udp;
	}

	public String getPref_transport_tcp() {
		return pref_transport_tcp;
	}

	public void setPref_transport_tcp(String pref_transport_tcp) {
		this.pref_transport_tcp = pref_transport_tcp;
	}

	public String getPref_transport_tls() {
		return pref_transport_tls;
	}

	public void setPref_transport_tls(String pref_transport_tls) {
		this.pref_transport_tls = pref_transport_tls;
	}

	public String getPref_transport_use_random_ports() {
		return pref_transport_use_random_ports;
	}

	public void setPref_transport_use_random_ports(
			String pref_transport_use_random_ports) {
		this.pref_transport_use_random_ports = pref_transport_use_random_ports;
	}

	public String getPref_sip_port_title() {
		return pref_sip_port_title;
	}

	public void setPref_sip_port_title(String pref_sip_port_title) {
		this.pref_sip_port_title = pref_sip_port_title;
	}

	public String getAt_least_a_protocol() {
		return at_least_a_protocol;
	}

	public void setAt_least_a_protocol(String at_least_a_protocol) {
		this.at_least_a_protocol = at_least_a_protocol;
	}

	public String getFirst_launch_ok() {
		return first_launch_ok;
	}

	public void setFirst_launch_ok(String first_launch_ok) {
		this.first_launch_ok = first_launch_ok;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getClick_to_show_first_login_view() {
		return click_to_show_first_login_view;
	}

	public void setClick_to_show_first_login_view(
			String click_to_show_first_login_view) {
		this.click_to_show_first_login_view = click_to_show_first_login_view;
	}

	public String getDialer_null_on_new_intent() {
		return dialer_null_on_new_intent;
	}

	public void setDialer_null_on_new_intent(String dialer_null_on_new_intent) {
		this.dialer_null_on_new_intent = dialer_null_on_new_intent;
	}

	public String getPref_add_account() {
		return pref_add_account;
	}

	public void setPref_add_account(String pref_add_account) {
		this.pref_add_account = pref_add_account;
	}

	public String getNo_phone_numbers() {
		return no_phone_numbers;
	}

	public void setNo_phone_numbers(String no_phone_numbers) {
		this.no_phone_numbers = no_phone_numbers;
	}

	public String getFilter_contacts() {
		return filter_contacts;
	}

	public void setFilter_contacts(String filter_contacts) {
		this.filter_contacts = filter_contacts;
	}

	public String getTitle_numbers_dialog() {
		return title_numbers_dialog;
	}

	public void setTitle_numbers_dialog(String title_numbers_dialog) {
		this.title_numbers_dialog = title_numbers_dialog;
	}

	public String getPref_delete_account() {
		return pref_delete_account;
	}

	public void setPref_delete_account(String pref_delete_account) {
		this.pref_delete_account = pref_delete_account;
	}

	public String getPref_default_account() {
		return pref_default_account;
	}

	public void setPref_default_account(String pref_default_account) {
		this.pref_default_account = pref_default_account;
	}

	public String getPref_echo_canceller_calibration() {
		return pref_echo_canceller_calibration;
	}

	public void setPref_echo_canceller_calibration(
			String pref_echo_canceller_calibration) {
		this.pref_echo_canceller_calibration = pref_echo_canceller_calibration;
	}

	public String getPref_video_use_front_camera_title() {
		return pref_video_use_front_camera_title;
	}

	public void setPref_video_use_front_camera_title(
			String pref_video_use_front_camera_title) {
		this.pref_video_use_front_camera_title = pref_video_use_front_camera_title;
	}

	public String getPref_video() {
		return pref_video;
	}

	public void setPref_video(String pref_video) {
		this.pref_video = pref_video;
	}

	public String getPref_preferences() {
		return pref_preferences;
	}

	public void setPref_preferences(String pref_preferences) {
		this.pref_preferences = pref_preferences;
	}

	public String getPref_video_codec_h263_title() {
		return pref_video_codec_h263_title;
	}

	public void setPref_video_codec_h263_title(
			String pref_video_codec_h263_title) {
		this.pref_video_codec_h263_title = pref_video_codec_h263_title;
	}

	public String getPref_video_codec_mpeg4_title() {
		return pref_video_codec_mpeg4_title;
	}

	public void setPref_video_codec_mpeg4_title(
			String pref_video_codec_mpeg4_title) {
		this.pref_video_codec_mpeg4_title = pref_video_codec_mpeg4_title;
	}

	public String getPref_video_codec_h264_title() {
		return pref_video_codec_h264_title;
	}

	public void setPref_video_codec_h264_title(
			String pref_video_codec_h264_title) {
		this.pref_video_codec_h264_title = pref_video_codec_h264_title;
	}

	public String getPref_video_codecs_title() {
		return pref_video_codecs_title;
	}

	public void setPref_video_codecs_title(String pref_video_codecs_title) {
		this.pref_video_codecs_title = pref_video_codecs_title;
	}

	public String getPref_preferred_video_size() {
		return pref_preferred_video_size;
	}

	public void setPref_preferred_video_size(String pref_preferred_video_size) {
		this.pref_preferred_video_size = pref_preferred_video_size;
	}

	public String getMenu_videocall_back_to_dialer_title() {
		return menu_videocall_back_to_dialer_title;
	}

	public void setMenu_videocall_back_to_dialer_title(
			String menu_videocall_back_to_dialer_title) {
		this.menu_videocall_back_to_dialer_title = menu_videocall_back_to_dialer_title;
	}

	public String getMenu_videocall_switch_camera_title() {
		return menu_videocall_switch_camera_title;
	}

	public void setMenu_videocall_switch_camera_title(
			String menu_videocall_switch_camera_title) {
		this.menu_videocall_switch_camera_title = menu_videocall_switch_camera_title;
	}

	public String getMenu_videocall_change_resolution_when_low_resolution() {
		return menu_videocall_change_resolution_when_low_resolution;
	}

	public void setMenu_videocall_change_resolution_when_low_resolution(
			String menu_videocall_change_resolution_when_low_resolution) {
		this.menu_videocall_change_resolution_when_low_resolution = menu_videocall_change_resolution_when_low_resolution;
	}

	public String getMenu_videocall_change_resolution_when_high_resolution() {
		return menu_videocall_change_resolution_when_high_resolution;
	}

	public void setMenu_videocall_change_resolution_when_high_resolution(
			String menu_videocall_change_resolution_when_high_resolution) {
		this.menu_videocall_change_resolution_when_high_resolution = menu_videocall_change_resolution_when_high_resolution;
	}

	public String getMenu_videocall_change_resolution_title() {
		return menu_videocall_change_resolution_title;
	}

	public void setMenu_videocall_change_resolution_title(
			String menu_videocall_change_resolution_title) {
		this.menu_videocall_change_resolution_title = menu_videocall_change_resolution_title;
	}

	public String getMenu_videocall_toggle_camera_title() {
		return menu_videocall_toggle_camera_title;
	}

	public void setMenu_videocall_toggle_camera_title(
			String menu_videocall_toggle_camera_title) {
		this.menu_videocall_toggle_camera_title = menu_videocall_toggle_camera_title;
	}

	public String getMenu_videocall_toggle_camera_disable() {
		return menu_videocall_toggle_camera_disable;
	}

	public void setMenu_videocall_toggle_camera_disable(
			String menu_videocall_toggle_camera_disable) {
		this.menu_videocall_toggle_camera_disable = menu_videocall_toggle_camera_disable;
	}

	public String getMenu_videocall_toggle_camera_enable() {
		return menu_videocall_toggle_camera_enable;
	}

	public void setMenu_videocall_toggle_camera_enable(
			String menu_videocall_toggle_camera_enable) {
		this.menu_videocall_toggle_camera_enable = menu_videocall_toggle_camera_enable;
	}

	public String getMenu_videocall_terminate_call_title() {
		return menu_videocall_terminate_call_title;
	}

	public void setMenu_videocall_terminate_call_title(
			String menu_videocall_terminate_call_title) {
		this.menu_videocall_terminate_call_title = menu_videocall_terminate_call_title;
	}

	public String getPref_video_settings_title() {
		return pref_video_settings_title;
	}

	public void setPref_video_settings_title(String pref_video_settings_title) {
		this.pref_video_settings_title = pref_video_settings_title;
	}

	public String getPref_video_automatically_share_my_video_title() {
		return pref_video_automatically_share_my_video_title;
	}

	public void setPref_video_automatically_share_my_video_title(
			String pref_video_automatically_share_my_video_title) {
		this.pref_video_automatically_share_my_video_title = pref_video_automatically_share_my_video_title;
	}

	public String getPref_video_automatically_accept_video_title() {
		return pref_video_automatically_accept_video_title;
	}

	public void setPref_video_automatically_accept_video_title(
			String pref_video_automatically_accept_video_title) {
		this.pref_video_automatically_accept_video_title = pref_video_automatically_accept_video_title;
	}

	public String getPref_video_automatically_share_my_video() {
		return pref_video_automatically_share_my_video;
	}

	public void setPref_video_automatically_share_my_video(
			String pref_video_automatically_share_my_video) {
		this.pref_video_automatically_share_my_video = pref_video_automatically_share_my_video;
	}

	public String getPref_video_automatically_accept_video() {
		return pref_video_automatically_accept_video;
	}

	public void setPref_video_automatically_accept_video(
			String pref_video_automatically_accept_video) {
		this.pref_video_automatically_accept_video = pref_video_automatically_accept_video;
	}

	public String getPref_video_initiate_call_with_video_title() {
		return pref_video_initiate_call_with_video_title;
	}

	public void setPref_video_initiate_call_with_video_title(
			String pref_video_initiate_call_with_video_title) {
		this.pref_video_initiate_call_with_video_title = pref_video_initiate_call_with_video_title;
	}

	public String getPref_video_initiate_call_with_video() {
		return pref_video_initiate_call_with_video;
	}

	public void setPref_video_initiate_call_with_video(
			String pref_video_initiate_call_with_video) {
		this.pref_video_initiate_call_with_video = pref_video_initiate_call_with_video;
	}

	public String getPref_video_enable_title() {
		return pref_video_enable_title;
	}

	public void setPref_video_enable_title(String pref_video_enable_title) {
		this.pref_video_enable_title = pref_video_enable_title;
	}

	public String getPref_video_enable_preview_title() {
		return pref_video_enable_preview_title;
	}

	public void setPref_video_enable_preview_title(
			String pref_video_enable_preview_title) {
		this.pref_video_enable_preview_title = pref_video_enable_preview_title;
	}

	public String getPref_animation_enable_title() {
		return pref_animation_enable_title;
	}

	public void setPref_animation_enable_title(
			String pref_animation_enable_title) {
		this.pref_animation_enable_title = pref_animation_enable_title;
	}

	public String getPref_escape_plus() {
		return pref_escape_plus;
	}

	public void setPref_escape_plus(String pref_escape_plus) {
		this.pref_escape_plus = pref_escape_plus;
	}

	public String getPref_ilbc_summary() {
		return pref_ilbc_summary;
	}

	public void setPref_ilbc_summary(String pref_ilbc_summary) {
		this.pref_ilbc_summary = pref_ilbc_summary;
	}

	public String getPref_echo_cancellation() {
		return pref_echo_cancellation;
	}

	public void setPref_echo_cancellation(String pref_echo_cancellation) {
		this.pref_echo_cancellation = pref_echo_cancellation;
	}

	public String getPref_autostart() {
		return pref_autostart;
	}

	public void setPref_autostart(String pref_autostart) {
		this.pref_autostart = pref_autostart;
	}

	public String getPref_enable_outbound_proxy() {
		return pref_enable_outbound_proxy;
	}

	public void setPref_enable_outbound_proxy(String pref_enable_outbound_proxy) {
		this.pref_enable_outbound_proxy = pref_enable_outbound_proxy;
	}

	public String getPref_codec_pcma() {
		return pref_codec_pcma;
	}

	public void setPref_codec_pcma(String pref_codec_pcma) {
		this.pref_codec_pcma = pref_codec_pcma;
	}

	public String getPref_codec_pcmu() {
		return pref_codec_pcmu;
	}

	public void setPref_codec_pcmu(String pref_codec_pcmu) {
		this.pref_codec_pcmu = pref_codec_pcmu;
	}

	public String getPref_codec_gsm() {
		return pref_codec_gsm;
	}

	public void setPref_codec_gsm(String pref_codec_gsm) {
		this.pref_codec_gsm = pref_codec_gsm;
	}

	public String getPref_codec_g722() {
		return pref_codec_g722;
	}

	public void setPref_codec_g722(String pref_codec_g722) {
		this.pref_codec_g722 = pref_codec_g722;
	}

	public String getPref_codec_amr() {
		return pref_codec_amr;
	}

	public void setPref_codec_amr(String pref_codec_amr) {
		this.pref_codec_amr = pref_codec_amr;
	}

	public String getPref_codec_amrwb() {
		return pref_codec_amrwb;
	}

	public void setPref_codec_amrwb(String pref_codec_amrwb) {
		this.pref_codec_amrwb = pref_codec_amrwb;
	}

	public String getPref_codec_ilbc() {
		return pref_codec_ilbc;
	}

	public void setPref_codec_ilbc(String pref_codec_ilbc) {
		this.pref_codec_ilbc = pref_codec_ilbc;
	}

	public String getPref_codec_speex8() {
		return pref_codec_speex8;
	}

	public void setPref_codec_speex8(String pref_codec_speex8) {
		this.pref_codec_speex8 = pref_codec_speex8;
	}

	public String getPref_codec_speex16() {
		return pref_codec_speex16;
	}

	public void setPref_codec_speex16(String pref_codec_speex16) {
		this.pref_codec_speex16 = pref_codec_speex16;
	}

	public String getPref_codec_opus() {
		return pref_codec_opus;
	}

	public void setPref_codec_opus(String pref_codec_opus) {
		this.pref_codec_opus = pref_codec_opus;
	}

	public String getPref_codec_silk8() {
		return pref_codec_silk8;
	}

	public void setPref_codec_silk8(String pref_codec_silk8) {
		this.pref_codec_silk8 = pref_codec_silk8;
	}

	public String getPref_codec_silk12() {
		return pref_codec_silk12;
	}

	public void setPref_codec_silk12(String pref_codec_silk12) {
		this.pref_codec_silk12 = pref_codec_silk12;
	}

	public String getPref_codec_silk16() {
		return pref_codec_silk16;
	}

	public void setPref_codec_silk16(String pref_codec_silk16) {
		this.pref_codec_silk16 = pref_codec_silk16;
	}

	public String getPref_codec_silk24() {
		return pref_codec_silk24;
	}

	public void setPref_codec_silk24(String pref_codec_silk24) {
		this.pref_codec_silk24 = pref_codec_silk24;
	}

	public String getPref_codec_g729() {
		return pref_codec_g729;
	}

	public void setPref_codec_g729(String pref_codec_g729) {
		this.pref_codec_g729 = pref_codec_g729;
	}

	public String getPref_codecs() {
		return pref_codecs;
	}

	public void setPref_codecs(String pref_codecs) {
		this.pref_codecs = pref_codecs;
	}

	public String getPref_communication_expire_title() {
		return pref_communication_expire_title;
	}

	public void setPref_communication_expire_title(
			String pref_communication_expire_title) {
		this.pref_communication_expire_title = pref_communication_expire_title;
	}

	public String getPref_incoming_expire_title() {
		return pref_incoming_expire_title;
	}

	public void setPref_incoming_expire_title(String pref_incoming_expire_title) {
		this.pref_incoming_expire_title = pref_incoming_expire_title;
	}

	public String getPref_video_port_title() {
		return pref_video_port_title;
	}

	public void setPref_video_port_title(String pref_video_port_title) {
		this.pref_video_port_title = pref_video_port_title;
	}

	public String getPref_audio_port_title() {
		return pref_audio_port_title;
	}

	public void setPref_audio_port_title(String pref_audio_port_title) {
		this.pref_audio_port_title = pref_audio_port_title;
	}

	public String getPref_video_port_description() {
		return pref_video_port_description;
	}

	public void setPref_video_port_description(
			String pref_video_port_description) {
		this.pref_video_port_description = pref_video_port_description;
	}

	public String getPref_audio_port_description() {
		return pref_audio_port_description;
	}

	public void setPref_audio_port_description(
			String pref_audio_port_description) {
		this.pref_audio_port_description = pref_audio_port_description;
	}

	public String getPref_incoming_call_timeout_title() {
		return pref_incoming_call_timeout_title;
	}

	public void setPref_incoming_call_timeout_title(
			String pref_incoming_call_timeout_title) {
		this.pref_incoming_call_timeout_title = pref_incoming_call_timeout_title;
	}

	public String getPref_incall_timeout() {
		return pref_incall_timeout;
	}

	public void setPref_incall_timeout(String pref_incall_timeout) {
		this.pref_incall_timeout = pref_incall_timeout;
	}

	public String getPlace_call_chooser() {
		return place_call_chooser;
	}

	public void setPlace_call_chooser(String place_call_chooser) {
		this.place_call_chooser = place_call_chooser;
	}

	public String getPref_adaptive_rate_control() {
		return pref_adaptive_rate_control;
	}

	public void setPref_adaptive_rate_control(String pref_adaptive_rate_control) {
		this.pref_adaptive_rate_control = pref_adaptive_rate_control;
	}

	public String getPref_adaptive_rate_algorithm() {
		return pref_adaptive_rate_algorithm;
	}

	public void setPref_adaptive_rate_algorithm(
			String pref_adaptive_rate_algorithm) {
		this.pref_adaptive_rate_algorithm = pref_adaptive_rate_algorithm;
	}

	public String getPref_codec_bitrate_limit() {
		return pref_codec_bitrate_limit;
	}

	public void setPref_codec_bitrate_limit(String pref_codec_bitrate_limit) {
		this.pref_codec_bitrate_limit = pref_codec_bitrate_limit;
	}

	public String getPref_debug() {
		return pref_debug;
	}

	public void setPref_debug(String pref_debug) {
		this.pref_debug = pref_debug;
	}

	public String getAbout_report_issue() {
		return about_report_issue;
	}

	public void setAbout_report_issue(String about_report_issue) {
		this.about_report_issue = about_report_issue;
	}

	public String getAbout_bugreport_email_text() {
		return about_bugreport_email_text;
	}

	public void setAbout_bugreport_email_text(String about_bugreport_email_text) {
		this.about_bugreport_email_text = about_bugreport_email_text;
	}

	public String getAbout_error_generating_bugreport_attachement() {
		return about_error_generating_bugreport_attachement;
	}

	public void setAbout_error_generating_bugreport_attachement(
			String about_error_generating_bugreport_attachement) {
		this.about_error_generating_bugreport_attachement = about_error_generating_bugreport_attachement;
	}

	public String getAbout_logs_not_found() {
		return about_logs_not_found;
	}

	public void setAbout_logs_not_found(String about_logs_not_found) {
		this.about_logs_not_found = about_logs_not_found;
	}

	public String getAbout_reading_logs() {
		return about_reading_logs;
	}

	public void setAbout_reading_logs(String about_reading_logs) {
		this.about_reading_logs = about_reading_logs;
	}

	public String getAbout_mailer_chooser_text() {
		return about_mailer_chooser_text;
	}

	public void setAbout_mailer_chooser_text(String about_mailer_chooser_text) {
		this.about_mailer_chooser_text = about_mailer_chooser_text;
	}

	public String getMenu_about() {
		return menu_about;
	}

	public void setMenu_about(String menu_about) {
		this.menu_about = menu_about;
	}

	public String getMenu_send_log() {
		return menu_send_log;
	}

	public void setMenu_send_log(String menu_send_log) {
		this.menu_send_log = menu_send_log;
	}

	public String getPref_audio() {
		return pref_audio;
	}

	public void setPref_audio(String pref_audio) {
		this.pref_audio = pref_audio;
	}

	public String getMenu_exit() {
		return menu_exit;
	}

	public void setMenu_exit(String menu_exit) {
		this.menu_exit = menu_exit;
	}

	public String getPref_prefix() {
		return pref_prefix;
	}

	public void setPref_prefix(String pref_prefix) {
		this.pref_prefix = pref_prefix;
	}

	public String getPref_advanced() {
		return pref_advanced;
	}

	public void setPref_advanced(String pref_advanced) {
		this.pref_advanced = pref_advanced;
	}

	public String getMenu_settings() {
		return menu_settings;
	}

	public void setMenu_settings(String menu_settings) {
		this.menu_settings = menu_settings;
	}

	public String getPref_proxy() {
		return pref_proxy;
	}

	public void setPref_proxy(String pref_proxy) {
		this.pref_proxy = pref_proxy;
	}

	public String getPref_domain() {
		return pref_domain;
	}

	public void setPref_domain(String pref_domain) {
		this.pref_domain = pref_domain;
	}

	public String getPref_outbound_proxy() {
		return pref_outbound_proxy;
	}

	public void setPref_outbound_proxy(String pref_outbound_proxy) {
		this.pref_outbound_proxy = pref_outbound_proxy;
	}

	public String getPref_passwd() {
		return pref_passwd;
	}

	public void setPref_passwd(String pref_passwd) {
		this.pref_passwd = pref_passwd;
	}

	public String getPref_username() {
		return pref_username;
	}

	public void setPref_username(String pref_username) {
		this.pref_username = pref_username;
	}

	public String getPref_sipaccount() {
		return pref_sipaccount;
	}

	public void setPref_sipaccount(String pref_sipaccount) {
		this.pref_sipaccount = pref_sipaccount;
	}

	public String getWrong_username() {
		return wrong_username;
	}

	public void setWrong_username(String wrong_username) {
		this.wrong_username = wrong_username;
	}

	public String getWrong_passwd() {
		return wrong_passwd;
	}

	public void setWrong_passwd(String wrong_passwd) {
		this.wrong_passwd = wrong_passwd;
	}

	public String getWrong_domain() {
		return wrong_domain;
	}

	public void setWrong_domain(String wrong_domain) {
		this.wrong_domain = wrong_domain;
	}

	public String getWrong_settings() {
		return wrong_settings;
	}

	public void setWrong_settings(String wrong_settings) {
		this.wrong_settings = wrong_settings;
	}

	public String getTab_dialer() {
		return tab_dialer;
	}

	public void setTab_dialer(String tab_dialer) {
		this.tab_dialer = tab_dialer;
	}

	public String getTab_contact() {
		return tab_contact;
	}

	public void setTab_contact(String tab_contact) {
		this.tab_contact = tab_contact;
	}

	public String getCall_error() {
		return call_error;
	}

	public void setCall_error(String call_error) {
		this.call_error = call_error;
	}

	public String getYes() {
		return yes;
	}

	public void setYes(String yes) {
		this.yes = yes;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getDismiss() {
		return dismiss;
	}

	public void setDismiss(String dismiss) {
		this.dismiss = dismiss;
	}

	public String getCont() {
		return cont;
	}

	public void setCont(String cont) {
		this.cont = cont;
	}

	public String getNever_remind() {
		return never_remind;
	}

	public void setNever_remind(String never_remind) {
		this.never_remind = never_remind;
	}

	public String getConfig_error() {
		return config_error;
	}

	public void setConfig_error(String config_error) {
		this.config_error = config_error;
	}

	public String getEc_calibration_launch_message() {
		return ec_calibration_launch_message;
	}

	public void setEc_calibration_launch_message(
			String ec_calibration_launch_message) {
		this.ec_calibration_launch_message = ec_calibration_launch_message;
	}

	public String getWarning_already_incall() {
		return warning_already_incall;
	}

	public void setWarning_already_incall(String warning_already_incall) {
		this.warning_already_incall = warning_already_incall;
	}

	public String getTab_history() {
		return tab_history;
	}

	public void setTab_history(String tab_history) {
		this.tab_history = tab_history;
	}

	public String getWarning_wrong_destination_address() {
		return warning_wrong_destination_address;
	}

	public void setWarning_wrong_destination_address(
			String warning_wrong_destination_address) {
		this.warning_wrong_destination_address = warning_wrong_destination_address;
	}

	public String getMenu_clear_history() {
		return menu_clear_history;
	}

	public void setMenu_clear_history(String menu_clear_history) {
		this.menu_clear_history = menu_clear_history;
	}

	public String getError_cannot_get_call_parameters() {
		return error_cannot_get_call_parameters;
	}

	public void setError_cannot_get_call_parameters(
			String error_cannot_get_call_parameters) {
		this.error_cannot_get_call_parameters = error_cannot_get_call_parameters;
	}

	public String getError_cannot_create_default_parameters() {
		return error_cannot_create_default_parameters;
	}

	public void setError_cannot_create_default_parameters(
			String error_cannot_create_default_parameters) {
		this.error_cannot_create_default_parameters = error_cannot_create_default_parameters;
	}

	public String getError_cannot_invite_address() {
		return error_cannot_invite_address;
	}

	public void setError_cannot_invite_address(
			String error_cannot_invite_address) {
		this.error_cannot_invite_address = error_cannot_invite_address;
	}

	public String getNotification_started() {
		return notification_started;
	}

	public void setNotification_started(String notification_started) {
		this.notification_started = notification_started;
	}

	public String getPref_echo_cancellation_summary() {
		return pref_echo_cancellation_summary;
	}

	public void setPref_echo_cancellation_summary(
			String pref_echo_cancellation_summary) {
		this.pref_echo_cancellation_summary = pref_echo_cancellation_summary;
	}

	public String getPref_stun_server() {
		return pref_stun_server;
	}

	public void setPref_stun_server(String pref_stun_server) {
		this.pref_stun_server = pref_stun_server;
	}

	public String getPref_ice_enable() {
		return pref_ice_enable;
	}

	public void setPref_ice_enable(String pref_ice_enable) {
		this.pref_ice_enable = pref_ice_enable;
	}

	public String getPref_ice_enable_summary() {
		return pref_ice_enable_summary;
	}

	public void setPref_ice_enable_summary(String pref_ice_enable_summary) {
		this.pref_ice_enable_summary = pref_ice_enable_summary;
	}

	public String getEc_calibrating() {
		return ec_calibrating;
	}

	public void setEc_calibrating(String ec_calibrating) {
		this.ec_calibrating = ec_calibrating;
	}

	public String getEc_calibrated() {
		return ec_calibrated;
	}

	public void setEc_calibrated(String ec_calibrated) {
		this.ec_calibrated = ec_calibrated;
	}

	public String getNo_echo() {
		return no_echo;
	}

	public void setNo_echo(String no_echo) {
		this.no_echo = no_echo;
	}

	public String getFailed() {
		return failed;
	}

	public void setFailed(String failed) {
		this.failed = failed;
	}

	public String getFirst_login_explanation() {
		return first_login_explanation;
	}

	public void setFirst_login_explanation(String first_login_explanation) {
		this.first_login_explanation = first_login_explanation;
	}

	public String getFirst_login_username() {
		return first_login_username;
	}

	public void setFirst_login_username(String first_login_username) {
		this.first_login_username = first_login_username;
	}

	public String getFirst_login_password() {
		return first_login_password;
	}

	public void setFirst_login_password(String first_login_password) {
		this.first_login_password = first_login_password;
	}

	public String getFirst_login_connect() {
		return first_login_connect;
	}

	public void setFirst_login_connect(String first_login_connect) {
		this.first_login_connect = first_login_connect;
	}

	public String getFirst_launch_no_login_password() {
		return first_launch_no_login_password;
	}

	public void setFirst_launch_no_login_password(
			String first_launch_no_login_password) {
		this.first_launch_no_login_password = first_launch_no_login_password;
	}

	public String getFirst_launch_bad_login_password() {
		return first_launch_bad_login_password;
	}

	public void setFirst_launch_bad_login_password(
			String first_launch_bad_login_password) {
		this.first_launch_bad_login_password = first_launch_bad_login_password;
	}

	public String getPref_amr_summary() {
		return pref_amr_summary;
	}

	public void setPref_amr_summary(String pref_amr_summary) {
		this.pref_amr_summary = pref_amr_summary;
	}

	public String getPref_video_codec_vp8_title() {
		return pref_video_codec_vp8_title;
	}

	public void setPref_video_codec_vp8_title(String pref_video_codec_vp8_title) {
		this.pref_video_codec_vp8_title = pref_video_codec_vp8_title;
	}

	public String getPref_media_encryption() {
		return pref_media_encryption;
	}

	public void setPref_media_encryption(String pref_media_encryption) {
		this.pref_media_encryption = pref_media_encryption;
	}

	public String getMedia_encryption_none() {
		return media_encryption_none;
	}

	public void setMedia_encryption_none(String media_encryption_none) {
		this.media_encryption_none = media_encryption_none;
	}

	public String getMedia_encryption_srtp() {
		return media_encryption_srtp;
	}

	public void setMedia_encryption_srtp(String media_encryption_srtp) {
		this.media_encryption_srtp = media_encryption_srtp;
	}

	public String getMedia_encryption_zrtp() {
		return media_encryption_zrtp;
	}

	public void setMedia_encryption_zrtp(String media_encryption_zrtp) {
		this.media_encryption_zrtp = media_encryption_zrtp;
	}

	public String getPref_video_codec_h264_unavailable() {
		return pref_video_codec_h264_unavailable;
	}

	public void setPref_video_codec_h264_unavailable(
			String pref_video_codec_h264_unavailable) {
		this.pref_video_codec_h264_unavailable = pref_video_codec_h264_unavailable;
	}

	public String getPref_video_codec_mpeg4_unavailable() {
		return pref_video_codec_mpeg4_unavailable;
	}

	public void setPref_video_codec_mpeg4_unavailable(
			String pref_video_codec_mpeg4_unavailable) {
		this.pref_video_codec_mpeg4_unavailable = pref_video_codec_mpeg4_unavailable;
	}

	public String getPref_sipaccounts() {
		return pref_sipaccounts;
	}

	public void setPref_sipaccounts(String pref_sipaccounts) {
		this.pref_sipaccounts = pref_sipaccounts;
	}

	public String getPref_dtmf() {
		return pref_dtmf;
	}

	public void setPref_dtmf(String pref_dtmf) {
		this.pref_dtmf = pref_dtmf;
	}

	public String getPref_wifi_only() {
		return pref_wifi_only;
	}

	public void setPref_wifi_only(String pref_wifi_only) {
		this.pref_wifi_only = pref_wifi_only;
	}

	public String getPref_push_notification() {
		return pref_push_notification;
	}

	public void setPref_push_notification(String pref_push_notification) {
		this.pref_push_notification = pref_push_notification;
	}

	public String getWizard_failed() {
		return wizard_failed;
	}

	public void setWizard_failed(String wizard_failed) {
		this.wizard_failed = wizard_failed;
	}

	public String getWizard_server_unavailable() {
		return wizard_server_unavailable;
	}

	public void setWizard_server_unavailable(String wizard_server_unavailable) {
		this.wizard_server_unavailable = wizard_server_unavailable;
	}

	public String getWizard_username_unavailable() {
		return wizard_username_unavailable;
	}

	public void setWizard_username_unavailable(
			String wizard_username_unavailable) {
		this.wizard_username_unavailable = wizard_username_unavailable;
	}

	public String getWizard_username_incorrect() {
		return wizard_username_incorrect;
	}

	public void setWizard_username_incorrect(String wizard_username_incorrect) {
		this.wizard_username_incorrect = wizard_username_incorrect;
	}

	public String getWizard_email_incorrect() {
		return wizard_email_incorrect;
	}

	public void setWizard_email_incorrect(String wizard_email_incorrect) {
		this.wizard_email_incorrect = wizard_email_incorrect;
	}

	public String getWizard_password_incorrect() {
		return wizard_password_incorrect;
	}

	public void setWizard_password_incorrect(String wizard_password_incorrect) {
		this.wizard_password_incorrect = wizard_password_incorrect;
	}

	public String getWizard_passwords_unmatched() {
		return wizard_passwords_unmatched;
	}

	public void setWizard_passwords_unmatched(String wizard_passwords_unmatched) {
		this.wizard_passwords_unmatched = wizard_passwords_unmatched;
	}

	public String getPref_help_proxy() {
		return pref_help_proxy;
	}

	public void setPref_help_proxy(String pref_help_proxy) {
		this.pref_help_proxy = pref_help_proxy;
	}

	public String getPref_help_outbound_proxy() {
		return pref_help_outbound_proxy;
	}

	public void setPref_help_outbound_proxy(String pref_help_outbound_proxy) {
		this.pref_help_outbound_proxy = pref_help_outbound_proxy;
	}

	public String getPref_help_username() {
		return pref_help_username;
	}

	public void setPref_help_username(String pref_help_username) {
		this.pref_help_username = pref_help_username;
	}

	public String getPref_help_domain() {
		return pref_help_domain;
	}

	public void setPref_help_domain(String pref_help_domain) {
		this.pref_help_domain = pref_help_domain;
	}

	public String getPref_help_password() {
		return pref_help_password;
	}

	public void setPref_help_password(String pref_help_password) {
		this.pref_help_password = pref_help_password;
	}

	public String getDelete() {
		return delete;
	}

	public void setDelete(String delete) {
		this.delete = delete;
	}

	public String getChat() {
		return chat;
	}

	public void setChat(String chat) {
		this.chat = chat;
	}

	public String getCall() {
		return call;
	}

	public void setCall(String call) {
		this.call = call;
	}

	public String getAdd_to_contacts() {
		return add_to_contacts;
	}

	public void setAdd_to_contacts(String add_to_contacts) {
		this.add_to_contacts = add_to_contacts;
	}

	public String getStatus_connected() {
		return status_connected;
	}

	public void setStatus_connected(String status_connected) {
		this.status_connected = status_connected;
	}

	public String getStatus_not_connected() {
		return status_not_connected;
	}

	public void setStatus_not_connected(String status_not_connected) {
		this.status_not_connected = status_not_connected;
	}

	public String getStatus_in_progress() {
		return status_in_progress;
	}

	public void setStatus_in_progress(String status_in_progress) {
		this.status_in_progress = status_in_progress;
	}

	public String getStatus_error() {
		return status_error;
	}

	public void setStatus_error(String status_error) {
		this.status_error = status_error;
	}

	public String getAddressHint() {
		return addressHint;
	}

	public void setAddressHint(String addressHint) {
		this.addressHint = addressHint;
	}

	public String getConference() {
		return conference;
	}

	public void setConference(String conference) {
		this.conference = conference;
	}

	public String getIncoming() {
		return incoming;
	}

	public void setIncoming(String incoming) {
		this.incoming = incoming;
	}

	public String getDraft() {
		return draft;
	}

	public void setDraft(String draft) {
		this.draft = draft;
	}

	public String getNew_fast_chat() {
		return new_fast_chat;
	}

	public void setNew_fast_chat(String new_fast_chat) {
		this.new_fast_chat = new_fast_chat;
	}

	public String getNo_call_history() {
		return no_call_history;
	}

	public void setNo_call_history(String no_call_history) {
		this.no_call_history = no_call_history;
	}

	public String getNo_missed_call_history() {
		return no_missed_call_history;
	}

	public void setNo_missed_call_history(String no_missed_call_history) {
		this.no_missed_call_history = no_missed_call_history;
	}

	public String getNo_contact() {
		return no_contact;
	}

	public void setNo_contact(String no_contact) {
		this.no_contact = no_contact;
	}

	public String getNo_sip_contact() {
		return no_sip_contact;
	}

	public void setNo_sip_contact(String no_sip_contact) {
		this.no_sip_contact = no_sip_contact;
	}

	public String getNo_chat_history() {
		return no_chat_history;
	}

	public void setNo_chat_history(String no_chat_history) {
		this.no_chat_history = no_chat_history;
	}

	public String getCall_stats_audio() {
		return call_stats_audio;
	}

	public void setCall_stats_audio(String call_stats_audio) {
		this.call_stats_audio = call_stats_audio;
	}

	public String getCall_stats_video() {
		return call_stats_video;
	}

	public void setCall_stats_video(String call_stats_video) {
		this.call_stats_video = call_stats_video;
	}

	public String getCall_stats_codec() {
		return call_stats_codec;
	}

	public void setCall_stats_codec(String call_stats_codec) {
		this.call_stats_codec = call_stats_codec;
	}

	public String getCall_stats_upload() {
		return call_stats_upload;
	}

	public void setCall_stats_upload(String call_stats_upload) {
		this.call_stats_upload = call_stats_upload;
	}

	public String getCall_stats_download() {
		return call_stats_download;
	}

	public void setCall_stats_download(String call_stats_download) {
		this.call_stats_download = call_stats_download;
	}

	public String getCall_stats_ice() {
		return call_stats_ice;
	}

	public void setCall_stats_ice(String call_stats_ice) {
		this.call_stats_ice = call_stats_ice;
	}

	public String getCall_stats_video_resolution() {
		return call_stats_video_resolution;
	}

	public void setCall_stats_video_resolution(
			String call_stats_video_resolution) {
		this.call_stats_video_resolution = call_stats_video_resolution;
	}

	public String getContent_description_add_contact() {
		return content_description_add_contact;
	}

	public void setContent_description_add_contact(
			String content_description_add_contact) {
		this.content_description_add_contact = content_description_add_contact;
	}

	public String getSetup_title() {
		return setup_title;
	}

	public void setSetup_title(String setup_title) {
		this.setup_title = setup_title;
	}

	public String getSetup_apply() {
		return setup_apply;
	}

	public void setSetup_apply(String setup_apply) {
		this.setup_apply = setup_apply;
	}

	public String getSetup_password_hint() {
		return setup_password_hint;
	}

	public void setSetup_password_hint(String setup_password_hint) {
		this.setup_password_hint = setup_password_hint;
	}

	public String getSetup_domain_hint() {
		return setup_domain_hint;
	}

	public void setSetup_domain_hint(String setup_domain_hint) {
		this.setup_domain_hint = setup_domain_hint;
	}

	public String getSetup_password_confirm_hint() {
		return setup_password_confirm_hint;
	}

	public void setSetup_password_confirm_hint(
			String setup_password_confirm_hint) {
		this.setup_password_confirm_hint = setup_password_confirm_hint;
	}

	public String getSetup_email_hint() {
		return setup_email_hint;
	}

	public void setSetup_email_hint(String setup_email_hint) {
		this.setup_email_hint = setup_email_hint;
	}

	public String getSetup_create() {
		return setup_create;
	}

	public void setSetup_create(String setup_create) {
		this.setup_create = setup_create;
	}

	public String getSetup_validate_account() {
		return setup_validate_account;
	}

	public void setSetup_validate_account(String setup_validate_account) {
		this.setup_validate_account = setup_validate_account;
	}

	public String getSetup_check_account_validation() {
		return setup_check_account_validation;
	}

	public void setSetup_check_account_validation(
			String setup_check_account_validation) {
		this.setup_check_account_validation = setup_check_account_validation;
	}

	public String getSetup_account_not_validated() {
		return setup_account_not_validated;
	}

	public void setSetup_account_not_validated(
			String setup_account_not_validated) {
		this.setup_account_not_validated = setup_account_not_validated;
	}

	public String getSetup_account_validated() {
		return setup_account_validated;
	}

	public void setSetup_account_validated(String setup_account_validated) {
		this.setup_account_validated = setup_account_validated;
	}

	public String getButton_history() {
		return button_history;
	}

	public void setButton_history(String button_history) {
		this.button_history = button_history;
	}

	public String getButton_contacts() {
		return button_contacts;
	}

	public void setButton_contacts(String button_contacts) {
		this.button_contacts = button_contacts;
	}

	public String getButton_settings() {
		return button_settings;
	}

	public void setButton_settings(String button_settings) {
		this.button_settings = button_settings;
	}

	public String getButton_chat() {
		return button_chat;
	}

	public void setButton_chat(String button_chat) {
		this.button_chat = button_chat;
	}

	public String getButton_about() {
		return button_about;
	}

	public void setButton_about(String button_about) {
		this.button_about = button_about;
	}

	public String getButton_setup_cancel() {
		return button_setup_cancel;
	}

	public void setButton_setup_cancel(String button_setup_cancel) {
		this.button_setup_cancel = button_setup_cancel;
	}

	public String getButton_setup_back() {
		return button_setup_back;
	}

	public void setButton_setup_back(String button_setup_back) {
		this.button_setup_back = button_setup_back;
	}

	public String getButton_setup_next() {
		return button_setup_next;
	}

	public void setButton_setup_next(String button_setup_next) {
		this.button_setup_next = button_setup_next;
	}

	public String getButton_new_chat() {
		return button_new_chat;
	}

	public void setButton_new_chat(String button_new_chat) {
		this.button_new_chat = button_new_chat;
	}

	public String getButton_edit() {
		return button_edit;
	}

	public void setButton_edit(String button_edit) {
		this.button_edit = button_edit;
	}

	public String getButton_cancel() {
		return button_cancel;
	}

	public void setButton_cancel(String button_cancel) {
		this.button_cancel = button_cancel;
	}

	public String getButton_ok() {
		return button_ok;
	}

	public void setButton_ok(String button_ok) {
		this.button_ok = button_ok;
	}

	public String getButton_all_contacts() {
		return button_all_contacts;
	}

	public void setButton_all_contacts(String button_all_contacts) {
		this.button_all_contacts = button_all_contacts;
	}

	public String getButton_sip_contacts() {
		return button_sip_contacts;
	}

	public void setButton_sip_contacts(String button_sip_contacts) {
		this.button_sip_contacts = button_sip_contacts;
	}

	public String getButton_add_contact() {
		return button_add_contact;
	}

	public void setButton_add_contact(String button_add_contact) {
		this.button_add_contact = button_add_contact;
	}

	public String getButton_all_call() {
		return button_all_call;
	}

	public void setButton_all_call(String button_all_call) {
		this.button_all_call = button_all_call;
	}

	public String getButton_missed_call() {
		return button_missed_call;
	}

	public void setButton_missed_call(String button_missed_call) {
		this.button_missed_call = button_missed_call;
	}

	public String getButton_delete_all() {
		return button_delete_all;
	}

	public void setButton_delete_all(String button_delete_all) {
		this.button_delete_all = button_delete_all;
	}

	public String getButton_transfer() {
		return button_transfer;
	}

	public void setButton_transfer(String button_transfer) {
		this.button_transfer = button_transfer;
	}

	public String getButton_add_call() {
		return button_add_call;
	}

	public void setButton_add_call(String button_add_call) {
		this.button_add_call = button_add_call;
	}

	public String getButton_video() {
		return button_video;
	}

	public void setButton_video(String button_video) {
		this.button_video = button_video;
	}

	public String getButton_micro() {
		return button_micro;
	}

	public void setButton_micro(String button_micro) {
		this.button_micro = button_micro;
	}

	public String getButton_speaker() {
		return button_speaker;
	}

	public void setButton_speaker(String button_speaker) {
		this.button_speaker = button_speaker;
	}

	public String getButton_route() {
		return button_route;
	}

	public void setButton_route(String button_route) {
		this.button_route = button_route;
	}

	public String getButton_receiver() {
		return button_receiver;
	}

	public void setButton_receiver(String button_receiver) {
		this.button_receiver = button_receiver;
	}

	public String getButton_bluetooth() {
		return button_bluetooth;
	}

	public void setButton_bluetooth(String button_bluetooth) {
		this.button_bluetooth = button_bluetooth;
	}

	public String getButton_options() {
		return button_options;
	}

	public void setButton_options(String button_options) {
		this.button_options = button_options;
	}

	public String getButton_send_message() {
		return button_send_message;
	}

	public void setButton_send_message(String button_send_message) {
		this.button_send_message = button_send_message;
	}

	public String getButton_send_picture() {
		return button_send_picture;
	}

	public void setButton_send_picture(String button_send_picture) {
		this.button_send_picture = button_send_picture;
	}

	public String getUploading_image() {
		return uploading_image;
	}

	public void setUploading_image(String uploading_image) {
		this.uploading_image = uploading_image;
	}

	public String getCall_update_title() {
		return call_update_title;
	}

	public void setCall_update_title(String call_update_title) {
		this.call_update_title = call_update_title;
	}

	public String getCall_update_desc() {
		return call_update_desc;
	}

	public void setCall_update_desc(String call_update_desc) {
		this.call_update_desc = call_update_desc;
	}

	public String getCall_update_yes() {
		return call_update_yes;
	}

	public void setCall_update_yes(String call_update_yes) {
		this.call_update_yes = call_update_yes;
	}

	public String getCall_update_no() {
		return call_update_no;
	}

	public void setCall_update_no(String call_update_no) {
		this.call_update_no = call_update_no;
	}

	public String getShare_picture_size_small() {
		return share_picture_size_small;
	}

	public void setShare_picture_size_small(String share_picture_size_small) {
		this.share_picture_size_small = share_picture_size_small;
	}

	public String getShare_picture_size_medium() {
		return share_picture_size_medium;
	}

	public void setShare_picture_size_medium(String share_picture_size_medium) {
		this.share_picture_size_medium = share_picture_size_medium;
	}

	public String getShare_picture_size_large() {
		return share_picture_size_large;
	}

	public void setShare_picture_size_large(String share_picture_size_large) {
		this.share_picture_size_large = share_picture_size_large;
	}

	public String getShare_picture_size_real() {
		return share_picture_size_real;
	}

	public void setShare_picture_size_real(String share_picture_size_real) {
		this.share_picture_size_real = share_picture_size_real;
	}

	public String getSave_picture() {
		return save_picture;
	}

	public void setSave_picture(String save_picture) {
		this.save_picture = save_picture;
	}

	public String getText_copied_to_clipboard() {
		return text_copied_to_clipboard;
	}

	public void setText_copied_to_clipboard(String text_copied_to_clipboard) {
		this.text_copied_to_clipboard = text_copied_to_clipboard;
	}

	public String getCopy_text() {
		return copy_text;
	}

	public void setCopy_text(String copy_text) {
		this.copy_text = copy_text;
	}

	public String getImage_picker_title() {
		return image_picker_title;
	}

	public void setImage_picker_title(String image_picker_title) {
		this.image_picker_title = image_picker_title;
	}

	public String getImage_saved() {
		return image_saved;
	}

	public void setImage_saved(String image_saved) {
		this.image_saved = image_saved;
	}

	public String getImage_not_saved() {
		return image_not_saved;
	}

	public void setImage_not_saved(String image_not_saved) {
		this.image_not_saved = image_not_saved;
	}

	public String getPref_linphone_friend_title() {
		return pref_linphone_friend_title;
	}

	public void setPref_linphone_friend_title(String pref_linphone_friend_title) {
		this.pref_linphone_friend_title = pref_linphone_friend_title;
	}

	public String getPref_auto_accept_friends_title() {
		return pref_auto_accept_friends_title;
	}

	public void setPref_auto_accept_friends_title(
			String pref_auto_accept_friends_title) {
		this.pref_auto_accept_friends_title = pref_auto_accept_friends_title;
	}

	public String getPref_auto_accept_friends_desc() {
		return pref_auto_accept_friends_desc;
	}

	public void setPref_auto_accept_friends_desc(
			String pref_auto_accept_friends_desc) {
		this.pref_auto_accept_friends_desc = pref_auto_accept_friends_desc;
	}

	public String getLinphone_friend_new_request_title() {
		return linphone_friend_new_request_title;
	}

	public void setLinphone_friend_new_request_title(
			String linphone_friend_new_request_title) {
		this.linphone_friend_new_request_title = linphone_friend_new_request_title;
	}

	public String getLinphone_friend_new_request_desc() {
		return linphone_friend_new_request_desc;
	}

	public void setLinphone_friend_new_request_desc(
			String linphone_friend_new_request_desc) {
		this.linphone_friend_new_request_desc = linphone_friend_new_request_desc;
	}

	public String getSetup_ec_calibration() {
		return setup_ec_calibration;
	}

	public void setSetup_ec_calibration(String setup_ec_calibration) {
		this.setup_ec_calibration = setup_ec_calibration;
	}

	public String getPref_image_sharing_server_title() {
		return pref_image_sharing_server_title;
	}

	public void setPref_image_sharing_server_title(
			String pref_image_sharing_server_title) {
		this.pref_image_sharing_server_title = pref_image_sharing_server_title;
	}

	public String getPref_remote_provisioning_title() {
		return pref_remote_provisioning_title;
	}

	public void setPref_remote_provisioning_title(
			String pref_remote_provisioning_title) {
		this.pref_remote_provisioning_title = pref_remote_provisioning_title;
	}

	public String getCastel_incall_rotation_title() {
		return castel_incall_rotation_title;
	}

	public void setCastel_incall_rotation_title(
			String castel_incall_rotation_title) {
		this.castel_incall_rotation_title = castel_incall_rotation_title;
	}

	public String getCastel_incall_rotation_text_system() {
		return castel_incall_rotation_text_system;
	}

	public void setCastel_incall_rotation_text_system(
			String castel_incall_rotation_text_system) {
		this.castel_incall_rotation_text_system = castel_incall_rotation_text_system;
	}

	public String getCastel_incall_rotation_text_portrait() {
		return castel_incall_rotation_text_portrait;
	}

	public void setCastel_incall_rotation_text_portrait(
			String castel_incall_rotation_text_portrait) {
		this.castel_incall_rotation_text_portrait = castel_incall_rotation_text_portrait;
	}

	public String getCastel_incall_rotation_text_landscape() {
		return castel_incall_rotation_text_landscape;
	}

	public void setCastel_incall_rotation_text_landscape(
			String castel_incall_rotation_text_landscape) {
		this.castel_incall_rotation_text_landscape = castel_incall_rotation_text_landscape;
	}

	public String getPref_playback_gain_title() {
		return pref_playback_gain_title;
	}

	public void setPref_playback_gain_title(String pref_playback_gain_title) {
		this.pref_playback_gain_title = pref_playback_gain_title;
	}

	public String getPref_micro_gain_title() {
		return pref_micro_gain_title;
	}

	public void setPref_micro_gain_title(String pref_micro_gain_title) {
		this.pref_micro_gain_title = pref_micro_gain_title;
	}

	public String getPref_network_limit() {
		return pref_network_limit;
	}

	public void setPref_network_limit(String pref_network_limit) {
		this.pref_network_limit = pref_network_limit;
	}

	public String getPref_upload_bandwidth_title() {
		return pref_upload_bandwidth_title;
	}

	public void setPref_upload_bandwidth_title(
			String pref_upload_bandwidth_title) {
		this.pref_upload_bandwidth_title = pref_upload_bandwidth_title;
	}

	public String getPref_download_bandwidth_title() {
		return pref_download_bandwidth_title;
	}

	public void setPref_download_bandwidth_title(
			String pref_download_bandwidth_title) {
		this.pref_download_bandwidth_title = pref_download_bandwidth_title;
	}

	public String getDelete_contact() {
		return delete_contact;
	}

	public void setDelete_contact(String delete_contact) {
		this.delete_contact = delete_contact;
	}

	public String getSip_address() {
		return sip_address;
	}

	public void setSip_address(String sip_address) {
		this.sip_address = sip_address;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getContact_first_name() {
		return contact_first_name;
	}

	public void setContact_first_name(String contact_first_name) {
		this.contact_first_name = contact_first_name;
	}

	public String getContact_last_name() {
		return contact_last_name;
	}

	public void setContact_last_name(String contact_last_name) {
		this.contact_last_name = contact_last_name;
	}

	public String getPref_primary_account_title() {
		return pref_primary_account_title;
	}

	public void setPref_primary_account_title(String pref_primary_account_title) {
		this.pref_primary_account_title = pref_primary_account_title;
	}

	public String getPref_display_name_title() {
		return pref_display_name_title;
	}

	public void setPref_display_name_title(String pref_display_name_title) {
		this.pref_display_name_title = pref_display_name_title;
	}

	public String getPref_user_name_title() {
		return pref_user_name_title;
	}

	public void setPref_user_name_title(String pref_user_name_title) {
		this.pref_user_name_title = pref_user_name_title;
	}

	public String getPref_expire_title() {
		return pref_expire_title;
	}

	public void setPref_expire_title(String pref_expire_title) {
		this.pref_expire_title = pref_expire_title;
	}

	public String getPref_avpf() {
		return pref_avpf;
	}

	public void setPref_avpf(String pref_avpf) {
		this.pref_avpf = pref_avpf;
	}

	public String getPref_avpf_rr_interval() {
		return pref_avpf_rr_interval;
	}

	public void setPref_avpf_rr_interval(String pref_avpf_rr_interval) {
		this.pref_avpf_rr_interval = pref_avpf_rr_interval;
	}

	public String getPref_force_prefix() {
		return pref_force_prefix;
	}

	public void setPref_force_prefix(String pref_force_prefix) {
		this.pref_force_prefix = pref_force_prefix;
	}

	public String getPref_rfc2833_dtmf() {
		return pref_rfc2833_dtmf;
	}

	public void setPref_rfc2833_dtmf(String pref_rfc2833_dtmf) {
		this.pref_rfc2833_dtmf = pref_rfc2833_dtmf;
	}

	public String getPref_sipinfo_dtmf() {
		return pref_sipinfo_dtmf;
	}

	public void setPref_sipinfo_dtmf(String pref_sipinfo_dtmf) {
		this.pref_sipinfo_dtmf = pref_sipinfo_dtmf;
	}

	public String getPref_voice_mail() {
		return pref_voice_mail;
	}

	public void setPref_voice_mail(String pref_voice_mail) {
		this.pref_voice_mail = pref_voice_mail;
	}

	public String getError_call_declined() {
		return error_call_declined;
	}

	public void setError_call_declined(String error_call_declined) {
		this.error_call_declined = error_call_declined;
	}

	public String getError_user_not_found() {
		return error_user_not_found;
	}

	public void setError_user_not_found(String error_user_not_found) {
		this.error_user_not_found = error_user_not_found;
	}

	public String getError_incompatible_media() {
		return error_incompatible_media;
	}

	public void setError_incompatible_media(String error_incompatible_media) {
		this.error_incompatible_media = error_incompatible_media;
	}

	public String getError_low_bandwidth() {
		return error_low_bandwidth;
	}

	public void setError_low_bandwidth(String error_low_bandwidth) {
		this.error_low_bandwidth = error_low_bandwidth;
	}

	public String getError_network_unreachable() {
		return error_network_unreachable;
	}

	public void setError_network_unreachable(String error_network_unreachable) {
		this.error_network_unreachable = error_network_unreachable;
	}

	public String getToday() {
		return today;
	}

	public void setToday(String today) {
		this.today = today;
	}

	public String getYesterday() {
		return yesterday;
	}

	public void setYesterday(String yesterday) {
		this.yesterday = yesterday;
	}

	public String getCall_state_missed() {
		return call_state_missed;
	}

	public void setCall_state_missed(String call_state_missed) {
		this.call_state_missed = call_state_missed;
	}

	public String getCall_state_outgoing() {
		return call_state_outgoing;
	}

	public void setCall_state_outgoing(String call_state_outgoing) {
		this.call_state_outgoing = call_state_outgoing;
	}

	public String getCall_state_incoming() {
		return call_state_incoming;
	}

	public void setCall_state_incoming(String call_state_incoming) {
		this.call_state_incoming = call_state_incoming;
	}

	public String getPref_background_mode() {
		return pref_background_mode;
	}

	public void setPref_background_mode(String pref_background_mode) {
		this.pref_background_mode = pref_background_mode;
	}

	public String getShow_image() {
		return show_image;
	}

	public void setShow_image(String show_image) {
		this.show_image = show_image;
	}

	public String getDownload_image() {
		return download_image;
	}

	public void setDownload_image(String download_image) {
		this.download_image = download_image;
	}

	public String getDownload_image_failed() {
		return download_image_failed;
	}

	public void setDownload_image_failed(String download_image_failed) {
		this.download_image_failed = download_image_failed;
	}

	public String getPref_auth_userid() {
		return pref_auth_userid;
	}

	public void setPref_auth_userid(String pref_auth_userid) {
		this.pref_auth_userid = pref_auth_userid;
	}

	public String getPref_help_auth_userid() {
		return pref_help_auth_userid;
	}

	public void setPref_help_auth_userid(String pref_help_auth_userid) {
		this.pref_help_auth_userid = pref_help_auth_userid;
	}

	public String getPref_display_name() {
		return pref_display_name;
	}

	public void setPref_display_name(String pref_display_name) {
		this.pref_display_name = pref_display_name;
	}

	public String getPref_help_display_name() {
		return pref_help_display_name;
	}

	public void setPref_help_display_name(String pref_help_display_name) {
		this.pref_help_display_name = pref_help_display_name;
	}

	public String getPref_upnp_enable() {
		return pref_upnp_enable;
	}

	public void setPref_upnp_enable(String pref_upnp_enable) {
		this.pref_upnp_enable = pref_upnp_enable;
	}

	public String getPref_manage() {
		return pref_manage;
	}

	public void setPref_manage(String pref_manage) {
		this.pref_manage = pref_manage;
	}

	public String getWait() {
		return wait;
	}

	public void setWait(String wait) {
		this.wait = wait;
	}

	public String getImporting_messages() {
		return importing_messages;
	}

	public void setImporting_messages(String importing_messages) {
		this.importing_messages = importing_messages;
	}

	public String getDefault_account_flag() {
		return default_account_flag;
	}

	public void setDefault_account_flag(String default_account_flag) {
		this.default_account_flag = default_account_flag;
	}

	public String getSetup_remote_provisioning() {
		return setup_remote_provisioning;
	}

	public void setSetup_remote_provisioning(String setup_remote_provisioning) {
		this.setup_remote_provisioning = setup_remote_provisioning;
	}

	public String getSetup_remote_provisioning_hint() {
		return setup_remote_provisioning_hint;
	}

	public void setSetup_remote_provisioning_hint(
			String setup_remote_provisioning_hint) {
		this.setup_remote_provisioning_hint = setup_remote_provisioning_hint;
	}

	public String getSetup_remote_provisioning_url_hint() {
		return setup_remote_provisioning_url_hint;
	}

	public void setSetup_remote_provisioning_url_hint(
			String setup_remote_provisioning_url_hint) {
		this.setup_remote_provisioning_url_hint = setup_remote_provisioning_url_hint;
	}

	public String getSetup_remote_provisioning_login_hint() {
		return setup_remote_provisioning_login_hint;
	}

	public void setSetup_remote_provisioning_login_hint(
			String setup_remote_provisioning_login_hint) {
		this.setup_remote_provisioning_login_hint = setup_remote_provisioning_login_hint;
	}

	public String getSetup_confirm_username() {
		return setup_confirm_username;
	}

	public void setSetup_confirm_username(String setup_confirm_username) {
		this.setup_confirm_username = setup_confirm_username;
	}

	public String getSetup_title_assistant() {
		return setup_title_assistant;
	}

	public void setSetup_title_assistant(String setup_title_assistant) {
		this.setup_title_assistant = setup_title_assistant;
	}

	public String getZrtp_accept() {
		return zrtp_accept;
	}

	public void setZrtp_accept(String zrtp_accept) {
		this.zrtp_accept = zrtp_accept;
	}

	public String getZrtp_deny() {
		return zrtp_deny;
	}

	public void setZrtp_deny(String zrtp_deny) {
		this.zrtp_deny = zrtp_deny;
	}

	public String getZrtp_help() {
		return zrtp_help;
	}

	public void setZrtp_help(String zrtp_help) {
		this.zrtp_help = zrtp_help;
	}

	public String getRemote_composing() {
		return remote_composing;
	}

	public void setRemote_composing(String remote_composing) {
		this.remote_composing = remote_composing;
	}

	public String getUnread_messages() {
		return unread_messages;
	}

	public void setUnread_messages(String unread_messages) {
		this.unread_messages = unread_messages;
	}

	public String getRetry() {
		return retry;
	}

	public void setRetry(String retry) {
		this.retry = retry;
	}

	public String getRemote_provisioning_failure() {
		return remote_provisioning_failure;
	}

	public void setRemote_provisioning_failure(
			String remote_provisioning_failure) {
		this.remote_provisioning_failure = remote_provisioning_failure;
	}

	public String getRemote_provisioning_again_title() {
		return remote_provisioning_again_title;
	}

	public void setRemote_provisioning_again_title(
			String remote_provisioning_again_title) {
		this.remote_provisioning_again_title = remote_provisioning_again_title;
	}

	public String getRemote_provisioning_again_message() {
		return remote_provisioning_again_message;
	}

	public void setRemote_provisioning_again_message(
			String remote_provisioning_again_message) {
		this.remote_provisioning_again_message = remote_provisioning_again_message;
	}

	public String getPref_dtmf_sequence_title() {
		return pref_dtmf_sequence_title;
	}

	public void setPref_dtmf_sequence_title(String pref_dtmf_sequence_title) {
		this.pref_dtmf_sequence_title = pref_dtmf_sequence_title;
	}

	public String getDtmf_sequence_not_set_failure() {
		return dtmf_sequence_not_set_failure;
	}

	public void setDtmf_sequence_not_set_failure(
			String dtmf_sequence_not_set_failure) {
		this.dtmf_sequence_not_set_failure = dtmf_sequence_not_set_failure;
	}

	public String getDtmf_sequence() {
		return dtmf_sequence;
	}

	public void setDtmf_sequence(String dtmf_sequence) {
		this.dtmf_sequence = dtmf_sequence;
	}

	/**
	 * 截取后两位字符
	 * 
	 * @param name
	 * @return
	 */
	public static String getEndTwoLength(String str) {
		if (TextUtils.isEmpty(str))
			return "";
		int k = 0;
		String substring = str.substring(0, str.length());
		CharSequence charSequence = str.subSequence(0, str.length());
		for (int i = charSequence.length() - 1; i != 0; i--) {
			char c = charSequence.charAt(i);
			if (Character.toString(c).getBytes().length > 1) {
				k++;
			}
			k++;
			if (k >= 4) {
				return str.substring(i, str.length());
			}
		}
		return substring;

	}

	/**
	 * 获取非null
	 * 
	 * @param name
	 * @return
	 */
	public static String getNoNull(String name) {
		return TextUtils.isEmpty(name) ? "" : name;
	}

	/**
	 * 获取正常的手机号码
	 * 
	 * @return
	 */
	public static String getNormalNumber(String number) {
		if (!TextUtils.isEmpty(number)) {
			number = number.replaceAll(" ", "");
			number = number.replace("+86", "");
		} else {
			number = "";
		}
		return number;
	}

	/**
	 * 如果是汉字取前面两个字 如果是英文取四个字符
	 */
	public static String getTwoWord(String str) {
		if (TextUtils.isEmpty(str))
			return "";
		int k = 0;
		String substring = str.substring(0, str.length());
		CharSequence charSequence = str.subSequence(0, str.length());
		for (int i = 0; i < charSequence.length(); i++) {
			char c = charSequence.charAt(i);
			if (Character.toString(c).getBytes().length > 1) {
				k++;
			}
			k++;
			if (k >= 4) {
				return str.substring(0, i + 1);
			}
		}
		return substring;
	}
}

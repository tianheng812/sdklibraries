package org.linphone.tools;

public class BooleanUtil {

	private static BooleanUtil bUtil = null;

	private BooleanUtil() {
	}

	public static synchronized BooleanUtil getInstance() {
		if (bUtil == null) {
			bUtil = new BooleanUtil();
		}
		return bUtil;
	}

	private boolean show_tutorials_instead_of_app = false;
	private boolean enable_push_id = false;
	private boolean override_domain_using_default_one = false;
	private boolean use_simple_history = true;
	private boolean replace_settings_by_about = false;
	private boolean replace_chat_by_about = false;
	private boolean display_about_in_settings = false;
	private boolean exit_button_on_dialer = true;
	private boolean hide_camera_settings = false;
	private boolean replace_wizard_with_old_interface = true;
	private boolean hide_wizard = true;
	private boolean hide_linphone_accounts_wizard = true;
	private boolean hide_generic_accounts_wizard = false;
	private boolean hide_remote_provisioning_in_wizard = false;
	private boolean allow_cancel_remote_provisioning_login_activity = true;
	private boolean use_first_login_activity = false;
	private boolean display_account_wizard_at_first_start = false;
	private boolean use_linphone_server_ports = true;
	private boolean use_android_native_contact_edit_interface = false;
	private boolean use_alternative_contact_layout = true;
	private boolean hide_phone_numbers_in_editor = true;
	private boolean hide_sip_addresses_in_editor = false;
	private boolean forbid_empty_new_contact_in_editor = true;
	private boolean disable_animations = false;
	private boolean show_statusbar_only_on_dialer = true;
	private boolean lock_statusbar = true;
	private boolean emoticons_in_messages = true;
	private boolean only_display_username_if_unknown = true;
	private boolean never_display_sip_addresses = false;
	private boolean display_messages_time_and_status = true;
	private boolean display_time_aside = false;
	private boolean enable_linphone_friends = false;
	private boolean display_call_stats = true;
	private boolean show_current_calls_above_video = true;
	private boolean disable_options_in_call = false;
	private boolean pre_fill_email_in_wizard = true;
	private boolean allow_chat_multiline = false;
	private boolean call_last_log_if_adress_is_empty = true;
	private boolean allow_ringing_while_early_media = true;
	private boolean allow_transfers = true;
	private boolean allow_edit_in_dialer = true;
	private boolean forbid_self_call = false;
	private boolean use_linphone_chat_storage = true;
	private boolean auto_answer_calls = false;
	private boolean intercept_outgoing_gsm_calls = false;
	private boolean automatically_start_intercepted_outgoing_gsm_call = true;
	private boolean use_linphonecore_ringing = false;
	private boolean display_sms_remote_provisioning_activity = true;
	private boolean forbid_app_usage_until_remote_provisioning_completed = false;
	private boolean display_confirmation_popup_after_first_configuration = false;
	private boolean hash_images_as_name_before_upload = true;
	private boolean enable_log_collect = false;
	private boolean disable_every_log = false;
	private boolean disable_all_security_features_for_markets = false;
	private boolean disable_all_patented_codecs_for_markets = false;
	private boolean enable_call_notification = true;
	private boolean pref_echo_canceller_default = false;
	private boolean pref_video_use_front_camera_default = true;
	private boolean pref_video_initiate_call_with_video_default = true;
	private boolean pref_video_automatically_share_my_video_default = true;
	private boolean pref_video_automatically_accept_video_default = true;
	private boolean pref_wifi_only_default = false;
	private boolean pref_ice_enabled_default = false;
	private boolean pref_transport_use_random_ports_default = false;
	private boolean pref_push_notification_default = false;
	private boolean pref_debug_default = false;
	private boolean pref_animation_enable_default = false;
	private boolean pref_autostart_default = true;
	private boolean pref_auto_accept_friends_default = false;
	private boolean pref_sipinfo_dtmf_default = false;
	private boolean pref_rfc2833_dtmf_default = true;
	private boolean isTablet = false;
	private boolean disable_chat=false;

	public boolean isShow_tutorials_instead_of_app() {
		return show_tutorials_instead_of_app;
	}

	public void setShow_tutorials_instead_of_app(
			boolean show_tutorials_instead_of_app) {
		this.show_tutorials_instead_of_app = show_tutorials_instead_of_app;
	}

	public boolean isEnable_push_id() {
		return enable_push_id;
	}

	public void setEnable_push_id(boolean enable_push_id) {
		this.enable_push_id = enable_push_id;
	}

	public boolean isOverride_domain_using_default_one() {
		return override_domain_using_default_one;
	}

	public void setOverride_domain_using_default_one(
			boolean override_domain_using_default_one) {
		this.override_domain_using_default_one = override_domain_using_default_one;
	}

	public boolean isUse_simple_history() {
		return use_simple_history;
	}

	public void setUse_simple_history(boolean use_simple_history) {
		this.use_simple_history = use_simple_history;
	}

	public boolean isReplace_settings_by_about() {
		return replace_settings_by_about;
	}

	public void setReplace_settings_by_about(boolean replace_settings_by_about) {
		this.replace_settings_by_about = replace_settings_by_about;
	}

	public boolean isReplace_chat_by_about() {
		return replace_chat_by_about;
	}

	public void setReplace_chat_by_about(boolean replace_chat_by_about) {
		this.replace_chat_by_about = replace_chat_by_about;
	}

	public boolean isDisplay_about_in_settings() {
		return display_about_in_settings;
	}

	public void setDisplay_about_in_settings(boolean display_about_in_settings) {
		this.display_about_in_settings = display_about_in_settings;
	}

	public boolean isExit_button_on_dialer() {
		return exit_button_on_dialer;
	}

	public void setExit_button_on_dialer(boolean exit_button_on_dialer) {
		this.exit_button_on_dialer = exit_button_on_dialer;
	}

	public boolean isHide_camera_settings() {
		return hide_camera_settings;
	}

	public void setHide_camera_settings(boolean hide_camera_settings) {
		this.hide_camera_settings = hide_camera_settings;
	}

	public boolean isReplace_wizard_with_old_interface() {
		return replace_wizard_with_old_interface;
	}

	public void setReplace_wizard_with_old_interface(
			boolean replace_wizard_with_old_interface) {
		this.replace_wizard_with_old_interface = replace_wizard_with_old_interface;
	}

	public boolean isHide_wizard() {
		return hide_wizard;
	}

	public void setHide_wizard(boolean hide_wizard) {
		this.hide_wizard = hide_wizard;
	}

	public boolean isHide_linphone_accounts_wizard() {
		return hide_linphone_accounts_wizard;
	}

	public void setHide_linphone_accounts_wizard(
			boolean hide_linphone_accounts_wizard) {
		this.hide_linphone_accounts_wizard = hide_linphone_accounts_wizard;
	}

	public boolean isHide_generic_accounts_wizard() {
		return hide_generic_accounts_wizard;
	}

	public void setHide_generic_accounts_wizard(boolean hide_generic_accounts_wizard) {
		this.hide_generic_accounts_wizard = hide_generic_accounts_wizard;
	}

	public boolean isHide_remote_provisioning_in_wizard() {
		return hide_remote_provisioning_in_wizard;
	}

	public void setHide_remote_provisioning_in_wizard(
			boolean hide_remote_provisioning_in_wizard) {
		this.hide_remote_provisioning_in_wizard = hide_remote_provisioning_in_wizard;
	}

	public boolean isAllow_cancel_remote_provisioning_login_activity() {
		return allow_cancel_remote_provisioning_login_activity;
	}

	public void setAllow_cancel_remote_provisioning_login_activity(
			boolean allow_cancel_remote_provisioning_login_activity) {
		this.allow_cancel_remote_provisioning_login_activity = allow_cancel_remote_provisioning_login_activity;
	}

	public boolean isUse_first_login_activity() {
		return use_first_login_activity;
	}

	public void setUse_first_login_activity(boolean use_first_login_activity) {
		this.use_first_login_activity = use_first_login_activity;
	}

	public boolean isDisplay_account_wizard_at_first_start() {
		return display_account_wizard_at_first_start;
	}

	public void setDisplay_account_wizard_at_first_start(
			boolean display_account_wizard_at_first_start) {
		this.display_account_wizard_at_first_start = display_account_wizard_at_first_start;
	}

	public boolean isUse_linphone_server_ports() {
		return use_linphone_server_ports;
	}

	public void setUse_linphone_server_ports(boolean use_linphone_server_ports) {
		this.use_linphone_server_ports = use_linphone_server_ports;
	}

	public boolean isUse_android_native_contact_edit_interface() {
		return use_android_native_contact_edit_interface;
	}

	public void setUse_android_native_contact_edit_interface(
			boolean use_android_native_contact_edit_interface) {
		this.use_android_native_contact_edit_interface = use_android_native_contact_edit_interface;
	}

	public boolean isUse_alternative_contact_layout() {
		return use_alternative_contact_layout;
	}

	public void setUse_alternative_contact_layout(
			boolean use_alternative_contact_layout) {
		this.use_alternative_contact_layout = use_alternative_contact_layout;
	}

	public boolean isHide_phone_numbers_in_editor() {
		return hide_phone_numbers_in_editor;
	}

	public void setHide_phone_numbers_in_editor(boolean hide_phone_numbers_in_editor) {
		this.hide_phone_numbers_in_editor = hide_phone_numbers_in_editor;
	}

	public boolean isHide_sip_addresses_in_editor() {
		return hide_sip_addresses_in_editor;
	}

	public void setHide_sip_addresses_in_editor(boolean hide_sip_addresses_in_editor) {
		this.hide_sip_addresses_in_editor = hide_sip_addresses_in_editor;
	}

	public boolean isForbid_empty_new_contact_in_editor() {
		return forbid_empty_new_contact_in_editor;
	}

	public void setForbid_empty_new_contact_in_editor(
			boolean forbid_empty_new_contact_in_editor) {
		this.forbid_empty_new_contact_in_editor = forbid_empty_new_contact_in_editor;
	}

	public boolean isDisable_animations() {
		return disable_animations;
	}

	public void setDisable_animations(boolean disable_animations) {
		this.disable_animations = disable_animations;
	}

	public boolean isShow_statusbar_only_on_dialer() {
		return show_statusbar_only_on_dialer;
	}

	public void setShow_statusbar_only_on_dialer(
			boolean show_statusbar_only_on_dialer) {
		this.show_statusbar_only_on_dialer = show_statusbar_only_on_dialer;
	}

	public boolean isLock_statusbar() {
		return lock_statusbar;
	}

	public void setLock_statusbar(boolean lock_statusbar) {
		this.lock_statusbar = lock_statusbar;
	}

	public boolean isEmoticons_in_messages() {
		return emoticons_in_messages;
	}

	public void setEmoticons_in_messages(boolean emoticons_in_messages) {
		this.emoticons_in_messages = emoticons_in_messages;
	}

	public boolean isOnly_display_username_if_unknown() {
		return only_display_username_if_unknown;
	}

	public void setOnly_display_username_if_unknown(
			boolean only_display_username_if_unknown) {
		this.only_display_username_if_unknown = only_display_username_if_unknown;
	}

	public boolean isNever_display_sip_addresses() {
		return never_display_sip_addresses;
	}

	public void setNever_display_sip_addresses(boolean never_display_sip_addresses) {
		this.never_display_sip_addresses = never_display_sip_addresses;
	}

	public boolean isDisplay_messages_time_and_status() {
		return display_messages_time_and_status;
	}

	public void setDisplay_messages_time_and_status(
			boolean display_messages_time_and_status) {
		this.display_messages_time_and_status = display_messages_time_and_status;
	}

	public boolean isDisplay_time_aside() {
		return display_time_aside;
	}

	public void setDisplay_time_aside(boolean display_time_aside) {
		this.display_time_aside = display_time_aside;
	}

	public boolean isEnable_linphone_friends() {
		return enable_linphone_friends;
	}

	public void setEnable_linphone_friends(boolean enable_linphone_friends) {
		this.enable_linphone_friends = enable_linphone_friends;
	}

	public boolean isDisplay_call_stats() {
		return display_call_stats;
	}

	public void setDisplay_call_stats(boolean display_call_stats) {
		this.display_call_stats = display_call_stats;
	}

	public boolean isShow_current_calls_above_video() {
		return show_current_calls_above_video;
	}

	public void setShow_current_calls_above_video(
			boolean show_current_calls_above_video) {
		this.show_current_calls_above_video = show_current_calls_above_video;
	}

	public boolean isDisable_options_in_call() {
		return disable_options_in_call;
	}

	public void setDisable_options_in_call(boolean disable_options_in_call) {
		this.disable_options_in_call = disable_options_in_call;
	}

	public boolean isPre_fill_email_in_wizard() {
		return pre_fill_email_in_wizard;
	}

	public void setPre_fill_email_in_wizard(boolean pre_fill_email_in_wizard) {
		this.pre_fill_email_in_wizard = pre_fill_email_in_wizard;
	}

	public boolean isAllow_chat_multiline() {
		return allow_chat_multiline;
	}

	public void setAllow_chat_multiline(boolean allow_chat_multiline) {
		this.allow_chat_multiline = allow_chat_multiline;
	}

	public boolean isCall_last_log_if_adress_is_empty() {
		return call_last_log_if_adress_is_empty;
	}

	public void setCall_last_log_if_adress_is_empty(
			boolean call_last_log_if_adress_is_empty) {
		this.call_last_log_if_adress_is_empty = call_last_log_if_adress_is_empty;
	}

	public boolean isAllow_ringing_while_early_media() {
		return allow_ringing_while_early_media;
	}

	public void setAllow_ringing_while_early_media(
			boolean allow_ringing_while_early_media) {
		this.allow_ringing_while_early_media = allow_ringing_while_early_media;
	}

	public boolean isAllow_transfers() {
		return allow_transfers;
	}

	public void setAllow_transfers(boolean allow_transfers) {
		this.allow_transfers = allow_transfers;
	}

	public boolean isAllow_edit_in_dialer() {
		return allow_edit_in_dialer;
	}

	public void setAllow_edit_in_dialer(boolean allow_edit_in_dialer) {
		this.allow_edit_in_dialer = allow_edit_in_dialer;
	}

	public boolean isForbid_self_call() {
		return forbid_self_call;
	}

	public void setForbid_self_call(boolean forbid_self_call) {
		this.forbid_self_call = forbid_self_call;
	}


	public boolean isUse_linphone_chat_storage() {
		return use_linphone_chat_storage;
	}

	public void setUse_linphone_chat_storage(boolean use_linphone_chat_storage) {
		this.use_linphone_chat_storage = use_linphone_chat_storage;
	}

	public boolean isAuto_answer_calls() {
		return auto_answer_calls;
	}

	public void setAuto_answer_calls(boolean auto_answer_calls) {
		this.auto_answer_calls = auto_answer_calls;
	}

	public boolean isIntercept_outgoing_gsm_calls() {
		return intercept_outgoing_gsm_calls;
	}

	public void setIntercept_outgoing_gsm_calls(boolean intercept_outgoing_gsm_calls) {
		this.intercept_outgoing_gsm_calls = intercept_outgoing_gsm_calls;
	}

	public boolean isAutomatically_start_intercepted_outgoing_gsm_call() {
		return automatically_start_intercepted_outgoing_gsm_call;
	}

	public void setAutomatically_start_intercepted_outgoing_gsm_call(
			boolean automatically_start_intercepted_outgoing_gsm_call) {
		this.automatically_start_intercepted_outgoing_gsm_call = automatically_start_intercepted_outgoing_gsm_call;
	}

	public boolean isUse_linphonecore_ringing() {
		return use_linphonecore_ringing;
	}

	public void setUse_linphonecore_ringing(boolean use_linphonecore_ringing) {
		this.use_linphonecore_ringing = use_linphonecore_ringing;
	}

	public boolean isDisplay_sms_remote_provisioning_activity() {
		return display_sms_remote_provisioning_activity;
	}

	public void setDisplay_sms_remote_provisioning_activity(
			boolean display_sms_remote_provisioning_activity) {
		this.display_sms_remote_provisioning_activity = display_sms_remote_provisioning_activity;
	}

	public boolean isForbid_app_usage_until_remote_provisioning_completed() {
		return forbid_app_usage_until_remote_provisioning_completed;
	}

	public void setForbid_app_usage_until_remote_provisioning_completed(
			boolean forbid_app_usage_until_remote_provisioning_completed) {
		this.forbid_app_usage_until_remote_provisioning_completed = forbid_app_usage_until_remote_provisioning_completed;
	}

	public boolean isDisplay_confirmation_popup_after_first_configuration() {
		return display_confirmation_popup_after_first_configuration;
	}

	public void setDisplay_confirmation_popup_after_first_configuration(
			boolean display_confirmation_popup_after_first_configuration) {
		this.display_confirmation_popup_after_first_configuration = display_confirmation_popup_after_first_configuration;
	}

	public boolean isHash_images_as_name_before_upload() {
		return hash_images_as_name_before_upload;
	}

	public void setHash_images_as_name_before_upload(
			boolean hash_images_as_name_before_upload) {
		this.hash_images_as_name_before_upload = hash_images_as_name_before_upload;
	}

	public boolean isEnable_log_collect() {
		return enable_log_collect;
	}

	public void setEnable_log_collect(boolean enable_log_collect) {
		this.enable_log_collect = enable_log_collect;
	}

	public boolean isDisable_every_log() {
		return disable_every_log;
	}

	public void setDisable_every_log(boolean disable_every_log) {
		this.disable_every_log = disable_every_log;
	}

	public boolean isDisable_all_security_features_for_markets() {
		return disable_all_security_features_for_markets;
	}

	public void setDisable_all_security_features_for_markets(
			boolean disable_all_security_features_for_markets) {
		this.disable_all_security_features_for_markets = disable_all_security_features_for_markets;
	}

	public boolean isDisable_all_patented_codecs_for_markets() {
		return disable_all_patented_codecs_for_markets;
	}

	public void setDisable_all_patented_codecs_for_markets(
			boolean disable_all_patented_codecs_for_markets) {
		this.disable_all_patented_codecs_for_markets = disable_all_patented_codecs_for_markets;
	}

	public boolean isEnable_call_notification() {
		return enable_call_notification;
	}

	public void setEnable_call_notification(boolean enable_call_notification) {
		this.enable_call_notification = enable_call_notification;
	}

	public boolean isPref_echo_canceller_default() {
		return pref_echo_canceller_default;
	}

	public void setPref_echo_canceller_default(boolean pref_echo_canceller_default) {
		this.pref_echo_canceller_default = pref_echo_canceller_default;
	}

	public boolean isPref_video_use_front_camera_default() {
		return pref_video_use_front_camera_default;
	}

	public void setPref_video_use_front_camera_default(
			boolean pref_video_use_front_camera_default) {
		this.pref_video_use_front_camera_default = pref_video_use_front_camera_default;
	}

	public boolean isPref_video_initiate_call_with_video_default() {
		return pref_video_initiate_call_with_video_default;
	}

	public void setPref_video_initiate_call_with_video_default(
			boolean pref_video_initiate_call_with_video_default) {
		this.pref_video_initiate_call_with_video_default = pref_video_initiate_call_with_video_default;
	}

	public boolean isPref_video_automatically_share_my_video_default() {
		return pref_video_automatically_share_my_video_default;
	}

	public void setPref_video_automatically_share_my_video_default(
			boolean pref_video_automatically_share_my_video_default) {
		this.pref_video_automatically_share_my_video_default = pref_video_automatically_share_my_video_default;
	}

	public boolean isPref_video_automatically_accept_video_default() {
		return pref_video_automatically_accept_video_default;
	}

	public void setPref_video_automatically_accept_video_default(
			boolean pref_video_automatically_accept_video_default) {
		this.pref_video_automatically_accept_video_default = pref_video_automatically_accept_video_default;
	}

	public boolean isPref_wifi_only_default() {
		return pref_wifi_only_default;
	}

	public void setPref_wifi_only_default(boolean pref_wifi_only_default) {
		this.pref_wifi_only_default = pref_wifi_only_default;
	}

	public boolean isPref_ice_enabled_default() {
		return pref_ice_enabled_default;
	}

	public void setPref_ice_enabled_default(boolean pref_ice_enabled_default) {
		this.pref_ice_enabled_default = pref_ice_enabled_default;
	}

	public boolean isPref_transport_use_random_ports_default() {
		return pref_transport_use_random_ports_default;
	}

	public void setPref_transport_use_random_ports_default(
			boolean pref_transport_use_random_ports_default) {
		this.pref_transport_use_random_ports_default = pref_transport_use_random_ports_default;
	}

	public boolean isPref_push_notification_default() {
		return pref_push_notification_default;
	}

	public void setPref_push_notification_default(
			boolean pref_push_notification_default) {
		this.pref_push_notification_default = pref_push_notification_default;
	}

	public boolean isPref_debug_default() {
		return pref_debug_default;
	}

	public void setPref_debug_default(boolean pref_debug_default) {
		this.pref_debug_default = pref_debug_default;
	}

	public boolean isPref_animation_enable_default() {
		return pref_animation_enable_default;
	}

	public void setPref_animation_enable_default(
			boolean pref_animation_enable_default) {
		this.pref_animation_enable_default = pref_animation_enable_default;
	}

	public boolean isPref_autostart_default() {
		return pref_autostart_default;
	}

	public void setPref_autostart_default(boolean pref_autostart_default) {
		this.pref_autostart_default = pref_autostart_default;
	}

	public boolean isPref_auto_accept_friends_default() {
		return pref_auto_accept_friends_default;
	}

	public void setPref_auto_accept_friends_default(
			boolean pref_auto_accept_friends_default) {
		this.pref_auto_accept_friends_default = pref_auto_accept_friends_default;
	}

	public boolean isPref_sipinfo_dtmf_default() {
		return pref_sipinfo_dtmf_default;
	}

	public void setPref_sipinfo_dtmf_default(boolean pref_sipinfo_dtmf_default) {
		this.pref_sipinfo_dtmf_default = pref_sipinfo_dtmf_default;
	}

	public boolean isPref_rfc2833_dtmf_default() {
		return pref_rfc2833_dtmf_default;
	}

	public void setPref_rfc2833_dtmf_default(boolean pref_rfc2833_dtmf_default) {
		this.pref_rfc2833_dtmf_default = pref_rfc2833_dtmf_default;
	}

	public boolean isTablet() {
		return isTablet;
	}

	public void setTablet(boolean isTablet) {
		this.isTablet = isTablet;
	}

	public boolean isDisable_chat() {
		return disable_chat;
	}

	public void setDisable_chat(boolean disable_chat) {
		this.disable_chat = disable_chat;
	}

	
	

}

package org.linphone;

import android.content.Context;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneAuthInfo;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.core.LpConfig;
import org.linphone.core.TunnelConfig;
import org.linphone.mediastream.Log;
import org.linphone.tools.StringUtil;


public class LinphonePreferences {
	private static final int LINPHONE_CORE_RANDOM_PORT = -1;
	private static LinphonePreferences instance;
	private Context mContext;
	private TunnelConfig tunnelConfig = null;

	private LinphoneAuthInfo getAuthInfo(int paramInt) {
		LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
		try {
			LinphoneAddress localLinphoneAddress = LinphoneCoreFactory
					.instance().createLinphoneAddress(
							localLinphoneProxyConfig.getIdentity());
			LinphoneAuthInfo localLinphoneAuthInfo = getLc().findAuthInfo(
					localLinphoneAddress.getUserName(), null,
					localLinphoneAddress.getDomain());
			return localLinphoneAuthInfo;
		} catch (LinphoneCoreException e) {
			e.printStackTrace();
		}
		return null;
	}

	private LinphoneAuthInfo getClonedAuthInfo(int paramInt) {
		LinphoneAuthInfo localLinphoneAuthInfo1 = getAuthInfo(paramInt);
		if (localLinphoneAuthInfo1 == null)
			return null;
		LinphoneAuthInfo localLinphoneAuthInfo2 = localLinphoneAuthInfo1
				.clone();
		getLc().removeAuthInfo(localLinphoneAuthInfo1);
		return localLinphoneAuthInfo2;
	}

	private LinphoneCore getLc() {
		if (!LinphoneManager.isInstanciated())
			return null;
		return LinphoneManager.getLcIfManagerNotDestroyedOrNull();
	}

	private LinphoneProxyConfig getProxyConfig(int paramInt) {
		LinphoneProxyConfig[] arrayOfLinphoneProxyConfig = getLc()
				.getProxyConfigList();
		if ((paramInt < 0) || (paramInt >= arrayOfLinphoneProxyConfig.length))
			return null;
		return arrayOfLinphoneProxyConfig[paramInt];
	}

	private String getString(int paramInt) {
		if ((mContext == null) && (LinphoneManager.isInstanciated()))
			mContext = LinphoneManager.getInstance().getContext();
		return mContext.getString(paramInt);
	}

	public static final LinphonePreferences instance() {
		if (instance == null)
			instance = new LinphonePreferences();
		return instance;
	}

	private void saveAuthInfo(LinphoneAuthInfo paramLinphoneAuthInfo) {
		getLc().addAuthInfo(paramLinphoneAuthInfo);
	}

	public boolean areAnimationsEnabled() {
		return getConfig().getBool("app", "animations", false);
	}

	public boolean avpfEnabled(int paramInt) {
		return getProxyConfig(paramInt).avpfEnabled();
	}

	public void deleteAccount(int paramInt) {
		LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
		if (localLinphoneProxyConfig != null)
			getLc().removeProxyConfig(localLinphoneProxyConfig);
		if (getLc().getProxyConfigList().length != 0) {
			resetDefaultProxyConfig();
			getLc().refreshRegisters();
		}
	}

	public void disableProvisioningLoginView() {
		if (isProvisioningLoginViewEnabled()) {
			getConfig().setBool("app", "show_login_view", false);
			return;
		}
		Log.w(new Object[] { "Remote provisioning login view wasn't enabled, ignoring" });
	}

	public void echoConfigurationUpdated() {
		getConfig().setBool("app", "ec_updated", true);
	}

	public void enableAdaptiveRateControl(boolean paramBoolean) {
		getLc().enableAdaptiveRateControl(paramBoolean);
	}

	public void enableAvpf(int paramInt, boolean paramBoolean) {
		LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
		localLinphoneProxyConfig.edit();
		localLinphoneProxyConfig.enableAvpf(paramBoolean);
		localLinphoneProxyConfig.done();
	}

	public void enableVideo(boolean paramBoolean) {
		getLc().enableVideo(paramBoolean, paramBoolean);
	}

	public boolean enableVideoPreview() {
		return getConfig().getBool("app", "delayed_early_media", false);
	}

	public void firstLaunchSuccessful() {
		getConfig().setBool("app", "first_launch", false);
	}

	public void firstRemoteProvisioningSuccessful() {
		getConfig().setBool("app", "first_remote_provisioning", false);
	}

	public int getAccountCount() {
		if ((getLc() == null) || (getLc().getProxyConfigList() == null))
			return 0;
		return getLc().getProxyConfigList().length;
	}

	public String getAccountDisplayName(int paramInt) {
		try {
			String str = LinphoneCoreFactory
					.instance()
					.createLinphoneAddress(
							getProxyConfig(paramInt).getIdentity())
					.getDisplayName();
			return str;
		} catch (Exception localException) {
			localException.printStackTrace();
		}
		return null;
	}

	public String getAccountDomain(int paramInt) {
		return getProxyConfig(paramInt).getDomain();
	}

	public String getAccountPassword(int paramInt) {
		LinphoneAuthInfo localLinphoneAuthInfo = getAuthInfo(paramInt);
		if (localLinphoneAuthInfo == null)
			return null;
		return localLinphoneAuthInfo.getPassword();
	}

	public String getAccountProxy(int paramInt) {
		return getProxyConfig(paramInt).getProxy();
	}

	public LinphoneAddress.TransportType getAccountTransport(int paramInt) {
		LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
		if (localLinphoneProxyConfig != null)
			try {
				LinphoneAddress.TransportType localTransportType = LinphoneCoreFactory
						.instance()
						.createLinphoneAddress(
								localLinphoneProxyConfig.getProxy())
						.getTransport();
				return localTransportType;
			} catch (LinphoneCoreException localLinphoneCoreException) {
				localLinphoneCoreException.printStackTrace();
			}
		return null;
	}

	public String getAccountTransportKey(int paramInt) {
		LinphoneAddress.TransportType localTransportType = getAccountTransport(paramInt);
		String str = StringUtil.getInstance().getPref_transport_udp_key();
		if ((localTransportType != null)
				&& (localTransportType == LinphoneAddress.TransportType.LinphoneTransportTcp))
			str = StringUtil.getInstance().getPref_transport_tcp_key();
		if ((localTransportType != null)
				&& (localTransportType == LinphoneAddress.TransportType.LinphoneTransportTls))
			str = StringUtil.getInstance().getPref_transport_tls_key();
		return str;
	}

	public String getAccountTransportString(int paramInt) {
		LinphoneAddress.TransportType localTransportType = getAccountTransport(paramInt);
		if ((localTransportType != null)
				&& (localTransportType == LinphoneAddress.TransportType.LinphoneTransportTcp))
			return StringUtil.getInstance().getPref_transport_tcp();
		if ((localTransportType != null)
				&& (localTransportType == LinphoneAddress.TransportType.LinphoneTransportTls))
			return StringUtil.getInstance().getPref_transport_tls();
		return StringUtil.getInstance().getPref_transport_udp();
	}

	public String getAccountUserId(int paramInt) {
		LinphoneAuthInfo localLinphoneAuthInfo = getAuthInfo(paramInt);
		if (localLinphoneAuthInfo == null)
			return null;
		return localLinphoneAuthInfo.getUserId();
	}

	public String getAccountUsername(int paramInt) {
		LinphoneAuthInfo localLinphoneAuthInfo = getAuthInfo(paramInt);
		if (localLinphoneAuthInfo == null)
			return null;
		return localLinphoneAuthInfo.getUsername();
	}

	public LinphoneCore.AdaptiveRateAlgorithm getAdaptiveRateAlgorithm() {
		return getLc().getAdaptiveRateAlgorithm();
	}

	public String getAudioPort() {
		return String.valueOf(getConfig().getInt("rtp", "audio_rtp_port", 0));
	}

	public String getAvpfRRInterval(int paramInt) {
		return String.valueOf(getProxyConfig(paramInt).getAvpfRRInterval());
	}

	public int getCodecBitrateLimit() {
		return getConfig().getInt("audio", "codec_bitrate_limit", 36);
	}

	public LpConfig getConfig() {
		LinphoneCore lc = getLc();
		if (lc != null)
			return lc.getConfig();
		if (!LinphoneManager.isInstanciated()) {
			Log.w("LinphoneManager not instanciated yet..." );
			if(mContext==null) return null;
			return LinphoneCoreFactory.instance().createLpConfig(mContext.getFilesDir().getAbsolutePath() + "/.linphonerc");
		}
		return LinphoneCoreFactory.instance().createLpConfig(LinphoneManager.getInstance().mLinphoneConfigFile);
	}

	public int getDefaultAccountIndex() {
		LinphoneProxyConfig localLinphoneProxyConfig = getLc()
				.getDefaultProxyConfig();
		if (localLinphoneProxyConfig == null) {
			return -1;
		}
		LinphoneProxyConfig[] arrayOfLinphoneProxyConfig = getLc()
				.getProxyConfigList();
		for (int i = 0; i < arrayOfLinphoneProxyConfig.length; i++) {
			if (localLinphoneProxyConfig.getIdentity().equals(
					arrayOfLinphoneProxyConfig[i].getIdentity()))
				return i;
		}
		return -1;
	}

	public String getDefaultDisplayName() {
		return getLc().getPrimaryContactDisplayName();
	}

	public String getDefaultUsername() {
		return getLc().getPrimaryContactUsername();
	}

	public int getDownloadBandwidth() {
		return getConfig().getInt("net", "download_bw", 0);
	}

	public int getEchoCalibration() {
		return getConfig().getInt("sound", "ec_delay", -1);
	}

	public String getExpires(int paramInt) {
		return String.valueOf(getProxyConfig(paramInt).getExpires());
	}

	public String getForcePrefix() {
		return getConfig().getString("app", "force_prefix", null);
	}

	public String getInCallTimeout() {
		return String.valueOf(getConfig().getInt("sip", "in_call_timeout", 0));
	}

	public LinphoneCore.MediaEncryption getMediaEncryption() {
		return getLc().getMediaEncryption();
	}

	public String getMicroGain() {
		return String.valueOf(getConfig().getInt("sound", "mic_gain_db", 0));
	}

	public String getOutboundProxy(int paramInt) {
		return getProxyConfig(paramInt).getRoute();
	}

	public int getOutgoingCallTimeout() {
		return getConfig().getInt("sip", "outc_timeout", 0);
	}

	public String getPreferredVideoSize() {
		return getConfig().getString("video", "size", "qvga");
	}

	public String getPrefix(int paramInt) {
		return getProxyConfig(paramInt).getDialPrefix();
	}

	public String getPushNotificationRegistrationID() {
		return getConfig().getString("app", "push_notification_regid", null);
	}

	public String getRemoteProvisioningUrl() {
		return getConfig().getString("misc", "config-uri", null);
	}

	public boolean getReplacePlusByZeroZero(int paramInt) {
		return getProxyConfig(paramInt).getDialEscapePlus();
	}

	public String getRingtone(String paramString) {
		String str = getConfig().getString("app", "ringtone", paramString);
		if ((str == null) || (str.length() == 0))
			str = paramString;
		return str;
	}

	public String getRotation() {
		return getConfig().getString("app", "rotation", getString(2131296403));
	}

	public String getSharingPictureServerUrl() {
		return getConfig().getString("app", "sharing_server", null);
	}

	public String getSipPort() {
		LinphoneCore.Transports localTransports = getLc()
				.getSignalingTransportPorts();
		if (localTransports.udp > 0)
			return String.valueOf(localTransports.udp);
		else {
			return String.valueOf(localTransports.tcp);
		}
	}

	public String getStunServer() {
		return getLc().getStunServer();
	}

	public TunnelConfig getTunnelConfig() {
		if (getLc().isTunnelAvailable()) {
			if (tunnelConfig == null) {
				TunnelConfig[] arrayOfTunnelConfig = getLc().tunnelGetServers();
				if (arrayOfTunnelConfig.length > 0) {
					tunnelConfig = arrayOfTunnelConfig[0];
				} else {
					tunnelConfig = new TunnelConfig();
					tunnelConfig.setDelay(500);
				}
			}
			return tunnelConfig;
		} else {
			return null;
		}
	}

	public String getTunnelHost() {
		TunnelConfig localTunnelConfig = getTunnelConfig();
		if (localTunnelConfig != null)
			return localTunnelConfig.getHost();
		return null;
	}

	public String getTunnelMode() {
		return getConfig().getString("app", "tunnel", null);
	}

	public int getTunnelPort() {
		TunnelConfig localTunnelConfig = getTunnelConfig();
		if (localTunnelConfig != null)
			return localTunnelConfig.getPort();
		return -1;
	}

	public int getUploadBandwidth() {
		return getConfig().getInt("net", "upload_bw", 384);
	}

	public String getVideoPort() {
		return String.valueOf(getConfig().getInt("rtp", "video_rtp_port", 0));
	}

	public boolean isAccountEnabled(int paramInt) {
		return getProxyConfig(paramInt).registerEnabled();
	}

	public boolean isAccountOutboundProxySet(int paramInt) {
		return getProxyConfig(paramInt).getRoute() != null;
	}

	public boolean isAdaptiveRateControlEnabled() {
		return getLc().isAdaptiveRateControlEnabled();
	}

	public boolean isAutoStartEnabled() {
		return getConfig().getBool("app", "auto_start", true);
	}

	public boolean isBackgroundModeEnabled() {
		return getConfig().getBool("app", "background_mode", true);
	}

	public boolean isDebugEnabled() {
		return getConfig().getBool("app", "debug", false);
	}

	public boolean isEchoCancellationEnabled() {
		return getLc().isEchoCancellationEnabled();
	}

	public boolean isEchoConfigurationUpdated() {
		return getConfig().getBool("app", "ec_updated", false);
	}

	public boolean isFirstLaunch() {
		return getConfig().getBool("app", "first_launch", true);
	}

	public boolean isFirstRemoteProvisioning() {
		return getConfig().getBool("app", "first_remote_provisioning", true);
	}

	public boolean isIceEnabled() {
		return getLc().getFirewallPolicy() == LinphoneCore.FirewallPolicy.UseIce;
	}

	public boolean isProvisioningLoginViewEnabled() {
		return getConfig().getBool("app", "show_login_view", false);
	}

	public boolean isPushNotificationEnabled() {
		return getConfig().getBool("app", "push_notification", false);
	}

	public boolean isUpnpEnabled() {
		return (getLc().upnpAvailable())
				&& (getLc().getFirewallPolicy() == LinphoneCore.FirewallPolicy.UseUpnp);
	}

	public boolean isUsingIpv6() {
		return getLc().isIpv6Enabled();
	}

	public boolean isUsingRandomPort() {
		return getConfig().getBool("app", "random_port", true);
	}

	public boolean isVideoEnabled() {
		return (getLc().isVideoSupported()) && (getLc().isVideoEnabled());
	}

	public boolean isWifiOnlyEnabled() {
		return getConfig().getBool("app", "wifi_only", false);
	}

	public void removePreviousVersionAuthInfoRemoval() {
		getConfig().setBool("sip", "store_auth_info", true);
	}

	public void resetDefaultProxyConfig() {
		int count = getLc().getProxyConfigList().length;
		for (int i = 0; i < count; i++) {
			if (isAccountEnabled(i)) {
				getLc().setDefaultProxyConfig(getProxyConfig(i));
				break;
			}
		}

		if(getLc().getDefaultProxyConfig() == null){
			getLc().setDefaultProxyConfig(getProxyConfig(0));
		}
	}

	public void sendDTMFsAsSipInfo(boolean paramBoolean) {
		getLc().setUseSipInfoForDtmfs(paramBoolean);
	}

	public void sendDtmfsAsRfc2833(boolean paramBoolean) {
		getLc().setUseRfc2833ForDtmfs(paramBoolean);
	}

	public void setAccountContactParameters(int paramInt, String paramString) {
		LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
		localLinphoneProxyConfig.edit();
		localLinphoneProxyConfig.setContactUriParameters(paramString);
		localLinphoneProxyConfig.done();
	}

	public void setAccountDisplayName(int paramInt, String paramString) {
		try {
			LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
			LinphoneAddress localLinphoneAddress = LinphoneCoreFactory
					.instance().createLinphoneAddress(
							localLinphoneProxyConfig.getIdentity());
			localLinphoneAddress.setDisplayName(paramString);
			localLinphoneProxyConfig.edit();
			localLinphoneProxyConfig.setIdentity(localLinphoneAddress
					.asString());
			localLinphoneProxyConfig.done();
		} catch (Exception localException) {
			localException.printStackTrace();
		}
	}

	public void setAccountDomain(int paramInt, String paramString) {
		String str = "sip:" + getAccountUsername(paramInt) + "@" + paramString;
		try {
			LinphoneAuthInfo localLinphoneAuthInfo = getClonedAuthInfo(paramInt);
			localLinphoneAuthInfo.setDomain(paramString);
			saveAuthInfo(localLinphoneAuthInfo);
			LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
			localLinphoneProxyConfig.edit();
			localLinphoneProxyConfig.setIdentity(str);
			localLinphoneProxyConfig.done();
		} catch (LinphoneCoreException localLinphoneCoreException) {
			localLinphoneCoreException.printStackTrace();
		}
	}

	public void setAccountEnabled(int paramInt, boolean paramBoolean) {
		LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
		localLinphoneProxyConfig.edit();
		localLinphoneProxyConfig.enableRegister(paramBoolean);
		localLinphoneProxyConfig.done();
		if ((!paramBoolean)
				&& (getLc().getDefaultProxyConfig().getIdentity()
						.equals(localLinphoneProxyConfig.getIdentity()))) {
			int i = getLc().getProxyConfigList().length;
			if (i > 1) {
				for (int j = 0; j < i; j++) {
					if (isAccountEnabled(j)) {
						getLc().setDefaultProxyConfig(getProxyConfig(j));
						break;
					}
				}
			}
		}

	}

	public void setAccountOutboundProxy(int paramInt, String paramString) {
		try {
			LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
			localLinphoneProxyConfig.edit();
			if (paramString != null) {
				localLinphoneProxyConfig.setRoute(paramString);
				localLinphoneProxyConfig.done();
				localLinphoneProxyConfig.setRoute(null);
			}
		} catch (LinphoneCoreException localLinphoneCoreException) {
			localLinphoneCoreException.printStackTrace();
		}
	}

	public void setAccountPassword(int paramInt, String paramString) {
		LinphoneAuthInfo localLinphoneAuthInfo = getClonedAuthInfo(paramInt);
		localLinphoneAuthInfo.setPassword(paramString);
		saveAuthInfo(localLinphoneAuthInfo);
	}

	public void setAccountProxy(int paramInt, String paramString) {
		if ((paramString == null) || (paramString.length() <= 0))
			paramString = getAccountDomain(paramInt);
		if (!paramString.contains("sip:"))
			paramString = "sip:" + paramString;
		try {
			LinphoneAddress localLinphoneAddress = LinphoneCoreFactory
					.instance().createLinphoneAddress(paramString);
			if (!paramString.contains("transport="))
				localLinphoneAddress
						.setTransport(getAccountTransport(paramInt));
			LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
			localLinphoneProxyConfig.edit();
			localLinphoneProxyConfig.setProxy(localLinphoneAddress
					.asStringUriOnly());
			localLinphoneProxyConfig.done();
		} catch (LinphoneCoreException localLinphoneCoreException) {
			localLinphoneCoreException.printStackTrace();
		}
	}

	public void setAccountTransport(int paramInt, String paramString) {
		LinphoneProxyConfig proxyConfig = getProxyConfig(paramInt);
		if ((proxyConfig != null) && (paramString != null)) {
			LinphoneAddress proxyAddr = null;
			int port = 0;
			try {
				proxyAddr = LinphoneCoreFactory.instance()
						.createLinphoneAddress(proxyConfig.getProxy());
				if (paramString.equals(StringUtil.getInstance().getPref_transport_udp_key())) {
					proxyAddr
							.setTransport(LinphoneAddress.TransportType.LinphoneTransportUdp);
				} else if (paramString
						.equals(StringUtil.getInstance().getPref_transport_tcp_key())) {
					proxyAddr
							.setTransport(LinphoneAddress.TransportType.LinphoneTransportTcp);
				} else if (paramString
						.equals(StringUtil.getInstance().getPref_transport_tls_key())) {
					proxyAddr
							.setTransport(LinphoneAddress.TransportType.LinphoneTransportTls);
					port = 5223;
				}

				/*
				 * if ("sip.linphone.org".equals(proxyConfig.getDomain())) {
				 * proxyAddr.setTransport(port); }
				 */

				LinphoneProxyConfig prxCfg = getProxyConfig(paramInt);
				prxCfg.edit();
				prxCfg.setProxy(proxyAddr.asStringUriOnly());
				prxCfg.done();

			} catch (LinphoneCoreException localLinphoneCoreException) {
				localLinphoneCoreException.printStackTrace();
			}

		}
	}

	public void setAccountUserId(int paramInt, String paramString) {
		LinphoneAuthInfo localLinphoneAuthInfo = getClonedAuthInfo(paramInt);
		localLinphoneAuthInfo.setUserId(paramString);
		saveAuthInfo(localLinphoneAuthInfo);
	}

	public void setAccountUsername(int paramInt, String paramString) {
		String str = "sip:" + paramString + "@" + getAccountDomain(paramInt);
		LinphoneAuthInfo localLinphoneAuthInfo = getClonedAuthInfo(paramInt);
		try {
			LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
			localLinphoneProxyConfig.edit();
			localLinphoneProxyConfig.setIdentity(str);
			localLinphoneProxyConfig.done();
			localLinphoneAuthInfo.setUsername(paramString);
			saveAuthInfo(localLinphoneAuthInfo);
		} catch (LinphoneCoreException localLinphoneCoreException) {
			localLinphoneCoreException.printStackTrace();
		}
	}

	public void setAdaptiveRateAlgorithm(
			LinphoneCore.AdaptiveRateAlgorithm paramAdaptiveRateAlgorithm) {
		getLc().setAdaptiveRateAlgorithm(paramAdaptiveRateAlgorithm);
	}

	public void setAnimationsEnabled(boolean paramBoolean) {
		getConfig().setBool("app", "animations", paramBoolean);
	}

	public void setAudioPort(int paramInt) {
		getLc().setAudioPort(paramInt);
	}

	public void setAutoStart(boolean paramBoolean) {
		getConfig().setBool("app", "auto_start", paramBoolean);
	}

	public void setAutomaticallyAcceptVideoRequests(boolean paramBoolean) {
		getLc().setVideoPolicy(shouldInitiateVideoCall(), paramBoolean);
	}

	public void setAvpfRRInterval(int paramInt, String paramString) {
		try {
			LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
			localLinphoneProxyConfig.edit();
			localLinphoneProxyConfig.setAvpfRRInterval(Integer
					.parseInt(paramString));
			localLinphoneProxyConfig.done();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public void setBackgroundModeEnabled(boolean paramBoolean) {
		getConfig().setBool("app", "background_mode", paramBoolean);
	}

	public void setCodecBitrateLimit(int paramInt) {
		getConfig().setInt("audio", "codec_bitrate_limit", paramInt);
	}

	public void setContext(Context paramContext) {
		mContext = paramContext;
	}

	public void setDebugEnabled(boolean paramBoolean) {
		getConfig().setBool("app", "debug", paramBoolean);
		LinphoneCoreFactory.instance().setDebugMode(paramBoolean,"sayee");
	}

	public void setDefaultAccount(int paramInt) {
		LinphoneProxyConfig[] arrayOfLinphoneProxyConfig = getLc()
				.getProxyConfigList();
		if ((paramInt >= 0) && (paramInt < arrayOfLinphoneProxyConfig.length))
			getLc().setDefaultProxyConfig(arrayOfLinphoneProxyConfig[paramInt]);
	}

	public void setDefaultDisplayName(String paramString) {
		getLc().setPrimaryContact(paramString, getDefaultUsername());
	}

	public void setDefaultUsername(String paramString) {
		getLc().setPrimaryContact(getDefaultDisplayName(), paramString);
	}

	public void setDownloadBandwidth(int paramInt) {
		getLc().setDownloadBandwidth(paramInt);
	}

	public void setEchoCancellation(boolean paramBoolean) {
		getLc().enableEchoCancellation(paramBoolean);
	}

	public void setExpires(int paramInt, String paramString) {
		try {
			LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
			localLinphoneProxyConfig.edit();
			localLinphoneProxyConfig.setExpires(Integer.parseInt(paramString));
			localLinphoneProxyConfig.done();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public void setForcePrefix(String paramString) {
		getConfig().setString("app", "force_prefix", paramString);
	}

	public void setFrontCamAsDefault(boolean paramBoolean) {
		getConfig().setBool("app", "front_camera_default", paramBoolean);
	}

	public void setIceEnabled(boolean paramBoolean) {
		if (paramBoolean) {
			getLc().setFirewallPolicy(LinphoneCore.FirewallPolicy.UseIce);
			return;
		}
		String str = getStunServer();
		if ((str != null) && (str.length() > 0)) {
			getLc().setFirewallPolicy(LinphoneCore.FirewallPolicy.UseStun);
			return;
		}
		getLc().setFirewallPolicy(LinphoneCore.FirewallPolicy.NoFirewall);
	}

	public void setInCallTimeout(int paramInt) {
		getLc().setInCallTimeout(paramInt);
		getConfig().setInt("sip", "in_call_timeout", paramInt);
	}

	public void setInitiateVideoCall(boolean paramBoolean) {
		getLc().setVideoPolicy(paramBoolean,
				shouldAutomaticallyAcceptVideoRequests());
	}

	public void setMediaEncryption(
			LinphoneCore.MediaEncryption paramMediaEncryption) {
		if (paramMediaEncryption == null)
			return;
		getLc().setMediaEncryption(paramMediaEncryption);
	}

	public void setMicroGain(int paramInt) {
		getLc().setMicrophoneGain(paramInt);
	}

	public void setOutgoingCallTimeout(int paramInt) {
		getConfig().setInt("sip", "outc_timeout", paramInt);
	}

	public void setPreferredVideoSize(String preferredVideoSize) {
		int bandwidth = 512;
		if (preferredVideoSize.equals("720p")) {
			bandwidth = 1024 + 128;
		} else if (preferredVideoSize.equals("qvga")) {
			bandwidth = 380;
		} else if (preferredVideoSize.equals("qcif")) {
			bandwidth = 256;
		}

		getLc().setPreferredVideoSizeByName(preferredVideoSize);
		getLc().setUploadBandwidth(bandwidth);
		getLc().setDownloadBandwidth(bandwidth);
	}

	public void setPrefix(int paramInt, String paramString) {
		LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
		localLinphoneProxyConfig.edit();
		localLinphoneProxyConfig.setDialPrefix(paramString);
		localLinphoneProxyConfig.done();
	}

	public void setPushNotificationEnabled(boolean paramBoolean) {
		getConfig().setBool("app", "push_notification", paramBoolean);
		if (paramBoolean) {
			String str1 = getPushNotificationRegistrationID();
			String str2 = StringUtil.getInstance().getPush_sender_id();
			if ((str1 != null) && (getLc().getProxyConfigList().length > 0)) {
				for (LinphoneProxyConfig localLinphoneProxyConfig2 : getLc()
						.getProxyConfigList()) {
					String str3 = "app-id=" + str2 + ";pn-type=google;pn-tok="
							+ str1;
					localLinphoneProxyConfig2.edit();
					localLinphoneProxyConfig2.setContactUriParameters(str3);
					localLinphoneProxyConfig2.done();
					Log.d(new Object[] { "Push notif infos added to proxy config" });
				}
				getLc().refreshRegisters();
			}
		}
		for (LinphoneProxyConfig localLinphoneProxyConfig1 : getLc()
				.getProxyConfigList()) {
			localLinphoneProxyConfig1.edit();
			localLinphoneProxyConfig1.setContactUriParameters(null);
			localLinphoneProxyConfig1.done();
			Log.d(new Object[] { "Push notif infos removed from proxy config" });
		}
		getLc().refreshRegisters();
	}

	public void setPushNotificationRegistrationID(String paramString) {
		getConfig().setString("app", "push_notification_regid", paramString);
	}

	public void setRemoteProvisioningUrl(String paramString) {
		if ((paramString != null) && (paramString.length() == 0))
			paramString = null;
		LpConfig localLpConfig = getConfig();
		localLpConfig.setString("misc", "config-uri", paramString);
		localLpConfig.sync();
	}

	public void setReplacePlusByZeroZero(int paramInt, boolean paramBoolean) {
		LinphoneProxyConfig localLinphoneProxyConfig = getProxyConfig(paramInt);
		localLinphoneProxyConfig.edit();
		localLinphoneProxyConfig.setDialEscapePlus(paramBoolean);
		localLinphoneProxyConfig.done();
	}

	public void setRingtone(String paramString) {
		getConfig().setString("app", "ringtone", paramString);
	}

	public void setRotation(String paramString) {
		getConfig().setString("app", "rotation", paramString);
	}

	public void setSharingPictureServerUrl(String paramString) {
		getConfig().setString("app", "sharing_server", paramString);
	}

	public void setSipPort(int paramInt) {
		LinphoneCore.Transports localTransports = getLc()
				.getSignalingTransportPorts();
		localTransports.udp = paramInt;
		localTransports.tcp = paramInt;
		localTransports.tls = -1;
		getLc().setSignalingTransportPorts(localTransports);
	}

	public void setStunServer(String paramString) {
		getLc().setStunServer(paramString);
	}

	public void setTunnelHost(String paramString) {
		TunnelConfig localTunnelConfig = getTunnelConfig();
		if (localTunnelConfig != null) {
			localTunnelConfig.setHost(paramString);
			LinphoneManager.getInstance().initTunnelFromConf();
		}
	}

	public void setTunnelMode(String paramString) {
		getConfig().setString("app", "tunnel", paramString);
		LinphoneManager.getInstance().initTunnelFromConf();
	}

	public void setTunnelPort(int paramInt) {
		TunnelConfig localTunnelConfig = getTunnelConfig();
		if (localTunnelConfig != null) {
			localTunnelConfig.setPort(paramInt);
			LinphoneManager.getInstance().initTunnelFromConf();
		}
	}

	public void setUploadBandwith(int paramInt) {
		getLc().setUploadBandwidth(paramInt);
	}

	public void setUpnpEnabled(boolean paramBoolean) {
		if (paramBoolean) {
			if (isIceEnabled()) {
				Log.e(new Object[] { "Cannot have both ice and upnp enabled, disabling upnp" });
				return;
			}
			getLc().setFirewallPolicy(LinphoneCore.FirewallPolicy.UseUpnp);
			return;
		}
		String str = getStunServer();
		if ((str != null) && (str.length() > 0)) {
			getLc().setFirewallPolicy(LinphoneCore.FirewallPolicy.UseStun);
			return;
		}
		getLc().setFirewallPolicy(LinphoneCore.FirewallPolicy.NoFirewall);
	}

	public void setVideoPort(int paramInt) {
		getLc().setVideoPort(paramInt);
	}

	public void setVideoPreview(boolean paramBoolean) {
		getConfig().setBool("app", "delayed_early_media", paramBoolean);
	}

	public void setWifiOnlyEnabled(Boolean paramBoolean) {
		getConfig().setBool("app", "wifi_only", paramBoolean.booleanValue());
	}

	public boolean shouldAutomaticallyAcceptFriendsRequests() {
		return false;
	}

	public boolean shouldAutomaticallyAcceptVideoRequests() {
		return getLc().getVideoAutoAcceptPolicy();
	}

	public boolean shouldInitiateVideoCall() {
		return getLc().getVideoAutoInitiatePolicy();
	}

	public boolean useFrontCam() {
		if(getConfig()==null) return true;
		return getConfig().getBool("app", "front_camera_default", true);
	}

	public void useIpv6(Boolean paramBoolean) {
		getLc().enableIpv6(paramBoolean.booleanValue());
	}

	public void useRandomPort(boolean paramBoolean) {
		useRandomPort(paramBoolean, true);
	}

	public void useRandomPort(boolean enabled, boolean apply) {
		getConfig().setBool("app", "random_port", enabled);
		if (apply) {
			if (enabled) {
				setSipPort(LINPHONE_CORE_RANDOM_PORT);
			} else {
				setSipPort(5060);
			}
		}
	}

	public boolean useRfc2833Dtmfs() {
		return getLc().getUseRfc2833ForDtmfs();
	}

	public boolean useSipInfoDtmfs() {
		return getLc().getUseSipInfoForDtmfs();
	}

	public static class AccountBuilder {
		private LinphoneCore lc;
		private boolean tempAvpfEnabled = false;
		private int tempAvpfRRInterval = 0;
		private String tempContactsParams;
		private String tempDomain;
		private boolean tempEnabled = true;
		private String tempExpire;
		private boolean tempNoDefault = false;
		private String tempOutboundProxy;
		private boolean tempOutboundProxyEnabled;
		private String tempPassword;
		private String tempProxy;
		private String tempQualityReportingCollector;
		private boolean tempQualityReportingEnabled = false;
		private int tempQualityReportingInterval = 0;
		private String tempRealm;
		private LinphoneAddress.TransportType tempTransport;
		private String tempUserId;
		private String tempUsername;
		private String tempDisplayName;

		public AccountBuilder(LinphoneCore paramLinphoneCore) {
			lc = paramLinphoneCore;
		}

		public boolean saveNewAccount() throws LinphoneCoreException {
			if (tempUsername == null || tempUsername.length() < 1
					|| tempDomain == null || tempDomain.length() < 1) {
				Log.w("Skipping account save: username or domain not provided");
				return false;
			}

			String identity = "sip:" + tempUsername + "@" + tempDomain;
			String proxy = "sip:";
			if (tempProxy == null) {
				proxy += tempDomain;
			} else {
				if (!tempProxy.startsWith("sip:")
						&& !tempProxy.startsWith("<sip:")
						&& !tempProxy.startsWith("sips:")
						&& !tempProxy.startsWith("<sips:")) {
					proxy += tempProxy;
				} else {
					proxy = tempProxy;
				}
			}
			LinphoneAddress proxyAddr = LinphoneCoreFactory.instance()
					.createLinphoneAddress(proxy);
			LinphoneAddress identityAddr = LinphoneCoreFactory.instance()
					.createLinphoneAddress(identity);

			if (tempDisplayName != null) {
				identityAddr.setDisplayName(tempDisplayName);
			}

			if (tempTransport != null) {
				proxyAddr.setTransport(tempTransport);
			}

			String route = tempOutboundProxy != null ? proxyAddr
					.asStringUriOnly() : null;

			LinphoneProxyConfig prxCfg = lc.createProxyConfig(
					identityAddr.asString(), proxyAddr.asStringUriOnly(),
					route, tempEnabled);

			if (tempContactsParams != null)
				prxCfg.setContactUriParameters(tempContactsParams);
			if (tempExpire != null) {
				try {
					prxCfg.setExpires(Integer.parseInt(tempExpire));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}

			prxCfg.enableAvpf(tempAvpfEnabled);
			prxCfg.setAvpfRRInterval(tempAvpfRRInterval);
			prxCfg.enableQualityReporting(tempQualityReportingEnabled);
			prxCfg.setQualityReportingCollector(tempQualityReportingCollector);
			prxCfg.setQualityReportingInterval(tempQualityReportingInterval);

			if (tempRealm != null)
				prxCfg.setRealm(tempRealm);

			LinphoneAuthInfo authInfo = LinphoneCoreFactory.instance()
					.createAuthInfo(tempUsername, tempUserId, tempPassword,null, null, tempDomain);

			lc.addProxyConfig(prxCfg);
			lc.addAuthInfo(authInfo);

			if (!tempNoDefault)
				lc.setDefaultProxyConfig(prxCfg);
			return true;
		}

		public AccountBuilder setAvpfEnabled(boolean paramBoolean) {
			tempAvpfEnabled = paramBoolean;
			return this;
		}

		public AccountBuilder setAvpfRRInterval(int paramInt) {
			tempAvpfRRInterval = paramInt;
			return this;
		}

		public AccountBuilder setContactParameters(String paramString) {
			tempContactsParams = paramString;
			return this;
		}

		public AccountBuilder setDomain(String paramString) {
			tempDomain = paramString;
			return this;
		}

		public AccountBuilder setEnabled(boolean paramBoolean) {
			tempEnabled = paramBoolean;
			return this;
		}

		public AccountBuilder setExpires(String paramString) {
			tempExpire = paramString;
			return this;
		}

		public AccountBuilder setNoDefault(boolean paramBoolean) {
			tempNoDefault = paramBoolean;
			return this;
		}

		public AccountBuilder setOutboundProxy(String paramString) {
			tempOutboundProxy = paramString;
			return this;
		}

		public AccountBuilder setOutboundProxyEnabled(boolean paramBoolean) {
			tempOutboundProxyEnabled = paramBoolean;
			return this;
		}

		public AccountBuilder setPassword(String paramString) {
			tempPassword = paramString;
			return this;
		}

		public AccountBuilder setProxy(String paramString) {
			tempProxy = paramString;
			return this;
		}

		public AccountBuilder setQualityReportingCollector(String paramString) {
			tempQualityReportingCollector = paramString;
			return this;
		}

		public AccountBuilder setQualityReportingEnabled(boolean paramBoolean) {
			tempQualityReportingEnabled = paramBoolean;
			return this;
		}

		public AccountBuilder setQualityReportingInterval(int paramInt) {
			tempQualityReportingInterval = paramInt;
			return this;
		}

		public AccountBuilder setRealm(String paramString) {
			tempRealm = paramString;
			return this;
		}

		public AccountBuilder setTransport(
				LinphoneAddress.TransportType paramTransportType) {
			tempTransport = paramTransportType;
			return this;
		}

		public AccountBuilder setUserId(String paramString) {
			tempUserId = paramString;
			return this;
		}

		public AccountBuilder setUsername(String paramString) {
			tempUsername = paramString;
			return this;
		}

		public AccountBuilder setTempDisplayName(String displayName) {
			tempDisplayName = displayName;
			return this;
		}
		
		
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.LinphonePreferences JD-Core Version: 0.6.2
 */
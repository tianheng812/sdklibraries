package org.linphone.mediastream;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioManager;
import android.os.Build;

public class MediastreamerAndroidContext {
	private static final int DEVICE_CHOICE = 0;
	private static final int DEVICE_HAS_BUILTIN_AEC = 1;
	private static final int DEVICE_HAS_BUILTIN_AEC_CRAPPY = 2;
	private static final int DEVICE_HAS_BUILTIN_OPENSLES_AEC = 8;
	private static final int DEVICE_USE_ANDROID_MIC = 4;
	private static MediastreamerAndroidContext instance;

	public static void addSoundDeviceDesc(String paramString1,
			String paramString2, String paramString3, int paramInt1,
			int paramInt2, int paramInt3) {
		try{
		getInstance().addSoundDeviceDescription(paramString1, paramString2,
				paramString3, paramInt1, paramInt2, paramInt3);
		}catch(Exception e){}
	}

	private native void addSoundDeviceDescription(String paramString1,
			String paramString2, String paramString3, int paramInt1,
			int paramInt2, int paramInt3);

	private static MediastreamerAndroidContext getInstance() {
		if (instance == null)
			instance = new MediastreamerAndroidContext();
		return instance;
	}

	private static int parseInt(String value, int defaultValue) {
		int returnedValue = defaultValue;
		try {
			returnedValue = Integer.parseInt(value);
		} catch (NumberFormatException nfe) {
			Log.e("Can't parse " + value + " to integer ; using default value "
					+ defaultValue);
		}
		return returnedValue;
	}

	@TargetApi(19)
	public static void setContext(Object paramObject) {
		if (paramObject == null)
			return;
		Context localContext = (Context) paramObject;
		MediastreamerAndroidContext localMediastreamerAndroidContext = getInstance();
		try {
		if (Build.VERSION.SDK_INT >= 19) {
			AudioManager localAudioManager = (AudioManager) localContext
					.getSystemService("audio");
			int i = parseInt(
					localAudioManager
							.getProperty("android.media.property.OUTPUT_FRAMES_PER_BUFFER"),
					64);
			int j = parseInt(
					localAudioManager.getProperty("android.media.property.OUTPUT_SAMPLE_RATE"),
					44100);
			Log.i("Setting buffer size to " + i+ " and sample rate to " + j + " for OpenSLES MS sound card.");
			localMediastreamerAndroidContext.setDeviceFavoriteSampleRate(j);
			localMediastreamerAndroidContext.setDeviceFavoriteBufferSize(i);

		} else {
			Log.i( "Android < 4.4 detected, android context not used." );
		}
		} catch (Exception nfe) {
			
		}
	}

	private native void setDeviceFavoriteBufferSize(int paramInt);

	private native void setDeviceFavoriteSampleRate(int paramInt);
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.mediastream.MediastreamerAndroidContext JD-Core
 * Version: 0.6.2
 */
package org.linphone.mediastream;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class Log {
	public static String TAG = "------  sayee   ";
	public static boolean isLogEnabled = false;

	public static void d(Throwable paramThrowable, Object[] paramArrayOfObject) {
		if (isLoggable(3))
			android.util.Log.d(TAG, toString(paramArrayOfObject),
					paramThrowable);
	}

	public static void d(Object[] paramArrayOfObject) {
		if (isLoggable(3))
			android.util.Log.d(TAG, toString(paramArrayOfObject));
	}

	public static void e(Throwable paramThrowable, Object[] paramArrayOfObject) {
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		StringBuffer toStringBuffer = new StringBuffer("[")
				.append(traceElement.getFileName()).append(" | ")
				.append(traceElement.getLineNumber()).append(" | ")
				.append(traceElement.getMethodName()).append("()").append("]");
		if (isLoggable(6))
			android.util.Log.e(TAG+toStringBuffer.toString(), toString(paramArrayOfObject),paramThrowable);
		
	}

	public static void e(Throwable paramThrowable, String paramArrayOfObject) {
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		StringBuffer toStringBuffer = new StringBuffer("[")
				.append(traceElement.getFileName()).append(" | ")
				.append(traceElement.getLineNumber()).append(" | ")
				.append(traceElement.getMethodName()).append("()").append("]");
		if (isLoggable(6))
			android.util.Log.e(TAG+toStringBuffer.toString(), paramArrayOfObject, paramThrowable);
	}

	public static void e(Object[] paramArrayOfObject) {
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		StringBuffer toStringBuffer = new StringBuffer("[")
				.append(traceElement.getFileName()).append(" | ")
				.append(traceElement.getLineNumber()).append(" | ")
				.append(traceElement.getMethodName()).append("()").append("]");
		if (isLoggable(6))
			android.util.Log.e(TAG+toStringBuffer.toString(), toString(paramArrayOfObject));
	}

	public static void f(Throwable paramThrowable, Object[] paramArrayOfObject) {
		if (isLoggable(6)) {
			android.util.Log.e(TAG, toString(paramArrayOfObject),
					paramThrowable);
			throw new RuntimeException("Fatal error : "
					+ toString(paramArrayOfObject), paramThrowable);
		}
	}

	public static void f(Object[] paramArrayOfObject) {
		if (isLoggable(6)) {
			android.util.Log.e(TAG, toString(paramArrayOfObject));
			throw new RuntimeException("Fatal error : "
					+ toString(paramArrayOfObject));
		}
	}

	public static void i(Throwable paramThrowable, Object[] paramArrayOfObject) {
		if (isLoggable(4))
			android.util.Log.i(TAG, toString(paramArrayOfObject),
					paramThrowable);
	}

	public static void i(Object[] paramArrayOfObject) {
		if (isLoggable(4))
			android.util.Log.i(TAG, toString(paramArrayOfObject));
	}

	public static void i(String paramArrayOfObject, String i) {
		if (isLogEnabled)
			android.util.Log.i(paramArrayOfObject, i);
	}

	private static boolean isLoggable(int paramInt) {
		return isLogEnabled;
	}

	private static String toString(Object[] paramArrayOfObject) {
		StringBuilder localStringBuilder = new StringBuilder();
		int i = paramArrayOfObject.length;
		for (int j = 0; j < i; j++)
			localStringBuilder.append(paramArrayOfObject[j]);
		return localStringBuilder.toString();
	}

	public static void w(Throwable paramThrowable, Object[] paramArrayOfObject) {
		if (isLoggable(5))
			android.util.Log.w(TAG, toString(paramArrayOfObject),
					paramThrowable);
	}

	public static void w(Object[] paramArrayOfObject) {
		if (isLoggable(5))
			android.util.Log.w(TAG, toString(paramArrayOfObject));
	}

	
	
	// 当前文件名 行号 函数名
	public static String getFileLineMethod() {
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		StringBuffer toStringBuffer = new StringBuffer("[")
				.append(traceElement.getFileName()).append(" | ")
				.append(traceElement.getLineNumber()).append(" | ")
				.append(traceElement.getMethodName()).append("()").append("]");
		return toStringBuffer.toString();
	}

	// 当前文件名
	public static String _FILE_() {
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		return traceElement.getFileName();
	}

	// 当前方法名
	public static String _FUNC_() {
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		return traceElement.getMethodName();
	}

	// 当前行号
	public static int _LINE_() {
		StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
		return traceElement.getLineNumber();
	}

	// 当前时间
	public static String _TIME_() {
		Date now = new Date(0);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return sdf.format(now);

	}

	public static void v(String msg) {
		if(isLogEnabled){
			StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
			StringBuffer toStringBuffer = new StringBuffer("[")
					.append(traceElement.getFileName()).append(" | ")
					.append(traceElement.getLineNumber()).append(" | ")
					.append(traceElement.getMethodName()).append("()").append("]");
			android.util.Log.v(TAG +toStringBuffer.toString(), msg);
		}
	}

	public static void d(String msg) {
		if(isLogEnabled){
			StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
			StringBuffer toStringBuffer = new StringBuffer("[")
					.append(traceElement.getFileName()).append(" | ")
					.append(traceElement.getLineNumber()).append(" | ")
					.append(traceElement.getMethodName()).append("()").append("]");
			android.util.Log.d(TAG+toStringBuffer.toString(), msg );
		}
	}

	public static void i(String msg) {
		if (isLogEnabled) {
			StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
			StringBuffer toStringBuffer = new StringBuffer("[")
					.append(traceElement.getFileName()).append(" | ")
					.append(traceElement.getLineNumber()).append(" | ")
					.append(traceElement.getMethodName()).append("()").append("]");
			android.util.Log.i(TAG+toStringBuffer.toString(), msg);
		}
	}

	public static void w(String msg) {
		if(isLogEnabled){
			StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
			StringBuffer toStringBuffer = new StringBuffer("[")
					.append(traceElement.getFileName()).append(" | ")
					.append(traceElement.getLineNumber()).append(" | ")
					.append(traceElement.getMethodName()).append("()").append("]");
			android.util.Log.w(TAG+toStringBuffer.toString(), msg );
		}
	}

	public static void e(String msg) {
		if(isLogEnabled){
			StackTraceElement traceElement = ((new Exception()).getStackTrace())[1];
			StringBuffer toStringBuffer = new StringBuffer("[")
					.append(traceElement.getFileName()).append(" | ")
					.append(traceElement.getLineNumber()).append(" | ")
					.append(traceElement.getMethodName()).append("()").append("]");
			android.util.Log.e(TAG+toStringBuffer.toString(), msg );
		}
	}
	
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.mediastream.Log JD-Core Version: 0.6.2
 */
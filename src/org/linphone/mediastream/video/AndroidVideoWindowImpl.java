package org.linphone.mediastream.video;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Bitmap.Config;
import android.opengl.GLSurfaceView;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.Surface.OutOfResourcesException;
import android.view.SurfaceView;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import org.linphone.mediastream.Log;
import org.linphone.mediastream.video.display.OpenGLESDisplay;

public class AndroidVideoWindowImpl {
	private Bitmap mBitmap = null;
	private VideoWindowListener mListener = null;
	private Surface mSurface = null;
	private SurfaceView mVideoPreviewView;
	private SurfaceView mVideoRenderingView;
	private Renderer renderer;
	private boolean useGLrendering;

	public AndroidVideoWindowImpl(SurfaceView paramSurfaceView1,
			SurfaceView paramSurfaceView2) {
		this.mVideoRenderingView = paramSurfaceView1;
		this.mVideoPreviewView = paramSurfaceView2;
		this.useGLrendering = (paramSurfaceView1 instanceof GLSurfaceView);
		init();
	}

	public AndroidVideoWindowImpl(SurfaceView paramSurfaceView1,
			SurfaceView paramSurfaceView2,
			VideoWindowListener paramVideoWindowListener) {
		this.mVideoRenderingView = paramSurfaceView1;
		this.mVideoPreviewView = paramSurfaceView2;
		this.useGLrendering = (paramSurfaceView1 instanceof GLSurfaceView);
		this.mListener = paramVideoWindowListener;
		init();
	}

	public static int rotationToAngle(int paramInt) {
		switch (paramInt) {
		case Surface.ROTATION_0:
		default:
			return 0;
		case Surface.ROTATION_90:
			return 90;
		case Surface.ROTATION_180:
			return 180;
		case Surface.ROTATION_270:
			return 270;
		}
	}

	public Bitmap getBitmap() {
		if (this.useGLrendering)
			Log.e(new Object[] { "View class does not match Video display filter used (you must use a non-GL View)" });
		return this.mBitmap;
	}

	public Surface getSurface() {
		if (this.useGLrendering)
			Log.e(new Object[] { "View class does not match Video display filter used (you must use a non-GL View)" });
		return this.mVideoRenderingView.getHolder().getSurface();
	}

	@SuppressLint("InlinedApi")
	public void init() {
		this.mVideoRenderingView.getHolder().addCallback(
				new SurfaceHolder.Callback() {
					public void surfaceChanged(SurfaceHolder holder,int format, int width, int height) {
						Log.i(new Object[] { "Video display surface is being changed." });
						if (!useGLrendering) {
							synchronized (AndroidVideoWindowImpl.this) {
								mBitmap = Bitmap.createBitmap(width, height,Config.RGB_565);
								mSurface = holder.getSurface();
							}
						}

						if (mListener != null){
							mListener.onVideoRenderingSurfaceReady(AndroidVideoWindowImpl.this, mVideoRenderingView);
						}
					}

					public void surfaceCreated(SurfaceHolder holder) {
						Log.w("Video display surface created");
					}

					public void surfaceDestroyed(SurfaceHolder holder) {
						if (!useGLrendering) {
							synchronized (AndroidVideoWindowImpl.this) {
								mSurface = null;
								mBitmap = null;
							}
						}
						if (mListener != null)
							mListener.onVideoRenderingSurfaceDestroyed(AndroidVideoWindowImpl.this);
						Log.d("Video display surface destroyed");
					}
				});
		if (this.mVideoPreviewView != null)
			this.mVideoPreviewView.getHolder().addCallback(
					new SurfaceHolder.Callback() {
						public void surfaceChanged(SurfaceHolder holder, int format,
								int width, int height) {
							Log.i(new Object[] { "Video preview surface is being changed." });
							if (mListener != null)
								mListener.onVideoPreviewSurfaceReady(
												AndroidVideoWindowImpl.this,
												mVideoPreviewView);
							Log.w( "Video preview surface changed" );
						}

						public void surfaceCreated(SurfaceHolder holder) {
							Log.w("Video preview surface created");
						}

						public void surfaceDestroyed(
								SurfaceHolder paramAnonymousSurfaceHolder) {
							if (mListener != null)
								mListener.onVideoPreviewSurfaceDestroyed(AndroidVideoWindowImpl.this);
							Log.d( "Video preview surface destroyed" );
						}
					});
		if (this.useGLrendering) {
			renderer = new Renderer();
			((GLSurfaceView) mVideoRenderingView).setRenderer(renderer);
			((GLSurfaceView) mVideoRenderingView).setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		}
	}

	public void release() {
	}

	public void requestRender() {
		((GLSurfaceView) this.mVideoRenderingView).requestRender();
	}

	public void setListener(VideoWindowListener l) {
		this.mListener = l;
	}

	public void setOpenGLESDisplay(int paramInt) {
		if (!this.useGLrendering)
			Log.e( "View class does not match Video display filter used (you must use a GL View)" );
		renderer.setOpenGLESDisplay(paramInt);
	}


	public void update() {
		if (mSurface!=null){
			try {
				Canvas canvas=mSurface.lockCanvas(null); 
				canvas.drawBitmap(mBitmap, 0, 0, null);
				mSurface.unlockCanvasAndPost(canvas);
				 
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (OutOfResourcesException e) {
				e.printStackTrace();
			}  
		} 
	}

	private static class Renderer implements GLSurfaceView.Renderer {
		int height;
		boolean initPending = false;
		int ptr = 0;
		int width;

		public void onDrawFrame(GL10 paramGL10) {
				if (this.ptr == 0)
					return;
				if (this.initPending) {
					OpenGLESDisplay.init(this.ptr, this.width, this.height);
					this.initPending = false;
				}
				OpenGLESDisplay.render(this.ptr);
		}

		public void onSurfaceChanged(GL10 gl, int width, int height) {
			this.width = width;
			this.height = height;
			this.initPending = true;
		}

		public void onSurfaceCreated(GL10 paramGL10, EGLConfig paramEGLConfig) {
		}

		public void setOpenGLESDisplay(int paramInt) {
				if ((this.ptr != 0) && (paramInt != this.ptr))
					this.initPending = true;
				this.ptr = paramInt;
		}
	}

	public static abstract interface VideoWindowListener {
		public abstract void onVideoPreviewSurfaceDestroyed(
				AndroidVideoWindowImpl paramAndroidVideoWindowImpl);

		public abstract void onVideoPreviewSurfaceReady(
				AndroidVideoWindowImpl paramAndroidVideoWindowImpl,
				SurfaceView paramSurfaceView);

		public abstract void onVideoRenderingSurfaceDestroyed(
				AndroidVideoWindowImpl paramAndroidVideoWindowImpl);

		public abstract void onVideoRenderingSurfaceReady(
				AndroidVideoWindowImpl paramAndroidVideoWindowImpl,
				SurfaceView paramSurfaceView);
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.mediastream.video.AndroidVideoWindowImpl JD-Core
 * Version: 0.6.2
 */
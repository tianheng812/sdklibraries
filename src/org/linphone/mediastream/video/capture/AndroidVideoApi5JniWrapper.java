package org.linphone.mediastream.video.capture;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.view.SurfaceView;
import java.util.List;
import org.linphone.mediastream.Log;
import org.linphone.mediastream.Version;
import org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfiguration;
import org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfiguration.AndroidCamera;

@SuppressWarnings("deprecation")
public class AndroidVideoApi5JniWrapper {
	public static boolean isRecording = false;

	public static void activateAutoFocus(Object paramObject) {
		Log.d("Turning on autofocus on camera " + paramObject);
		Camera localCamera = (Camera) paramObject;
		if ((localCamera != null)
				&& ((localCamera.getParameters().getFocusMode() == Parameters.FOCUS_MODE_AUTO) || (localCamera
						.getParameters().getFocusMode() == Parameters.FOCUS_MODE_MACRO)))
			localCamera.autoFocus(null);
	}

	protected static void applyCameraParameters(Camera camera, int width,
			int height, int requestedFps) {
		Parameters params = camera.getParameters();

		params.setPreviewSize(width, height);

		List<Integer> supported = params.getSupportedPreviewFrameRates();
		if (supported != null) {
			int nearest = Integer.MAX_VALUE;
			for (Integer fr : supported) {
				int diff = Math.abs(fr.intValue() - requestedFps);
				if (diff < nearest) {
					nearest = diff;
					params.setPreviewFrameRate(fr.intValue());
				}
			}
			Log.d("Preview framerate set:" + params.getPreviewFrameRate());
		}

		camera.setParameters(params);
	}

	static public int detectCameras(int[] indexes, int[] frontFacing,
			int[] orientation) {
		Log.d("detectCameras\n");
		AndroidCamera[] cameras = AndroidCameraConfiguration.retrieveCameras();

		int nextIndex = 0;
		for (AndroidCamera androidCamera : cameras) {
			if (nextIndex == 2) {
				Log.w("Returning only the 2 first cameras (increase buffer size to retrieve all)");
				break;
			}
			// skip already added cameras
			indexes[nextIndex] = androidCamera.id;
			frontFacing[nextIndex] = androidCamera.frontFacing ? 1 : 0;
			orientation[nextIndex] = androidCamera.orientation;
			nextIndex++;
		}
		return nextIndex;
	}

	public static native void putImage(long paramLong, byte[] paramArrayOfByte);

	public static int[] selectNearestResolutionAvailable(int cameraId,
			int requestedW, int requestedH) {
		Log.d("selectNearestResolutionAvailable: " + cameraId + ", "
				+ requestedW + "x" + requestedH);

		return selectNearestResolutionAvailableForCamera(cameraId, requestedW,
				requestedH);
	}

	protected static int[] selectNearestResolutionAvailableForCamera(int id,
			int requestedW, int requestedH) {
		if (requestedH > requestedW) {
			int t = requestedH;
			requestedH = requestedW;
			requestedW = t;
		}

		AndroidCamera[] cameras = AndroidCameraConfiguration.retrieveCameras();
		List<Size> supportedSizes = null;
		for (AndroidCamera c : cameras) {
			if (c.id == id)
				supportedSizes = c.resolutions;
		}
		if (supportedSizes == null) {
			Log.e("Failed to retrieve supported resolutions.");
			return null;
		}
		Log.d(supportedSizes.size() + " supported resolutions :");
		for (Size s : supportedSizes) {
			Log.d("\t" + s.width + "x" + s.height);
		}
		int r[] = null;

		int rW = Math.max(requestedW, requestedH);
		int rH = Math.min(requestedW, requestedH);

		try {
			// look for nearest size
			Size result = supportedSizes.get(0); /* by default return first value */
			int req = rW * rH;
			int minDist = Integer.MAX_VALUE;
			int useDownscale = 0;
			for (Size s : supportedSizes) {
				int dist = /* Math.abs */-1 * (req - s.width * s.height);
				if (((s.width >= rW && s.height >= rH) || (s.width >= rH && s.height >= rW))
						&& dist < minDist) {
					minDist = dist;
					result = s;
					useDownscale = 0;
				}

				/* MS2 has a NEON downscaler, so we test this too */
				int downScaleDist = /* Math.abs */-1
						* (req - s.width * s.height / 4);
				if (((s.width / 2 >= rW && s.height / 2 >= rH) || (s.width / 2 >= rH && s.height / 2 >= rW))
						&& downScaleDist < minDist) {
					if (Version.hasFastCpuWithAsmOptim()) {
						minDist = downScaleDist;
						result = s;
						useDownscale = 1;
					} else {
						result = s;
						useDownscale = 0;
					}
				}
				if (s.width == rW && s.height == rH) {
					result = s;
					useDownscale = 0;
					break;
				}
			}
			r = new int[] { result.width, result.height, useDownscale };
			Log.d("resolution selection done (" + r[0] + ", " + r[1] + ", "
					+ r[2] + ")");
			return r;
		} catch (Exception exc) {
			exc.printStackTrace();
			return null;
		}
	}

	public static void setPreviewDisplaySurface(Object cam, Object surf) {
		Log.d("setPreviewDisplaySurface(" + cam + ", " + surf + ")");
		Camera camera = (Camera) cam;
		SurfaceView localSurfaceView = (SurfaceView) surf;
		try {
			camera.setPreviewDisplay(localSurfaceView.getHolder());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Object startRecording(int cameraId, int width, int height,
			int fps, int rotation, final long nativePtr) {
		Log.d("startRecording(" + cameraId + ", " + width + ", " + height
				+ ", " + fps + ", " + rotation + ", " + nativePtr + ")");
		Camera camera = Camera.open();

		applyCameraParameters(camera, width, height, fps);

		camera.setPreviewCallback(new Camera.PreviewCallback() {
			public void onPreviewFrame(byte[] data, Camera camera) {
				if (isRecording) {
					// forward image data to JNI
					putImage(nativePtr, data);
				}
			}
		});

		camera.startPreview();
		isRecording = true;
		Log.d("Returning camera object: " + camera);
		return camera;
	}

	public static void stopRecording(Object cam) {
		isRecording = false;
		Log.d("stopRecording(" + cam + ")"); 
		Camera localCamera = (Camera) cam;
		if (localCamera != null) {
			localCamera.setPreviewCallback(null);
			localCamera.stopPreview();
			localCamera.release();
		}else{
			Log.i( "Cannot stop recording ('camera' is null)" );
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:
 * org.linphone.mediastream.video.capture.AndroidVideoApi5JniWrapper JD-Core
 * Version: 0.6.2
 */
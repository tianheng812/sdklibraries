package org.linphone.mediastream.video.capture.hwconf;

import android.annotation.SuppressLint;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import java.util.ArrayList;
import java.util.List;

import org.linphone.mediastream.Log;
import org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfiguration.AndroidCamera;

@SuppressLint("NewApi")
class AndroidCameraConfigurationReader9 {
	@SuppressWarnings("deprecation")
	static public AndroidCamera[] probeCameras() {
		List<AndroidCamera> cam = new ArrayList<AndroidCamera>(
				Camera.getNumberOfCameras());

		for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
			try {
				CameraInfo info = new CameraInfo();
				Camera.getCameraInfo(i, info);
				Camera c = Camera.open(i);
				if (c != null) {
					cam.add(new AndroidCamera(
							i,
							info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT,
							info.orientation, c.getParameters()
									.getSupportedPreviewSizes()));
					c.release();
				}
			} catch (Exception e) {
				Log.i(" 开启摄像头失败    ");
				e.printStackTrace();
			}
		}

		AndroidCamera[] result = new AndroidCamera[cam.size()];
		result = cam.toArray(result);
		return result;
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:
 * org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfigurationReader9
 * JD-Core Version: 0.6.2
 */
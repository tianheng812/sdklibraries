package org.linphone.mediastream.video.capture.hwconf;

import android.hardware.Camera;
import android.hardware.Camera.Size;
import java.util.List;
import org.linphone.mediastream.Log;
import org.linphone.mediastream.Version;

public class AndroidCameraConfiguration {
	private static AndroidCamera[] camerasCache;

	public static boolean hasFrontCamera() {
		initCamerasCache();
		for (AndroidCamera cam : camerasCache) {
			if (cam.frontFacing)
				return true;
		}
		return false;
	}

	public static boolean hasSeveralCameras() {
		initCamerasCache();
		return camerasCache.length > 1;
	}

	private static void initCamerasCache() {
		if (camerasCache != null && camerasCache.length!=0)
			return;
		
		try {
			if (Version.sdk() < 9)
				camerasCache = AndroidCameraConfiguration.probeCamerasSDK5();
			else
				camerasCache = AndroidCameraConfiguration.probeCamerasSDK9();
		} catch (Exception exc) {
			Log.e("Error: cannot retrieve cameras information (busy )");
			exc.printStackTrace();
			camerasCache = new AndroidCamera[0];
		}
	}

	static AndroidCamera[] probeCamerasSDK5() {
		return AndroidCameraConfigurationReader5.probeCameras();
	}

	static AndroidCamera[] probeCamerasSDK9() {
		return AndroidCameraConfigurationReader9.probeCameras();
	}

	public static AndroidCamera[] retrieveCameras() {
		initCamerasCache();
		return camerasCache;
	}

	public static class AndroidCamera {
		public boolean frontFacing;
		public int id;
		public int orientation;
		public List<Camera.Size> resolutions;

		public AndroidCamera(int paramInt1, boolean paramBoolean,
				int paramInt2, List<Camera.Size> paramList) {
			this.id = paramInt1;
			this.frontFacing = paramBoolean;
			this.orientation = paramInt2;
			this.resolutions = paramList;
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:
 * org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfiguration
 * JD-Core Version: 0.6.2
 */
package org.linphone.mediastream.video.capture;

import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import org.linphone.mediastream.Log;

public class AndroidVideoApi8JniWrapper
{
  public static int detectCameras(int[] paramArrayOfInt1, int[] paramArrayOfInt2, int[] paramArrayOfInt3)
  {
    return AndroidVideoApi5JniWrapper.detectCameras(paramArrayOfInt1, paramArrayOfInt2, paramArrayOfInt3);
  }

  public static int[] selectNearestResolutionAvailable(int paramInt1, int paramInt2, int paramInt3)
  {
    return AndroidVideoApi5JniWrapper.selectNearestResolutionAvailable(paramInt1, paramInt2, paramInt3);
  }

  public static void setPreviewDisplaySurface(Object paramObject1, Object paramObject2)
  {
    AndroidVideoApi5JniWrapper.setPreviewDisplaySurface(paramObject1, paramObject2);
  }

  public static Object startRecording(int cameraId, int width, int height, int fps, int rotation, final long nativePtr) {
		Log.d("startRecording(" + cameraId + ", " + width + ", " + height + ", " + fps + ", " + rotation + ", " + nativePtr + ")");
		Camera camera = Camera.open(); 

		AndroidVideoApi5JniWrapper.applyCameraParameters(camera, width, height, fps);
		  
		int bufferSize = (width * height * ImageFormat.getBitsPerPixel(camera.getParameters().getPreviewFormat())) / 8;
		camera.addCallbackBuffer(new byte[bufferSize]);
		camera.addCallbackBuffer(new byte[bufferSize]);
		
		camera.setPreviewCallbackWithBuffer(new Camera.PreviewCallback() {
			public void onPreviewFrame(byte[] data, Camera camera) {
				if (AndroidVideoApi5JniWrapper.isRecording) {
					// forward image data to JNI
					AndroidVideoApi5JniWrapper.putImage(nativePtr, data);
					camera.addCallbackBuffer(data);
				}
			}
		});
		 
		camera.startPreview();
		AndroidVideoApi5JniWrapper.isRecording = true;
		Log.d("Returning camera object: " + camera);
		return camera; 
	} 

  public static void stopRecording(Object cam)
  {
    AndroidVideoApi5JniWrapper.isRecording = false;
    Log.d("stopRecording(" + cam + ")"); 
    Camera localCamera = (Camera)cam;
    if (localCamera != null)
    {
      localCamera.setPreviewCallbackWithBuffer(null);
      localCamera.stopPreview();
      localCamera.release();
      return;
    }
    Log.i("Cannot stop recording ('camera' is null)" );
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.mediastream.video.capture.AndroidVideoApi8JniWrapper
 * JD-Core Version:    0.6.2
 */
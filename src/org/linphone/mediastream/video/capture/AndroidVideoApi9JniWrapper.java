package org.linphone.mediastream.video.capture;

import android.annotation.TargetApi;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import java.util.Iterator;
import java.util.List;
import org.linphone.mediastream.Log;

@TargetApi(9)
public class AndroidVideoApi9JniWrapper {
	public static int detectCameras(int[] paramArrayOfInt1,
			int[] paramArrayOfInt2, int[] paramArrayOfInt3) {
		return AndroidVideoApi5JniWrapper.detectCameras(paramArrayOfInt1,
				paramArrayOfInt2, paramArrayOfInt3);
	}

	private static int[] findClosestEnclosingFpsRange(int expectedFps,
			List<int[]> fpsRanges) {
		Log.d("Searching for closest fps range from " + expectedFps);
		if (fpsRanges == null || fpsRanges.size() == 0) {
			return new int[] { 0, 0 };
		}

		// init with first element
		int[] closestRange = fpsRanges.get(0);
		int measure = Math.abs(closestRange[0] - expectedFps)
				+ Math.abs(closestRange[1] - expectedFps);
		for (int[] curRange : fpsRanges) {
			if (curRange[0] > expectedFps || curRange[1] < expectedFps)
				continue;
			int curMeasure = Math.abs(curRange[0] - expectedFps)
					+ Math.abs(curRange[1] - expectedFps);
			if (curMeasure < measure) {
				closestRange = curRange;
				measure = curMeasure;
				Log.d("a better range has been found: w=" + closestRange[0]
						+ ",h=" + closestRange[1]);
			}
		}
		Log.d("The closest fps range is w=" + closestRange[0] + ",h="
				+ closestRange[1]);
		return closestRange;
	}

	public static int[] selectNearestResolutionAvailable(int paramInt1,
			int paramInt2, int paramInt3) {
		Log.d("selectNearestResolutionAvailable: " + paramInt1 + ", "
				+ paramInt2 + "x" + paramInt3);
		return AndroidVideoApi5JniWrapper
				.selectNearestResolutionAvailableForCamera(paramInt1,
						paramInt2, paramInt3);
	}

	private static void setCameraDisplayOrientation(int rotationDegrees,
			int cameraId, Camera camera) {
		android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + rotationDegrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - rotationDegrees + 360) % 360;
		}

		Log.w("Camera preview orientation: " + result);
		try {
			camera.setDisplayOrientation(result);
		} catch (Exception exc) {
			Log.e("Failed to execute: camera[" + camera
					+ "].setDisplayOrientation(" + result + ")");
			exc.printStackTrace();
		}
	}

	public static void setPreviewDisplaySurface(Object paramObject1,
			Object paramObject2) {
		AndroidVideoApi5JniWrapper.setPreviewDisplaySurface(paramObject1,
				paramObject2);
	}

	public static Object startRecording(int cameraId, int width, int height,
			int fps, int rotation, final long nativePtr) {
		Log.d("startRecording(" + cameraId + ", " + width + ", " + height
				+ ", " + fps + ", " + rotation + ", " + nativePtr + ")");
		try {
			Camera camera = Camera.open(cameraId);
			Parameters params = camera.getParameters();

			params.setPreviewSize(width, height);
			int[] chosenFps = findClosestEnclosingFpsRange(fps * 1000,
					params.getSupportedPreviewFpsRange());
			params.setPreviewFpsRange(chosenFps[0], chosenFps[1]);
			camera.setParameters(params);

			int bufferSize = (width * height * ImageFormat
					.getBitsPerPixel(params.getPreviewFormat())) / 8;
			camera.addCallbackBuffer(new byte[bufferSize]);
			camera.addCallbackBuffer(new byte[bufferSize]);

			camera.setPreviewCallbackWithBuffer(new Camera.PreviewCallback() {
				public void onPreviewFrame(byte[] data, Camera camera) {
					// forward image data to JNI
					if (data == null) {
						// It appears that there is a bug in the camera driver
						// that is asking for a buffer size bigger than it
						// should
						Parameters params = camera.getParameters();
						Size size = params.getPreviewSize();
						int bufferSize = (size.width * size.height * ImageFormat
								.getBitsPerPixel(params.getPreviewFormat())) / 8;
						bufferSize += bufferSize / 20;
						camera.addCallbackBuffer(new byte[bufferSize]);
					} else if (AndroidVideoApi5JniWrapper.isRecording) {
						AndroidVideoApi5JniWrapper.putImage(nativePtr, data);
						camera.addCallbackBuffer(data);
					}
				}
			});

			setCameraDisplayOrientation(rotation, cameraId, camera);
			camera.startPreview();
			AndroidVideoApi5JniWrapper.isRecording = true;
			Log.d("Returning camera object: " + camera);
			return camera;
		} catch (Exception exc) {
			exc.printStackTrace();
			return null;
		}
	}

	public static void stopRecording(Object paramObject) {
		AndroidVideoApi5JniWrapper.isRecording = false;
		AndroidVideoApi8JniWrapper.stopRecording(paramObject);
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:
 * org.linphone.mediastream.video.capture.AndroidVideoApi9JniWrapper JD-Core
 * Version: 0.6.2
 */
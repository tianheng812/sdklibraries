package org.linphone.mediastream.video.display;

public class OpenGLESDisplay
{
	public static native void init(int ptr, int width, int height);
    public static native void render(int ptr);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.mediastream.video.display.OpenGLESDisplay
 * JD-Core Version:    0.6.2
 */
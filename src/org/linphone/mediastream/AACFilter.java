package org.linphone.mediastream;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaFormat;
import java.nio.ByteBuffer;

@TargetApi(16)
public class AACFilter {
	private static AACFilter singleton;
	int bitrate;
	int channelCount;
	MediaCodec decoder;
	MediaCodec.BufferInfo decoderBufferInfo;
	ByteBuffer[] decoderInputBuffers;
	ByteBuffer[] decoderOutputBuffers;
	MediaCodec encoder;
	MediaCodec.BufferInfo encoderBufferInfo;
	ByteBuffer[] encoderInputBuffers;
	ByteBuffer[] encoderOutputBuffers;
	boolean initialized = false;
	int sampleRate;

	private static int dequeueData(MediaCodec codec, ByteBuffer[] ouputBuffers,
			BufferInfo bufferInfo, byte[] b) {
		int pcmbufPollCount = 0;
		while (pcmbufPollCount < 1) {
			int decBufIdx = codec.dequeueOutputBuffer(bufferInfo, 100);

			if (decBufIdx >= 0) {
				if (b.length < bufferInfo.size)
					Log.e("array is too small " + b.length + " < "
							+ bufferInfo.size);
				if (bufferInfo.flags == MediaCodec.BUFFER_FLAG_CODEC_CONFIG) {
					Log.i("JUST READ MediaCodec.BUFFER_FLAG_CODEC_CONFIG buffer");

				}
				ouputBuffers[decBufIdx].get(b, 0, bufferInfo.size);
				ouputBuffers[decBufIdx].position(0);

				codec.releaseOutputBuffer(decBufIdx, false);
				return bufferInfo.size;
			} else if (decBufIdx == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
				return MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED;
			} else if (decBufIdx == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
				Log.i("MediaCodec.INFO_OUTPUT_FORMAT_CHANGED");
				Log.i("CHANNEL_COUNT: "
						+ codec.getOutputFormat().getInteger(
								MediaFormat.KEY_CHANNEL_COUNT));
				Log.i("SAMPLE_RATE: "
						+ codec.getOutputFormat().getInteger(
								MediaFormat.KEY_SAMPLE_RATE));

			} else if (decBufIdx == MediaCodec.INFO_TRY_AGAIN_LATER) {
				// Log.i("MediaCodec.INFO_TRY_AGAIN_LATER");
			}
			++pcmbufPollCount;
		}
		return 0;
	}

	public static AACFilter instance() {
		if (singleton == null)
			singleton = new AACFilter();
		return singleton;
	}

	private static boolean queueData(MediaCodec paramMediaCodec,
			ByteBuffer[] paramArrayOfByteBuffer, byte[] paramArrayOfByte,
			int paramInt) {
		int i = paramMediaCodec.dequeueInputBuffer(0L);
		boolean bool = false;
		if (i >= 0) {
			paramArrayOfByteBuffer[i].position(0);
			paramArrayOfByteBuffer[i].put(paramArrayOfByte, 0, paramInt);
			paramMediaCodec.queueInputBuffer(i, 0, paramInt, 0L, 0);
			bool = true;
		}
		return bool;
	}

	public boolean postprocess() {
		if (this.initialized) {
			Log.i(new Object[] { "Stopping encoder" });
			this.encoder.stop();
			Log.i(new Object[] { "Stopping decoder" });
			this.decoder.stop();
			Log.i(new Object[] { "Release encoder" });
			Log.i(new Object[] { "Release decoder" });
			this.encoder = null;
			this.decoder = null;
			this.initialized = false;
		}
		return true;
	}

	public boolean preprocess(int sampleRate, int channelCount, int bitrate,
			boolean sbr_enabled) {
		if (initialized)
			return true;

		this.sampleRate = sampleRate;
		this.channelCount = channelCount;
		this.bitrate = bitrate;

		byte[] asc = null;
		try {
			MediaFormat mediaFormat = MediaFormat.createAudioFormat(
					"audio/mp4a-latm", sampleRate, channelCount);
			mediaFormat.setInteger(MediaFormat.KEY_AAC_PROFILE,
					MediaCodecInfo.CodecProfileLevel.AACObjectELD);
			mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, bitrate);

			// Configuring the SBR for AAC requires API lvl 21, which is not out
			// there right now (Oct 2014)
			// AAC is supposed to autoconfigure itself, but we should react to
			// fmtp, so activate this when
			// the API is here.
			// if( sbr_enabled ) {
			// mediaFormat.setInteger(MediaFormat.KEY_AAC_SBR_MODE, 2);
			// }

			encoder = MediaCodec.createByCodecName("OMX.google.aac.encoder");
			encoder.configure(mediaFormat, null, null,
					MediaCodec.CONFIGURE_FLAG_ENCODE);

			encoder.start();

			encoderBufferInfo = new MediaCodec.BufferInfo();

			int ascPollCount = 0;
			while (asc == null && ascPollCount < 1000) {
				// Try to get the asc
				int encInBufIdx = -1;
				encInBufIdx = encoder.dequeueOutputBuffer(encoderBufferInfo, 0);
				if (encInBufIdx >= 0) {
					if (encoderBufferInfo.flags == MediaCodec.BUFFER_FLAG_CODEC_CONFIG) {
						asc = new byte[encoderBufferInfo.size];
						encoder.getOutputBuffers()[encInBufIdx].get(asc, 0,
								encoderBufferInfo.size);
						encoder.getOutputBuffers()[encInBufIdx].position(0);
						encoder.releaseOutputBuffer(encInBufIdx, false);
					}
				}
				ascPollCount++;
			}
			encoderOutputBuffers = encoder.getOutputBuffers();
			encoderInputBuffers = encoder.getInputBuffers();
			if (asc == null) {
				Log.e("Sigh, failed to read asc from encoder");
			}
		} catch (Exception exc) {
			Log.e(exc, "Unable to create AAC Encoder");
			return false;
		}
		Log.i("AAC encoder initialized");

		try {
			MediaFormat mediaFormat = null;
			if (asc != null) {
				mediaFormat = MediaFormat.createAudioFormat("audio/mp4a-latm",
						0, 0);
				ByteBuffer ascBuf = ByteBuffer.wrap(asc);
				/* csd-0 = codec specific data */
				mediaFormat.setByteBuffer("csd-0", ascBuf);
			} else {
				mediaFormat = MediaFormat.createAudioFormat("audio/mp4a-latm",
						sampleRate, channelCount);
				mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, bitrate);
			}

			decoder = MediaCodec.createByCodecName("OMX.google.aac.decoder");
			decoder.configure(mediaFormat, null, null, 0);
			decoder.start();

			decoderOutputBuffers = decoder.getOutputBuffers();
			decoderInputBuffers = decoder.getInputBuffers();

			decoderBufferInfo = new MediaCodec.BufferInfo();
		} catch (Exception exc) {
			Log.e(exc, "Unable to create AAC Decoder");
			return false;
		}

		Log.i("AAC decoder initialized");
		initialized = true;

		return true;
	}

	public int pullFromDecoder(byte[] paramArrayOfByte) {
		try {
			int i = dequeueData(this.decoder, this.decoderOutputBuffers,
					this.decoderBufferInfo, paramArrayOfByte);
			if (i == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
				this.decoderOutputBuffers = this.decoder.getOutputBuffers();
				int j = pullFromDecoder(paramArrayOfByte);
				i = j;
			}
			return i;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int pullFromEncoder(byte[] paramArrayOfByte) {
		try {
			int i = dequeueData(this.encoder, this.encoderOutputBuffers,
					this.encoderBufferInfo, paramArrayOfByte);
			if (i == -3) {
				this.encoderOutputBuffers = this.encoder.getOutputBuffers();
				int j = pullFromDecoder(paramArrayOfByte);
				i = j;
			}
			return i;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public boolean pushToDecoder(byte[] data, int size) {
		try {
			if (data != null && decoder != null) {
				/* request available input buffer (0 == no wait) */
				return queueData(decoder, decoderInputBuffers, data, size);
			} else {
				return false;
			}
		} catch (Exception e) {
			Log.e(e, "Push to decoder failed");
			return false;
		}
	}

	public boolean pushToEncoder(byte[] data, int size) {
		try {
			if (data != null && decoder != null) {
				/* request available input buffer (0 == no wait) */
				return queueData(decoder, decoderInputBuffers, data, size);
			} else {
				return false;
			}
		} catch (Exception e) {
			Log.e(e, "Push to decoder failed");
			return false;
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.mediastream.AACFilter JD-Core Version: 0.6.2
 */
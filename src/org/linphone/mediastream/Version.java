package org.linphone.mediastream;

import java.util.ArrayList;
import java.util.List;

import org.linphone.mediastream.video.capture.hwconf.Hacks;

import android.content.Context;
import android.os.Build;

public class Version {
	public static final int API03_CUPCAKE_15 = 3;
	public static final int API04_DONUT_16 = 4;
	public static final int API05_ECLAIR_20 = 5;
	public static final int API06_ECLAIR_201 = 6;
	public static final int API07_ECLAIR_21 = 7;
	public static final int API08_FROYO_22 = 8;
	public static final int API09_GINGERBREAD_23 = 9;
	public static final int API10_GINGERBREAD_MR1_233 = 10;
	public static final int API11_HONEYCOMB_30 = 11;
	public static final int API12_HONEYCOMB_MR1_31X = 12;
	public static final int API13_HONEYCOMB_MR2_32 = 13;
	public static final int API14_ICE_CREAM_SANDWICH_40 = 14;
	public static final int API15_ICE_CREAM_SANDWICH_403 = 15;
	public static final int API16_JELLY_BEAN_41 = 16;
	public static final int API17_JELLY_BEAN_42 = 17;
	public static final int API18_JELLY_BEAN_43 = 18;
	public static final int API19_KITKAT_44 = 19;
	public static final int API21_LOLLIPOP_50 = 21;
	private static final int buildVersion = Build.VERSION.SDK_INT;
	private static Boolean hasNeon;
	private static Boolean sCacheHasZrtp;

	public static void dumpCapabilities() {
		StringBuilder localStringBuilder = new StringBuilder(
				" ==== Capabilities dump ====\n");
		localStringBuilder.append("Has neon: ")
				.append(Boolean.toString(hasNeon())).append("\n");
		localStringBuilder.append("Has ZRTP: ")
				.append(Boolean.toString(hasZrtp())).append("\n");
		Log.i(localStringBuilder.toString());
	}

	public static boolean hasFastCpu() {
		return (isArmv7()) || (isX86());
	}

	public static boolean hasFastCpuWithAsmOptim() {
		return ((isArmv7()) && (hasNeon())) || (isX86());
	}

	public static boolean hasNeon() {
		if (hasNeon == null)
			hasNeon = nativeHasNeon();
		return hasNeon;
	}

	public static boolean hasZrtp() {
		if (sCacheHasZrtp == null)
			sCacheHasZrtp = nativeHasZrtp();
		return sCacheHasZrtp;
	}

	public static boolean isArmv7() {
		try {
			return sdkAboveOrEqual(4)
			&& Build.class.getField("CPU_ABI").get(null).toString().startsWith("armeabi-v7");
		} catch (Throwable e) {}
		return false;
	}

	public static boolean isHDVideoCapable() {
		int i = Runtime.getRuntime().availableProcessors();
		return (isVideoCapable()) && (hasFastCpuWithAsmOptim()) && (i > 1);
	}

	public static boolean isVideoCapable() {
		return (!sdkStrictlyBelow(5)) && (hasFastCpu()) && (Hacks.hasCamera());
	}

	public static boolean isX86() {
		try {
			return sdkAboveOrEqual(4)
			&& Build.class.getField("CPU_ABI").get(null).toString().startsWith("x86");
		} catch (Throwable e) {}
		return false;
	}

	public static boolean isXLargeScreen(Context paramContext) {
		return (0xF & paramContext.getResources().getConfiguration().screenLayout) >= 4;
	}

	private static native boolean nativeHasNeon();

	private static native boolean nativeHasZrtp();

	public static int sdk() {
		return buildVersion;
	}

	public static final boolean sdkAboveOrEqual(int paramInt) {
		return buildVersion >= paramInt;
	}

	public static final boolean sdkStrictlyBelow(int paramInt) {
		return buildVersion < paramInt;
	}

	public static List<String> getCpuAbis() {
		List<String> cpuabis = new ArrayList<String>();
		if (sdkAboveOrEqual(API21_LOLLIPOP_50)) {
			try {
				String abis[] = (String[]) Build.class.getField("SUPPORTED_ABIS").get(null);
				for (String abi : abis) {
					cpuabis.add(abi);
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
		} else {
			cpuabis.add(Build.CPU_ABI);
			cpuabis.add(Build.CPU_ABI2);
		}
		return cpuabis;
	}

}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.mediastream.Version JD-Core Version: 0.6.2
 */
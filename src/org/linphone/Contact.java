package org.linphone;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.linphone.compatibility.Compatibility;
import org.linphone.core.LinphoneFriend;

public class Contact implements Serializable {
	private static final long serialVersionUID = 3790717505065723499L;
	private LinphoneFriend friend;
	private String id;
	private String name;
	private List<String> numerosOrAddresses;
	private transient Bitmap photo;
	private transient Uri photoUri;
	private String number;

	public Contact(String id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.photoUri = null;
	}
	
	public Contact(String id, String name,String number) {
		super();
		this.id = id;
		this.name = name;
		this.number=number;
		this.photoUri = null;
	}
	
	public Contact(String id, String name, Uri photo) {
		super();
		this.id = id;
		this.name = name;
		this.photoUri = photo;
		this.photo = null;
	}
	
	public Contact(String id, String name, Uri photo, Bitmap picture) {
		super();
		this.id = id;
		this.name = name;
		this.photoUri = photo;
		this.photo = picture;
	}

	public LinphoneFriend getFriend() {
		return friend;
	}

	public String getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public List<String> getNumerosOrAddresses() {
		if (this.numerosOrAddresses == null)
			this.numerosOrAddresses = new ArrayList<String>();
		return numerosOrAddresses;
	}

	public Bitmap getPhoto() {
		return photo;
	}

	public Uri getPhotoUri() {
		return photoUri;
	}

	public void refresh(ContentResolver paramContentResolver) {
		this.numerosOrAddresses = Compatibility.extractContactNumbersAndAddresses(id,paramContentResolver);
		this.name = Compatibility.refreshContactName(paramContentResolver,id);
	}

	public void setFriend(LinphoneFriend paramLinphoneFriend) {
		this.friend = paramLinphoneFriend;
	}

	@Override
	public String toString() {
		return "Contact [name=" + name + ", number=" + number + "]";
	}
	
	
	
}


package org.linphone;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;
import org.linphone.mediastream.Log;
import org.linphone.mediastream.Version;
import org.linphone.mediastream.video.capture.hwconf.Hacks;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Contacts;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.sayee.linphone.R;
import com.sayee.sdk.LinphoneService;

public final class LinphoneUtils {
	public static void clearLogs() {
		try {
			Runtime.getRuntime().exec(new String[] { "logcat", "-c" });
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void collectLogs(Context context, String email) {
		BufferedReader br = null;
		Process p = null;
		StringBuilder sb = new StringBuilder();

		try {
			p = Runtime
					.getRuntime()
					.exec(new String[] { "logcat", "-d", "|", "grep",
							"`adb shell ps | grep org.linphone | cut -c10-15`" });
			br = new BufferedReader(new InputStreamReader(p.getInputStream()),
					2048);

			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append("\r\n");
			}
			String zipFilePath = context.getExternalFilesDir(null)
					.getAbsolutePath() + "/logs.zip";
			Log.i("Saving logs to " + zipFilePath);

			if (zipLogs(sb, zipFilePath)) {
				final String appName = (context != null) ? context
						.getString(R.string.app_name) : "sayeeLinphone";

				Uri zipURI = Uri.parse("file://" + zipFilePath);
				Intent i = new Intent(Intent.ACTION_SEND);
				i.putExtra(Intent.EXTRA_EMAIL, new String[] { email });
				i.putExtra(Intent.EXTRA_SUBJECT, appName + " Logs");
				i.putExtra(Intent.EXTRA_TEXT, appName + " logs");
				i.setType("application/zip");
				i.putExtra(Intent.EXTRA_STREAM, zipURI);
				try {
					context.startActivity(Intent.createChooser(i,
							"Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {

				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static final int countConferenceCalls(LinphoneCore paramLinphoneCore) {
		int i = paramLinphoneCore.getConferenceSize();
		if (paramLinphoneCore.isInConference())
			i--;
		return i;
	}

	public static int countNonConferenceCalls(LinphoneCore paramLinphoneCore) {
		return paramLinphoneCore.getCallsNb()
				- countConferenceCalls(paramLinphoneCore);
	}

	public static int countVirtualCalls(LinphoneCore paramLinphoneCore) {
		return paramLinphoneCore.getCallsNb()
				- countConferenceCalls(paramLinphoneCore);
	}

	public static Bitmap downloadBitmap(Uri uri) {
		URL url;
		InputStream is = null;
		try {
			url = new URL(uri.toString());
			is = url.openStream();
			return BitmapFactory.decodeStream(is);
		} catch (MalformedURLException e) {
			Log.e(e, e.getMessage());
		} catch (IOException e) {
			Log.e(e, e.getMessage());
		} finally {
			try {
				is.close();
			} catch (IOException x) {
			}
		}
		return null;

	}

	public static void enableView(View root, int id, OnClickListener l,
			boolean enable) {
		View v = root.findViewById(id);
		v.setVisibility(enable ? View.VISIBLE : View.GONE);
		v.setOnClickListener(l);
	}

	public static Uri findUriPictureOfContactAndSetDisplayName(
			LinphoneAddress paramLinphoneAddress,
			ContentResolver paramContentResolver) {
		ContactHelper localContactHelper = new ContactHelper(
				paramLinphoneAddress, paramContentResolver);
		localContactHelper.query();
		return localContactHelper.getUri();
	}

	public static final List<LinphoneCall> getCallsInState(
			LinphoneCore paramLinphoneCore,
			Collection<LinphoneCall.State> paramCollection) {
		ArrayList<LinphoneCall> localArrayList = new ArrayList<LinphoneCall>();
		Iterator<LinphoneCall> localIterator = getLinphoneCalls(
				paramLinphoneCore).iterator();
		while (localIterator.hasNext()) {
			LinphoneCall localLinphoneCall = (LinphoneCall) localIterator
					.next();
			if (paramCollection.contains(localLinphoneCall.getState()))
				localArrayList.add(localLinphoneCall);
		}
		return localArrayList;
	}

	public static final List<LinphoneCall> getLinphoneCalls(
			LinphoneCore paramLinphoneCore) {
		return new ArrayList<LinphoneCall>(Arrays.asList(paramLinphoneCore
				.getCalls()));
	}

	public static final List<LinphoneCall> getLinphoneCallsInConf(
			LinphoneCore paramLinphoneCore) {
		ArrayList<LinphoneCall> localArrayList = new ArrayList<LinphoneCall>();
		for (LinphoneCall localLinphoneCall : paramLinphoneCore.getCalls())
			if (localLinphoneCall.isInConference())
				localArrayList.add(localLinphoneCall);
		return localArrayList;
	}

	public static final List<LinphoneCall> getLinphoneCallsNotInConf(
			LinphoneCore paramLinphoneCore) {
		List<LinphoneCall> localArrayList = new ArrayList<LinphoneCall>();
		for (LinphoneCall localLinphoneCall : paramLinphoneCore.getCalls()) {
			if (!localLinphoneCall.isInConference())
				localArrayList.add(localLinphoneCall);
		}
		return localArrayList;
	}

	public static final List<LinphoneCall> getRunningOrPausedCalls(
			LinphoneCore paramLinphoneCore) {
		LinphoneCall.State[] arrayOfState = new LinphoneCall.State[3];
		arrayOfState[0] = LinphoneCall.State.Paused;
		arrayOfState[1] = LinphoneCall.State.PausedByRemote;
		arrayOfState[2] = LinphoneCall.State.StreamsRunning;
		return getCallsInState(paramLinphoneCore, Arrays.asList(arrayOfState));
	}

	public static String getUsernameFromAddress(String paramString) {
		if (paramString.contains("sip:"))
			paramString = paramString.replace("sip:", "");
		if (paramString.contains("@"))
			paramString = paramString.split("@")[0];
		paramString = paramString.replace("<", "");
		return paramString;
	}

	public static final boolean hasExistingResumeableCall(
			LinphoneCore paramLinphoneCore) {
		Iterator<LinphoneCall> localIterator = getLinphoneCalls(
				paramLinphoneCore).iterator();
		while (localIterator.hasNext())
			if (((LinphoneCall) localIterator.next()).getState() == LinphoneCall.State.Paused)
				return true;
		return false;
	}

	public static boolean isCallEstablished(LinphoneCall paramLinphoneCall) {
		if (paramLinphoneCall == null)
			return false;
		LinphoneCall.State localState = paramLinphoneCall.getState();
		return (isCallRunning(paramLinphoneCall)
				|| (localState == LinphoneCall.State.Paused)
				|| (localState == LinphoneCall.State.PausedByRemote) || (localState == LinphoneCall.State.Pausing));
	}

	public static boolean isCallRunning(LinphoneCall call) {
		if (call == null) {
			return false;
		}

		LinphoneCall.State state = call.getState();

		return state == LinphoneCall.State.Connected
				|| state == LinphoneCall.State.CallUpdating
				|| state == LinphoneCall.State.CallUpdatedByRemote
				|| state == LinphoneCall.State.StreamsRunning
				|| state == LinphoneCall.State.Resuming;
	}

	private static boolean isConnectionFast(int type, int subType) {
		if (type == ConnectivityManager.TYPE_WIFI) {
			return true;
		} else if (type == ConnectivityManager.TYPE_MOBILE) {
			switch (subType) {
			case TelephonyManager.NETWORK_TYPE_1xRTT:
				return false; // ~ 50-100 kbps
			case TelephonyManager.NETWORK_TYPE_CDMA:
				return false; // ~ 14-64 kbps
			case TelephonyManager.NETWORK_TYPE_EDGE:
				return false; // ~ 50-100 kbps
			case TelephonyManager.NETWORK_TYPE_GPRS:
				return false; // ~ 100 kbps
			case TelephonyManager.NETWORK_TYPE_EVDO_0:
				return false; // ~25 kbps
			case TelephonyManager.NETWORK_TYPE_LTE:
				return true; // ~ 400-1000 kbps
			case TelephonyManager.NETWORK_TYPE_EVDO_A:
				return true; // ~ 600-1400 kbps
			case TelephonyManager.NETWORK_TYPE_HSDPA:
				return true; // ~ 2-14 Mbps
			case TelephonyManager.NETWORK_TYPE_HSPA:
				return true; // ~ 700-1700 kbps
			case TelephonyManager.NETWORK_TYPE_HSUPA:
				return true; // ~ 1-23 Mbps
			case TelephonyManager.NETWORK_TYPE_UMTS:
				return true; // ~ 400-7000 kbps
			case TelephonyManager.NETWORK_TYPE_EHRPD:
				return true; // ~ 1-2 Mbps
			case TelephonyManager.NETWORK_TYPE_EVDO_B:
				return true; // ~ 5 Mbps
			case TelephonyManager.NETWORK_TYPE_HSPAP:
				return true; // ~ 10-20 Mbps
			case TelephonyManager.NETWORK_TYPE_IDEN:
				return true; // ~ 10+ Mbps
			case TelephonyManager.NETWORK_TYPE_UNKNOWN:
			default:
				return false;
			}
		} else {
			return false;
		}
	}

	public static boolean isHightBandwidthConnection(Context paramContext) {
		NetworkInfo localNetworkInfo = ((ConnectivityManager) paramContext
				.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getActiveNetworkInfo();
		return (localNetworkInfo != null)
				&& (localNetworkInfo.isConnected())
				&& (isConnectionFast(localNetworkInfo.getType(),
						localNetworkInfo.getSubtype()));
	}

	public static boolean isNumberAddress(String paramString) {
		return paramString.matches("[-+]?\\d*\\.?\\d+");
	}

	public static boolean isSipAddress(String paramString) {
		try {
			LinphoneCoreFactory.instance().createLinphoneAddress(paramString);
			return true;
		} catch (LinphoneCoreException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isStrictSipAddress(String paramString) {
		return (isSipAddress(paramString)) && (paramString.startsWith("sip:"));
	}

	public static boolean onKeyBackGoHome(Activity paramActivity, int paramInt,
			KeyEvent paramKeyEvent) {
		if ((paramInt != 4) || (paramKeyEvent.getRepeatCount() != 0))
			return false;
		paramActivity.startActivity(new Intent().setAction(
				"android.intent.action.MAIN").addCategory(
				"android.intent.category.HOME"));
		return true;
	}

	public static boolean onKeyVolumeAdjust(int keyCode) {
		if (!((keyCode == KeyEvent.KEYCODE_VOLUME_UP || keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
				&& (Hacks.needSoftvolume()) || Build.VERSION.SDK_INT >= 15)) {
			return false; // continue
		}

		if (!LinphoneService.isReady()) {
			Log.i("Couldn't change softvolume has service is not running");
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			LinphoneManager.getInstance().adjustVolume(1);
		} else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			LinphoneManager.getInstance().adjustVolume(-1);
		}
		return true;
	}

	public static int pixelsToDpi(Resources paramResources, int paramInt) {
		return (int) TypedValue.applyDimension(1, paramInt,
				paramResources.getDisplayMetrics());
	}

	public static void setImagePictureFromUri(Context paramContext,
			ImageView paramImageView, Uri paramUri, int paramInt) {
		if (paramUri == null) {
			paramImageView.setImageResource(paramInt);
			return;
		}
		if (paramUri.getScheme().startsWith("http")) {
			Bitmap localBitmap = downloadBitmap(paramUri);
			if (localBitmap == null)
				paramImageView.setImageResource(paramInt);
			paramImageView.setImageBitmap(localBitmap);
			return;
		}
		if (Version.sdkAboveOrEqual(6)) {
			paramImageView.setImageURI(paramUri);
			return;
		}
		paramImageView.setImageBitmap(Contacts.People.loadContactPhoto(
				paramContext, paramUri, paramInt, null));
	}

	public static void setVisibility(View v, int id, boolean visible) {
		v.findViewById(id).setVisibility(visible ? View.VISIBLE : View.GONE);
	}

	public static void setVisibility(View v, boolean visible) {
		v.setVisibility(visible ? View.VISIBLE : View.GONE);
	}

	public static boolean zipLogs(StringBuilder paramStringBuilder,
			String paramString) {
		try {
			ZipOutputStream localZipOutputStream = new ZipOutputStream(
					new BufferedOutputStream(new FileOutputStream(paramString)));
			localZipOutputStream.putNextEntry(new ZipEntry("logs.txt"));
			localZipOutputStream
					.write(paramStringBuilder.toString().getBytes());
			localZipOutputStream.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 涓枃杞琧ode
	 * 
	 * @param chinese
	 * @return
	 */
	public static String string2CodePoint(String chinese) {
		if (TextUtils.isEmpty(chinese)) {
			return "";
		}
		String cp = "";
		for (int i = 0, codePoint = 0; i < chinese.length(); i += Character
				.charCount(codePoint)) {
			codePoint = chinese.codePointAt(i);
			cp = cp + codePoint + ",";
		}
		return cp;
	}

	/**
	 * code杞腑鏂�
	 * 
	 * @param chinese
	 * @return
	 */
	public static String codePoint2String(String code_point) {
		if (TextUtils.isEmpty(code_point)) {
			return "";
		}
		String[] charArray;
		String str = "";
		charArray = code_point.split(",");
		if (charArray != null) {
			for (int i = 0; i < charArray.length; i++) {
				try {
					str = str
							+ String.valueOf((char) Integer
									.parseInt(charArray[i]));
				} catch (Exception e) {
					return code_point;
				}
			}
		} else {
			return code_point;
		}
		return str;
	}

	// 闊抽鑾峰彇婧�
	public static int audioSource = MediaRecorder.AudioSource.MIC;
	// 璁剧疆闊抽閲囨牱鐜囷紝44100鏄洰鍓嶇殑鏍囧噯锛屼絾鏄煇浜涜澶囦粛鐒舵敮鎸�22050锛�16000锛�11025
	public static int sampleRateInHz = 44100;
	// 璁剧疆闊抽鐨勫綍鍒剁殑澹伴亾CHANNEL_IN_STEREO涓哄弻澹伴亾锛孋HANNEL_CONFIGURATION_MONO涓哄崟澹伴亾
	public static int channelConfig = AudioFormat.CHANNEL_IN_STEREO;
	// 闊抽鏁版嵁鏍煎紡:PCM 16浣嶆瘡涓牱鏈�備繚璇佽澶囨敮鎸併�侾CM 8浣嶆瘡涓牱鏈�備笉涓�瀹氳兘寰楀埌璁惧鏀寔銆�
	public static int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
	// 缂撳啿鍖哄瓧鑺傚ぇ灏�
	public static int bufferSizeInBytes = 0;

	/**
	 * 鍒ゆ柇鏄槸鍚︽湁褰曢煶鏉冮檺
	 */
	public static boolean isHasPermission(final Context context) {
		bufferSizeInBytes = 0;
		bufferSizeInBytes = AudioRecord.getMinBufferSize(sampleRateInHz,
				channelConfig, audioFormat);
		AudioRecord audioRecord =null; 
		// 寮�濮嬪綍鍒堕煶棰�
		try {
			audioRecord=new AudioRecord(audioSource, sampleRateInHz,channelConfig, audioFormat, bufferSizeInBytes);
			// 闃叉鏌愪簺鎵嬫満宕╂簝锛屼緥濡傝仈鎯�
			audioRecord.startRecording();
		} catch (Exception e) {
			e.printStackTrace();
			audioRecord = null;
			System.gc();
			return false;
		}
		/**
		 * 鏍规嵁寮�濮嬪綍闊冲垽鏂槸鍚︽湁褰曢煶鏉冮檺
		 */
		if (audioRecord!=null&&audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
			audioRecord = null;
			System.gc();
			return false;
		}
		if(audioRecord!=null){
			audioRecord.stop();
			audioRecord.release();
			audioRecord = null;
		}
		System.gc();
		return true;
	}

	/**
	 * 鑾峰彇褰撳墠椤圭洰鍖呭悕
	 * 
	 * @param context
	 * @return int versionCode
	 */
	public static String getPackageName(Context context) {
		PackageManager packmanager = context.getPackageManager();
		String PackageName = "";
		try {
			PackageInfo info = packmanager.getPackageInfo(
					context.getPackageName(), 0);
			PackageName = info.packageName;
		} catch (NameNotFoundException e) {
			return PackageName;
		}
		return PackageName;
	}

	public static void goHuaWeiMainager(Context context) {
		try {
			Intent intent = new Intent("demo.vincent.com.tiaozhuan");
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			ComponentName comp = new ComponentName("com.huawei.systemmanager",
					"com.huawei.permissionmanager.ui.MainActivity");
			intent.setComponent(comp);
			context.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
			goIntentSetting(context);
		}
	}

	public static void goXiaoMiMainager(Context context) {
		try {
			Intent localIntent = new Intent(
					"miui.intent.action.APP_PERM_EDITOR");
			localIntent
					.setClassName("com.miui.securitycenter",
							"com.miui.permcenter.permissions.AppPermissionsEditorActivity");
			localIntent.putExtra("extra_pkgname", context.getPackageName());
			localIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(localIntent);
		} catch (ActivityNotFoundException localActivityNotFoundException) {
			goIntentSetting(context);
		}
	}

	public static void goMeizuMainager(Context context) {
		try {
			Intent intent = new Intent("com.meizu.safe.security.SHOW_APPSEC");
			intent.addCategory(Intent.CATEGORY_DEFAULT);
			intent.putExtra("packageName", LinphoneUtils.getPackageName(context
					.getApplicationContext()));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		} catch (ActivityNotFoundException localActivityNotFoundException) {
			localActivityNotFoundException.printStackTrace();
			goIntentSetting(context);
		}
	}

	public static void goSangXinMainager(Context context) {
		// 涓夋槦4.3鍙互鐩存帴璺宠浆
		goIntentSetting(context);
	}

	public static void goIntentSetting(Context context) {
		Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		Uri uri = Uri.fromParts("package", context.getPackageName(), null);
		intent.setData(uri);
		try {
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void goOppoMainager(Context context) {
		doStartApplicationWithPackageName("com.coloros.safecenter", context);
	}

	public static void goCoolpadMainager(Context context) {
		doStartApplicationWithPackageName("com.yulong.android.security:remote",
				context);
	}

	// vivo
	public static void goVivoMainager(Context context) {
		doStartApplicationWithPackageName("com.bairenkeji.icaller", context);
	}

	public static void doStartApplicationWithPackageName(String packagename,
			Context context) {
		// 閫氳繃鍖呭悕鑾峰彇姝PP璇︾粏淇℃伅锛屽寘鎷珹ctivities銆乻ervices銆乿ersioncode銆乶ame绛夌瓑
		PackageInfo packageinfo = null;
		try {
			packageinfo = context.getPackageManager().getPackageInfo(
					packagename, 0);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		if (packageinfo == null) {
			return;
		}
		// 鍒涘缓涓�涓被鍒负CATEGORY_LAUNCHER鐨勮鍖呭悕鐨処ntent
		Intent resolveIntent = new Intent(Intent.ACTION_MAIN, null);
		resolveIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		resolveIntent.setPackage(packageinfo.packageName);
		// 閫氳繃getPackageManager()鐨剄ueryIntentActivities鏂规硶閬嶅巻
		List<ResolveInfo> resolveinfoList = context.getPackageManager()
				.queryIntentActivities(resolveIntent, 0);
		if (resolveinfoList != null) {
			ResolveInfo resolveinfo = resolveinfoList.iterator().next();
			if (resolveinfo != null) {
				// packagename = 鍙傛暟packname
				String packageName = resolveinfo.activityInfo.packageName;
				// 杩欎釜灏辨槸鎴戜滑瑕佹壘鐨勮APP鐨凩AUNCHER鐨凙ctivity[缁勭粐褰㈠紡锛歱ackagename.mainActivityname]
				String className = resolveinfo.activityInfo.name;
				// LAUNCHER Intent
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_LAUNCHER);
				// 璁剧疆ComponentName鍙傛暟1:packagename鍙傛暟2:MainActivity璺緞
				ComponentName cn = new ComponentName(packageName, className);
				intent.setComponent(cn);
				try {
					context.startActivity(intent);
				} catch (Exception e) {
					goIntentSetting(context);
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
	/**
	 * 鑾峰彇搴旂敤绋嬪簭鍚嶇О
	 */
	public static String getAppName(Context context) {
		try {
			if(context==null) return "";
			PackageManager packageManager = context.getPackageManager();
			if(packageManager==null) return "";
			PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
			if(packageInfo==null) return "";
			return context.getResources().getString(packageInfo.applicationInfo.labelRes);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}

}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.LinphoneUtils JD-Core Version: 0.6.2
 */
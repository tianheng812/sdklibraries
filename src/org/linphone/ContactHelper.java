package org.linphone;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import org.linphone.core.LinphoneAddress;
import org.linphone.mediastream.Version;

public final class ContactHelper {
	private LinphoneAddress address;
	private String displayName;
	private String domain;
	private Uri foundPhotoUri;
	private ContentResolver resolver;
	private String username;

	public ContactHelper(LinphoneAddress paramLinphoneAddress,
			ContentResolver paramContentResolver) {
		this.username = paramLinphoneAddress.getUserName();
		this.domain = paramLinphoneAddress.getDomain();
		this.resolver = paramContentResolver;
		this.address = paramLinphoneAddress;
	}

	public static Intent prepareEditContactIntent(int paramInt) {
		Intent localIntent = new Intent("android.intent.action.EDIT",
				ContactsContract.Contacts.CONTENT_URI);
		localIntent.setData(ContentUris.withAppendedId(
				ContactsContract.Contacts.CONTENT_URI, paramInt));
		return localIntent;
	}

	private final boolean queryOldContactAPI() {
		String str1 = PhoneNumberUtils.getStrippedReversed(this.username);
		if (TextUtils.isEmpty(str1))
			return false;
		String[] arrayOfString = { "person", "display_name" };
		String str2 = "number_key=" + str1;
		Cursor localCursor = resolver.query(
				android.provider.Contacts.Phones.CONTENT_URI, arrayOfString,
				str2, null, null);
		if (localCursor == null)
			return false;
		while (localCursor.moveToNext()) {
			long l = localCursor.getLong(localCursor.getColumnIndex("person"));
			Uri localUri1 = ContentUris.withAppendedId(
					android.provider.Contacts.People.CONTENT_URI, l);
			Uri localUri2 = Uri.withAppendedPath(localUri1, "photo");
			if (testPhotoUri(this.resolver, localUri2, "data")) {
				this.displayName = localCursor.getString(localCursor
						.getColumnIndex("display_name"));
				this.foundPhotoUri = localUri1;
				localCursor.close();
				return true;
			}
		}
		localCursor.close();
		return false;
	}

	public static boolean testPhotoUri(ContentResolver paramContentResolver,
			Uri paramUri, String paramString) {
		return testPhotoUriAndCloseCursor(paramContentResolver.query(paramUri,
				new String[] { paramString }, null, null, null));
	}

	public static boolean testPhotoUri(Cursor c) {
		if (c == null)
			return false;
		if (!c.moveToNext()) {
			return false;
		}
		byte[] data = c.getBlob(0);
		if (data == null) {
			return false;
		}
		return true;
	}

	public static boolean testPhotoUriAndCloseCursor(Cursor paramCursor) {
		boolean bool = testPhotoUri(paramCursor);
		if (paramCursor != null)
			paramCursor.close();
		return bool;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public Uri getUri() {
		return this.foundPhotoUri;
	}

	public boolean query() {
		boolean succeeded;
		if (Version.sdkAboveOrEqual(Version.API05_ECLAIR_20)) {
			ContactHelperNew helper = new ContactHelperNew();
			succeeded = helper.queryNewContactAPI();
		} else {
			succeeded = queryOldContactAPI();
		}
		if (succeeded && !TextUtils.isEmpty(displayName)) {
			address.setDisplayName(displayName);
		}
		return succeeded;
	}

	private class ContactHelperNew {
		private ContactHelperNew() {
		}

		private final boolean checkPhotosUris(
				ContentResolver paramContentResolver, Cursor paramCursor,
				String paramString1, String paramString2) {
			if (paramCursor == null)
				return false;
			while (paramCursor.moveToNext()) {
				long l = paramCursor.getLong(paramCursor
						.getColumnIndex(paramString1));
				Uri localUri = Uri.withAppendedPath(ContentUris.withAppendedId(
						ContactsContract.Contacts.CONTENT_URI, l), "photo");
				if (localUri == null)
					return false;
				Cursor localCursor = paramContentResolver.query(localUri,
						new String[] { "data15" }, null, null, null);
				String str = paramCursor.getString(paramCursor
						.getColumnIndex(paramString2));
				if (ContactHelper.testPhotoUriAndCloseCursor(localCursor)) {
					foundPhotoUri = localUri;
					displayName = str;
					paramCursor.close();
					return true;
				}
			}
			paramCursor.close();
			return false;
		}

		@SuppressLint("InlinedApi")
		private final boolean queryNewContactAPI() {
			String sipUri = username + "@" + domain;

			// Try first using sip field
			Uri uri = android.provider.ContactsContract.Data.CONTENT_URI;
			String[] projection = {
					android.provider.ContactsContract.Data.CONTACT_ID,
					android.provider.ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME };

			// Then using custom SIP field
			if (Version.sdkAboveOrEqual(Version.API09_GINGERBREAD_23)) {
				String selection = new StringBuilder()
						.append(android.provider.ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS)
						.append(" = ? AND ")
						.append(android.provider.ContactsContract.Data.MIMETYPE)
						.append(" = '")
						.append(android.provider.ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE)
						.append("'").toString();
				Cursor c = resolver.query(uri, projection, selection,
						new String[] { sipUri }, null);
				boolean valid = checkPhotosUris(
						resolver,
						c,
						android.provider.ContactsContract.Data.CONTACT_ID,
						android.provider.ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
				c.close();
				if (valid)
					return true;
			}

			String selection = new StringBuilder()
					.append(android.provider.ContactsContract.CommonDataKinds.Im.DATA)
					.append(" =  ? AND ")
					.append(android.provider.ContactsContract.Data.MIMETYPE)
					.append(" = '")
					.append(android.provider.ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)
					.append("' AND lower(")
					.append(android.provider.ContactsContract.CommonDataKinds.Im.CUSTOM_PROTOCOL)
					.append(") = 'sip'").toString();
			Cursor c = resolver.query(uri, projection, selection,
					new String[] { sipUri }, null);
			boolean valid = checkPhotosUris(
					resolver,
					c,
					android.provider.ContactsContract.Data.CONTACT_ID,
					android.provider.ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
			c.close();
			if (valid)
				return true;

			// Finally using phone number
			String normalizedNumber = PhoneNumberUtils
					.getStrippedReversed(username);
			if (TextUtils.isEmpty(normalizedNumber)) {
				// non phone username
				return false;
			}
			Uri lookupUri = Uri
					.withAppendedPath(
							android.provider.ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
							Uri.encode(username));
			projection = new String[] {
					android.provider.ContactsContract.PhoneLookup._ID,
					android.provider.ContactsContract.PhoneLookup.NUMBER,
					android.provider.ContactsContract.PhoneLookup.DISPLAY_NAME };
			c = resolver.query(lookupUri, projection, null, null, null);
			while (c != null && c.moveToNext()) {
				long id = c
						.getLong(c
								.getColumnIndex(android.provider.ContactsContract.PhoneLookup._ID));
				String enteredNumber = c
						.getString(c
								.getColumnIndex(android.provider.ContactsContract.PhoneLookup.NUMBER));
				if (!normalizedNumber.equals(PhoneNumberUtils
						.getStrippedReversed(enteredNumber))) {
					continue;
				}

				Uri contactUri = ContentUris.withAppendedId(
						android.provider.ContactsContract.Contacts.CONTENT_URI,
						id);
				Uri photoUri = Uri
						.withAppendedPath(
								contactUri,
								android.provider.ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
				String[] photoProj = { android.provider.ContactsContract.CommonDataKinds.Photo.PHOTO };
				Cursor cursor = resolver.query(photoUri, photoProj, null, null,
						null);
				valid = testPhotoUriAndCloseCursor(cursor);
				displayName = c
						.getString(c
								.getColumnIndex(android.provider.ContactsContract.PhoneLookup.DISPLAY_NAME));
				if (valid) {
					foundPhotoUri = photoUri;
				} else {
					foundPhotoUri = null;
				}
				c.close();
				return true;
			}
			c.close();
			return false;
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.ContactHelper JD-Core Version: 0.6.2
 */
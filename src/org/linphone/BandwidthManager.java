package org.linphone;

import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneCore;

public class BandwidthManager {
	public static final int HIGH_RESOLUTION = 0;
	public static final int LOW_BANDWIDTH = 2;
	public static final int LOW_RESOLUTION = 1;
	private static BandwidthManager instance;
	private int currentProfile = 0;

	public static final BandwidthManager getInstance() {
			if (instance == null)
				instance = new BandwidthManager();
			return instance;
	}

	public int getCurrentProfile() {
		return this.currentProfile;
	}

	public boolean isVideoPossible() {
		return this.currentProfile != LOW_BANDWIDTH;
	}

	public void updateWithProfileSettings(LinphoneCore paramLinphoneCore,
			LinphoneCallParams paramLinphoneCallParams) {
		if (paramLinphoneCallParams != null) {
			if (!isVideoPossible()) {
				paramLinphoneCallParams.setVideoEnabled(false);
				paramLinphoneCallParams.setAudioBandwidth(40);
			} else {
				paramLinphoneCallParams.setVideoEnabled(true);
				paramLinphoneCallParams.setAudioBandwidth(0);
			}
		}
	}

}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.BandwidthManager JD-Core Version: 0.6.2
 */
package org.linphone;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.core.LinphoneContent;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneEvent;
import org.linphone.core.LinphoneProxyConfig;

public abstract interface LinphoneSimpleListener
{
  public static abstract interface ConnectivityChangedListener extends LinphoneSimpleListener
  {
    public abstract void onConnectivityChanged(Context paramContext, NetworkInfo paramNetworkInfo, ConnectivityManager paramConnectivityManager);
  }

  public static abstract interface LinphoneOnAudioChangedListener extends LinphoneSimpleListener
  {
    public abstract void onAudioStateChanged(AudioState paramAudioState);

    public static enum AudioState
    {
       EARPIECE,
       SPEAKER,
       BLUETOOTH;
    }
  }

  public static abstract interface LinphoneOnCallEncryptionChangedListener extends LinphoneSimpleListener
  {
    public abstract void onCallEncryptionChanged(LinphoneCall paramLinphoneCall, boolean paramBoolean, String paramString);
  }

	public static abstract interface LinphoneOnCallStateChangedListener extends LinphoneSimpleListener
  {
    public abstract void onCallStateChanged(LinphoneCall paramLinphoneCall, LinphoneCall.State paramState, String paramString);
  }

  public static abstract interface LinphoneOnComposingReceivedListener extends LinphoneSimpleListener
  {
    public abstract void onComposingReceived(LinphoneChatRoom paramLinphoneChatRoom);
  }

  public static abstract interface LinphoneOnDTMFReceivedListener extends LinphoneSimpleListener
  {
    public abstract void onDTMFReceived(LinphoneCall paramLinphoneCall, int paramInt);
  }

  public static abstract interface LinphoneOnGlobalStateChangedListener extends LinphoneSimpleListener
  {
    public abstract void onGlobalStateChanged(LinphoneCore.GlobalState paramGlobalState, String paramString);
  }

  public static abstract interface LinphoneOnMessageReceivedListener extends LinphoneSimpleListener
  {
    public abstract void onMessageReceived(LinphoneAddress paramLinphoneAddress, LinphoneChatMessage paramLinphoneChatMessage, int paramInt);
  }

  public static abstract interface LinphoneOnNotifyReceivedListener extends LinphoneSimpleListener
  {
    public abstract void onNotifyReceived(LinphoneEvent paramLinphoneEvent, String paramString, LinphoneContent paramLinphoneContent);
  }

  public static abstract interface LinphoneOnRegistrationStateChangedListener extends LinphoneSimpleListener
  {
    public abstract void onRegistrationStateChanged(LinphoneProxyConfig paramLinphoneProxyConfig, LinphoneCore.RegistrationState paramRegistrationState, String paramString);
  }

  public static abstract interface LinphoneOnRemoteProvisioningListener extends LinphoneSimpleListener
  {
    public abstract void onConfiguringStatus(LinphoneCore.RemoteProvisioningState paramRemoteProvisioningState);
  }

  public static abstract interface LinphoneServiceListener extends LinphoneSimpleListener.LinphoneOnGlobalStateChangedListener, LinphoneSimpleListener.LinphoneOnCallStateChangedListener, LinphoneSimpleListener.LinphoneOnCallEncryptionChangedListener
  {
    public abstract void onDisplayStatus(String paramString);

    public abstract void onRegistrationStateChanged(LinphoneProxyConfig paramLinphoneProxyConfig, LinphoneCore.RegistrationState paramRegistrationState, String paramString);

    public abstract void tryingNewOutgoingCallButAlreadyInCall();

    public abstract void tryingNewOutgoingCallButCannotGetCallParameters();

    public abstract void tryingNewOutgoingCallButWrongDestinationAddress();
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.LinphoneSimpleListener
 * JD-Core Version:    0.6.2
 */
package org.linphone.compatibility;

import android.annotation.TargetApi;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.Data;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import org.linphone.Contact;
import org.linphone.core.LinphoneAddress;
import org.linphone.tools.BooleanUtil;
import org.linphone.tools.StringUtil;

import com.sayee.linphone.R;

@TargetApi(9)
public class ApiNinePlus
{
	public static void addSipAddressToContact(Context context, ArrayList<ContentProviderOperation> ops, String sipAddress) {
		ContentProviderOperation c=ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
				.withValue(ContactsContract.Data.MIMETYPE, CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE)
				.withValue(ContactsContract.CommonDataKinds.SipAddress.DATA, sipAddress)
				.withValue(CommonDataKinds.SipAddress.TYPE, CommonDataKinds.SipAddress.TYPE_CUSTOM)
				.withValue(CommonDataKinds.SipAddress.LABEL,StringUtil.getInstance().getAddressbook_label())
				.build();
		
		ops.add(c);
	}
	public static void addSipAddressToContact(Context context, ArrayList<ContentProviderOperation> ops, String sipAddress, String rawContactID) {
		ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
		    .withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
	        .withValue(ContactsContract.Data.MIMETYPE, CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE)
	        .withValue(ContactsContract.CommonDataKinds.SipAddress.DATA, sipAddress)
			.withValue(CommonDataKinds.SipAddress.TYPE, CommonDataKinds.SipAddress.TYPE_CUSTOM)
			.withValue(CommonDataKinds.SipAddress.LABEL, StringUtil.getInstance().getAddressbook_label())
	        .build()
	    );
	}

	public static void deleteSipAddressFromContact(ArrayList<ContentProviderOperation> ops, String oldSipAddress, String contactID) {
		String select = ContactsContract.Data.CONTACT_ID + "=? AND "
				+ ContactsContract.Data.MIMETYPE + "='" + ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE +  "' AND "
				+ ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS + "=? ";
		String[] args = new String[] { String.valueOf(contactID), oldSipAddress };
		
        ops.add(ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI) 
    		.withSelection(select, args) 
            .build()
        );
	}

  public static List<String> extractContactNumbersAndAddresses(String id, ContentResolver cr)
  {
	  List<String> list = new ArrayList<String>();
	  Uri uri = Data.CONTENT_URI;
		String[] projection;

		// Phone Numbers
		Cursor c = cr.query(Phone.CONTENT_URI, new String[] { Phone.NUMBER }, Phone.CONTACT_ID + " = " + id, null, null);
		if (c != null) {
	        while (c.moveToNext()) {
	            String number = c.getString(c.getColumnIndex(Phone.NUMBER));
	            list.add(number); 
	        }
	        c.close();
		}

		// SIP addresses
		String selection2 = new StringBuilder()
			.append(Data.CONTACT_ID)
			.append(" = ? AND ")
			.append(Data.MIMETYPE)
			.append(" = '")
			.append(ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE)
			.append("'")
			.toString();
		projection = new String[] {ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS};
		c = cr.query(uri, projection, selection2, new String[]{id}, null);
		if (c != null) {
			int nbid = c.getColumnIndex(ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS);
			while (c.moveToNext()) {
				list.add("sip:" + c.getString(nbid)); 
			}
			c.close();
		}

		return list;
  }

  public static Uri findUriPictureOfContactAndSetDisplayName(LinphoneAddress paramLinphoneAddress, ContentResolver paramContentResolver)
  {
    String str1 = paramLinphoneAddress.getUserName();
    String str2 = paramLinphoneAddress.getDomain();
    String str3 = str1 + "@" + str2;
    Cursor localCursor = getSIPContactCursor(paramContentResolver, str3);
    Contact localContact = ApiFivePlus.getContact(paramContentResolver, localCursor, 0);
    if ((localContact != null) && (localContact.getNumerosOrAddresses().contains(str3)))
    {
      paramLinphoneAddress.setDisplayName(localContact.getName());
      localCursor.close();
      return localContact.getPhotoUri();
    }
    localCursor.close();
    return null;
  }

 /* public static Cursor getContactsCursor(ContentResolver paramContentResolver, String paramString)
  {
    String str = "(mimetype = 'vnd.android.cursor.item/phone_v2' AND data1 IS NOT NULL OR (mimetype = 'vnd.android.cursor.item/im' AND lower(data6) = 'sip' AND data1 IS NOT NULL) OR (mimetype = 'vnd.android.cursor.item/sip_address' AND data1 IS NOT NULL))";
    if (paramString != null)
      str = str + " AND display_name LIKE '%" + paramString + "%'";
    return ApiFivePlus.getGeneralContactCursor(paramContentResolver, str, true);
  }*/
  
  public static Cursor getContactsCursor(ContentResolver cr, String search) {
		 String	req = "(" + Data.MIMETYPE + " = '" + CommonDataKinds.Phone.CONTENT_ITEM_TYPE
				+ "' AND " + CommonDataKinds.Phone.NUMBER + " IS NOT NULL "
				+ " OR (" + Data.MIMETYPE + " = '" + CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE
				+ "' AND " + ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS + " IS NOT NULL))";

		if (search != null) {
			req += " AND " + Data.DISPLAY_NAME + " LIKE '%" + search + "%'";
		}

		return ApiFivePlus.getGeneralContactCursor(cr, req, true);
	}
  

  private static Cursor getSIPContactCursor(ContentResolver paramContentResolver, String paramString)
  {
    return ApiFivePlus.getGeneralContactCursor(paramContentResolver, "(mimetype = 'vnd.android.cursor.item/im AND lower(data6) = 'sip' AND data1 LIKE '" + paramString + "' " + " OR " + "mimetype" + " = '" + "vnd.android.cursor.item/sip_address" + " AND " + "data1" + " LIKE '" + paramString + "'", false);
  }

  public static Cursor getSIPContactsCursor(ContentResolver paramContentResolver, String paramString)
  {
    String str = "(mimetype = 'vnd.android.cursor.item/im' AND lower(data6) = 'sip' AND data1 IS NOT NULL OR (mimetype = 'vnd.android.cursor.item/sip_address' AND data1 IS NOT NULL))";
    if (paramString != null)
      str = str + " AND display_name LIKE '%" + paramString + "%'";
    return ApiFivePlus.getGeneralContactCursor(paramContentResolver, str, true);
  }

  public static void updateSipAddressForContact(ArrayList<ContentProviderOperation> paramArrayList, String paramString1, String paramString2, String paramString3)
  {
    String[] arrayOfString = new String[2];
    arrayOfString[0] = String.valueOf(paramString3);
    arrayOfString[1] = paramString1;
    paramArrayList.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection("contact_id=? AND mimetype='vnd.android.cursor.item/im' AND data1=? AND lower(data6) = 'sip'", arrayOfString).withValue("mimetype", "vnd.android.cursor.item/im").withValue("data1", paramString2).build());
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.compatibility.ApiNinePlus
 * JD-Core Version:    0.6.2
 */
package org.linphone.compatibility;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.preference.Preference;
import android.view.ViewTreeObserver;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.linphone.Contact;
import org.linphone.core.LinphoneAddress;
import org.linphone.mediastream.Version;

public class Compatibility {
	public static void addSipAddressToContact(Context paramContext,
			ArrayList<ContentProviderOperation> paramArrayList,
			String paramString) {
		if (Version.sdkAboveOrEqual(9)) {
			ApiNinePlus.addSipAddressToContact(paramContext, paramArrayList,
					paramString);
			return;
		}
		ApiFivePlus.addSipAddressToContact(paramContext, paramArrayList,
				paramString);
	}

	public static void addSipAddressToContact(Context paramContext,
			ArrayList<ContentProviderOperation> paramArrayList,
			String paramString1, String paramString2) {
		if (Version.sdkAboveOrEqual(9)) {
			ApiNinePlus.addSipAddressToContact(paramContext, paramArrayList,
					paramString1, paramString2);
		} else {
			ApiFivePlus.addSipAddressToContact(paramContext, paramArrayList,
					paramString1, paramString2);
		}
	}

	public static void copyTextToClipboard(Context paramContext,
			String paramString) {
		if (Version.sdkAboveOrEqual(11)) {
			ApiElevenPlus.copyTextToClipboard(paramContext, paramString);
			return;
		}
		ApiFivePlus.copyTextToClipboard(paramContext, paramString);
	}

	public static Notification createInCallNotification(Context paramContext,
			String paramString1, String paramString2, int paramInt,
			Bitmap paramBitmap, String paramString3,
			PendingIntent paramPendingIntent) {
		if (Version.sdkAboveOrEqual(16))
			return ApiSixteenPlus.createInCallNotification(paramContext,
					paramString1, paramString2, paramInt, paramBitmap,
					paramString3, paramPendingIntent);
		if (Version.sdkAboveOrEqual(11))
			return ApiElevenPlus.createInCallNotification(paramContext,
					paramString1, paramString2, paramInt, paramBitmap,
					paramString3, paramPendingIntent);
		return ApiFivePlus.createInCallNotification(paramContext, paramString1,
				paramString2, paramInt, paramPendingIntent);
	}

	public static Notification createMessageNotification(Context context,
			int msgCount, String msgSender, String msg, Bitmap contactIcon,
			PendingIntent intent) {
		Notification notif = null;
		String title;
		if (msgCount == 1) {
			title = "Unread message from %s".replace("%s", msgSender);
		} else {
			title = "%i unread messages"
					.replace("%i", String.valueOf(msgCount));
		}

		if (Version.sdkAboveOrEqual(Version.API16_JELLY_BEAN_41)) {
			notif = ApiSixteenPlus.createMessageNotification(context, msgCount,
					msgSender, msg, contactIcon, intent);
		} else if (Version.sdkAboveOrEqual(Version.API11_HONEYCOMB_30)) {
			notif = ApiElevenPlus.createMessageNotification(context, msgCount,
					msgSender, msg, contactIcon, intent);
		} else {
			notif = ApiFivePlus.createMessageNotification(context, title, msg,
					intent);
		}
		return notif;
	}

	public static Notification createNotification(Context paramContext,
			String paramString1, String paramString2, int paramInt1,
			int paramInt2, Bitmap paramBitmap,
			PendingIntent paramPendingIntent, boolean paramBoolean) {
		if (Version.sdkAboveOrEqual(16))
			return ApiSixteenPlus.createNotification(paramContext,
					paramString1, paramString2, paramInt1, paramInt2,
					paramBitmap, paramPendingIntent, paramBoolean);
		if (Version.sdkAboveOrEqual(11))
			return ApiElevenPlus.createNotification(paramContext, paramString1,
					paramString2, paramInt1, paramInt2, paramBitmap,
					paramPendingIntent, paramBoolean);
		return ApiFivePlus.createNotification(paramContext, paramString1,
				paramString2, paramInt1, paramInt2, paramPendingIntent,
				paramBoolean);
	}

	public static void deleteSipAddressFromContact(
			ArrayList<ContentProviderOperation> paramArrayList,
			String paramString1, String paramString2) {
		if (Version.sdkAboveOrEqual(9)) {
			ApiNinePlus.deleteSipAddressFromContact(paramArrayList,
					paramString1, paramString2);
			return;
		}
		ApiFivePlus.deleteSipAddressFromContact(paramArrayList, paramString1,
				paramString2);
	}

	public static List<String> extractContactNumbersAndAddresses(
			String paramString, ContentResolver paramContentResolver) {
		if (Version.sdkAboveOrEqual(9))
			return ApiNinePlus.extractContactNumbersAndAddresses(paramString,
					paramContentResolver);
		return ApiFivePlus.extractContactNumbersAndAddresses(paramString,
				paramContentResolver);
	}

	public static Uri findUriPictureOfContactAndSetDisplayName(
			LinphoneAddress paramLinphoneAddress,
			ContentResolver paramContentResolver) {
		if (Version.sdkAboveOrEqual(9))
			return ApiNinePlus.findUriPictureOfContactAndSetDisplayName(
					paramLinphoneAddress, paramContentResolver);
		return ApiFivePlus.findUriPictureOfContactAndSetDisplayName(
				paramLinphoneAddress, paramContentResolver);
	}

	public static String getAudioManagerEventForBluetoothConnectionStateChangedEvent() {
		if (Version.sdkAboveOrEqual(14))
			return ApiFourteenPlus
					.getAudioManagerEventForBluetoothConnectionStateChangedEvent();
		return ApiEightPlus
				.getAudioManagerEventForBluetoothConnectionStateChangedEvent();
	}

	public static Contact getContact(ContentResolver paramContentResolver,
			Cursor paramCursor, int paramInt) {
		if (Version.sdkAboveOrEqual(5))
			return ApiFivePlus.getContact(paramContentResolver, paramCursor,
					paramInt);
		return null;
	}

	public static InputStream getContactPictureInputStream(
			ContentResolver paramContentResolver, String paramString) {
		if (Version.sdkAboveOrEqual(5))
			return ApiFivePlus.getContactPictureInputStream(
					paramContentResolver, paramString);
		return null;
	}

	public static Cursor getContactsCursor(ContentResolver paramContentResolver) {
		if (Version.sdkAboveOrEqual(9))
			return ApiNinePlus.getContactsCursor(paramContentResolver, null);
		return ApiFivePlus.getContactsCursor(paramContentResolver);
	}

	public static Cursor getContactsCursor(
			ContentResolver paramContentResolver, String paramString) {
		if (Version.sdkAboveOrEqual(9))
			return ApiNinePlus.getContactsCursor(paramContentResolver,
					paramString);
		return ApiFivePlus.getContactsCursor(paramContentResolver);
	}

	public static int getCursorDisplayNameColumnIndex(Cursor paramCursor) {
		if (Version.sdkAboveOrEqual(5))
			return ApiFivePlus.getCursorDisplayNameColumnIndex(paramCursor);
		return -1;
	}

	public static Cursor getSIPContactsCursor(
			ContentResolver paramContentResolver) {
		if (Version.sdkAboveOrEqual(9))
			return ApiNinePlus.getSIPContactsCursor(paramContentResolver, null);
		return ApiFivePlus.getSIPContactsCursor(paramContentResolver);
	}

	public static Cursor getSIPContactsCursor(
			ContentResolver paramContentResolver, String paramString) {
		if (Version.sdkAboveOrEqual(9))
			return ApiNinePlus.getSIPContactsCursor(paramContentResolver,
					paramString);
		return ApiFivePlus.getSIPContactsCursor(paramContentResolver);
	}

	public static CompatibilityScaleGestureDetector getScaleGestureDetector(
			Context paramContext,
			CompatibilityScaleGestureListener paramCompatibilityScaleGestureListener) {
		if (Version.sdkAboveOrEqual(8)) {
			CompatibilityScaleGestureDetector localCompatibilityScaleGestureDetector = new CompatibilityScaleGestureDetector(
					paramContext);
			localCompatibilityScaleGestureDetector
					.setOnScaleListener(paramCompatibilityScaleGestureListener);
			return localCompatibilityScaleGestureDetector;
		}
		return null;
	}

	public static void hideNavigationBar(Activity paramActivity) {
		if (Version.sdkAboveOrEqual(14))
			ApiFourteenPlus.hideNavigationBar(paramActivity);
	}

	public static void initPushNotificationService(Context paramContext) {
		if (Version.sdkAboveOrEqual(8))
			ApiEightPlus.initPushNotificationService(paramContext);
	}

	public static boolean isPreferenceChecked(Preference paramPreference) {
		if (Version.sdkAboveOrEqual(14))
			return ApiFourteenPlus.isPreferenceChecked(paramPreference);
		return ApiFivePlus.isPreferenceChecked(paramPreference);
	}

	public static void overridePendingTransition(Activity paramActivity,
			int paramInt1, int paramInt2) {
		if (Version.sdkAboveOrEqual(5))
			ApiFivePlus.overridePendingTransition(paramActivity, paramInt1,
					paramInt2);
	}

	public static Intent prepareAddContactIntent(String paramString1,
			String paramString2) {
		if (Version.sdkAboveOrEqual(11))
			return ApiElevenPlus.prepareAddContactIntent(paramString1,
					paramString2);
		return ApiFivePlus.prepareAddContactIntent(paramString1, paramString2);
	}

	public static Intent prepareEditContactIntent(int paramInt) {
		if (Version.sdkAboveOrEqual(5))
			return ApiFivePlus.prepareEditContactIntent(paramInt);
		return null;
	}

	public static Intent prepareEditContactIntentWithSipAddress(int paramInt,
			String paramString) {
		if (Version.sdkAboveOrEqual(11))
			return ApiElevenPlus.prepareEditContactIntentWithSipAddress(
					paramInt, paramString);
		return ApiFivePlus.prepareEditContactIntent(paramInt);
	}

	public static String refreshContactName(
			ContentResolver paramContentResolver, String paramString) {
		if (Version.sdkAboveOrEqual(5))
			return ApiFivePlus.refreshContactName(paramContentResolver,
					paramString);
		return null;
	}

	public static void removeGlobalLayoutListener(
			ViewTreeObserver paramViewTreeObserver,
			ViewTreeObserver.OnGlobalLayoutListener paramOnGlobalLayoutListener) {
		if (Version.sdkAboveOrEqual(16)) {
			ApiSixteenPlus.removeGlobalLayoutListener(paramViewTreeObserver,
					paramOnGlobalLayoutListener);
			return;
		}
		ApiFivePlus.removeGlobalLayoutListener(paramViewTreeObserver,
				paramOnGlobalLayoutListener);
	}

	public static void setAudioManagerInCallMode(AudioManager paramAudioManager) {
		if (Version.sdkAboveOrEqual(11)) {
			ApiElevenPlus.setAudioManagerInCallMode(paramAudioManager);
			return;
		}
		ApiFivePlus.setAudioManagerInCallMode(paramAudioManager);
	}

	public static void setPreferenceChecked(Preference paramPreference,
			boolean paramBoolean) {
		if (Version.sdkAboveOrEqual(14)) {
			ApiFourteenPlus.setPreferenceChecked(paramPreference, paramBoolean);
			return;
		}
		ApiFivePlus.setPreferenceChecked(paramPreference, paramBoolean);
	}

	public static void showNavigationBar(Activity paramActivity) {
		if (Version.sdkAboveOrEqual(14))
			ApiFourteenPlus.showNavigationBar(paramActivity);
	}

	public static void updateSipAddressForContact(
			ArrayList<ContentProviderOperation> paramArrayList,
			String paramString1, String paramString2, String paramString3) {
		if (Version.sdkAboveOrEqual(9)) {
			ApiNinePlus.updateSipAddressForContact(paramArrayList,
					paramString1, paramString2, paramString3);
		} else {
			ApiFivePlus.updateSipAddressForContact(paramArrayList,
					paramString1, paramString2, paramString3);
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.compatibility.Compatibility JD-Core Version:
 * 0.6.2
 */
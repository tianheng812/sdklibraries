package org.linphone.compatibility;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;

@TargetApi(8)
public class CompatibilityScaleGestureDetector extends ScaleGestureDetector.SimpleOnScaleGestureListener
{
  private ScaleGestureDetector detector ;
  private CompatibilityScaleGestureListener listener;

  public CompatibilityScaleGestureDetector(Context paramContext)
  {
	  detector = new ScaleGestureDetector(paramContext, this);
  }

  public void destroy()
  {
    this.listener = null;
    this.detector = null;
  }

  public float getScaleFactor()
  {
    return this.detector.getScaleFactor();
  }

  public boolean onScale(ScaleGestureDetector paramScaleGestureDetector)
  {
    if (this.listener == null)
      return false;
    return this.listener.onScale(this);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    return this.detector.onTouchEvent(paramMotionEvent);
  }

  public void setOnScaleListener(CompatibilityScaleGestureListener paramCompatibilityScaleGestureListener)
  {
    this.listener = paramCompatibilityScaleGestureListener;
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.compatibility.CompatibilityScaleGestureDetector
 * JD-Core Version:    0.6.2
 */
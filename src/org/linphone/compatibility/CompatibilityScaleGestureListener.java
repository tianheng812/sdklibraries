package org.linphone.compatibility;

public abstract interface CompatibilityScaleGestureListener
{
  public abstract boolean onScale(CompatibilityScaleGestureDetector paramCompatibilityScaleGestureDetector);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.compatibility.CompatibilityScaleGestureListener
 * JD-Core Version:    0.6.2
 */
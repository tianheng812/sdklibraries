package org.linphone.compatibility;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

@TargetApi(16)
public class ApiSixteenPlus
{
  public static Notification createInCallNotification(Context paramContext, String paramString1, String paramString2, int paramInt, Bitmap paramBitmap, String paramString3, PendingIntent paramPendingIntent)
  {
    Notification localNotification = new Notification.Builder(paramContext).setContentTitle(paramString3).setContentText(paramString2).setSmallIcon(paramInt).setAutoCancel(false).setContentIntent(paramPendingIntent).setWhen(System.currentTimeMillis()).setLargeIcon(paramBitmap).build();
    localNotification.flags = (0x2 | localNotification.flags);
    return localNotification;
  }

  public static Notification createMessageNotification(Context paramContext, int paramInt, String paramString1, String paramString2, Bitmap paramBitmap, PendingIntent paramPendingIntent)
  {
    if (paramInt == 1);
    for (String str = paramString1; ; str = paramContext.getString(2131296830).replace("%i", String.valueOf(paramInt)))
      return new Notification.Builder(paramContext).setContentTitle(str).setContentText(paramString2).setSmallIcon(2130837582).setAutoCancel(true).setContentIntent(paramPendingIntent).setDefaults(7).setWhen(System.currentTimeMillis()).setLargeIcon(paramBitmap).build();
  }

  public static Notification createNotification(Context paramContext, String paramString1, String paramString2, int paramInt1, int paramInt2, Bitmap paramBitmap, PendingIntent paramPendingIntent, boolean paramBoolean)
  {
    if (paramBitmap != null);
    for (Notification localNotification = new Notification.Builder(paramContext).setContentTitle(paramString1).setContentText(paramString2).setSmallIcon(paramInt1, paramInt2).setLargeIcon(paramBitmap).setContentIntent(paramPendingIntent).setWhen(System.currentTimeMillis()).build(); ; localNotification = new Notification.Builder(paramContext).setContentTitle(paramString1).setContentText(paramString2).setSmallIcon(paramInt1, paramInt2).setContentIntent(paramPendingIntent).setWhen(System.currentTimeMillis()).build())
    {
      if (paramBoolean)
        localNotification.flags = (0x2 | localNotification.flags);
      return localNotification;
    }
  }

  public static void removeGlobalLayoutListener(ViewTreeObserver paramViewTreeObserver, ViewTreeObserver.OnGlobalLayoutListener paramOnGlobalLayoutListener)
  {
    paramViewTreeObserver.removeOnGlobalLayoutListener(paramOnGlobalLayoutListener);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.compatibility.ApiSixteenPlus
 * JD-Core Version:    0.6.2
 */
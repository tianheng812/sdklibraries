package org.linphone.compatibility;

import android.annotation.TargetApi;
import android.app.Activity;
import android.preference.Preference;
import android.preference.TwoStatePreference;
import android.view.View;
import android.view.Window;

@TargetApi(14)
public class ApiFourteenPlus
{
  public static String getAudioManagerEventForBluetoothConnectionStateChangedEvent()
  {
    return "android.media.ACTION_SCO_AUDIO_STATE_UPDATED";
  }

  public static void hideNavigationBar(Activity paramActivity)
  {
    paramActivity.getWindow().getDecorView().setSystemUiVisibility(2);
  }

  public static boolean isPreferenceChecked(Preference paramPreference)
  {
    return ((TwoStatePreference)paramPreference).isChecked();
  }

  public static void setPreferenceChecked(Preference paramPreference, boolean paramBoolean)
  {
    ((TwoStatePreference)paramPreference).setChecked(paramBoolean);
  }

  public static void showNavigationBar(Activity paramActivity)
  {
    paramActivity.getWindow().getDecorView().setSystemUiVisibility(0);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.compatibility.ApiFourteenPlus
 * JD-Core Version:    0.6.2
 */
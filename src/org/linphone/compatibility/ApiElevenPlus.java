package org.linphone.compatibility;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Build;
import android.provider.ContactsContract.Contacts;
import java.util.ArrayList;

import org.linphone.mediastream.Log;

import com.sayee.linphone.R;



@TargetApi(11)
public class ApiElevenPlus {
	public static void copyTextToClipboard(Context paramContext,
			String paramString) {
		((ClipboardManager) paramContext.getSystemService("clipboard"))
				.setPrimaryClip(ClipData.newPlainText("Message", paramString));
	}

	public static Notification createInCallNotification(Context paramContext,
			String paramString1, String paramString2, int paramInt,
			Bitmap paramBitmap, String paramString3,
			PendingIntent paramPendingIntent) {
		Notification localNotification = new Notification.Builder(paramContext)
				.setContentTitle(paramString3).setContentText(paramString2)
				.setSmallIcon(paramInt).setAutoCancel(false)
				.setContentIntent(paramPendingIntent)
				.setWhen(System.currentTimeMillis()).setLargeIcon(paramBitmap)
				.getNotification();
		localNotification.flags = (0x2 | localNotification.flags);
		return localNotification;
	}

	public static Notification createMessageNotification(Context context,
			int msgCount, String msgSender, String msg, Bitmap contactIcon,
			PendingIntent intent) {
		String title;
		if (msgCount == 1) {
			title = "Unread message from %s".replace("%s", msgSender);
		} else {
			title = "%i unread messages"
					.replace("%i", String.valueOf(msgCount));
		}

		Notification notif = new Notification.Builder(context)
				.setContentTitle(title)
				.setContentText(msg)
				.setSmallIcon(R.drawable.ic_launcher)
				.setAutoCancel(true)
				.setDefaults(
						Notification.DEFAULT_LIGHTS
								| Notification.DEFAULT_SOUND
								| Notification.DEFAULT_VIBRATE)
				.setWhen(System.currentTimeMillis()).setLargeIcon(contactIcon)
				.getNotification();
		notif.contentIntent = intent;

		return notif;
	}

	public static Notification createNotification(Context context, String title, String message, int icon, int level, Bitmap largeIcon, PendingIntent intent, boolean isOngoingEvent) {
		Notification notif;
		
		if (largeIcon != null) {
			notif = new Notification.Builder(context)
	        .setContentTitle(title)
	        .setContentText(message)
	        .setSmallIcon(icon, level)
	        .setLargeIcon(largeIcon)
	        .setContentIntent(intent)
	        .setWhen(System.currentTimeMillis())
	        .getNotification();
		} else {
			notif = new Notification.Builder(context)
	        .setContentTitle(title)
	        .setContentText(message)
	        .setSmallIcon(icon, level)
	        .setContentIntent(intent)
	        .setWhen(System.currentTimeMillis())
	        .getNotification();
		}
		if (isOngoingEvent) {
			notif.flags |= Notification.FLAG_ONGOING_EVENT;
		}
		
		return notif;
	}

	public static Intent prepareAddContactIntent(String paramString1,
			String paramString2) {
		Intent localIntent = new Intent("android.intent.action.INSERT",
				Contacts.CONTENT_URI);
		localIntent.putExtra("name", paramString1);
		if ((paramString2 != null) && (paramString2.startsWith("sip:")))
			paramString2 = paramString2.substring(4);
		ArrayList localArrayList = new ArrayList();
		ContentValues localContentValues = new ContentValues();
		localContentValues.put("mimetype", "vnd.android.cursor.item/im");
		localContentValues.put("data1", paramString2);
		localContentValues.put("data6", "Sip");
		localArrayList.add(localContentValues);
		localIntent.putParcelableArrayListExtra("data", localArrayList);
		return localIntent;
	}

	public static Intent prepareEditContactIntentWithSipAddress(int paramInt,
			String paramString) {
		Intent localIntent = new Intent("android.intent.action.EDIT",
				Contacts.CONTENT_URI);
		localIntent.setData(ContentUris.withAppendedId(Contacts.CONTENT_URI,
				paramInt));
		ArrayList localArrayList = new ArrayList();
		ContentValues localContentValues = new ContentValues();
		localContentValues.put("mimetype", "vnd.android.cursor.item/im");
		localContentValues.put("data1", paramString);
		localContentValues.put("data6", "Sip");
		localArrayList.add(localContentValues);
		localIntent.putParcelableArrayListExtra("data", localArrayList);
		return localIntent;
	}

	public static void setAudioManagerInCallMode(AudioManager manager) {
		if (manager.getMode() == AudioManager.MODE_IN_COMMUNICATION) {
			Log.w("---AudioManager: already in MODE_IN_COMMUNICATION, skipping...");
			return;
		}
		Log.d("---AudioManager: set mode to MODE_IN_COMMUNICATION");
		manager.setMode(AudioManager.MODE_IN_COMMUNICATION);
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.compatibility.ApiElevenPlus JD-Core Version:
 * 0.6.2
 */
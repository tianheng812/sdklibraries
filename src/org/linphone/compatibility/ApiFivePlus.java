package org.linphone.compatibility;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.Uri;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Data;
import android.text.ClipboardManager;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.linphone.Contact;
import org.linphone.core.LinphoneAddress;
import org.linphone.mediastream.Version;
import org.linphone.tools.StringUtil;

@TargetApi(5)
public class ApiFivePlus {
	public static void addSipAddressToContact(Context paramContext,
			ArrayList<ContentProviderOperation> paramArrayList,
			String paramString) {
		paramArrayList
				.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
						.withValueBackReference("raw_contact_id", 0)
						.withValue("mimetype", "vnd.android.cursor.item/im")
						.withValue("data1", paramString)
						.withValue("data2", Integer.valueOf(0))
						.withValue("data3", paramContext.getString(2131296263))
						.build());
	}

	public static void addSipAddressToContact(Context paramContext,
			ArrayList<ContentProviderOperation> paramArrayList,
			String paramString1, String paramString2) {
		paramArrayList
				.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
						.withValue("raw_contact_id", paramString2)
						.withValue("mimetype", "vnd.android.cursor.item/im")
						.withValue("data1", paramString1)
						.withValue("data2", Integer.valueOf(0))
						.withValue("data3", paramContext.getString(2131296263))
						.build());
	}

	public static void copyTextToClipboard(Context paramContext,
			String paramString) {
		((ClipboardManager) paramContext.getSystemService("clipboard"))
				.setText(paramString);
	}

	public static Notification createInCallNotification(Context paramContext,
			String paramString1, String paramString2, int paramInt,
			PendingIntent paramPendingIntent) {
		Notification localNotification = new Notification();
		localNotification.icon = paramInt;
		localNotification.iconLevel = 0;
		localNotification.when = System.currentTimeMillis();
		localNotification.flags = (0x2 & localNotification.flags);
		//localNotification.setLatestEventInfo(paramContext, paramString1,paramString2, paramPendingIntent);
		return localNotification;
	}

	public static Notification createMessageNotification(Context paramContext,
			String paramString1, String paramString2,
			PendingIntent paramPendingIntent) {
		Notification localNotification = new Notification();
		localNotification.icon = 2130837582;
		localNotification.iconLevel = 0;
		localNotification.when = System.currentTimeMillis();
		localNotification.flags = (0x2 & localNotification.flags);
		localNotification.defaults = (0x2 | localNotification.defaults);
		localNotification.defaults = (0x1 | localNotification.defaults);
		localNotification.defaults = (0x4 | localNotification.defaults);
		//localNotification.setLatestEventInfo(paramContext, paramString1,
		//		paramString2, paramPendingIntent);
		return localNotification;
	}

	public static Notification createNotification(Context paramContext,
			String paramString1, String paramString2, int paramInt1,
			int paramInt2, PendingIntent paramPendingIntent,
			boolean paramBoolean) {
		Notification localNotification = new Notification();
		localNotification.icon = paramInt1;
		localNotification.iconLevel = paramInt2;
		localNotification.when = System.currentTimeMillis();
		if (paramBoolean)
			localNotification.flags = (0x2 | localNotification.flags);
		//localNotification.setLatestEventInfo(paramContext, paramString1,
		//		paramString2, paramPendingIntent);
		return localNotification;
	}

	public static void deleteSipAddressFromContact(
			ArrayList<ContentProviderOperation> paramArrayList,
			String paramString1, String paramString2) {
		String[] arrayOfString = new String[2];
		arrayOfString[0] = String.valueOf(paramString2);
		arrayOfString[1] = paramString1;
		paramArrayList
				.add(ContentProviderOperation
						.newDelete(ContactsContract.Data.CONTENT_URI)
						.withSelection(
								"contact_id=? AND mimetype='vnd.android.cursor.item/im' AND data1=?",
								arrayOfString).build());
	}

	/*public static List<String> extractContactNumbersAndAddresses(
			String paramString, ContentResolver paramContentResolver) {
		ArrayList localArrayList = new ArrayList();
		Uri localUri = ContactsContract.Data.CONTENT_URI;
		String[] arrayOfString = { "data1" };
		Cursor localCursor1 = paramContentResolver.query(
				CommonDataKinds.Phone.CONTENT_URI, new String[] { "data1" },
				"contact_id = " + paramString, null, null);
		if (localCursor1 != null) {
			while (localCursor1.moveToNext())
				localArrayList.add(localCursor1.getString(localCursor1
						.getColumnIndex("data1")));
			localCursor1.close();
		}
		Cursor localCursor2 = paramContentResolver.query(localUri,
				arrayOfString, "contact_id" + " =  ? AND " + "mimetype"
						+ " = '" + "vnd.android.cursor.item/im"
						+ "' AND lower(" + "data6" + ") = 'sip'",
				new String[] { paramString }, null);
		if (localCursor2 != null) {
			int i = localCursor2.getColumnIndex("data1");
			while (localCursor2.moveToNext())
				localArrayList.add("sip:" + localCursor2.getString(i));
			localCursor2.close();
		}
		return localArrayList;
	}*/
	
	@SuppressWarnings("resource") 
	 public static List<String> extractContactNumbersAndAddresses(String id, ContentResolver cr) { 
	  List<String> list = new ArrayList<String>(); 
	 
	  Uri uri = Data.CONTENT_URI; 
	  String[] projection = {ContactsContract.CommonDataKinds.Im.DATA}; 
	 
	  // Phone Numbers 
	  Cursor c = cr.query(Phone.CONTENT_URI, new String[] { Phone.NUMBER }, Phone.CONTACT_ID + " = " + id, null, null); 
	  if (c != null) { 
	         while (c.moveToNext()) { 
	             String number = c.getString(c.getColumnIndex(Phone.NUMBER)); 
	             list.add(number);  
	         } 
	         c.close(); 
	  } 
	   
	  // SIP addresses 
	  if (Version.sdkAboveOrEqual(Version.API09_GINGERBREAD_23)) { 
	   String selection = new StringBuilder() 
	    .append(Data.CONTACT_ID) 
	    .append(" = ? AND ") 
	    .append(Data.MIMETYPE) 
	    .append(" = '") 
	    .append(ContactsContract.CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE) 
	    .append("'") 
	    .toString(); 
	   projection = new String[] {ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS}; 
	   c = cr.query(uri, projection, selection, new String[]{id}, null); 
	   if (c != null) { 
	    int nbId = c.getColumnIndex(ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS); 
	    while (c.moveToNext()) { 
	     list.add("sip:" + c.getString(nbId));  
	    } 
	    c.close(); 
	   } 
	  } else { 
	   String selection = new StringBuilder() 
	    .append(Data.CONTACT_ID).append(" =  ? AND ") 
	    .append(Data.MIMETYPE).append(" = '") 
	    .append(ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE) 
	    .append("' AND lower(") 
	    .append(ContactsContract.CommonDataKinds.Im.CUSTOM_PROTOCOL) 
	    .append(") = 'sip'") 
	    .toString(); 
	   c = cr.query(uri, projection, selection, new String[]{id}, null); 
	   if (c != null) { 
	    int nbId = c.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA); 
	    while (c.moveToNext()) { 
	     list.add("sip:" + c.getString(nbId));  
	    } 
	    c.close(); 
	   } 
	  } 
	 
	  return list; 
	 } 
	

	public static Uri findUriPictureOfContactAndSetDisplayName(
			LinphoneAddress paramLinphoneAddress,
			ContentResolver paramContentResolver) {
		String str1 = paramLinphoneAddress.getUserName();
		String str2 = paramLinphoneAddress.getDomain();
		String str3 = str1 + "@" + str2;
		Cursor localCursor = getSIPContactCursor(paramContentResolver, str3);
		Contact localContact = getContact(paramContentResolver, localCursor, 0);
		if ((localContact != null)
				&& (localContact.getNumerosOrAddresses().contains(str3))) {
			paramLinphoneAddress.setDisplayName(localContact.getName());
			localCursor.close();
			return localContact.getPhotoUri();
		}
		localCursor.close();
		return null;
	}

	public static Contact getContact(ContentResolver cr, Cursor cursor, int position) {
		try {
			cursor.moveToFirst();
			boolean success = cursor.move(position);
			if (!success)
				return null;
			String id = cursor.getString(cursor.getColumnIndex(Data.CONTACT_ID));
			String number=cursor.getString(cursor.getColumnIndex(Phone.NUMBER));
	    	String name = getContactDisplayName(cursor);
	        return new Contact(id, name , number);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static String getContactDisplayName(Cursor paramCursor) {
		return paramCursor
				.getString(paramCursor.getColumnIndex("display_name"));
	}

	public static InputStream getContactPictureInputStream(
			ContentResolver paramContentResolver, String paramString) {
		return android.provider.ContactsContract.Contacts.openContactPhotoInputStream(paramContentResolver,
				getContactPictureUri(paramString));
	}

	private static Uri getContactPictureUri(String paramString) {
		return ContentUris.withAppendedId(Contacts.CONTENT_URI,
				Long.parseLong(paramString));
	}

	public static Cursor getContactsCursor(ContentResolver paramContentResolver) {
		return getGeneralContactCursor(
				paramContentResolver,
				"mimetype = 'vnd.android.cursor.item/phone_v2' AND data1 IS NOT NULL"
						+ " OR (mimetype = 'vnd.android.cursor.item/im' AND lower(data6) = 'sip')",
				true);
	}

	public static int getCursorDisplayNameColumnIndex(Cursor paramCursor) {
		return paramCursor.getColumnIndex("display_name");
	}

	public static Cursor getGeneralContactCursor(ContentResolver cr, String select, boolean shouldGroupBy) { 
		   
		  String[] projection = new String[] { Data.CONTACT_ID,Phone.NUMBER, Data.DISPLAY_NAME }; 
		   
		  String query = Data.DISPLAY_NAME + " IS NOT NULL AND (" + select + ")"; 
		  Cursor cursor = cr.query(Data.CONTENT_URI, projection, query, null, Data.DISPLAY_NAME + " COLLATE NOCASE ASC"); 
		   
		  if (!shouldGroupBy || cursor == null) { 
		   return cursor; 
		  } 
		   
		  MatrixCursor result = new MatrixCursor(cursor.getColumnNames()); 
		  Set<String> groupBy = new HashSet<String>(); 
		  while (cursor.moveToNext()) { 
		      String name = cursor.getString(getCursorDisplayNameColumnIndex(cursor)); 
		      if (!groupBy.contains(name)) { 
		       groupBy.add(name); 
		       Object[] newRow = new Object[cursor.getColumnCount()]; 
		        
		       int contactID = cursor.getColumnIndex(Data.CONTACT_ID); 
		       int displayName = cursor.getColumnIndex(Data.DISPLAY_NAME); 
		       int number = cursor.getColumnIndex(Phone.NUMBER); 
		        
		       newRow[contactID] = cursor.getString(contactID); 
		       newRow[displayName] = cursor.getString(displayName); 
		       newRow[number] = StringUtil.getNormalNumber(cursor.getString(number)); 
		        
		        result.addRow(newRow); 
		      } 
		     } 
		  cursor.close(); 
		  return result; 
		 } 
	

	private static Cursor getSIPContactCursor(
			ContentResolver paramContentResolver, String paramString) {
		return getGeneralContactCursor(
				paramContentResolver,
				"mimetype = 'vnd.android.cursor.item/im AND lower(data6) = 'sip' AND data1 LIKE '"
						+ paramString + "'", false);
	}

	public static Cursor getSIPContactsCursor(
			ContentResolver paramContentResolver) {
		return getGeneralContactCursor(
				paramContentResolver,
				"mimetype = 'vnd.android.cursor.item/im' AND lower(data6) = 'sip'",
				true);
	}

	public static boolean isPreferenceChecked(Preference paramPreference) {
		return ((CheckBoxPreference) paramPreference).isChecked();
	}

	public static void overridePendingTransition(Activity paramActivity,
			int paramInt1, int paramInt2) {
		paramActivity.overridePendingTransition(paramInt1, paramInt2);
	}

	public static Intent prepareAddContactIntent(String paramString1,
			String paramString2) {
		Intent localIntent = new Intent("android.intent.action.INSERT",
				Contacts.CONTENT_URI);
		localIntent.putExtra("name", paramString1);
		localIntent.putExtra("im_handle", paramString2);
		localIntent.putExtra("im_protocol", "sip");
		return localIntent;
	}

	public static Intent prepareEditContactIntent(int paramInt) {
		Intent localIntent = new Intent("android.intent.action.EDIT",
				Contacts.CONTENT_URI);
		localIntent.setData(ContentUris.withAppendedId(Contacts.CONTENT_URI,
				paramInt));
		return localIntent;
	}

	public static Intent prepareEditContactIntentWithSipAddress(int paramInt,
			String paramString) {
		Intent localIntent = new Intent("android.intent.action.EDIT",
				Contacts.CONTENT_URI);
		localIntent.setData(ContentUris.withAppendedId(Contacts.CONTENT_URI,
				paramInt));
		localIntent.putExtra("im_handle", paramString);
		localIntent.putExtra("im_protocol", "sip");
		return localIntent;
	}

	public static String refreshContactName(
			ContentResolver paramContentResolver, String paramString) {
		Cursor localCursor = getGeneralContactCursor(paramContentResolver,
				"contact_id = '" + paramString + "'", false);
		if ((localCursor != null) && (localCursor.moveToFirst())) {
			String str = getContactDisplayName(localCursor);
			localCursor.close();
			return str;
		}
		localCursor.close();
		return null;
	}

	public static void removeGlobalLayoutListener(
			ViewTreeObserver paramViewTreeObserver,
			ViewTreeObserver.OnGlobalLayoutListener paramOnGlobalLayoutListener) {
		paramViewTreeObserver
				.removeGlobalOnLayoutListener(paramOnGlobalLayoutListener);
	}

	public static void setAudioManagerInCallMode(AudioManager paramAudioManager) {
	}

	public static void setPreferenceChecked(Preference paramPreference,
			boolean paramBoolean) {
		((CheckBoxPreference) paramPreference).setChecked(paramBoolean);
	}

	public static void updateSipAddressForContact(
			ArrayList<ContentProviderOperation> paramArrayList,
			String paramString1, String paramString2, String paramString3) {
		String[] arrayOfString = new String[2];
		arrayOfString[0] = String.valueOf(paramString3);
		arrayOfString[1] = paramString1;
		paramArrayList
				.add(ContentProviderOperation
						.newUpdate(ContactsContract.Data.CONTENT_URI)
						.withSelection(
								"contact_id=? AND mimetype='vnd.android.cursor.item/im' AND data1=?",
								arrayOfString)
						.withValue("mimetype", "vnd.android.cursor.item/im")
						.withValue("data1", paramString2).build());
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.compatibility.ApiFivePlus JD-Core Version: 0.6.2
 */
package org.linphone.compatibility;

import android.annotation.TargetApi;
import android.content.Context;
import com.google.android.gcm.GCMRegistrar;
import org.linphone.LinphonePreferences;
import org.linphone.mediastream.Log;

@TargetApi(8)
public class ApiEightPlus
{
  public static String getAudioManagerEventForBluetoothConnectionStateChangedEvent()
  {
    return "android.media.SCO_AUDIO_STATE_CHANGED";
  }

  public static void initPushNotificationService(Context context)
  {
		try {
			// Starting the push notification service
			GCMRegistrar.checkDevice(context);
			GCMRegistrar.checkManifest(context);
			final String regId = GCMRegistrar.getRegistrationId(context);
			String newPushSenderID = context.getString(2131296279);
			String currentPushSenderID = LinphonePreferences.instance().getPushNotificationRegistrationID();
			if (regId.equals("") || currentPushSenderID == null || !currentPushSenderID.equals(newPushSenderID)) {
				GCMRegistrar.register(context, newPushSenderID);

				Log.d("Push Notification : storing current sender id = " + newPushSenderID);
				LinphonePreferences.instance().setPushNotificationRegistrationID(newPushSenderID);
			} else {
				Log.d("Push Notification : already registered with id = " + regId);
				 LinphonePreferences.instance().setPushNotificationRegistrationID(regId);
			}
		} catch (java.lang.UnsupportedOperationException e) {
			Log.i("Push Notification not activated");
		}
	  
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     org.linphone.compatibility.ApiEightPlus
 * JD-Core Version:    0.6.2
 */
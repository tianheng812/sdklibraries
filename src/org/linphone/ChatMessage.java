package org.linphone;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.linphone.core.LinphoneChatMessage;

public class ChatMessage {
	private int id;
	private Bitmap image;
	private boolean incoming;
	private boolean isRead;
	private String message;
	private int status;
	private String timestamp;
	private String url;

	public ChatMessage(int paramInt1, String paramString1, Bitmap paramBitmap,
			String paramString2, boolean paramBoolean1, int paramInt2,
			boolean paramBoolean2) {
		this.id = paramInt1;
		this.message = paramString1;
		this.timestamp = paramString2;
		this.incoming = paramBoolean1;
		this.status = paramInt2;
		this.image = paramBitmap;
		this.isRead = paramBoolean2;
	}

	public ChatMessage(int paramInt1, String paramString1, byte[] rawImage,
			String paramString2, boolean paramBoolean1, int paramInt2,
			boolean read) {
		this.id = paramInt1;
		this.message = paramString1;
		this.timestamp = paramString2;
		this.incoming = paramBoolean1;
		this.status = paramInt2;
		this.image = rawImage != null ? BitmapFactory.decodeByteArray(rawImage,
				0, rawImage.length) : null;
		this.isRead = read;
	}

	public int getId() {
		return this.id;
	}

	public Bitmap getImage() {
		return this.image;
	}

	public String getMessage() {
		return this.message;
	}

	public LinphoneChatMessage.State getStatus() {
		return LinphoneChatMessage.State.fromInt(this.status);
	}

	public String getTimestamp() {
		return this.timestamp;
	}

	public String getUrl() {
		return this.url;
	}

	public boolean isIncoming() {
		return this.incoming;
	}

	public boolean isRead() {
		return this.isRead;
	}

	public void setIncoming(boolean paramBoolean) {
		this.incoming = paramBoolean;
	}

	public void setMessage(String paramString) {
		this.message = paramString;
	}

	public void setStatus(int paramInt) {
		this.status = paramInt;
	}

	public void setTimestamp(String paramString) {
		this.timestamp = paramString;
	}

	public void setUrl(String paramString) {
		this.url = paramString;
	}

	public String toString() {
		return this.id + " : " + this.message + " (" + this.url + ") @ "
				+ this.timestamp + ", read= " + this.isRead + ", incoming= "
				+ this.incoming + ", status = " + this.status;
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.ChatMessage JD-Core Version: 0.6.2
 */
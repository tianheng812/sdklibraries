package org.linphone;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import org.linphone.LinphonePreferences.AccountBuilder;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.tools.BooleanUtil;
import org.linphone.tools.StringUtil;


public class PreferencesMigrator {
	private LinphonePreferences mNewPrefs = LinphonePreferences.instance();
	private SharedPreferences mOldPrefs;

	public PreferencesMigrator(Context paramContext) {
		this.mOldPrefs = PreferenceManager
				.getDefaultSharedPreferences(paramContext);
	}

	private void deleteAllOldPreferences() {
		SharedPreferences.Editor localEditor = this.mOldPrefs.edit();
		localEditor.clear();
		localEditor.commit();
	}

	private void doAccountMigration(int index, boolean isDefaultAccount) {
		String key = index == 0 ? "" : String.valueOf(index);
		
		String username = getPrefString(StringUtil.getInstance().getPref_username_key() + key, null);
		String userid = getPrefString(StringUtil.getInstance().getPref_auth_userid_key() + key, null);
		String password = getPrefString(StringUtil.getInstance().getPref_passwd_key() + key, null);
		String domain = getPrefString(StringUtil.getInstance().getPref_domain_key() + key, null);
		String displayName = getPrefString(StringUtil.getInstance().getPref_display_name_key() + key, null);
		if(TextUtils.isEmpty(displayName)){
			displayName=username;
		}
		if (username != null && username.length() > 0 && password != null) {
			String proxy = getPrefString(StringUtil.getInstance().getPref_proxy_key() + key, null);
			String expire = getPrefString(StringUtil.getInstance().getPref_expire_key(), null);

			AccountBuilder builder = new AccountBuilder(LinphoneManager.getLc())
			.setUsername(username)
			.setUserId(userid)
			.setDomain(domain)
			.setPassword(password)
			.setTempDisplayName(displayName)
			.setProxy(proxy)
			.setExpires(expire);
			
			if (getPrefBoolean(StringUtil.getInstance().getPref_enable_outbound_proxy_key() + key, false)) {
				builder.setOutboundProxyEnabled(true);
			}
			if (BooleanUtil.getInstance().isEnable_push_id()) {
				String regId = mNewPrefs.getPushNotificationRegistrationID();
				String appId = StringUtil.getInstance().getPush_sender_id();
				if (regId != null && mNewPrefs.isPushNotificationEnabled()) {
					String contactInfos = "app-id=" + appId + ";pn-type=google;pn-tok=" + regId;
					builder.setContactParameters(contactInfos);
				}
			}
			
			try {
				builder.saveNewAccount();
			} catch (LinphoneCoreException e) {
				e.printStackTrace();
			}
			//if (isDefaultAccount) {
				mNewPrefs.setDefaultAccount(index);
			//}
		}
	}

	private void doAccountsMigration() {
		LinphoneCore localLinphoneCore = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		localLinphoneCore.clearAuthInfos();
		localLinphoneCore.clearProxyConfigs();
		for (int i = 0; i < mOldPrefs.getInt(StringUtil.getInstance().getPref_extra_accounts(), 1); i++) {
			doAccountMigration(i,i == getPrefInt(StringUtil.getInstance().getPref_default_account_key(), 0));
		}
	}

	private boolean getPrefBoolean(String paramString, boolean paramBoolean) {
		return this.mOldPrefs.getBoolean(paramString, paramBoolean);
	}

	private int getPrefInt(String paramInt1, int paramInt2) {
		return this.mOldPrefs.getInt(paramInt1,
				paramInt2);
	}

	private String getPrefString(String paramString1, String paramString2) {
		return this.mOldPrefs.getString(paramString1, paramString2);
	}

	public void doEchoMigration() {
		LinphoneCore localLinphoneCore = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		if (!localLinphoneCore.needsEchoCalibration()) {
			mNewPrefs.setEchoCancellation(false);
		}
	}

	public void doMigration() {
		mNewPrefs.firstLaunchSuccessful();
		mNewPrefs.removePreviousVersionAuthInfoRemoval();
		mNewPrefs.setFrontCamAsDefault(getPrefBoolean(StringUtil.getInstance().getPref_video_use_front_camera_key(), true));
		mNewPrefs.setWifiOnlyEnabled(getPrefBoolean(StringUtil.getInstance().getPref_wifi_only_key(), false));
		mNewPrefs.setPushNotificationEnabled(getPrefBoolean(StringUtil.getInstance().getPref_push_notification_key(), false));
		mNewPrefs.setPushNotificationRegistrationID(getPrefString(StringUtil.getInstance().getPush_reg_id_key(), null));
		mNewPrefs.setDebugEnabled(getPrefBoolean(StringUtil.getInstance().getPref_debug_key(), false));
		douseRandomPort();
		mNewPrefs.setBackgroundModeEnabled(getPrefBoolean(StringUtil.getInstance().getPref_background_mode_key(), true));
		mNewPrefs.setAnimationsEnabled(getPrefBoolean(StringUtil.getInstance().getPref_animation_enable_key(), false));
		mNewPrefs.setAutoStart(getPrefBoolean(StringUtil.getInstance().getPref_autostart_key(), false));
		mNewPrefs.setSharingPictureServerUrl(getPrefString(StringUtil.getInstance().getPref_image_sharing_server_key(), null));
		mNewPrefs.setRemoteProvisioningUrl(getPrefString(StringUtil.getInstance().getPref_remote_provisioning_key(), null));
		doAccountsMigration();
		deleteAllOldPreferences();
	}
	
	
	public void douseRandomPort(){
		if(mNewPrefs!=null){
			mNewPrefs.useRandomPort(true);
		}
	}

	public boolean isEchoMigratioNeeded() {
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		if (lc == null) {
			return false;
		}
		if (mNewPrefs.isEchoConfigurationUpdated()) {
			return false;
		}

		return (!lc.needsEchoCalibration() && mNewPrefs
				.isEchoCancellationEnabled());
	}

	public boolean isMigrationNeeded() {
		return mOldPrefs.getInt(StringUtil.getInstance().getPref_extra_accounts(), -1) != -1;
	}

	public void migrateRemoteProvisioningUriIfNeeded() {
		String str1 = mNewPrefs.getConfig().getString("app","remote_provisioning", null);
		String str2 = mNewPrefs.getRemoteProvisioningUrl();
		if ((str1 != null) && (str1.length() > 0) && (str2 == null)) {
			mNewPrefs.setRemoteProvisioningUrl(str1);
			mNewPrefs.getConfig().setString("app", "remote_provisioning",null);
			mNewPrefs.getConfig().sync();
		}
	}
	
	public void migrateSharingServerUrlIfNeeded() {
		String currentUrl = mNewPrefs.getConfig().getString("app", "sharing_server", null);
		if (currentUrl == null || currentUrl.equals("https://www.linphone.org:444/upload.php")) {
			mNewPrefs.setSharingPictureServerUrl("");//"https://www.linphone.org:444/lft.php");
			mNewPrefs.getConfig().sync();
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.PreferencesMigrator JD-Core Version: 0.6.2
 */
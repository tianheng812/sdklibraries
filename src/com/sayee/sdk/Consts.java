package com.sayee.sdk;

public class Consts {

	private Consts(){}
	
	public static final String SAYEE_NEIGBIOR_NAME_KEY = "sayee_neigbiorName";
	public static final String SAYEE_PATH = "sayee_path_url";
	public static final String SAYEE_TOKEN = "sayee_token";
	public static final String SAYEE_USER_NAME = "sayee_username";
	public static final String SAYEE_NEIGBOR_ID= "sayee_neigbor_id";//社区id
	public static final String SAYEE_HOUSE_ID= "sayee_house_id";//房屋id
	public static final String SAYEE_RANDOM_CODE_ACTION= "com.sayee.sdk.action.random.code";
	public static final String SAYEE_RANDOM_PASSWORD="sayee_random_password";
	public static final String SAYEE_RANDOM_PASSWORD_DEADLINE="sayee_random_password_deadline";
	public static final String SAYEE_FROM_SIP_NUMBER="sayee_from_sip_number";
	public static final String SAYEE_FROM_SIP_DOMAIN="sayee_from_sip_domain";
	public static final String SAYEE_DOMAIN_SN="sayee_domain_sn";
	public static final String SAYEE_DEAL_TIME="sayee_deal_time";
	public static final String SAYEE_IS_VIDEO="sayee_is_video";
	public static final String SAYEE_SPEAKER_ENABLED="sayee_speaker_enabled";
	public static final String SAYEE_CALL_INCOMING="sayee_call_incoming";
	public static final String SAYEE_DOOR_NAME="sayee_door_name";
	public static final String SAYEE_EARLY_MEDIA="sayee_early_media";
	public static final String SAYEE_TOKEN_FAIL_ACTION= "com.sayee.sdk.action.token.fail";//token失效
	public static final String SAYEE_GET_TOKEN_FAIL_ACTION= "com.sayee.sdk.action.get.token.fail";//获取token失败
	public static final String SAYEE_CALLBACK_CODE= "sayee_callback_code";//获取token失败错误码
	public static final String SAYEE_ERROR_MSG= "sayee_error_msg";//获取token失败错误信息
	
	public static final String SAYEE_IS_CALL_KEY="sayee_is_call_key";//是否是主叫key
	public static final int TONEK_FAIL_ERROR_CODE=3;//身份验证失败错误码
	public static final String AGAIN_GET_TOKEN_ERROR_MSG="token重新获取失败";
	public static final String SAYEE_NUMBER_KEY="sayee_number_key";//子账号添加账号key
	public static final String SAYEE_ALIAS_KEY="sayee_alias_key";//子账号别名key
	public static final String SAYEE_NICK_NAME_KEY="sayee_nick_name_key";//通讯备注名key

	
	/**
	 * token失效时，回调接口code
	 */
	public static final int NOT_BACKCALL=0x0;//不回调code
	public static final int OPEN_LOCK_CODE=0x1;//一键开门code
	public static final int PASSWORD_OPEN_LOCK_CODE=0x2;//密码开门code
	public static final int HOUSE_MESSAGE_CODE=0x3;//获取房产信息code
	public static final int CHILD_ACCOUNTS_CODE=0x4;//子账号管理code
	public static final int ADD_SUBACCOUNT_CODE=0x5;//添加子账号管理code
	public static final int EDIT_SUBACCOUNT_CODE=0x6;//编辑子账号管理code
	public static final int DELETE_SUBACCOUNT_CODE=0x7;//删除子账号管理code
	public static final int SET_CALL_ACCOUNT_CODE=0x8;//设置紧急被叫code
	public static final int DISTURBING_CODE=0x9;//免打扰code
	public static final int IS_CAN_BINDING_CODE=0x10;//是否可以绑定该小区code
}

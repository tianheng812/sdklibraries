package com.sayee.sdk.adapter;

import java.util.List;

import org.linphone.Contact;
import org.linphone.tools.StringUtil;

import com.sayee.sdk.utils.ToolsUtil;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ContactsAdapter extends BaseAdapter {
	private List<Contact> list = null;
	private Context mContext;
	 int[] colors=new int[4];

	public ContactsAdapter(Context mContext, List<Contact> list) {
		this.mContext = mContext;
		this.list = list;
		
		try {
			colors[0]=ToolsUtil.getIdByName(mContext, "drawable", "round_ffa300");
		} catch (Exception e) {
			colors[0]=Color.parseColor("#ffa300");
		}
		
		try {
			colors[1]=ToolsUtil.getIdByName(mContext, "drawable", "round_ff6159");
		} catch (Exception e) {
			colors[0]=Color.parseColor("#ff6159");
		}
		
		try {
			colors[2]=ToolsUtil.getIdByName(mContext, "drawable", "round_00d8ca");
		} catch (Exception e) {
			colors[0]=Color.parseColor("#00d8ca");
		}
		
		try {
			colors[3]=ToolsUtil.getIdByName(mContext, "drawable", "round_db8df9");
		} catch (Exception e) {
			colors[0]=Color.parseColor("#db8df9");
		}
		
	}

	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * 
	 * @param list
	 */
	public void updateListView(List<Contact> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	public int getCount() {
		return list==null?0:this.list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup arg2) {
		ViewHolder viewHolder = null;
		final Contact mContent = list.get(position);
		if (view == null) {
			viewHolder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(ToolsUtil.getIdByName(mContext, "layout","sayee_item_contact"), null);
			viewHolder.rl_head = (RelativeLayout) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "rl_head"));
			viewHolder.tv_head = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "tv_head"));
			viewHolder.tv_name = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "tv_name"));
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}
		
		if(colors!=null){
			viewHolder.rl_head.setBackgroundResource(colors[position%colors.length]);
		}
		String number=StringUtil.getNormalNumber(mContent.getNumber());
		String displayName=mContent.getName();
		if(TextUtils.isEmpty(displayName)){
			displayName=number;
		}
		viewHolder.tv_head.setText(StringUtil.getEndTwoLength(displayName));
		viewHolder.tv_name.setText(number+" ("+ mContent.getName()+")" );
		return view;

	}

	final static class ViewHolder {
		RelativeLayout rl_head;
		TextView tv_head;
		TextView tv_name;
	}

}
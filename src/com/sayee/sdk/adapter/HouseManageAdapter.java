package com.sayee.sdk.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sayee.sdk.bean.HouseBean;
import com.sayee.sdk.utils.ToolsUtil;

public class HouseManageAdapter extends BaseAdapter {
	private List<HouseBean> list = null;
	private Activity mContext;

	public HouseManageAdapter(Activity mContext, List<HouseBean> list) {
		this.mContext = mContext;
		this.list = list;
	}

	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * 
	 * @param list
	 */
	public void updateListView(List<HouseBean> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	public int getCount() {
		return list==null?0:this.list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup arg2) {
		ViewHolder viewHolder = null;
		final HouseBean mContent = list.get(position);
		if (view == null) {
			viewHolder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(ToolsUtil.getIdByName(mContext, "layout","sayee_item_house"), null);
			viewHolder.tv_address = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "tv_address"));
			viewHolder.tv_owner = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "tv_owner"));
			viewHolder.iv_disturbing = (ImageView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "iv_disturbing"));
			viewHolder.iv_select = (ImageView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "iv_select"));
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}
		viewHolder.tv_address.setText(mContent.getHouse_address());
		
		if(mContent.isIs_owner()){
			viewHolder.tv_owner.setText("业主");
		}else{
			viewHolder.tv_owner.setText("子账号");
		}
		
		if(mContent.isShow()){
			 viewHolder.iv_select.setVisibility(View.VISIBLE);
			 viewHolder.iv_disturbing.setVisibility(View.GONE);
			 if (mContent.getDisturbing()==1) {
	             viewHolder.iv_select.setImageResource(ToolsUtil.getIdByName(mContext, "drawable","round"));
	         } else {
	             viewHolder.iv_select.setImageResource(ToolsUtil.getIdByName(mContext, "drawable","round_gray"));
	         }
		}else{
			 viewHolder.iv_select.setVisibility(View.GONE);
			 if(mContent.getDisturbing()==1){
				 viewHolder.iv_disturbing.setVisibility(View.VISIBLE);
			 }else{
				 viewHolder.iv_disturbing.setVisibility(View.GONE);
			 }
		}
		
		return view;

	}

	final static class ViewHolder {
		TextView tv_address;
		TextView tv_owner;
		ImageView iv_disturbing;
		ImageView iv_select;
	}

}
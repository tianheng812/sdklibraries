package com.sayee.sdk.adapter;

import java.util.List;

import com.sayee.sdk.bean.NeiborIdBean;
import com.sayee.sdk.utils.ToolsUtil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

public class SelectedCommunityAdapter extends BaseAdapter implements SectionIndexer {
	private List<NeiborIdBean> list = null;
	private Context mContext;

	public SelectedCommunityAdapter(Context mContext, List<NeiborIdBean> list) {
		this.mContext = mContext;
		this.list = list;
	}

	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * 
	 * @param list
	 */
	public void updateListView(List<NeiborIdBean> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	public int getCount() {
		return list==null?0:this.list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup arg2) {
		ViewHolder viewHolder = null;
		final NeiborIdBean mContent = list.get(position);
		if (view == null) {
			viewHolder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(ToolsUtil.getIdByName(mContext, "layout","sayee_item_community"), null);
			viewHolder.tvTitle = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "title"));
			viewHolder.tvLetter = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "catalog"));
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		if (mContent != null) {
			viewHolder.tvLetter.setText(mContent.getFneibname());
			viewHolder.tvTitle.setText(mContent.getFaddress());
		}
		return view;

	}

	final static class ViewHolder {
		TextView tvLetter;
		TextView tvTitle;
	}

	/**
	 * 根据ListView的当前位置获取分类的首字母
	 */
	public int getSectionForPosition(int position) {
		return list.get(position).getFip().charAt(0);
	}

	/**
	 * 根据分类的首字母的获取其第一次出现该首字母的位置
	 */
	public int getPositionForSection(int section) {
		for (int i = 0; i < getCount(); i++) {
			String sortStr = list.get(i).getFip();
			char firstChar = sortStr.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}

		return -1;
	}

	public Object[] getSections() {
		return null;
	}

}
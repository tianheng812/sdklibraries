package com.sayee.sdk.adapter;

import java.util.List;

import org.linphone.Contact;
import org.linphone.tools.StringUtil;

import com.sayee.sdk.bean.ChildAccountBean;
import com.sayee.sdk.utils.ToolsUtil;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ChildAccountManageAdapter extends BaseAdapter {
	private List<ChildAccountBean> list = null;
	private Context mContext;

	public ChildAccountManageAdapter(Context mContext, List<ChildAccountBean> list) {
		this.mContext = mContext;
		this.list = list;
	}

	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * 
	 * @param list
	 */
	public void updateListView(List<ChildAccountBean> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	public int getCount() {
		return list==null?0:this.list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup arg2) {
		ViewHolder viewHolder = null;
		final ChildAccountBean mContent = list.get(position);
		if (view == null) {
			viewHolder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(ToolsUtil.getIdByName(mContext, "layout","sayee_item_child_account"), null);
			viewHolder.tv_head = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "tv_head"));
			viewHolder.tv_name = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "tv_name"));
			viewHolder.tv_is_call = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "tv_is_call"));
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}
		String alias=mContent.getAlias();
		if(TextUtils.isEmpty(alias)){
			alias=mContent.getMobile_phone();
		}
		viewHolder.tv_head.setText(StringUtil.getTwoWord(alias));
		viewHolder.tv_name.setText(alias);
		if(mContent.isIs_called_number()){
			viewHolder.tv_is_call.setVisibility(View.VISIBLE);
		}else{
			viewHolder.tv_is_call.setVisibility(View.GONE);
		}
		return view;

	}

	final static class ViewHolder {
		TextView tv_head;
		TextView tv_name;
		TextView tv_is_call;
	}

}
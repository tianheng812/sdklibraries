package com.sayee.sdk.adapter;

import java.util.List;

import com.sayee.sdk.bean.LockBean;
import com.sayee.sdk.utils.ToolsUtil;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LockListAdapter extends BaseAdapter {
	private List<LockBean> list = null;
	private Context mContext;

	public LockListAdapter(Context mContext, List<LockBean> list) {
		this.mContext = mContext;
		this.list = list;
	}

	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * 
	 * @param list
	 */
	public void updateListView(List<LockBean> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	public int getCount() {
		return list==null?0:this.list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup arg2) {
		ViewHolder viewHolder = null;
		final LockBean mContent = list.get(position);
		if (view == null) {
			viewHolder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(ToolsUtil.getIdByName(mContext, "layout","sayee_item_lock_list"), null);
			viewHolder.tvTitle = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "tv_title"));
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}
		String name=mContent.getLock_parent_name();
		if(!TextUtils.isEmpty(name)){
			viewHolder.tvTitle.setText(name);
		}else{
			viewHolder.tvTitle.setText(mContent.getLock_name());
		}
		return view;

	}

	final static class ViewHolder {
		TextView tvTitle;
	}

}
package com.sayee.sdk.adapter;

import java.util.List;

import com.sayee.sdk.bean.CommunityBean;
import com.sayee.sdk.utils.ToolsUtil;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CommunityAdapter extends BaseAdapter {
	private List<CommunityBean> list = null;
	private Context mContext;

	public CommunityAdapter(Context mContext, List<CommunityBean> list) {
		this.mContext = mContext;
		this.list = list;
	}

	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * 
	 * @param list
	 */
	public void updateListView(List<CommunityBean> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	public int getCount() {
		return list==null?0:this.list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View view, ViewGroup arg2) {
		ViewHolder viewHolder = null;
		final CommunityBean mContent = list.get(position);
		if (view == null) {
			viewHolder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(ToolsUtil.getIdByName(mContext, "layout","sayee_item_community"), null);
			viewHolder.tv_community_name = (TextView) view.findViewById(ToolsUtil.getIdByName(mContext, "id", "tv_community_name"));
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		if(mContent!=null&&!TextUtils.isEmpty(mContent.getFneib_name()))
			viewHolder.tv_community_name.setText(mContent.getFneib_name());
		else{
			viewHolder.tv_community_name.setText("百步亭花园");
		}
		return view;

	}

	final static class ViewHolder {
		TextView tv_community_name;
	}


}
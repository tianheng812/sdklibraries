package com.sayee.sdk.result;

import java.util.List;

import com.sayee.sdk.bean.ChildAccountBean;

public class ChildAccountResult extends BaseResult{

	private int code;
	private String msg;
	private List<ChildAccountBean> result;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<ChildAccountBean> getResult() {
		return result;
	}

	public void setResult(List<ChildAccountBean> result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "ChildAccountResult [result=" + result + "]";
	}

}

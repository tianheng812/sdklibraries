package com.sayee.sdk.result;

import com.sayee.sdk.bean.NeiborBean;

public class NeiborResult extends BaseResult{
	
	private NeiborBean result;

	public NeiborBean getResult() {
		return result;
	}

	public void setResult(NeiborBean result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "NeiborResult [result=" + result + "]";
	}

}

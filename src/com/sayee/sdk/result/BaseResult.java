package com.sayee.sdk.result;

import java.io.Serializable;

public class BaseResult implements Serializable {
	private static final long serialVersionUID = 6818693284083308012L;
	private int code;
	private String msg;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "BaseResult [code=" + code + ", msg=" + msg + "]";
	}
	
	
}
package com.sayee.sdk.result;


import com.sayee.sdk.bean.BidingBean;

/**
 *判断是否可以绑定社区的结果集
 */
public class BidingResult extends BaseResult{

	private BidingBean result;

	public BidingBean getResult() {
		return result;
	}

	public void setResult(BidingBean result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "CanBidingResult [result=" + result + "]";
	}

}

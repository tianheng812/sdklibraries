package com.sayee.sdk.result;

import java.util.List;

import com.sayee.sdk.bean.CommunityBean;

/**
 * 登陆后获取社区列表以及ip 结果
 *
 */
public class CommunityListResult extends BaseResult{

	private static final long serialVersionUID = 691871803938377250L;
	private int code;
	private String msg;
	private List<CommunityBean> result;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<CommunityBean> getResult() {
		return result;
	}
	public void setResult(List<CommunityBean> result) {
		this.result = result;
	}
	@Override
	public String toString() {
		return "CommunityListResult [code=" + code + ", msg=" + msg
				+ ", result=" + result + "]";
	}
	
}

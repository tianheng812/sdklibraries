package com.sayee.sdk.result;

import com.sayee.sdk.bean.OpenLockPasswordBean;

public class OpenLockPasswordResult extends BaseResult{

	private OpenLockPasswordBean result;
	private String msg;
	private int code;
	
	public OpenLockPasswordBean getResult() {
		return result;
	}
	public void setResult(OpenLockPasswordBean result) {
		this.result = result;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	
	@Override
	public String toString() {
		return "OpenLockPasswordResult [result=" + result + ", msg=" + msg
				+ ", code=" + code + "]";
	}
	
	

}

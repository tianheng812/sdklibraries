package com.sayee.sdk.result;

import com.sayee.sdk.bean.HouseInfoBean;

/**
 *房产管理结果集
 */
public class HouseResult extends BaseResult{

	private int code;
	private String msg;
	private HouseInfoBean result;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public HouseInfoBean getResult() {
		return result;
	}

	public void setResult(HouseInfoBean result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "HouseResult [result=" + result + "]";
	}


}

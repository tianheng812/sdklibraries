package com.sayee.sdk.result;

import com.sayee.sdk.bean.TokenBean;

public class TokenResult extends BaseResult{

	private int code;
	private String msg;
	private TokenBean result;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public TokenBean getResult() {
		return result;
	}
	public void setResult(TokenBean result) {
		this.result = result;
	}
	
	@Override
	public String toString() {
		return "TokenResult [code=" + code + ", msg=" + msg + ", result="
				+ result + "]";
	}
	
	
	
}

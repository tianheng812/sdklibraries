package com.sayee.sdk.result;



/**
 *判断是否可以绑定社区的结果集
 */
public class SaveUserResult extends BaseResult{

	private int code;
	private String msg;
	private String result;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "CanBidingResult [result=" + result + "]";
	}

}

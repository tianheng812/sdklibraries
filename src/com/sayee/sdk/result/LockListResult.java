package com.sayee.sdk.result;

import java.util.List;

import com.sayee.sdk.bean.LockBean;

public class LockListResult extends BaseResult{

	private List<LockBean> lockList;

	public List<LockBean> getLockList() {
		return lockList;
	}

	public void setLockList(List<LockBean> lockList) {
		this.lockList = lockList;
	}

	@Override
	public String toString() {
		return "LockListResult [lockList=" + lockList + "]";
	}
	
}

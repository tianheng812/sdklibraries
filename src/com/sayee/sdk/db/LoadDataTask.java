package com.sayee.sdk.db;


import com.sayee.sdk.result.BaseResult;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;


/**
 * TODO:网络请求异步类
 */
public class LoadDataTask extends AsyncTask<String, Integer, BaseResult> {
    private Context context;
    private DataOperate callback;
    private int clazz;

    /**
     * 构造方法
     *
     * @param callback 数据回调接口
     */
    public LoadDataTask(Context context,DataOperate callback, int clazz) {
        this.context=context;
        this.callback = callback;
        this.clazz = clazz;
    }

    @Override
    protected BaseResult doInBackground(String... params) {
        BaseResult result=null;
       
        return result;
    }

    @Override
    protected void onPostExecute(BaseResult result) {
        if (null != result&&result.getCode()==0) {
            callback.displayData(false, result);
        }
        callback.loadData(false);
    }


    public interface DataOperate<T extends BaseResult> {
        void loadData(final boolean isLoadMore);
        void displayData(boolean isLoadMore, T result);
    }
}

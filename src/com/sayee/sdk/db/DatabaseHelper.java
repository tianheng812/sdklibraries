package com.sayee.sdk.db;

import java.util.ArrayList;
import java.util.List;

import org.linphone.mediastream.Log;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import com.sayee.sdk.bean.LockBean;
import com.sayee.sdk.bean.NeiborIdBean;
import com.sayee.sdk.utils.SharedPreferencesUtil;

public class DatabaseHelper extends SQLiteOpenHelper {

    final String NEIBOR_ID_LIST_TABLE = "neiborIdList";//选中多的社区列表
    final String LOCK_LIST_TABLE="lockList";//锁列表

    public DatabaseHelper(Context context) {
        super(context, (TextUtils.isEmpty(SharedPreferencesUtil.getUserName(context)) ? "data":SharedPreferencesUtil.getUserName(context)) + ".db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + NEIBOR_ID_LIST_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + LOCK_LIST_TABLE);
        createTable(db);
    }

    private void createTable(SQLiteDatabase db) {
        db.beginTransaction();
        db.execSQL("CREATE TABLE IF NOT EXISTS " +NEIBOR_ID_LIST_TABLE
	    		+ "(fip varchar ,fport varchar,fneibname varchar,faddress varchar,department_id varchar,fremark varchar,neighborhoods_id varchar primary key)");
        db.execSQL("CREATE TABLE IF NOT EXISTS " +LOCK_LIST_TABLE
        		+ "(url varchar,lock_name varchar,domain_sn varchar,sip_number varchar primary key,lock_parent_name varchar)");
        db.setTransactionSuccessful();
        db.endTransaction();
    }
    
    /**
     * 根据sip账号，查询锁对象
     * @param url
     * @param sip_number
     * @return
     */
    public LockBean selectLock(String url,String sip_number){
    	try{
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cur = db.rawQuery("SELECT * FROM " + LOCK_LIST_TABLE + " Where url='" + url + "'  and sip_number ='"+sip_number+"'",null);
			LockBean bean=new LockBean();
			while (cur.moveToNext()) {
				bean.setLock_name(cur.getString(cur.getColumnIndex("lock_name")));
				bean.setDomain_sn(cur.getString(cur.getColumnIndex("domain_sn")));
				bean.setSip_number(cur.getString(cur.getColumnIndex("sip_number")));
				bean.setLock_parent_name(cur.getString(cur.getColumnIndex("lock_parent_name")));
			}
			cur.close();
			db.close();
			return bean;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
    }
    
    
    /**
     * 通过sipNumber查询domain_sn
     * @param sip_number
     * @return
     */
    public String getDisplayName(String sip_number){
    	if(TextUtils.isEmpty(sip_number)) return "";
    	String displayName="";
    	try{
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cur = db.rawQuery("SELECT * FROM " + LOCK_LIST_TABLE + " Where sip_number='" + sip_number + "'",null);
			while (cur.moveToNext()) {
				return cur.getString(cur.getColumnIndex("lock_parent_name"));
			}
			cur.close();
			db.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return displayName;
    }
    
    
    /**
     * 通过sipNumber查询domain_sn
     * @param sip_number
     * @return
     */
    public String getDomainSn(String sip_number){
    	if(TextUtils.isEmpty(sip_number)) return "";
    	String domain_sn="";
    	try{
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cur = db.rawQuery("SELECT * FROM " + LOCK_LIST_TABLE + " Where sip_number='" + sip_number + "'",null);
			while (cur.moveToNext()) {
				return cur.getString(cur.getColumnIndex("domain_sn"));
			}
			cur.close();
			db.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return domain_sn;
    }
    
    
    /**
     * 根据url来获取社区锁列表
     * @param url
     * @return
     */
    public List<LockBean> selectLockList(String url){
    	List<LockBean> list=new ArrayList<LockBean>();
    	try{
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cur = db.rawQuery("SELECT * FROM " + LOCK_LIST_TABLE + " Where url='" + url + "'",null);
			LockBean bean=null;
			while (cur.moveToNext()) {
				bean=new LockBean();
				bean.setLock_name(cur.getString(cur.getColumnIndex("lock_name")));
				bean.setDomain_sn(cur.getString(cur.getColumnIndex("domain_sn")));
				bean.setSip_number(cur.getString(cur.getColumnIndex("sip_number")));
				bean.setLock_parent_name(cur.getString(cur.getColumnIndex("lock_parent_name")));
				list.add(bean);
			}
			cur.close();
			db.close();
		}catch(Exception e){
			e.printStackTrace();
		}
    	return list;
    }
    
    
    /**
     * 保存锁列表 
     * @param lockList
     * @param url
     */
    public void saveLockList(List<LockBean> lockList,String url){
    	if(lockList!=null){
    		for(LockBean bean:lockList){
    			saveLock(bean, url);
    		}
    	}
    }
    
    
    /**
     * 保存锁信息
     */
    public boolean saveLock(LockBean bean,String url){
    	if(bean==null) return false;
    	boolean isExe=false;
		SQLiteDatabase db=null;
		try {
			db=this.getWritableDatabase();
			db.beginTransaction();
			db.execSQL("replace into "+ LOCK_LIST_TABLE+ " values ('"
					+url
					+"','"+bean.getLock_name()
					+"','"+bean.getDomain_sn()
					+"','"+bean.getSip_number()
					+"','"+bean.getLock_parent_name()
					+"')");
			db.setTransactionSuccessful();
			isExe=true;
		} catch (Exception ex) {
			ex.toString();        
		}finally{
			if(db!=null){
			  db.endTransaction();
			  db.close();
			}
		}
		return isExe;
    }
    
    /**
	 * 添加选中的社区信息数据
	 * @param adBean
	 */
	public boolean addNeiboridBean(NeiborIdBean bean){
		boolean isExe=false;
		SQLiteDatabase db=null;
		try {
			db=this.getWritableDatabase();
		    db.beginTransaction();
			db.execSQL("replace into "+NEIBOR_ID_LIST_TABLE+ " values ('"+bean.getFip()
					+"','"+bean.getFport()
					+"','"+bean.getFneibname()
					+"','"+bean.getFaddress()
					+"','"+bean.getDepartment_id()
					+"','"+bean.getFremark()
					+"','"+bean.getNeighborhoods_id()
					+"')");
			db.setTransactionSuccessful();
			isExe=true;
		} catch (Exception ex) {
			ex.toString();        
		}finally{
			if(db!=null){
				db.endTransaction();
				db.close();
			}
		}
		return isExe;
	}
	/**
	 * 删除选中的社区信息数据
	 * @param adBean
	 */
	public boolean removeNeiboridBean(String neighborhoods_id){
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			db.execSQL("delete from " + NEIBOR_ID_LIST_TABLE + " Where neighborhoods_id='" + neighborhoods_id + "'");
			db.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	/**
	 * 查询选中的社区信息数据
	 * @return
	 */
	public List<NeiborIdBean> selectNeiboridBeanBeanList(){
		List<NeiborIdBean> list=new ArrayList<NeiborIdBean>();
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cur = db.rawQuery("SELECT * FROM " + NEIBOR_ID_LIST_TABLE,null);
			NeiborIdBean bean=null;
			while (cur.moveToNext()) {
				bean=new NeiborIdBean();
				bean.setFip(cur.getString(cur.getColumnIndex("fip")));
				bean.setFport(cur.getString(cur.getColumnIndex("fport")));
				bean.setFneibname(cur.getString(cur.getColumnIndex("fneibname")));
				bean.setFaddress(cur.getString(cur.getColumnIndex("faddress")));
				bean.setDepartment_id(cur.getString(cur.getColumnIndex("department_id")));
				bean.setFremark(cur.getString(cur.getColumnIndex("fremark")));
				bean.setNeighborhoods_id(cur.getString(cur.getColumnIndex("neighborhoods_id")));
				list.add(bean);
			}
			cur.close();
			db.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return list;
	}

    
}

package com.sayee.sdk.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.linphone.LinphoneManager;
import org.linphone.LinphoneSimpleListener;
import org.linphone.LinphoneUtils;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sayee.sdk.Consts;
import com.sayee.sdk.HttpRespListener;
import com.sayee.sdk.adapter.CommunityAdapter;
import com.sayee.sdk.adapter.LockListAdapter;
import com.sayee.sdk.bean.BidingBean;
import com.sayee.sdk.bean.CommunityBean;
import com.sayee.sdk.bean.LockBean;
import com.sayee.sdk.bean.NeiborIdBean;
import com.sayee.sdk.bean.OpenLockPasswordBean;
import com.sayee.sdk.db.DatabaseHelper;
import com.sayee.sdk.result.BaseResult;
import com.sayee.sdk.result.BidingResult;
import com.sayee.sdk.result.CommunityListResult;
import com.sayee.sdk.result.LockListResult;
import com.sayee.sdk.result.OpenLockPasswordResult;
import com.sayee.sdk.utils.HttpUtils;
import com.sayee.sdk.utils.SharedPreferencesUtil;
import com.sayee.sdk.utils.ToolsUtil;
import com.sayee.sdk.view.AlertDialog;
import com.sayee.sdk.view.OpenLockDialog;

/**
 * 锁列表界面
 * @author Administrator
 *
 */
public class LockListActivity extends BaseActivity implements LinphoneSimpleListener.LinphoneOnCallStateChangedListener {

	public static final String NEIGBIOR_NAME_KEY = "neigbiorName";
	public static final String PATH = "path_url";
	public static final String TOKEN = "token";
	public static final String USER_NAME = "username";
	public static final String NEIGBOR_ID = "neigbor_id";
	public static final String NEIGBOR_NAME = "neigbor_name";
	public static final String DEAL_TIME = "deal_time";
	public static final String USER_ID = "user_id";
	
	private ListView ls_lock_list,ls_community_list;
	private static LockListAdapter adapter;
	private static List<LockBean> list;
	private static TextView tv_community;
	private List<CommunityBean> communitys;
	private LinearLayout ll_top_left,ll_top_center,ll_content;
	private TextView tv_top_right;
	//private TextView tv_top_left;
	private LinearLayout ll_top_right;
	private static String path;
	private static String neigbor_id;
	private String neigbor_name;
	private CommunityAdapter communityAdapter;
	private boolean isCommunityClick=false;
	private ImageView iv_arrow;
	private static String user_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		try {
			setContentView(ToolsUtil.getIdByName(getApplication(), "layout","sayee_activity_lock_list"));
			ll_content = (LinearLayout) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ll_content"));
			ll_top_left = (LinearLayout) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ll_top_left"));
			ll_top_right = (LinearLayout) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ll_top_right"));
			ll_top_center = (LinearLayout) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ll_top_center"));
			//tv_top_left = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_top_left"));
			tv_community = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_community"));
			tv_top_right = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_top_right"));
			ls_lock_list = (ListView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ls_lock_list"));
			ls_community_list = (ListView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ls_community_list"));
			iv_arrow = (ImageView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "iv_arrow"));
			ll_top_right.setVisibility(View.VISIBLE);
			tv_top_right.setText("房产管理");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		
		if (getIntent() != null) {
			Intent intent = getIntent();
			path = intent.getStringExtra(PATH);
			token = intent.getStringExtra(TOKEN);
			neigbor_id = intent.getStringExtra(NEIGBOR_ID);
			neigbor_name = intent.getStringExtra(NEIGBOR_NAME);
			token = intent.getStringExtra(TOKEN);
			userName = intent.getStringExtra(USER_NAME);
			deal_time = intent.getLongExtra(DEAL_TIME, 0);
			user_id = intent.getStringExtra(USER_ID);
			
			/*path=SharedPreferencesUtil.getDataString(this, SharedPreferencesUtil.SAYEE_LAST_SAVE_PAHT_KEY);
			if(TextUtils.isEmpty(path)){
				path = intent.getStringExtra(PATH);
				SharedPreferencesUtil.saveData(this, SharedPreferencesUtil.SAYEE_LAST_SAVE_PAHT_KEY, path);
			}
			SharedPreferencesUtil.saveData(this, SharedPreferencesUtil.SAYEE_TWO_URL_KEY, path);
			token = intent.getStringExtra(TOKEN);
			userName = intent.getStringExtra(USER_NAME);
			neigbor_id=SharedPreferencesUtil.getUserNeigborId(this);
			neigbor_name=SharedPreferencesUtil.getDataString(this,SharedPreferencesUtil.SAYEE_USER_NEIGBOR_NAME_KEY);
			if(TextUtils.isEmpty(neigbor_id)){
				neigbor_id = intent.getStringExtra(NEIGBOR_ID);
				SharedPreferencesUtil.saveData(this, SharedPreferencesUtil.SAYEE_USER_NEIGBOR_ID_KEY, neigbor_id);
			}
			if(TextUtils.isEmpty(neigbor_name)){
				neigbor_name = intent.getStringExtra(NEIGBOR_NAME);
				SharedPreferencesUtil.saveData(this, SharedPreferencesUtil.SAYEE_USER_NEIGBOR_NAME_KEY, neigbor_name);
			}*/
			
		} 
		
		tv_community.setText(neigbor_name);
		communitys=new ArrayList<CommunityBean>();
		CommunityBean communityBean=new CommunityBean();
		communityBean.setFneib_name(neigbor_name);
		communitys.add(communityBean);
		communityAdapter=new CommunityAdapter(this, communitys);
		ls_community_list.setAdapter(communityAdapter);

		if (savedInstanceState != null) {
			path = savedInstanceState.getString(PATH);
			token = savedInstanceState.getString(TOKEN);
			userName = savedInstanceState.getString(USER_NAME);
			neigbor_id = savedInstanceState.getString(NEIGBOR_ID);
			deal_time = savedInstanceState.getLong(DEAL_TIME);
		}
		
		if (TextUtils.isEmpty(token) || TextUtils.isEmpty(userName)) {
			finish();
		}

		isGetToken=true;
		getNewToken();
		
		list = new ArrayList<LockBean>();
		adapter = new LockListAdapter(this, list);
		ls_lock_list.setAdapter(adapter);

		//loadCommunityList();
		
		loadLockListData(this);

		//点击锁列表
		ls_lock_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3) {

				getNewToken();

				final LockBean lockListBean = list.get(position);
				final OpenLockDialog dialog = new OpenLockDialog(LockListActivity.this, lockListBean.getLock_name());
				final String domain_sn = lockListBean.getDomain_sn();
				final String sip_number = lockListBean.getSip_number();
				SharedPreferencesUtil.saveData(LockListActivity.this, SharedPreferencesUtil.SAYEE_TO_SIP_NUMBER_KEY, sip_number);
				dialog.setPath(path);
				dialog.setToken(token);
				dialog.setUserName(userName);
				dialog.setDomain_sn(domain_sn);
				dialog.setToSipNumber(sip_number);
				dialog.setOpenVideoListener(new View.OnClickListener() {
					@Override
					public void onClick(View arg0) {
						if (!LinphoneUtils.isHasPermission(getApplicationContext())) {
							final AlertDialog ad = new AlertDialog(LockListActivity.this);
							ad.setPositiveButton(new OnClickListener() {
								@Override
								public void onClick(View v) {
									String name = Build.MANUFACTURER;
									if ("HUAWEI".equals(name)) {
										LinphoneUtils.goHuaWeiMainager(LockListActivity.this);
									} else if ("vivo".equals(name)) {
										LinphoneUtils.goVivoMainager(LockListActivity.this);
									} else if ("OPPO".equals(name)) {
										LinphoneUtils.goOppoMainager(LockListActivity.this);
									} else if ("Coolpad".equals(name)) {
										LinphoneUtils.goCoolpadMainager(LockListActivity.this);
									} else if ("Meizu".equals(name)) {
										LinphoneUtils.goMeizuMainager(LockListActivity.this);
									} else if ("Xiaomi".equals(name)) {
										LinphoneUtils.goXiaoMiMainager(LockListActivity.this);
									} else if ("samsung".equals(name)) {
										LinphoneUtils.goSangXinMainager(LockListActivity.this);
									} else {
										LinphoneUtils.goIntentSetting(LockListActivity.this);
									}
									ad.dismiss();
								}
							});
							ad.setNegativeButton(new OnClickListener() {
								@Override
								public void onClick(View v) {
									ad.dismiss();
								}
							});
							return;
						} else {

							if (ToolsUtil.isNetworkConnected(getApplicationContext())) {
								Intent intent = new Intent(LockListActivity.this,VideoCallActivity.class);
								intent.putExtra(Consts.SAYEE_IS_CALL_KEY,true);
								intent.putExtra(Consts.SAYEE_PATH, path);
								intent.putExtra(Consts.SAYEE_TOKEN, token);
								intent.putExtra(Consts.SAYEE_DEAL_TIME,deal_time);
								intent.putExtra(Consts.SAYEE_USER_NAME,userName);
								intent.putExtra(Consts.SAYEE_FROM_SIP_NUMBER,sip_number);
								intent.putExtra(Consts.SAYEE_DOMAIN_SN,domain_sn);
								intent.putExtra(Consts.SAYEE_DOOR_NAME,lockListBean.getLock_parent_name());
								ToolsUtil.outgoingCall(LockListActivity.this,sip_number, intent);
								dialog.dismiss();
								isClick=true;
							} else {
								ToolsUtil.toast(LockListActivity.this, "请连接网络");
							}
						}
					}
				});

				dialog.setOpenDoorForpasswordListener(new View.OnClickListener() {
					@Override
					public void onClick(View arg0) {
						HttpUtils.getOpenLockPassword(path,token, userName,sip_number, new HttpRespListener() {
							@Override
							public void onSuccess(int code,BaseResult result) {
								OpenLockPasswordBean bean = ((OpenLockPasswordResult) result).getResult();
								Intent intent = new Intent();
								intent.setAction(Consts.SAYEE_RANDOM_CODE_ACTION);
								intent.putExtra(Consts.SAYEE_RANDOM_PASSWORD,bean.getRandom_pw());
								intent.putExtra(Consts.SAYEE_RANDOM_PASSWORD_DEADLINE,bean.getRandomkey_dead_time());
								sendBroadcast(intent);
								dialog.dismiss();
							}

							@Override
							public void onFail(int code, String msg) {
								ToolsUtil.toast(LockListActivity.this, msg);
								if(code!=3){
									ToolsUtil.toast(LockListActivity.this, msg);
								}else{
									Intent intent=new Intent();
									intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
									intent.putExtra(Consts.SAYEE_CALLBACK_CODE, Consts.PASSWORD_OPEN_LOCK_CODE);
									intent.putExtra(Consts.SAYEE_ERROR_MSG, Consts.AGAIN_GET_TOKEN_ERROR_MSG);
									sendBroadcast(intent);
								}
							}
						});
					}
				});

				dialog.show();

			}
		});
		
		
		ls_community_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
				/*if(communitys!=null){
					CommunityBean communityBean=communitys.get(position);
					if(communityBean!=null){
						String fip=communityBean.getFip();
						int fport=communityBean.getFport();
						String fneibName=communityBean.getFneib_name();
						if(!TextUtils.isEmpty(fip)&&!TextUtils.isEmpty(fneibName)&&fport!=0){
							fip="https://"+fip+":"+fport;
							isCanBinding(fip, fneibName,communityBean.getId());
						}
					}
				}*/
				if(ls_community_list!=null&&ls_community_list.getVisibility()==View.VISIBLE){
					ls_community_list.setVisibility(View.GONE);
					isCommunityClick=false;
					openAnim(iv_arrow,isCommunityClick);
				}
			}
		});

		ll_top_left.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
		ll_top_right.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(LockListActivity.this,HouseManageActivity.class);
				intent.putExtra(Consts.SAYEE_PATH, path);
				intent.putExtra(Consts.SAYEE_NEIGBOR_ID, neigbor_id);
				startActivity(intent);
			}
		});
		
		ll_top_center.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				isShow();
			}
		});
		
		
		ll_content.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				if(ls_community_list!=null&&ls_community_list.getVisibility()==View.VISIBLE){
					ls_community_list.setVisibility(View.GONE);
					isCommunityClick=false;
					openAnim(iv_arrow,isCommunityClick);
				}
				return true;
			}
		});
	}

	
	private void isShow(){
		if(ls_community_list!=null&&!isCommunityClick){
			if(ls_community_list.getVisibility()==View.GONE)
				ls_community_list.setVisibility(View.VISIBLE);
		}else{
			if(ls_community_list.getVisibility()==View.VISIBLE)
			ls_community_list.setVisibility(View.GONE);
		}
		isCommunityClick=!isCommunityClick;
		openAnim(iv_arrow,isCommunityClick);
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		path = savedInstanceState.getString(PATH);
		token = savedInstanceState.getString(TOKEN);
		userName = savedInstanceState.getString(USER_NAME);
		neigbor_id = savedInstanceState.getString(NEIGBOR_ID);
		deal_time = savedInstanceState.getLong(DEAL_TIME);
	}

	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putString(PATH, path);
		savedInstanceState.putString(TOKEN, token);
		savedInstanceState.putString(USER_NAME, userName);
		savedInstanceState.putString(NEIGBOR_ID, neigbor_id);
		savedInstanceState.putLong(DEAL_TIME, deal_time);
	}
	
	

	
	
	/**
	 * 判断用户是否可以绑定该小区
	 * @param inPath
	 * @param nei_name
	 * @param ip_id
	 */
	/*private void isCanBinding(final String inPath,final String nei_name,final String ip_id){
		if(TextUtils.isEmpty(userName)||TextUtils.isEmpty(token)||TextUtils.isEmpty(user_id)){
			return;
		}
		
		Map<String,String> headParams=new HashMap<String, String>();
		headParams.put("token", token);
		headParams.put("username", userName);
		
		Map<String,String> params=new HashMap<String, String>();
		params.put("username", userName);
		params.put("nei_name", nei_name);
		params.put("user_id", user_id);
		
		SharedPreferencesUtil.saveData(this, SharedPreferencesUtil.SAYEE_IN_PATH_KEY,inPath);
		
		HttpUtils.getIscanBind(this,inPath,headParams, params, new HttpRespListener() {
			@Override
			public void onSuccess(int code, BaseResult result) {
				canBindSuccess(LockListActivity.this,inPath, nei_name, ip_id, result);
			}
			
			@Override
			public void onFail(int code, String msg) {
				if(code==3){
					SharedPreferencesUtil.saveData(LockListActivity.this, SharedPreferencesUtil.SAYEE_USER_ID_KEY, user_id);
					SharedPreferencesUtil.saveData(LockListActivity.this, SharedPreferencesUtil.SAYEE_NEI_NAME_KEY, nei_name);
					
					Intent intent=new Intent();
					intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
					intent.putExtra(Consts.SAYEE_CALLBACK_CODE, Consts.IS_CAN_BINDING_CODE);
					intent.putExtra(Consts.SAYEE_ERROR_MSG, Consts.AGAIN_GET_TOKEN_ERROR_MSG);
					sendBroadcast(intent);
				}else{
				   ToolsUtil.toast(LockListActivity.this, msg);
				}
			}
		});
	}*/
	
	
	/**
	 * 绑定社区成功后的操作
	 * @param inPath
	 * @param nei_name
	 * @param ip_id
	 * @param result
	 *//*
	public  static void canBindSuccess(Context context,final String inPath,final String nei_name, final String ip_id, BaseResult result) {
		if (result instanceof BidingResult) {
			BidingResult bidingResult=(BidingResult) result;
			if(bidingResult!=null){
				BidingBean biningbean=bidingResult.getResult();
				if(biningbean!=null&&biningbean.getCan_binding()){
					if(tv_community!=null){
						tv_community.setText(nei_name);
					}
					NeiborIdBean neigborIdBean=biningbean.getNeibor_id();
					if(neigborIdBean!=null){
						String get_neigbor_id=neigborIdBean.getNeighborhoods_id();
						if(!TextUtils.isEmpty(get_neigbor_id)){
							neigbor_id=get_neigbor_id;
							path=inPath;
							loadLockListData(context);
							if(!TextUtils.isEmpty(ip_id)){
								saveUserInfo(context,ip_id);
							}
						}
					}
				}else{
					ToolsUtil.toast(context, "你在该社区没有房子，不能切换！");
				}
			}
		}
	}
	*/
	
	/**
	 * 保存用户绑定小区信息
	 * @param ip_id
	 */
	/*private static void saveUserInfo(Context context,String ip_id){
		if(TextUtils.isEmpty(userName)||TextUtils.isEmpty(token)||TextUtils.isEmpty(user_id)){
			return;
		}
		
		Map<String,String> headParams=new HashMap<String, String>();
		headParams.put("token", token);
		headParams.put("username", userName);
		
		Map<String,String> params=new HashMap<String, String>();
		params.put("user_id", user_id);
		params.put("ip_id", ip_id);
		HttpUtils.postSaveUserInfo(context, headParams, params);
	}
	*/
	
	
	/**
	 * 获取社区列表
	 */
	/*private void loadCommunityList(){
		if(TextUtils.isEmpty(userName)||TextUtils.isEmpty(token)||TextUtils.isEmpty(user_id)){
			return;
		}
		
		Map<String,String> headParams=new HashMap<String, String>();
		headParams.put("token", token);
		headParams.put("username", userName);
		
		Map<String,String> params=new HashMap<String, String>();
		params.put("user_id", user_id);
		
		HttpUtils.getNeiIpList(this,headParams, params, new HttpRespListener() {
			
			@Override
			public void onSuccess(int code, BaseResult result) {
				if (result instanceof CommunityListResult) {
					CommunityListResult communityListResult=(CommunityListResult) result;
					if(communityListResult!=null){
						List<CommunityBean> communityBeans=communityListResult.getResult();
						if(communityBeans!=null&&communityBeans.size()>0&&communitys!=null&&communityAdapter!=null){
							communitys.clear();
							communitys.addAll(communityBeans);
							communityAdapter.notifyDataSetChanged();
						}
					}
				}
			}
			
			@Override
			public void onFail(int code, String msg) {
				
			}
		});
		
	}
	*/


	/**
	 * 请求锁列表
	 */
	private static void loadLockListData(final Context context) {
		final String url = path + userName + neigbor_id;
		if (!TextUtils.isEmpty(url)) {
			List<LockBean> lockList = getDatabaseHelper(context).selectLockList(url);
			if (lockList != null && lockList.size() > 0) {
				if (list != null) {
					list.clear();
					list.addAll(lockList);
					if (adapter != null)
						adapter.notifyDataSetChanged();
				}
			}
		}

		if(TextUtils.isEmpty(userName)||TextUtils.isEmpty(token)||TextUtils.isEmpty(neigbor_id)){
			return;
		}
		HttpUtils.getLockList(path, token, userName, neigbor_id,new HttpRespListener() {
					@Override
					public void onSuccess(int code, BaseResult result) {
						LockListResult listResult = (LockListResult) result;
						if (listResult != null) {
							List<LockBean> lockList = listResult.getLockList();
							if (lockList != null) {
								if (list != null) {
									list.clear();
									list.addAll(lockList);
									if (adapter != null)
										adapter.notifyDataSetChanged();
								}
								if (!TextUtils.isEmpty(url)) {
									getDatabaseHelper(context).saveLockList(lockList,url);
								}
							}

						}
					}

					@Override
					public void onFail(int code, String msg) {
						if (list != null && list.size() == 0) {
							ToolsUtil.toast(context, msg);
						}
					}
				});
	}

	
	 /**
     * 详情箭头
     */
    private void openAnim(ImageView imageView, boolean isOpen) {
        Animation animation = null;
        if (animation == null) {
            if (isOpen) {
                animation = AnimationUtils.loadAnimation(this, ToolsUtil.getIdByName(getApplication(), "anim", "rotate"));
            } else {
                animation = AnimationUtils.loadAnimation(this, ToolsUtil.getIdByName(getApplication(), "anim", "rotate_return"));
            }
        }
        if (imageView != null && animation != null) {
            imageView.startAnimation(animation);
        }
    }
	
	private static DatabaseHelper databaseHelper = null;

	private static DatabaseHelper getDatabaseHelper(Context context) {
		if (databaseHelper == null)
			databaseHelper = new DatabaseHelper(context);
		return databaseHelper;
	}
	
	
	
	@Override
	protected void onResume() {
		super.onResume();
		LinphoneManager.addListener(this);
		
		if(tv_community!=null&&!TextUtils.isEmpty(neigbor_name)){
			tv_community.setText(neigbor_name);
		}
		if(communitys==null)
			communitys=new ArrayList<CommunityBean>();
		if(communitys.size()<=0){
			CommunityBean communityBean=new CommunityBean();
			communityBean.setFneib_name(neigbor_name);
			communitys.add(communityBean);
		}
		if(communityAdapter==null)
			communityAdapter=new CommunityAdapter(this, communitys);
		
		if(ls_community_list!=null)
			ls_community_list.setAdapter(communityAdapter);
	}
	
	
	@Override
	protected void onPause() {
		super.onPause();
		LinphoneManager.removeListener(this);
		isGetToken=false;
	}
	

	private boolean isBusy = false;
	private boolean isClick = false;
	@Override
	public void onCallStateChanged(LinphoneCall paramLinphoneCall, State state,String paramString) {
		if (state == State.Error) {
			if (ToolsUtil.isRegistration() && isClick && !isBusy) {
				ToolsUtil.toast(this, "门口机正在通话中，请稍后再试！");
			}
			isBusy = true;
			isClick = false;
		}
	}


}

package com.sayee.sdk.activity;

import java.util.HashMap;
import java.util.Map;

import org.linphone.tools.StringUtil;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sayee.sdk.Consts;
import com.sayee.sdk.HttpRespListener;
import com.sayee.sdk.result.BaseResult;
import com.sayee.sdk.utils.HttpUtils;
import com.sayee.sdk.utils.SharedPreferencesUtil;
import com.sayee.sdk.utils.ToolsUtil;
import com.sayee.sdk.view.SelectDialog;


/**
 * 添加子账号界面
 */
public class ChildAccountAddActivity extends BaseActivity {

	private String path,house_id,alias,nick_name;
	private String number;
	private String token;
	private TextView tv_top_left,tv_head,tv_name,tv_number;
	private LinearLayout ll_top_left;
	private EditText et_number,et_name,et_remark;
	private Button btn_submit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(ToolsUtil.getIdByName(getApplication(), "layout","sayee_activity_child_account_add"));
			ll_top_left = (LinearLayout) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ll_top_left"));
			tv_top_left = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_top_left"));
			tv_head = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_head"));
			tv_name = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_name"));
			tv_number = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_number"));
			et_name = (EditText) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "et_name"));
			et_remark = (EditText) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "et_remark"));
			btn_submit = (Button) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "btn_submit"));
			tv_top_left.setText("子账号管理");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (getIntent() != null) {
			Intent intent = getIntent();
			house_id = intent.getStringExtra(Consts.SAYEE_HOUSE_ID);
			number=intent.getStringExtra(Consts.SAYEE_NUMBER_KEY);
			nick_name=intent.getStringExtra(Consts.SAYEE_NICK_NAME_KEY);
		}

		String remark="";
		if(!TextUtils.isEmpty(nick_name)){
			remark="("+nick_name+")";
			if(et_remark!=null){
				et_remark.setText(nick_name);
				et_remark.setSelection(et_remark.length());
			}
		}
		tv_name.setText((number+remark));
		tv_number.setText(number);
		tv_head.setText(StringUtil.getTwoWord(number));
		
		path=SharedPreferencesUtil.getTwoUrl(this);	
		token = SharedPreferencesUtil.getToken(this);
		//token ="xxxxxxxxxxx";
		userName = SharedPreferencesUtil.getUserName(this);

		if (TextUtils.isEmpty(token) || TextUtils.isEmpty(userName)) {
			finish();
		}

		ll_top_left.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
		btn_submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if(et_remark!=null)
					alias=et_remark.getText().toString();
				
				if(TextUtils.isEmpty(alias)){
					ToolsUtil.toast(ChildAccountAddActivity.this, "请输入备注");
					return;
				}
				
				final SelectDialog dialog=new SelectDialog(ChildAccountAddActivity.this);
				dialog.setMessage("确定添加子账号？");
				dialog.setNegativeButton(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						dialog.dismiss();
					}
				});
				dialog.setPositiveButton(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						submitData();
						dialog.dismiss();
					}
				});
			}
		});
		
		
	}

	private void submitData() {
		if(!TextUtils.isEmpty(number)){
			number=number.replaceAll(" ", "");
			number=number.replace("+86", "");
				
			if(!ToolsUtil.checkPhone(number)){
				ToolsUtil.toast(this, "手机号无效");
				return;
			}
			
			if(number.equals(userName)){
				ToolsUtil.toast(this, "该号码已是该房产的业主账号，不能添加为子账号！");
				return;
			}
		
			final Map<String, String> headParams = new HashMap<String, String>();
			headParams.put("token", token);
			headParams.put("username", userName);
			final Map<String, String> params = new HashMap<String, String>();
			params.put("username", userName);
			params.put("house_id", house_id);
			params.put("sub_account", number);
			params.put("is_add", "true");
			params.put("alias", alias);
			
			SharedPreferencesUtil.saveData(this, SharedPreferencesUtil.SAYEE_HOUSE_ID_KEY, house_id);
			SharedPreferencesUtil.saveData(this, SharedPreferencesUtil.SAYEE_ADD_ACCOUNT_NUMBER_KEY, number);
			SharedPreferencesUtil.saveData(this, SharedPreferencesUtil.SAYEE_ADD_ACCOUNT_ALIAS_KEY, alias);
			
			HttpUtils.setSubAccount(this, path, headParams, params,new HttpRespListener() {
				@Override
				public void onSuccess(int code, BaseResult result) {
					ToolsUtil.toast(ChildAccountAddActivity.this, "添加成功");
					Intent intent=new Intent();
					intent.putExtra("isFresh", true);
					setResult(RESULT_OK,intent);
					finish();
				}
	
				@Override
				public void onFail(int code, String msg) {
					if(code==3){
						Intent intent=new Intent();
						intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
						intent.putExtra(Consts.SAYEE_CALLBACK_CODE, Consts.ADD_SUBACCOUNT_CODE);
						intent.putExtra(Consts.SAYEE_ERROR_MSG, Consts.AGAIN_GET_TOKEN_ERROR_MSG);
						sendBroadcast(intent);
					}else{
						ToolsUtil.toast(ChildAccountAddActivity.this, msg);
					}
				}
			});
		}
	}

}

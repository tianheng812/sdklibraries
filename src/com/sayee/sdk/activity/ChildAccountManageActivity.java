package com.sayee.sdk.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sayee.sdk.Consts;
import com.sayee.sdk.HttpRespListener;
import com.sayee.sdk.adapter.ChildAccountManageAdapter;
import com.sayee.sdk.bean.ChildAccountBean;
import com.sayee.sdk.result.BaseResult;
import com.sayee.sdk.result.ChildAccountResult;
import com.sayee.sdk.utils.HttpUtils;
import com.sayee.sdk.utils.SharedPreferencesUtil;
import com.sayee.sdk.utils.ToolsUtil;

/**
 *子账号管理界面 
 *
 */
public class ChildAccountManageActivity extends BaseActivity {

	public static final int REQUEST_CODE=0X101;
	private String path,house_id;
	private String token;
	private TextView tv_top_left,tv_top_right;
	private LinearLayout ll_top_right;
	private LinearLayout ll_top_left;
	private ListView ls_accounts;
	private static List<ChildAccountBean> accounts;
	private static ChildAccountManageAdapter adapter;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(ToolsUtil.getIdByName(getApplication(), "layout","sayee_activity_child_account_manage"));
			ll_top_left = (LinearLayout) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ll_top_left"));
			tv_top_left = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_top_left"));
			ll_top_right = (LinearLayout) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ll_top_right"));
			tv_top_right = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_top_right"));
			ls_accounts = (ListView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ls_accounts"));
			tv_top_left.setText("子账号管理");
			ll_top_right.setVisibility(View.VISIBLE);
			tv_top_right.setText("添加");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (getIntent() != null) {
			Intent intent = getIntent();
			house_id = intent.getStringExtra(Consts.SAYEE_HOUSE_ID);
		}

		path=SharedPreferencesUtil.getTwoUrl(this);	
		token = SharedPreferencesUtil.getToken(this);
		userName = SharedPreferencesUtil.getUserName(this);

		if (TextUtils.isEmpty(token) || TextUtils.isEmpty(userName)) {
			finish();
		}

		accounts = new ArrayList<ChildAccountBean>();
		adapter=new ChildAccountManageAdapter(this, accounts);
		ls_accounts.setAdapter(adapter);
		loadChildAccounts();

		ll_top_left.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
		ll_top_right.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(!TextUtils.isEmpty(house_id)){
					Intent intent=new Intent(ChildAccountManageActivity.this,ContactsActivity.class);
					intent.putExtra(Consts.SAYEE_HOUSE_ID, house_id);
					startActivityForResult(intent, REQUEST_CODE);
				}
			}
		});
		
		ls_accounts.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
				if(!TextUtils.isEmpty(house_id)){
					ChildAccountBean bean=accounts.get(position);
					Intent intent=new Intent(ChildAccountManageActivity.this,ChildAccountEditActivity.class);
					intent.putExtra(Consts.SAYEE_HOUSE_ID, house_id);
					intent.putExtra(Consts.SAYEE_NUMBER_KEY, bean.getMobile_phone());
					intent.putExtra(Consts.SAYEE_ALIAS_KEY, bean.getAlias());
					intent.putExtra(Consts.SAYEE_IS_CALL_KEY, bean.isIs_called_number());
					startActivityForResult(intent, REQUEST_CODE);
				}
			}
		});
	}
	
	
	/**
	 * 更新ListView
	 * 
	 * @param result
	 */
	public static void updateAdapter(BaseResult result) {
		if (result instanceof ChildAccountResult) {
			ChildAccountResult result2=(ChildAccountResult)result;
			if(accounts==null) accounts=new ArrayList<ChildAccountBean>();
			if(result2!=null&&accounts!=null&&adapter!=null&&result2.getResult()!=null){
			   accounts.clear();
			   accounts.addAll(result2.getResult());
			   adapter.updateListView(accounts);
			}
		}
	}
	
	/**
	 * 获取子账号信息
	 */
	private void loadChildAccounts() {
		final Map<String, String> headParams = new HashMap<String, String>();
		headParams.put("token", token);
		headParams.put("username", userName);
		final Map<String, String> params = new HashMap<String, String>();
		params.put("username", userName);
		params.put("house_id", house_id);
		HttpUtils.getSubAccount(this, path, headParams, params,new HttpRespListener() {
			@Override
			public void onSuccess(int code, BaseResult result) {
				updateAdapter(result);
			}

			@Override
			public void onFail(int code, String msg) {
				if(code==3){
					Intent intent=new Intent();
					intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
					intent.putExtra(Consts.SAYEE_CALLBACK_CODE, Consts.CHILD_ACCOUNTS_CODE);
					intent.putExtra(Consts.SAYEE_ERROR_MSG, Consts.AGAIN_GET_TOKEN_ERROR_MSG);
					sendBroadcast(intent);
				}else{
					ToolsUtil.toast(ChildAccountManageActivity.this, msg);
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if(requestCode==REQUEST_CODE && resultCode==RESULT_OK){
			loadChildAccounts();
		}
		
	}
	


}

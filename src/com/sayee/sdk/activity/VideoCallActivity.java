package com.sayee.sdk.activity;

import org.linphone.CallManager;
import org.linphone.LinphoneManager;
import org.linphone.LinphoneSimpleListener;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCore;
import org.linphone.mediastream.Log;
import org.linphone.mediastream.video.AndroidVideoWindowImpl;
import org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfiguration;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sayee.sdk.db.DatabaseHelper;
import com.sayee.sdk.result.BaseResult;
import com.sayee.sdk.Consts;
import com.sayee.sdk.HttpRespListener;
import com.sayee.sdk.utils.HttpUtils;
import com.sayee.sdk.utils.SharedPreferencesUtil;
import com.sayee.sdk.utils.ToolsUtil;
import com.sayee.sdk.view.SlidingLinearLayout;

/**
 * 监控界面
 * @author Administrator
 *
 */
public class VideoCallActivity extends BaseActivity implements LinphoneSimpleListener.LinphoneOnCallStateChangedListener {
	private AndroidVideoWindowImpl androidVideoWindowImpl;
	private SurfaceView mCaptureView;
	private SurfaceView mVideoView;

	SlidingLinearLayout btn_open;
	TextView tv_top_left, tv_top_right, tv_address;
	LinearLayout ll_top_left, ll_top_right;
	boolean isVideo = true;
	boolean isInComing = false;
	Chronometer tv_time;
	String path, token, userName, fromSipName, toSipDomain;
	long deal_time;
	String doorName;
	boolean isCall=false;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		try {
			setContentView(ToolsUtil.getIdByName(getApplication(), "layout",
					"sayee_activity_video_call"));
			mVideoView = ((SurfaceView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "videoSurface")));
			mCaptureView = ((SurfaceView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "videoCaptureSurface")));
			btn_open = (SlidingLinearLayout) findViewById(ToolsUtil
					.getIdByName(getApplication(), "id", "btn_open"));
			ll_top_left = (LinearLayout) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "ll_top_left"));
			ll_top_right = (LinearLayout) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "ll_top_right"));
			tv_top_left = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_top_left"));
			tv_top_right = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_top_right"));
			tv_time = (Chronometer) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_time"));
			tv_address = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_address"));
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		ll_top_right.setVisibility(View.VISIBLE);
		tv_top_left.setText("门口监控");
		tv_top_right.setText("听筒");

		if (getIntent() != null) {
			initData();
			this.isSpeakerEnabled = true;
			if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null) {
				LinphoneManager.getInstance().routeAudioToSpeaker();
				LinphoneManager.getLc().enableSpeaker(this.isSpeakerEnabled);
			}
		}else{
			if (savedInstanceState != null) {
				isInComing = savedInstanceState.getBoolean(Consts.SAYEE_CALL_INCOMING, false);
				isVideo = savedInstanceState.getBoolean(Consts.SAYEE_IS_VIDEO);
				domain_sn = savedInstanceState.getString(Consts.SAYEE_DOMAIN_SN);
				toSipNumber = savedInstanceState.getString(Consts.SAYEE_FROM_SIP_NUMBER);
				toSipDomain = savedInstanceState.getString(Consts.SAYEE_FROM_SIP_DOMAIN);
				isSpeakerEnabled = savedInstanceState.getBoolean(Consts.SAYEE_SPEAKER_ENABLED);
				path = savedInstanceState.getString(Consts.SAYEE_PATH);
				token = savedInstanceState.getString(Consts.SAYEE_TOKEN);
				userName = savedInstanceState.getString(Consts.SAYEE_USER_NAME);
				deal_time = savedInstanceState.getLong(Consts.SAYEE_DEAL_TIME, 0);
				doorName = savedInstanceState.getString(Consts.SAYEE_DOOR_NAME);
				hasOutgoingEarlyMedia=savedInstanceState.getBoolean(Consts.SAYEE_EARLY_MEDIA, false);
				isCall = savedInstanceState.getBoolean(Consts.SAYEE_IS_CALL_KEY,false);
	
				if (isSpeakerEnabled) {
					tv_top_right.setText("听筒");
					LinphoneManager.getInstance().routeAudioToSpeaker();
				} else {
					tv_top_right.setText("免提");
					LinphoneManager.getInstance().routeAudioToReceiver();
				}
				
				if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null) {
					LinphoneManager.getLc().enableSpeaker(this.isSpeakerEnabled);
				}
			} else {
				this.isSpeakerEnabled = true;
				if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null) {
					LinphoneManager.getInstance().routeAudioToSpeaker();
					LinphoneManager.getLc().enableSpeaker(this.isSpeakerEnabled);
				}
			}
		}

		tv_address.setText(doorName);

		if (!isVideo) {
			mVideoView.setVisibility(View.GONE);
			mCaptureView.setVisibility(View.GONE);
			tv_time.setTextColor(Color.BLACK);
		}

		mCaptureView.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		fixZOrder(mVideoView, mCaptureView);

		androidVideoWindowImpl = new AndroidVideoWindowImpl(mVideoView,mCaptureView, new AndroidVideoWindowImpl.VideoWindowListener() {
					public void onVideoPreviewSurfaceDestroyed(AndroidVideoWindowImpl paramAnonymousAndroidVideoWindowImpl) {
						LinphoneCore localLinphoneCore = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
						if (localLinphoneCore != null)
							localLinphoneCore.setVideoWindow(null);
					}

					public void onVideoPreviewSurfaceReady(AndroidVideoWindowImpl paramAnonymousAndroidVideoWindowImpl,
							SurfaceView paramAnonymousSurfaceView) {
						mCaptureView = paramAnonymousSurfaceView;
						if(LinphoneManager.getLcIfManagerNotDestroyedOrNull()!=null)
							LinphoneManager.getLc().setPreviewWindow(mCaptureView);
					}

					public void onVideoRenderingSurfaceDestroyed(AndroidVideoWindowImpl paramAnonymousAndroidVideoWindowImpl) {
						LinphoneCore localLinphoneCore = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
						if (localLinphoneCore != null)
							localLinphoneCore.setVideoWindow(null);
					}

					public void onVideoRenderingSurfaceReady(AndroidVideoWindowImpl vw,SurfaceView paramAnonymousSurfaceView) {
						LinphoneCore localLinphoneCore =LinphoneManager.getLcIfManagerNotDestroyedOrNull();
						if(localLinphoneCore!=null)
							localLinphoneCore.setVideoWindow(vw);
						mVideoView = paramAnonymousSurfaceView;
					}
				});

		findViewById(ToolsUtil.getIdByName(getApplication(), "id", "btn_hangup"))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						hangUp();
					}
				});

		tv_top_right.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				toggleSpeaker();
			}
		});

		ll_top_left.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				try {
					long secondTime = System.currentTimeMillis();
					if (secondTime - firstTime > 2000) { // 如果两次按键时间间隔大于2秒，则不退出
						Toast.makeText(VideoCallActivity.this, "再按一次将挂断通话", Toast.LENGTH_SHORT).show();
						firstTime = secondTime;// 更新firstTime
					} else { // 两次按键小于2秒时，退出应用
						hangUp();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	private void initData() {
		isInComing = getIntent().getBooleanExtra(Consts.SAYEE_CALL_INCOMING, false);
		doorName = getIntent().getStringExtra(Consts.SAYEE_DOOR_NAME);
		isVideo = getIntent().getBooleanExtra(Consts.SAYEE_IS_VIDEO, true);
		domain_sn = getIntent().getStringExtra(Consts.SAYEE_DOMAIN_SN);
		deal_time = getIntent().getLongExtra(Consts.SAYEE_DEAL_TIME, 0);
		toSipNumber = getIntent().getStringExtra(Consts.SAYEE_FROM_SIP_NUMBER);
		toSipDomain = getIntent().getStringExtra(Consts.SAYEE_FROM_SIP_DOMAIN);
		token = getIntent().getStringExtra(Consts.SAYEE_TOKEN);
		userName = getIntent().getStringExtra(Consts.SAYEE_USER_NAME);
		path = getIntent().getStringExtra(Consts.SAYEE_PATH);
		isCall = getIntent().getBooleanExtra(Consts.SAYEE_IS_CALL_KEY,false);
		SharedPreferencesUtil.saveData(getApplicationContext(), SharedPreferencesUtil.SAYEE_SIP_IS_CALL_KEY,isCall);
	}
	
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		if(getIntent()!=null)
			initData();
	}
	

	private void fixZOrder(SurfaceView paramSurfaceView1,SurfaceView paramSurfaceView2) {
		paramSurfaceView1.setZOrderOnTop(false);
		paramSurfaceView2.setZOrderOnTop(true);
		paramSurfaceView2.setZOrderMediaOverlay(true);
	}

	@SuppressLint("Wakelock")
	public void onDestroy() {
		this.mCaptureView = null;
		if (this.mVideoView != null) {
			this.mVideoView.setOnTouchListener(null);
			this.mVideoView = null;
		}
		if (this.androidVideoWindowImpl != null) {
			this.androidVideoWindowImpl.release();
			this.androidVideoWindowImpl = null;
		}

		if (mWakeLock != null && mWakeLock.isHeld()) {
			mWakeLock.release();
		}
		LinphoneManager.removeListener(this);
		SharedPreferencesUtil.saveData(getApplicationContext(), SharedPreferencesUtil.SAYEE_SIP_IS_CALL_KEY,true);
		super.onDestroy();
		System.gc();
	}

	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (btn_open != null) {
			if (btn_open.handleActivityEvent(event)) {
				if (!TextUtils.isEmpty(toSipNumber)) {
					if(TextUtils.isEmpty(userName)) userName=SharedPreferencesUtil.getUserName(this);
					//SharedPreferencesUtil.saveData(VideoCallActivity.this, SharedPreferencesUtil.SAYEE_USER_NAME_KEY, userName);
					SharedPreferencesUtil.saveData(VideoCallActivity.this, SharedPreferencesUtil.SAYEE_DOMAIN_SN_KEY, domain_sn);
					SharedPreferencesUtil.saveData(VideoCallActivity.this, SharedPreferencesUtil.SAYEE_TYPE_KEY, 1);
					
					HttpUtils.openDoorLock(VideoCallActivity.this, path, token,userName, domain_sn, 1, toSipNumber, toSipDomain,new HttpRespListener() {
						@Override
						public void onSuccess(int code,BaseResult result) {
							ToolsUtil.toast(VideoCallActivity.this,"已发送开锁请求");
						}

						@Override
						public void onFail(int code, String msg) {
							if(code!=3){
								ToolsUtil.toast(VideoCallActivity.this, msg);
							}else{
								Intent intent=new Intent();
								intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
								intent.putExtra(Consts.SAYEE_CALLBACK_CODE, Consts.OPEN_LOCK_CODE);
								intent.putExtra(Consts.SAYEE_ERROR_MSG, Consts.AGAIN_GET_TOKEN_ERROR_MSG);
								sendBroadcast(intent);
							}
						}
					});
				}

			}
		}
		return super.onTouchEvent(event);
	}

	String toSipNumber;
	String domain_sn;

	public void onPause() {
		if (androidVideoWindowImpl != null)
			synchronized (androidVideoWindowImpl) {
				if(LinphoneManager.isInstanciated()&&LinphoneManager.getLcIfManagerNotDestroyedOrNull()!=null)
					LinphoneManager.getLcIfManagerNotDestroyedOrNull().setVideoWindow(null);
			}

		if (this.mVideoView != null)
			((GLSurfaceView) this.mVideoView).onPause();
		

		if (mWakeLock != null && mWakeLock.isHeld()) {
			mWakeLock.release();
		}
		super.onPause();
	}

	public void onResume() {
		super.onResume();
		// 切到别的应用后，切换来时，如果挂断，则销毁界面
		if (LinphoneManager.isInstanciated()&&LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null && LinphoneManager.getLcIfManagerNotDestroyedOrNull().getCallsNb() == 0) {
			finish();
			return;
		}
		
		LinphoneManager.removeListener(this);
		LinphoneManager.addListener(this);
		if (mVideoView != null)
			((GLSurfaceView) mVideoView).onResume();

		if (androidVideoWindowImpl != null) {
			synchronized (androidVideoWindowImpl) {
				if(LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null)
					LinphoneManager.getLc().setVideoWindow(androidVideoWindowImpl);
			}
		}

		refreshTime();
		
		if(TextUtils.isEmpty(path)){
			path = (String) SharedPreferencesUtil.getData(this,Consts.SAYEE_PATH, "");
		}

		if (TextUtils.isEmpty(domain_sn))
			domain_sn = getDatabaseHelper().getDomainSn(toSipNumber);
	}

	private void refreshTime() {
		if (tv_time != null && LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null) {
			for (LinphoneCall call : LinphoneManager.getLc().getCalls()) {
				if (call != null) {
					int i = call.getDuration();
					if ((i == 0)&& (call.getState() != LinphoneCall.State.StreamsRunning)){
						tv_time.stop();
						return;
					}
					tv_time.setBase(SystemClock.elapsedRealtime() - i * 1000);
					tv_time.start();
				}
			}
		}
	}


	public void switchCamera() {
		try {
			if(LinphoneManager.getLcIfManagerNotDestroyedOrNull()==null) return;
			int videoDeviceId = LinphoneManager.getLc().getVideoDevice();
			videoDeviceId = (videoDeviceId + 1) % AndroidCameraConfiguration.retrieveCameras().length;
			LinphoneManager.getLc().setVideoDevice(videoDeviceId);
			CallManager.getInstance().updateCall();
			if (mCaptureView != null) {
				LinphoneManager.getLc().setPreviewWindow(mCaptureView);
			}
		} catch (ArithmeticException ae) {
			ae.printStackTrace();
			Log.e("Cannot swtich camera : no camera");
		}
	}

	boolean isSpeakerEnabled = false;
	private void toggleSpeaker() {
		if (!this.isSpeakerEnabled) {
			if(LinphoneManager.isInstanciated())
				LinphoneManager.getInstance().routeAudioToSpeaker();
			this.isSpeakerEnabled = true;
			tv_top_right.setText("听筒");
		} else {
			if(LinphoneManager.isInstanciated())
				LinphoneManager.getInstance().routeAudioToReceiver();
			tv_top_right.setText("免提");
			this.isSpeakerEnabled = false;
		}

		if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null) {
			LinphoneManager.getLc().enableSpeaker(this.isSpeakerEnabled);
		}
	}

	private void hangUp() {
		isMyHandup=true;
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		if (lc != null) {
			LinphoneCall currentCall = lc.getCurrentCall();
			if (currentCall != null) {
				lc.terminateCall(currentCall);
			} else if (lc.isInConference()) {
				lc.terminateConference();
			} else {
				lc.terminateAllCalls();
			}
		}
		finish();
	}

	private boolean hasOutgoingEarlyMedia = false;
	private boolean isMyHandup=false;
	private boolean isBusy=false;

	@Override
	public void onCallStateChanged(LinphoneCall call, State state,String paramString) {
		if (state == State.OutgoingEarlyMedia) {
			hasOutgoingEarlyMedia = true;
		}else if(state == State.Error){
			if(ToolsUtil.isRegistration()&&!isInComing&&!isBusy){
				ToolsUtil.toast(VideoCallActivity.this, "门口机正在通话中，请稍后再试！");
			}
			isBusy=true;
			hasOutgoingEarlyMedia = true;
		}else if (state == State.CallEnd) {
			if (!hasOutgoingEarlyMedia&&!isInComing&&!isMyHandup&&call!=null&&call.getDuration() * 1000<3000) {
				ToolsUtil.toast(VideoCallActivity.this, "门口机在不线，请稍后再试！");
			}
			hasOutgoingEarlyMedia = false;
			//Log.i(" ---  finish --");
			/*	finish();
				return;*/
		}else if(state==State.StreamsRunning){
			if (LinphoneManager.getLcIfManagerNotDestroyedOrNull()!=null){
				LinphoneManager.getLc().getCurrentCall().zoomVideo(1.2f, 0, 0);
			}
		}

		if (LinphoneManager.getLcIfManagerNotDestroyedOrNull()==null||LinphoneManager.getLc().getCallsNb() == 0) {
			finish();
			if(LinphoneManager.getLcIfManagerNotDestroyedOrNull()!=null)
				LinphoneManager.getLcIfManagerNotDestroyedOrNull().terminateAllCalls();
			return;
		}
		refreshTime();
	}

	private PowerManager.WakeLock mWakeLock;
	private PowerManager powerManager;

	@SuppressWarnings("deprecation")
	@Override
	protected void onStart() {
		super.onStart();

		if (powerManager == null)
			powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		if (powerManager != null && mWakeLock == null) {
			mWakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK
					| PowerManager.ACQUIRE_CAUSES_WAKEUP,
					VideoCallActivity.class.getSimpleName());
			if (mWakeLock != null) {
				mWakeLock.acquire();
			}
		}
	}

	private DatabaseHelper databaseHelper = null;

	private DatabaseHelper getDatabaseHelper() {
		if (databaseHelper == null)
			databaseHelper = new DatabaseHelper(getApplicationContext());
		return databaseHelper;
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		isVideo = savedInstanceState.getBoolean(Consts.SAYEE_IS_VIDEO);
		domain_sn = savedInstanceState.getString(Consts.SAYEE_DOMAIN_SN);
		deal_time = savedInstanceState.getLong(Consts.SAYEE_DEAL_TIME, 0);
		toSipNumber = savedInstanceState.getString(Consts.SAYEE_FROM_SIP_NUMBER);
		toSipDomain = savedInstanceState.getString(Consts.SAYEE_FROM_SIP_DOMAIN);
		isSpeakerEnabled = savedInstanceState.getBoolean(Consts.SAYEE_SPEAKER_ENABLED);
		path = savedInstanceState.getString(Consts.SAYEE_PATH);
		token = savedInstanceState.getString(Consts.SAYEE_TOKEN);
		userName = savedInstanceState.getString(Consts.SAYEE_USER_NAME);
		isInComing = savedInstanceState.getBoolean(Consts.SAYEE_CALL_INCOMING,false);
		doorName = savedInstanceState.getString(Consts.SAYEE_DOOR_NAME);
		hasOutgoingEarlyMedia = savedInstanceState.getBoolean(Consts.SAYEE_EARLY_MEDIA, false);
		isCall = savedInstanceState.getBoolean(Consts.SAYEE_IS_CALL_KEY,false);
	}

	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putBoolean(Consts.SAYEE_IS_VIDEO, isVideo);
		savedInstanceState.putString(Consts.SAYEE_DOMAIN_SN, domain_sn);
		savedInstanceState.putLong(Consts.SAYEE_DEAL_TIME, deal_time);
		savedInstanceState.putString(Consts.SAYEE_FROM_SIP_NUMBER, toSipNumber);
		savedInstanceState.putString(Consts.SAYEE_FROM_SIP_DOMAIN, toSipDomain);
		savedInstanceState.putBoolean(Consts.SAYEE_SPEAKER_ENABLED,isSpeakerEnabled);
		savedInstanceState.putString(Consts.SAYEE_PATH, path);
		savedInstanceState.putString(Consts.SAYEE_TOKEN, token);
		savedInstanceState.putString(Consts.SAYEE_USER_NAME, userName);
		savedInstanceState.putBoolean(Consts.SAYEE_CALL_INCOMING, isInComing);
		savedInstanceState.putString(Consts.SAYEE_DOOR_NAME, doorName);
		savedInstanceState.putBoolean(Consts.SAYEE_EARLY_MEDIA,hasOutgoingEarlyMedia);
		savedInstanceState.putBoolean(Consts.SAYEE_IS_CALL_KEY,isCall);

	}

	long firstTime = 0;
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			try {
				long secondTime = System.currentTimeMillis();
				if (secondTime - firstTime > 2000) { // 如果两次按键时间间隔大于2秒，则不退出
					Toast.makeText(this, "再按一次将挂断通话", Toast.LENGTH_SHORT).show();
					firstTime = secondTime;// 更新firstTime
					return true;
				} else { // 两次按键小于2秒时，退出应用
					hangUp();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
		return super.onKeyUp(keyCode, event);
	}
}

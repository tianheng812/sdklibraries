package com.sayee.sdk.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sayee.sdk.Consts;
import com.sayee.sdk.HttpRespListener;
import com.sayee.sdk.adapter.HouseManageAdapter;
import com.sayee.sdk.bean.HouseBean;
import com.sayee.sdk.bean.HouseInfoBean;
import com.sayee.sdk.result.BaseResult;
import com.sayee.sdk.result.HouseResult;
import com.sayee.sdk.utils.HttpUtils;
import com.sayee.sdk.utils.SharedPreferencesUtil;
import com.sayee.sdk.utils.ToolsUtil;

/**
 * 房产管理界面
 * 
 */
public class HouseManageActivity extends BaseActivity {

	private String path, neigbor_id;
	private String token;
	private LinearLayout ll_top_left, ll_top_right;
	private static ListView ls_houses;
	private static List<HouseBean> houses;
	private static HouseManageAdapter adapter;
	private TextView tv_top_left, tv_top_right;
	private boolean isTopRightShow = true;// 免打扰的文字是否显示

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(ToolsUtil.getIdByName(getApplication(), "layout",
					"sayee_activity_house_manage"));
			ll_top_left = (LinearLayout) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "ll_top_left"));
			tv_top_left = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_top_left"));
			ll_top_right = (LinearLayout) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "ll_top_right"));
			tv_top_right = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_top_right"));
			ls_houses = (ListView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "ls_houses"));
			tv_top_left.setText("房产管理");
			ll_top_right.setVisibility(View.VISIBLE);
			tv_top_right.setText("免打扰");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (getIntent() != null) {
			Intent intent = getIntent();
			path = intent.getStringExtra(Consts.SAYEE_PATH);
			neigbor_id = intent.getStringExtra(Consts.SAYEE_NEIGBOR_ID);
		}

		token = SharedPreferencesUtil.getToken(this);
		//token = "zzzzzzzzzzz";
		userName = SharedPreferencesUtil.getUserName(this);

		if (TextUtils.isEmpty(token) || TextUtils.isEmpty(userName)) {
			finish();
		}

		houses = new ArrayList<HouseBean>();
		adapter = new HouseManageAdapter(this, houses);
		ls_houses.setAdapter(adapter);
		loadHouseMessage();

		ll_top_left.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!isTopRightShow) {// 如果点击了免打扰,回到免打扰前的转态
					tv_top_right.setText("免打扰");
					if (houses != null && houses.size() > 0) {
						for (HouseBean bean : houses) {
							bean.setIsShow(false);
						}

						if (adapter != null) {
							adapter.notifyDataSetChanged();
						}
					}
					isTopRightShow = true;
				} else {
					finish();
				}
			}
		});

		ll_top_right.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isTopRightShow) {// 显示免打扰编辑转态
					tv_top_right.setText("");
					if (houses != null && houses.size() > 0) {
						for (HouseBean bean : houses) {
							bean.setIsShow(true);
						}

						if (adapter != null) {
							adapter.notifyDataSetChanged();
						}
					}
					isTopRightShow = false;
				}
			}
		});

		ls_houses.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				if (houses != null) {
					HouseBean mContent = houses.get(position);
					if (mContent != null) {
						if (mContent.isShow()) {
							if (mContent.getDisturbing() == 1) {
								updateDisturbing(position,mContent.getHouse_id(), 0);
							} else {
								updateDisturbing(position,mContent.getHouse_id(), 1);
							}
						} else {
							if (mContent.isIs_owner()) {
								Intent intent = new Intent(
										HouseManageActivity.this,
										ChildAccountManageActivity.class);
								intent.putExtra(Consts.SAYEE_HOUSE_ID,
										mContent.getHouse_id());
								startActivity(intent);
							} else {
								ToolsUtil.toast(HouseManageActivity.this,
										"非业主没有操作权限");
							}
						}
					}
				}
			}
		});

	}

	/**
	 * 更新状态
	 * @param context
	 * @param position
	 */
	public static void updateDisturbing(Context context,final int position) {
		if(position==-1) return;
		if (houses != null) {
			HouseBean mContent = houses.get(position);
			if (mContent != null) {
				if (mContent != null) {
					if (mContent.getDisturbing() == 0) {
						ToolsUtil.toast(context, "设置免打扰成功");
						mContent.setDisturbing(1);
					} else {
						ToolsUtil.toast(context, "取消免打扰成功");
						mContent.setDisturbing(0);
					}

					if (adapter != null) {
						adapter.notifyDataSetChanged();
					}
				}
			}
		}
	}

	/**
	 * 更新免打扰模式
	 */
	private void updateDisturbing(final int position, String house_id, int is_disturbing) {
		final Map<String, String> headParams = new HashMap<String, String>();
		headParams.put("token", token);
		headParams.put("username", userName);
		final Map<String, String> params = new HashMap<String, String>();
		params.put("house_id", house_id);
		params.put("is_disturbing", is_disturbing + "");
		
		SharedPreferencesUtil.saveData(this, SharedPreferencesUtil.SAYEE_IS_DISTURBING_KEY, is_disturbing+"");
		SharedPreferencesUtil.saveData(this, SharedPreferencesUtil.SAYEE_POSITION_KEY, position);
		
		HttpUtils.setDisturbing(this, path, headParams, params,
				new HttpRespListener() {

					@Override
					public void onSuccess(int code, BaseResult result) {
						updateDisturbing(HouseManageActivity.this,position);
					}

					@Override
					public void onFail(int code, String msg) {
						if (code == 3) {
							Intent intent = new Intent();
							intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
							intent.putExtra(Consts.SAYEE_CALLBACK_CODE,
									Consts.DISTURBING_CODE);
							intent.putExtra(Consts.SAYEE_ERROR_MSG,
									Consts.AGAIN_GET_TOKEN_ERROR_MSG);
							sendBroadcast(intent);
						} else {
							ToolsUtil.toast(HouseManageActivity.this, msg);
						}
					}
				});

	}

	/**
	 * 更新ListView
	 * 
	 * @param result
	 */
	public static void updateAdapter(BaseResult result) {
		if (result instanceof HouseResult) {
			HouseResult houseResult = (HouseResult) result;
			if (houseResult != null) {
				HouseInfoBean houseInfoBean = houseResult.getResult();
				if (houseInfoBean != null) {
					List<HouseBean> list = houseInfoBean.getSipInfoList();
					if (houses != null && adapter != null) {
						houses.clear();
						houses.addAll(list);
						adapter.updateListView(houses);
					}
				}
			}
		}
	}

	// 获取房产信息
	private void loadHouseMessage() {
		final Map<String, String> headParams = new HashMap<String, String>();
		headParams.put("token", token);
		headParams.put("username", userName);
		final Map<String, String> params = new HashMap<String, String>();
		params.put("username", userName);
		params.put("neigbor_id", neigbor_id);
		params.put("type", "1");
		HttpUtils.getHouseMessage(this, path, headParams, params,
				new HttpRespListener() {
					@Override
					public void onSuccess(int code, BaseResult result) {
						updateAdapter(result);
					}

					@Override
					public void onFail(int code, String msg) {
						if (code == 3) {
							Intent intent = new Intent();
							intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
							intent.putExtra(Consts.SAYEE_CALLBACK_CODE,Consts.HOUSE_MESSAGE_CODE);
							intent.putExtra(Consts.SAYEE_ERROR_MSG,Consts.AGAIN_GET_TOKEN_ERROR_MSG);
							sendBroadcast(intent);
						} else {
							ToolsUtil.toast(HouseManageActivity.this, msg);
						}
					}
				});
	}

	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (!isTopRightShow) {// 如果点击了免打扰,回到免打扰前的转态
				tv_top_right.setText("免打扰");
				if (houses != null && houses.size() > 0) {
					for (HouseBean bean : houses) {
						bean.setIsShow(false);
					}

					if (adapter != null) {
						adapter.notifyDataSetChanged();
					}
				}
				isTopRightShow = true;
				return true;
			}
		}
		return super.onKeyUp(keyCode, event);
	}

}

package com.sayee.sdk.activity;

import java.util.ArrayList;
import java.util.List;

import org.linphone.Contact;
import org.linphone.compatibility.Compatibility;
import org.linphone.tools.StringUtil;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sayee.sdk.Consts;
import com.sayee.sdk.adapter.ContactsAdapter;
import com.sayee.sdk.utils.ToolsUtil;

/**
 * 通讯录界面
 *
 */
public class ContactsActivity extends BaseActivity {
	public static final int REQUEST_CODE=0X102;
	private String house_id;
	private LinearLayout ll_top_left;
	private EditText et_search;
	private ListView ls_contacts;
	private ContactsAdapter adapter;
	private TextView tv_hint;
	private TextView tv_name;
	private LinearLayout rl_hint;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(ToolsUtil.getIdByName(getApplication(), "layout","sayee_activity_contacts"));
			ll_top_left = (LinearLayout) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ll_top_left"));
			tv_hint = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_hint"));
			tv_name = (TextView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "tv_name"));
			et_search = (EditText) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "et_search"));
			ls_contacts = (ListView) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ls_contacts"));
			rl_hint = (LinearLayout) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "rl_hint"));
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (getIntent() != null) {
			Intent intent = getIntent();
			house_id = intent.getStringExtra(Consts.SAYEE_HOUSE_ID);
		}
		
		contactList = new ArrayList<Contact>();
		adapter = new ContactsAdapter(this, contactList);
		ls_contacts.setAdapter(adapter);
		ll_top_left.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(isfresh)
					setResult(RESULT_OK);
				finish();
			}
		});

		et_search.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				searchNumber(s.toString());
			}
			
		});
		
		ls_contacts.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
				Intent intent=new Intent(ContactsActivity.this,ChildAccountAddActivity.class);
				intent.putExtra(Consts.SAYEE_HOUSE_ID, house_id);
				if(et_search!=null&&!TextUtils.isEmpty(et_search.getText())&&searchList!=null){
					if(searchList.get(position)!=null){
						intent.putExtra(Consts.SAYEE_NUMBER_KEY,StringUtil.getNormalNumber(searchList.get(position).getNumber()));
						intent.putExtra(Consts.SAYEE_NICK_NAME_KEY,searchList.get(position).getName());
					}
				}else{
					if(contactList.get(position)!=null){
						intent.putExtra(Consts.SAYEE_NUMBER_KEY,StringUtil.getNormalNumber(contactList.get(position).getNumber()));
						intent.putExtra(Consts.SAYEE_NICK_NAME_KEY,contactList.get(position).getName());
					}
				}
				startActivityForResult(intent, REQUEST_CODE);
			}
		});
		
		rl_hint.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(tv_name!=null&&!TextUtils.isEmpty(tv_name.getText())){
					String number=tv_name.getText().toString();
					if(!ToolsUtil.checkPhone(number)){
						ToolsUtil.toast(ContactsActivity.this, "请输入正确的手机号码");
						return;
					}
					Intent intent=new Intent(ContactsActivity.this,ChildAccountAddActivity.class);
					intent.putExtra(Consts.SAYEE_HOUSE_ID, house_id);
					intent.putExtra(Consts.SAYEE_NUMBER_KEY,tv_name.getText().toString());
					startActivityForResult(intent, REQUEST_CODE);
				}
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		prepareContactsInBackground();
	}

	private Cursor contactCursor;
	private List<Contact> contactList;
	private List<Contact> searchList;

	/**
	 * 查询通讯录
	 */
	public void prepareContactsInBackground() {
		contactCursor = Compatibility.getContactsCursor(getContentResolver());
		if (contactList == null)
			contactList = new ArrayList<Contact>();
		else {
			contactList.clear();
		}
		Thread localThread = new Thread(new Runnable() {
			public void run() {
				if (contactCursor != null) {
					for (int i = 0; i < contactCursor.getCount(); i++) {
						Contact localObject = Compatibility.getContact(getContentResolver(), contactCursor, i);
						if (localObject == null)
							continue;
						contactList.add(localObject);
					}
					runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							if (adapter != null) {
								adapter.updateListView(contactList);
							} else {
								adapter = new ContactsAdapter(ContactsActivity.this,contactList);
								if (ls_contacts != null)
									ls_contacts.setAdapter(adapter);
							}
							
							if(et_search!=null){
								String s=et_search.getText().toString();
								searchNumber(s);
							}
							
						}
					});
					try {
						contactCursor.close();
					} catch (Exception e) {
					}
				}
			}
		});
		localThread.start();
	}
	
	
	/**
	 * 搜索名字显示
	 * @param s
	 */
	private void searchNumber(String s) {
		if(adapter==null) return;
		if (TextUtils.isEmpty(s)) {
			if(rl_hint!=null&&rl_hint.getVisibility()==View.VISIBLE){ rl_hint.setVisibility(View.GONE);}
			if(ls_contacts!=null&&ls_contacts.getVisibility()==View.GONE){
				ls_contacts.setVisibility(View.VISIBLE);
			}
			if (contactList != null) {
				adapter.updateListView(contactList);
			}
		} else {
			if(searchList==null) searchList=new ArrayList<Contact>();
			if (contactList != null && searchList != null) {
				searchList.clear();//清除之前的数据
				for (Contact b : contactList) {
					if(b!=null){
						String searchString=StringUtil.getNoNull(b.getNumber())+StringUtil.getNoNull(b.getName());
						String ss=s.toString();
						if(searchString.contains(ss)){
							searchList.add(b);
						}
					}
				}
				if(searchList.size()<=0){//在通讯录搜索不到数据
					if(rl_hint!=null&&rl_hint.getVisibility()==View.GONE){ 
						rl_hint.setVisibility(View.VISIBLE);
					}
					if(ls_contacts!=null&&ls_contacts.getVisibility()==View.VISIBLE){
						ls_contacts.setVisibility(View.GONE);
					}
					
					if(tv_hint!=null){
						tv_hint.setText("不在手机通讯录");
					}
					if(tv_name!=null){
						tv_name.setText(s);
					}
				}else{//在通讯录中搜索到数据
					if(rl_hint!=null&&rl_hint.getVisibility()==View.VISIBLE){ rl_hint.setVisibility(View.GONE);}
					if(ls_contacts!=null&&ls_contacts.getVisibility()==View.GONE){
						ls_contacts.setVisibility(View.VISIBLE);
					}
					if(tv_hint!=null)
						tv_hint.setText("手机通讯录");
					adapter.updateListView(searchList);
					if(ls_contacts!=null){
						ls_contacts.setSelection(0);
					}
				}
			}
		}
	}
	
	
	boolean isfresh=false;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==REQUEST_CODE&&resultCode==RESULT_OK&&data!=null){
			isfresh=data.getBooleanExtra("isFresh", true);
			setResult(RESULT_OK);
			finish();
		}
	}
	

}

package com.sayee.sdk.activity;

import org.linphone.mediastream.Log;

import com.sayee.sdk.Consts;
import com.sayee.sdk.HttpRespListener;
import com.sayee.sdk.bean.TokenBean;
import com.sayee.sdk.result.BaseResult;
import com.sayee.sdk.result.TokenResult;
import com.sayee.sdk.utils.ActivitysManager;
import com.sayee.sdk.utils.HttpUtils;
import com.sayee.sdk.utils.SharedPreferencesUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Window;

/**
 * 基本的activity
 */
public abstract class BaseActivity extends Activity {
	protected boolean isGetToken=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 无标题
		ActivitysManager.getActivityManager().pushActivity(this);//添加activity
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(token) || deal_time == 0) {
			userName = (String) SharedPreferencesUtil.getData(this,SharedPreferencesUtil.SAYEE_USER_NAME_KEY, "");
			token = (String) SharedPreferencesUtil.getData(this,SharedPreferencesUtil.SAYEE_USER_TOKEN_KEY, "");
			deal_time = SharedPreferencesUtil.getDealTime(this);
		}
		if(!isGetToken&&!TextUtils.isEmpty(userName)&&!TextUtils.isEmpty(token)){
			getNewToken();
		}
	}
	
	protected long deal_time;
	protected static String userName;
	protected static String token;
	/**
	 * 重新获取token
	 */
	protected void getNewToken() {
		if (System.currentTimeMillis()- 300000 > deal_time ) {
			HttpUtils.getToken(this, userName, token, new HttpRespListener() {
				@Override
				public void onSuccess(int code, BaseResult result) {
					Log.i("------------重新获取成功-----------");
					if (result instanceof TokenResult) {
						TokenResult tokenResult = (TokenResult) result;
						TokenBean tokenbean=tokenResult.getResult();
						if(tokenbean!=null){
							token = tokenbean.getToken();
							deal_time = tokenbean.getDead_time();
						}
					}
				}

				@Override
				public void onFail(int code, String msg) {
					Log.i("------------重新获取失败-----------"+code);
					if(code==3){
						Intent intent=new Intent();
						intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
						intent.putExtra(Consts.SAYEE_CALLBACK_CODE, Consts.NOT_BACKCALL);
						intent.putExtra(Consts.SAYEE_ERROR_MSG, Consts.AGAIN_GET_TOKEN_ERROR_MSG);
						sendBroadcast(intent);
					}
				}

			});
		}
	}

}
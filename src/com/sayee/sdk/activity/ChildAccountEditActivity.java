package com.sayee.sdk.activity;

import java.util.HashMap;
import java.util.Map;

import org.linphone.tools.StringUtil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sayee.sdk.Consts;
import com.sayee.sdk.HttpRespListener;
import com.sayee.sdk.result.BaseResult;
import com.sayee.sdk.utils.HttpUtils;
import com.sayee.sdk.utils.SharedPreferencesUtil;
import com.sayee.sdk.utils.ToolsUtil;
import com.sayee.sdk.view.SayeeSwitchButton;
import com.sayee.sdk.view.SelectDialog;

/**
 * 子账号编辑
 * 
 */
public class ChildAccountEditActivity extends BaseActivity {

	private String path, house_id, alias;
	private String number;
	private String token;
	private TextView tv_top_left, tv_head, tv_name, tv_number;
	private static TextView tv_is_call;
	private LinearLayout ll_top_left;
	private TextView tv_top_right;
	private LinearLayout ll_top_right;
	private EditText et_alias;
	private Button btn_delete;
	private SayeeSwitchButton sbtn_call;
	private String is_called_number;
	private boolean isCall;
	private static boolean isChangeData;
	private static boolean currentIsCall;//标志当前是否是已经是紧急被叫号

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(ToolsUtil.getIdByName(getApplication(), "layout","sayee_activity_child_account_edit"));
			ll_top_left = (LinearLayout) findViewById(ToolsUtil.getIdByName(getApplication(), "id", "ll_top_left"));
			tv_top_left = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_top_left"));

			ll_top_right = (LinearLayout) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "ll_top_right"));
			tv_top_right = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_top_right"));

			tv_head = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_head"));
			tv_name = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_name"));
			tv_number = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_number"));
			tv_is_call = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_is_call"));
			et_alias = (EditText) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "et_alias"));
			btn_delete = (Button) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "btn_delete"));
			sbtn_call = (SayeeSwitchButton) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "sbtn_call"));

			tv_top_left.setText("子账号管理");
			ll_top_right.setVisibility(View.VISIBLE);
			tv_top_right.setText("提交");
			setSubmitClickEnable(false);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (getIntent() != null) {
			Intent intent = getIntent();
			house_id = intent.getStringExtra(Consts.SAYEE_HOUSE_ID);
			number = intent.getStringExtra(Consts.SAYEE_NUMBER_KEY);
			alias = intent.getStringExtra(Consts.SAYEE_ALIAS_KEY);
			isCall = intent.getBooleanExtra(Consts.SAYEE_IS_CALL_KEY, false);
			if (isCall) {
				tv_is_call.setVisibility(View.VISIBLE);
			} else {
				tv_is_call.setVisibility(View.INVISIBLE);
			}
			sbtn_call.setChecked(isCall);
			currentIsCall=isCall;
		}

		path = SharedPreferencesUtil.getTwoUrl(this);
		token = SharedPreferencesUtil.getToken(this);
		userName = SharedPreferencesUtil.getUserName(this);

		tv_head.setText(StringUtil.getTwoWord(number));
		tv_name.setText(number);
		tv_number.setText(number);
		et_alias.setText(alias);

		ll_top_left.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (isChangeData)
					setResult(RESULT_OK);
				finish();
			}
		});
		
		
		ll_top_right.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				editAccount(false);
			}
		});
		

		sbtn_call.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (ToolsUtil.isNetworkConnected(ChildAccountEditActivity.this)) {
							isCall = isChecked;
							setCallAccount(false);
						} else {
							sbtn_call.setChecked(isCall);
						}
					}
				});

		btn_delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				final SelectDialog dialog = new SelectDialog(ChildAccountEditActivity.this);
				dialog.setMessage("确定删除子账号？");
				dialog.setNegativeButton(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						dialog.dismiss();
					}
				});
				dialog.setPositiveButton(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						editAccount(true);
						dialog.dismiss();
					}
				});
			}
		});

		et_alias.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,int arg2, int arg3) {}

			@Override
			public void afterTextChanged(Editable a) {
				if(TextUtils.isEmpty(a)){
					setSubmitClickEnable(false);
				}else{
					String str=a.toString();
					if(str.equals(alias)){
					   setSubmitClickEnable(false);
					}else{
					   setSubmitClickEnable(true);
					}
				}
			}
		});
	}

	/**
	 * 设置提交按钮是否能提交
	 * @param isClick
	 */
	private void setSubmitClickEnable(boolean isClick) {
		if (ll_top_right != null && tv_top_right != null) {
			int color = 0;
			if (!isClick) {
				try {
					color = getResources().getColor(ToolsUtil.getIdByName(getApplication(), "color","col_submit_transparent"));
				} catch (Exception e) {
					color = Color.parseColor("#55fafafa");
				}
			} else {
				try {
					color = getResources().getColor(ToolsUtil.getIdByName(getApplication(), "color","col_submit"));
				} catch (Exception e) {
					color = Color.parseColor("#ffffff");
				}
			}
			tv_top_right.setTextColor(color);
			ll_top_right.setClickable(isClick);
		}
	}
	

	/**
	 * 设置紧急被叫号
	 */
	private  void setCallAccount(final boolean isMy) {

		if (!TextUtils.isEmpty(number)) {
			number = number.replaceAll(" ", "");
			number = number.replace("+86", "");

			if (!ToolsUtil.checkPhone(number)) {
				ToolsUtil.toast(this, "手机号码格式错误");
				return;
			}

			final Map<String, String> headParams = new HashMap<String, String>();
			headParams.put("token", token);
			headParams.put("username", userName);
			final Map<String, String> params = new HashMap<String, String>();
			params.put("username", userName);
			params.put("house_id", house_id);
			if(isMy){
				params.put("called_number", userName);
			}else{
				params.put("called_number", number);
			}

			HttpUtils.setCalledNumber(this, path, headParams, params,new HttpRespListener() {
						@Override
						public void onSuccess(int code, BaseResult result) {
							updateCalledNumber(ChildAccountEditActivity.this,isMy);
						}

						@Override
						public void onFail(int code, String msg) {
							if(code==3){
								SharedPreferencesUtil.saveData(ChildAccountEditActivity.this, SharedPreferencesUtil.SAYEE_HOUSE_ID_KEY, house_id);
								SharedPreferencesUtil.saveData(ChildAccountEditActivity.this, SharedPreferencesUtil.SAYEE_CALLED_NUMBER_KEY, number);
								SharedPreferencesUtil.saveData(ChildAccountEditActivity.this, SharedPreferencesUtil.SAYEE_CALLED_NUMBER_IS_MY_KEY, isMy);
								SharedPreferencesUtil.saveData(ChildAccountEditActivity.this, SharedPreferencesUtil.SAYEE_IS_CALLED_KEY, isCall);
								
								Intent intent=new Intent();
								intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
								intent.putExtra(Consts.SAYEE_CALLBACK_CODE, Consts.SET_CALL_ACCOUNT_CODE);
								intent.putExtra(Consts.SAYEE_ERROR_MSG, Consts.AGAIN_GET_TOKEN_ERROR_MSG);
								sendBroadcast(intent);
							}else{
								ToolsUtil.toast(ChildAccountEditActivity.this, msg);
								sbtn_call.setChecked(isCall);
							}
							
						}
					});
		}
	}
	
	
	/**
	 * token失效从新获取后，回调更新状态
	 * @param isSuccess
	 */
	public static void callBackUpdate(boolean isSuccess){
		isChangeData = true;
		if(tv_is_call!=null){
			if(isSuccess){
				currentIsCall=true;
				tv_is_call.setVisibility(View.VISIBLE);
			}else{
				currentIsCall=false;
				tv_is_call.setVisibility(View.GONE);
			}
		}
	}
	
	
	/**
	 * 更新紧急被叫号的状态
	 * @param isMy
	 */
	public  void updateCalledNumber(Context context,final boolean isMy) {
		isChangeData = true;
		if (isCall) {
			if(!isMy){
				currentIsCall=true;
				ToolsUtil.toast(context,"设置紧急被叫号成功");
			}
			tv_is_call.setVisibility(View.VISIBLE);
		} else {
			if(!isMy){
			  currentIsCall=false;
			  setCallAccount(true);
			  ToolsUtil.toast(context,"取消紧急被叫号成功");
			}
			tv_is_call.setVisibility(View.GONE);
		}
	}
	
	

	/**
	 * 编辑账号 删除或者修改昵称
	 */
	private void editAccount(final boolean isDelete) {
		
		if (!TextUtils.isEmpty(number)) {
			number = number.replaceAll(" ", "");
			number = number.replace("+86", "");

			if (!ToolsUtil.checkPhone(number)) {
				ToolsUtil.toast(this, "手机号无效");
				return;
			}

			final Map<String, String> headParams = new HashMap<String, String>();
			headParams.put("token", token);
			headParams.put("username", userName);
			final Map<String, String> params = new HashMap<String, String>();
			params.put("username", userName);
			params.put("house_id", house_id);
			params.put("sub_account", number);
			if(isDelete){
				params.put("is_add", "false");
				params.put("alias", alias);
				if(currentIsCall){
					ToolsUtil.toast(this, "当前紧急被叫号，不能删除");
					return;
				}
			}else{
				params.put("is_add", "true");
				if (et_alias!=null) {
					alias=et_alias.getText().toString();
					params.put("alias", alias);
				}else{
				   params.put("alias", alias);
				}
			}
			
			params.put("is_called_number", isCall+"");
			
			HttpUtils.setSubAccount(this, path, headParams, params,new HttpRespListener() {
						@Override
						public void onSuccess(int code, BaseResult result) {
							if(isDelete){
								ToolsUtil.toast(ChildAccountEditActivity.this,"删除成功");
							}else{
								ToolsUtil.toast(ChildAccountEditActivity.this,"修改备注成功");
							}
							setResult(RESULT_OK);
							finish();
						}

						@Override
						public void onFail(int code, String msg) {
							if(code==3){
								SharedPreferencesUtil.saveData(ChildAccountEditActivity.this, SharedPreferencesUtil.SAYEE_HOUSE_ID_KEY, house_id);
								SharedPreferencesUtil.saveData(ChildAccountEditActivity.this, SharedPreferencesUtil.SAYEE_ADD_ACCOUNT_NUMBER_KEY, number);
								SharedPreferencesUtil.saveData(ChildAccountEditActivity.this, SharedPreferencesUtil.SAYEE_IS_ADD_ACCOUNT_KEY, isDelete);
								SharedPreferencesUtil.saveData(ChildAccountEditActivity.this, SharedPreferencesUtil.SAYEE_ADD_ACCOUNT_ALIAS_KEY, alias);
								SharedPreferencesUtil.saveData(ChildAccountEditActivity.this, SharedPreferencesUtil.SAYEE_IS_CALLED_NUMBER_KEY, isCall);
								Intent intent=new Intent();
								intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
								if(isDelete){
									intent.putExtra(Consts.SAYEE_CALLBACK_CODE, Consts.DELETE_SUBACCOUNT_CODE);
								}else{
									intent.putExtra(Consts.SAYEE_CALLBACK_CODE, Consts.EDIT_SUBACCOUNT_CODE);
								}
								intent.putExtra(Consts.SAYEE_ERROR_MSG, Consts.AGAIN_GET_TOKEN_ERROR_MSG);
								sendBroadcast(intent);
							}else{
								ToolsUtil.toast(ChildAccountEditActivity.this, msg);
								sbtn_call.setChecked(isCall);
							}
						}
					});
		}
	}

}

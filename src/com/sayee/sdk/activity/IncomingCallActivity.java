package com.sayee.sdk.activity;

import java.util.List;

import org.linphone.LinphoneManager;
import org.linphone.LinphoneSimpleListener;
import org.linphone.LinphoneUtils;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.mediastream.Log;

import com.sayee.sdk.Consts;
import com.sayee.sdk.activity.VideoCallActivity;
import com.sayee.sdk.db.DatabaseHelper;
import com.sayee.sdk.utils.SharedPreferencesUtil;
import com.sayee.sdk.utils.ToolsUtil;
import com.sayee.sdk.view.AlertDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 来电接听界面
 */
public class IncomingCallActivity extends Activity implements
		LinphoneSimpleListener.LinphoneOnCallStateChangedListener {

	static IncomingCallActivity instance;
	private Button btn_anwser;
	private Button btn_hangup;
	boolean isAnswer;
	private TextView tv_address;
	private String domain, fromSipName, displayName = "";

	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		instance = this;
		try {
			setContentView(ToolsUtil.getIdByName(getApplication(), "layout",
					"sayee_activtiy_incoming_call"));
			tv_address = (TextView) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "tv_address"));
			btn_anwser = (Button) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "btn_answer"));
			btn_hangup = (Button) findViewById(ToolsUtil.getIdByName(
					getApplication(), "id", "btn_hangup"));
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		setListener();
	}

	private void setListener() {
		btn_anwser.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(LinphoneManager.getLcIfManagerNotDestroyedOrNull()!=null&&LinphoneManager.getLc().isIncall()){
					LinphoneCall[] calls=LinphoneManager.getLc().getCalls();
					if(calls!=null){
						for(LinphoneCall call:calls){
							if(call!=null && call!=mCall){
								LinphoneManager.getLc().terminateCall(call);
							}
						}
					}
				}
				
				if (!LinphoneUtils.isHasPermission(getApplicationContext())) {
					final AlertDialog ad = new AlertDialog(IncomingCallActivity.this);
					ad.setPositiveButton(new OnClickListener() {
						@Override
						public void onClick(View v) {
							String name = Build.MANUFACTURER;
							if ("HUAWEI".equals(name)) {
								LinphoneUtils.goHuaWeiMainager(IncomingCallActivity.this);
							} else if ("vivo".equals(name)) {
								LinphoneUtils.goVivoMainager(IncomingCallActivity.this);
							} else if ("OPPO".equals(name)) {
								LinphoneUtils.goOppoMainager(IncomingCallActivity.this);
							} else if ("Coolpad".equals(name)) {
								LinphoneUtils.goCoolpadMainager(IncomingCallActivity.this);
							} else if ("Meizu".equals(name)) {
								LinphoneUtils.goMeizuMainager(IncomingCallActivity.this);
							} else if ("Xiaomi".equals(name)) {
								LinphoneUtils.goXiaoMiMainager(IncomingCallActivity.this);
							} else if ("samsung".equals(name)) {
								LinphoneUtils.goSangXinMainager(IncomingCallActivity.this);
							} else {
								LinphoneUtils.goIntentSetting(IncomingCallActivity.this);
							}
							ad.dismiss();
						}
					});
					ad.setNegativeButton(new OnClickListener() {
						@Override
						public void onClick(View v) {
							ad.dismiss();
						}
					});
					return;
				}
				
				answer();
			}
		});
		btn_hangup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				SharedPreferencesUtil.saveData(getApplicationContext(), SharedPreferencesUtil.SAYEE_SIP_IS_CALL_KEY,true);
				hangUp();
			}
		});
	}

	private void hangUp() {
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		if (lc != null) {
			if (mCall != null) {
				lc.terminateCall(mCall);
			} else if (lc.isInConference()) {
				lc.terminateConference();
			} else {
				lc.terminateAllCalls();
			}
		}
		finish();
	}

	public static IncomingCallActivity instance() {
		return instance;
	}

	public static boolean isInstanciated() {
		return instance != null;
	}

	private void answer() {
		LinphoneCallParams call = LinphoneManager.getLc().createDefaultCallParameters();
		if (!LinphoneUtils.isHightBandwidthConnection(this)) {
			call.enableLowBandwidth(true);
		}

		if (mCall == null|| mCall.getState() == LinphoneCall.State.CallReleased || mCall.getState() == LinphoneCall.State.Error) {
			finish();
			return;
		}

		if (!LinphoneManager.getInstance().acceptCallWithParams(mCall, call))
			Toast.makeText(this, "不能接通电话", Toast.LENGTH_SHORT).show();
		else {
			LinphoneCallParams param = mCall.getRemoteParams();
			if ((param != null) && (param.getVideoEnabled())) {
				acceptCallUpdate(true);
				Intent intent = new Intent(this, VideoCallActivity.class);
				intent.putExtra(Consts.SAYEE_IS_CALL_KEY,false);
				intent.putExtra(Consts.SAYEE_CALL_INCOMING, true);
				intent.putExtra(Consts.SAYEE_FROM_SIP_DOMAIN, domain);
				intent.putExtra(Consts.SAYEE_FROM_SIP_NUMBER, fromSipName);
				intent.putExtra(Consts.SAYEE_DOOR_NAME, displayName);
				startActivity(intent);
				finish();
			} else {
				acceptCallUpdate(false);
				Intent intent = new Intent(this, VideoCallActivity.class);
				intent.putExtra(Consts.SAYEE_IS_CALL_KEY,false);
				intent.putExtra(Consts.SAYEE_CALL_INCOMING, true);
				intent.putExtra(Consts.SAYEE_IS_VIDEO, false);
				intent.putExtra(Consts.SAYEE_FROM_SIP_DOMAIN, domain);
				intent.putExtra(Consts.SAYEE_FROM_SIP_NUMBER, fromSipName);
				intent.putExtra(Consts.SAYEE_DOOR_NAME, displayName);
				startActivity(intent);
				finish();
			}
		}

	}

	private void acceptCallUpdate(boolean paramBoolean) {
		if (mCall == null)
			return;
		LinphoneCallParams params = mCall.getCurrentParamsCopy();
		if (paramBoolean) {
			params.setVideoEnabled(true);
			LinphoneManager.getLc().enableVideo(true, true);
		}
		try {
			LinphoneManager.getLc().acceptCallUpdate(mCall, params);
		} catch (LinphoneCoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onCallStateChanged(LinphoneCall call, State state,
			String paramString) {
		if (call == mCall && (LinphoneCall.State.CallEnd == state || LinphoneCall.State.CallReleased == state)) {
			SharedPreferencesUtil.saveData(getApplicationContext(), SharedPreferencesUtil.SAYEE_SIP_IS_CALL_KEY,true);
			finish();
		} else if (state == LinphoneCall.State.StreamsRunning) {
			LinphoneManager.getLc().enableSpeaker(
					LinphoneManager.getLc().isSpeakerEnabled());
		}

	}

	private LinphoneCall mCall;

	@Override
	protected void onResume() {
		super.onResume();
		LinphoneManager.addListener(this);
		if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null) {
			List<LinphoneCall> calls = LinphoneUtils.getLinphoneCalls(LinphoneManager.getLc());
			for (LinphoneCall call : calls) {
				if (State.IncomingReceived == call.getState()) {
					mCall = call;
					break;
				}
			}
		}
		if (mCall == null) {
			Log.e("Couldn't find incoming call");
			finish();
			return;
		}

		if (mCall.getState() == LinphoneCall.State.CallReleased|| mCall.getState() == LinphoneCall.State.Error) {
			finish();
			return;
		}

		LinphoneAddress localLinphoneAddress = mCall.getRemoteAddress();
		if (localLinphoneAddress != null) {
			domain = localLinphoneAddress.getDomain();
			fromSipName = localLinphoneAddress.getUserName();
			displayName = localLinphoneAddress.getDisplayName();
			if (!TextUtils.isEmpty(displayName)) {
				if (localLinphoneAddress.getDisplayName().contains(",")) {
					displayName = LinphoneUtils.codePoint2String(displayName);
					tv_address.setText(displayName);
				} else {
					displayName = getDatabaseHelper().getDisplayName(displayName);
					tv_address.setText(displayName);
				}
			}
		}
	}

	private PowerManager.WakeLock mWakeLock;
	private PowerManager powerManager;

	@SuppressWarnings("deprecation")
	@Override
	protected void onStart() {
		super.onStart();
		if (powerManager == null)
			powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		if (powerManager != null && mWakeLock == null) {
			mWakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK
					| PowerManager.ACQUIRE_CAUSES_WAKEUP,
					IncomingCallActivity.class.getSimpleName());
			if (mWakeLock != null) {
				mWakeLock.acquire();
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		LinphoneManager.removeListener(this);
		if (mWakeLock != null && mWakeLock.isHeld()) {
			mWakeLock.release();
		}
	}

	@SuppressLint("Wakelock")
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mWakeLock != null && mWakeLock.isHeld()) {
			mWakeLock.release();
		}
	}

	private DatabaseHelper databaseHelper = null;

	private DatabaseHelper getDatabaseHelper() {
		if (databaseHelper == null)
			databaseHelper = new DatabaseHelper(getApplicationContext());
		return databaseHelper;
	}

	long firstTime = 0;
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			try {
				long secondTime = System.currentTimeMillis();
				if (secondTime - firstTime > 2000) { // 如果两次按键时间间隔大于2秒，则不退出
					Toast.makeText(this, "再按一次将挂断通话", Toast.LENGTH_SHORT).show();
					firstTime = secondTime;// 更新firstTime
					return true;
				} else { // 两次按键小于2秒时，退出应用
					hangUp();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
		return super.onKeyUp(keyCode, event);
	}

}

package com.sayee.sdk.view;

import com.sayee.sdk.utils.ToolsUtil;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AlertDialog {
	Context context;
	android.app.AlertDialog ad;
	TextView tv_title;
	TextView tv_message;
	Button btn_left, btn_right;

	public AlertDialog(Context context) {
		this.context = context;
		ad = new android.app.AlertDialog.Builder(context).create();
		ad.show();
		try {
			ad.setContentView(ToolsUtil.getIdByName(context, "layout","sayee_alert_dialog"));
			tv_title = (TextView) ad.findViewById(ToolsUtil.getIdByName(context, "id", "tv_title"));
			tv_message = (TextView) ad.findViewById(ToolsUtil.getIdByName(context, "id", "tv_message"));
			btn_left = (Button) ad.findViewById(ToolsUtil.getIdByName(context, "id", "btn_left"));
			btn_right = (Button) ad.findViewById(ToolsUtil.getIdByName(context, "id", "btn_right"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void setTitle(int resId) {
		tv_title.setText(resId);
	}

	public void setTitle(String title) {
		tv_title.setText(title);
	}

	public void setMessage(int resId) {
		tv_message.setText(resId);
	}

	public void setMessage(String message) {
		tv_message.setText(message);
	}

	/**
	 * 设置按钮
	 * 
	 * @param text
	 * @param listener
	 */
	public void setPositiveButton(final View.OnClickListener listener) {
		if (btn_right != null) {
			btn_right.setOnClickListener(listener);
		}
	}

	/**
	 * 设置按钮
	 * 
	 * @param text
	 * @param listener
	 */
	public void setNegativeButton(final View.OnClickListener listener) {
		if (btn_left != null) {
			btn_left.setOnClickListener(listener);
		}
	}

	/**
	 * 关闭对话框
	 */
	public void dismiss() {
		ad.dismiss();
	}

}

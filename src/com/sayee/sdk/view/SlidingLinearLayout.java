package com.sayee.sdk.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class SlidingLinearLayout extends LinearLayout {


	public SlidingLinearLayout(Context context) {
		super(context);
	}

	private boolean isMe = false;

	public boolean isMe() {
		return isMe;
	}

	public void setMe(boolean isMe) {
		this.isMe = isMe;
	}

	public SlidingLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			isMe = true;
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			isMe = false;
		}
		return false;
	}

	public boolean handleActivityEvent(MotionEvent activityEvent) {
		boolean result = false;
		if (isMe()) {
			if (activityEvent.getAction() == MotionEvent.ACTION_UP) {
				if(getLeft() + getWidth()/2 > ((FrameLayout)getParent().getParent()).getWidth() - this.getWidth()){
					LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)getLayoutParams();
					lp.leftMargin = 0;
					setLayoutParams(lp);
					setMe(false);
					result = true;
				}else{
					LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)getLayoutParams();
					lp.leftMargin = 0;
					setLayoutParams(lp);
					setMe(false);
					result = false;
				}
			} else {
				LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) getLayoutParams();
				lp.leftMargin = (int) (activityEvent.getX() - ((FrameLayout)this.getParent().getParent()).getLeft()) - this.getWidth()/2;
				if (lp.leftMargin > 0 && lp.leftMargin < ((FrameLayout)this.getParent().getParent()).getWidth() - this.getWidth()) {
					setLayoutParams(lp);
				}
			}
		}
		return result;
	}
	
}

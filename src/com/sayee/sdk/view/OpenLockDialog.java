package com.sayee.sdk.view;


import com.sayee.sdk.Consts;
import com.sayee.sdk.HttpRespListener;
import com.sayee.sdk.result.BaseResult;
import com.sayee.sdk.utils.HttpUtils;
import com.sayee.sdk.utils.SharedPreferencesUtil;
import com.sayee.sdk.utils.ToolsUtil;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class OpenLockDialog extends Dialog {

	private Context context;
	private String doorName;
	private Button btn_open_video,btn_get_password,btn_back;
	private SlidingLinearLayout btn_open;

	public OpenLockDialog(Context context,String doorName) {
		super(context);
		this.doorName=doorName;
		this.context = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(ToolsUtil.getIdByName(context, "layout","sayee_dialog_open_lock"));
		initView();
		setListener();
		android.view.WindowManager.LayoutParams p = getWindow().getAttributes();
		WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = windowManager.getDefaultDisplay();
		p.height = (int) (display.getHeight() * 0.4);
		p.width = (int) (display.getWidth() * 0.9);
		getWindow().setAttributes(p);

	}
	
	
	/**
	 * 设置按钮监听
	 */
	public void setListener() {
		if(openVideoListener!=null&&btn_open_video!=null){
			btn_open_video.setOnClickListener(openVideoListener);
		}
		
		if(openDoorForpasswordListener!=null&&btn_get_password!=null){
			btn_get_password.setOnClickListener(openDoorForpasswordListener);
		}
		
		btn_back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				dismiss();
			}

		});
	}

	/**
	 *初始化控件
	 */
	private void initView() {
		btn_open_video=(Button) findViewById(ToolsUtil.getIdByName(context, "id", "btn_open_video"));
		btn_get_password=(Button) findViewById(ToolsUtil.getIdByName(context, "id", "btn_get_password"));
		btn_open=(SlidingLinearLayout) findViewById(ToolsUtil.getIdByName(context, "id", "btn_open"));
		btn_back=(Button) findViewById(ToolsUtil.getIdByName(context, "id", "btn_back"));
	}



	/**
	 * 创建dialog布局
	 * 
	 * @return
	 */
	@SuppressLint("NewApi")
/*	private View getView() {
		RelativeLayout relativeLayout = new RelativeLayout(context);
		ViewGroup.LayoutParams reaLayoutParams = new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		relativeLayout.setLayoutParams(reaLayoutParams);

		LinearLayout linearLayout = new LinearLayout(context);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		linearLayout.setLayoutParams(layoutParams);
		linearLayout.setOrientation(LinearLayout.VERTICAL);

		LinearLayout ll_dialog_bottom = new LinearLayout(context);
		LayoutParams ll_dialog_bottom_params = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		ll_dialog_bottom_params.bottomMargin = dip2px(14);
		ll_dialog_bottom.setLayoutParams(ll_dialog_bottom_params);
		ll_dialog_bottom.setOrientation(LinearLayout.VERTICAL);

		TextView tv_title = new TextView(context);
		LinearLayout.LayoutParams tv_title_params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, dip2px(20));
		tv_title_params.topMargin = dip2px(16);
		tv_title.setLayoutParams(tv_title_params);
		tv_title.setTextSize(14);
		tv_title.setTextColor(Color.parseColor("#666666"));
		if(!TextUtils.isEmpty(doorName)){
			tv_title.setText("此操作将远程打开 "+doorName+" 大门门锁");
		}else{
		    tv_title.setText("此操作将远程开锁");
		}
		tv_title.setGravity(Gravity.CENTER);

		TextView tv_title2 = new TextView(context);
		LinearLayout.LayoutParams tv_title_params2 = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, dip2px(20));
		tv_title2.setLayoutParams(tv_title_params2);
		tv_title2.setTextSize(14);
		tv_title2.setTextColor(Color.parseColor("#666666"));
		tv_title2.setText("请谨慎操作");
		tv_title2.setGravity(Gravity.CENTER);

		View v_line = new View(context);
		LinearLayout.LayoutParams v_line_params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, 1);
		v_line_params.topMargin = dip2px(22);
		v_line.setLayoutParams(v_line_params);
		v_line.setBackgroundColor(Color.parseColor("#d1d1d1"));

		LinearLayout ll_lock_add_video = new LinearLayout(context);
		LayoutParams ll_lock_add_video_params = new LayoutParams(
				LayoutParams.MATCH_PARENT, dip2px(50));
		ll_lock_add_video.setLayoutParams(ll_lock_add_video_params);
		ll_lock_add_video.setOrientation(LinearLayout.HORIZONTAL);

		TextView tv_opendoor_monitor = new TextView(context);
		LinearLayout.LayoutParams tv_opendoor_monitor_params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		tv_opendoor_monitor_params.weight = 1;
		tv_opendoor_monitor_params.gravity=Gravity.CENTER;
		tv_opendoor_monitor.setLayoutParams(tv_opendoor_monitor_params);
		tv_opendoor_monitor.setTextSize(20);
		tv_opendoor_monitor.setTextColor(Color.parseColor("#00b4c7"));
		tv_opendoor_monitor.setText("门口视频");
		tv_opendoor_monitor.setGravity(Gravity.CENTER);
		if(openVideoListener!=null){
			tv_opendoor_monitor.setOnClickListener(openVideoListener);
		}
		
		View v_line2 = new View(context);
		LinearLayout.LayoutParams v_line_params2 = new LinearLayout.LayoutParams(
				1, LayoutParams.MATCH_PARENT);
		v_line2.setLayoutParams(v_line_params2);
		v_line2.setBackgroundColor(Color.parseColor("#d1d1d1"));

		TextView tv_opendoor_password = new TextView(context);
		LinearLayout.LayoutParams tv_opendoor_password_params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		tv_opendoor_password_params.weight = 1;
		tv_opendoor_password_params.gravity=Gravity.CENTER;
		tv_opendoor_password.setLayoutParams(tv_opendoor_password_params);
		tv_opendoor_password.setTextSize(20);
		tv_opendoor_password.setTextColor(Color.parseColor("#00b4c7"));
		tv_opendoor_password.setText("密码开锁");
		tv_opendoor_password.setGravity(Gravity.CENTER);
		if(openDoorForpasswordListener!=null){
			tv_opendoor_password.setOnClickListener(openDoorForpasswordListener);
		}
		
		ll_lock_add_video.addView(tv_opendoor_monitor);
		ll_lock_add_video.addView(v_line2);
		ll_lock_add_video.addView(tv_opendoor_password);

		View v_line3 = new View(context);
		LinearLayout.LayoutParams v_line_params3 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 1);
		v_line3.setLayoutParams(v_line_params3);
		v_line3.setBackgroundColor(Color.parseColor("#d1d1d1"));

		FrameLayout fl_open_lock = new FrameLayout(context);
		LinearLayout.LayoutParams fl_open_lock_params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		fl_open_lock_params.gravity=Gravity.CENTER_VERTICAL;
		fl_open_lock_params.topMargin=dip2px(10);
		fl_open_lock_params.leftMargin=dip2px(10);
		fl_open_lock_params.rightMargin=dip2px(10);
		fl_open_lock.setLayoutParams(fl_open_lock_params);
		int dp2 = dip2px(2);
		fl_open_lock.setPadding(dp2, dp2, dp2, dp2);

		TextView tv_slider = new TextView(context);
		FrameLayout.LayoutParams tv_slide_params = new FrameLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		tv_slider.setLayoutParams(tv_slide_params);
		tv_slider.setTextSize(14);
		tv_slider.setTextColor(Color.WHITE);
		tv_slider.setText("右滑打开门锁");
		tv_slider.setGravity(Gravity.CENTER);
		tv_slider.setBackgroundColor(Color.parseColor("#D1CAEF"));

		LinearLayout ll_sb=new LinearLayout(context);
		FrameLayout.LayoutParams ll_sb_params=new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,dip2px(40));
		ll_sb.setLayoutParams(ll_sb_params);
		
		ll_slider = new SlidingLinearLayout(context);
		FrameLayout.LayoutParams sb_btn_params = new FrameLayout.LayoutParams(dip2px(60),FrameLayout.LayoutParams.MATCH_PARENT);
		int marginDistance = dip2px(2);
		sb_btn_params.setMargins(marginDistance, marginDistance, marginDistance, marginDistance);
		sb_btn_params.gravity=Gravity.CENTER_VERTICAL;
		ll_slider.setLayoutParams(sb_btn_params);
		ll_slider.setGravity(Gravity.CENTER);
		ll_slider.setBackgroundColor(Color.WHITE);
	
		ImageView iv_slider=new ImageView(context);
		LinearLayout.LayoutParams iv_slider_params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		iv_slider.setLayoutParams(iv_slider_params);
		iv_slider.setImageBitmap(ToolsUtil.getBitmap4Assets(context, "slider.png"));
		ll_slider.addView(iv_slider);
		
		ll_sb.addView(ll_slider);
		fl_open_lock.addView(tv_slider);
		fl_open_lock.addView(ll_sb);

		LinearLayout ll_back = new LinearLayout(context);
		LinearLayout.LayoutParams ll_call_back_params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, dip2px(50));
		ll_call_back_params.topMargin=dip2px(5);
		ll_back.setLayoutParams(ll_call_back_params);
		ll_back.setOrientation(LinearLayout.VERTICAL);
		ll_back.setGravity(Gravity.CENTER);
		
		
		View v_transparent=new View(context);
		LinearLayout.LayoutParams v_transparent_params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, dip2px(10));
		v_transparent.setLayoutParams(v_transparent_params);
		v_transparent.setBackgroundColor(Color.parseColor("#00000000"));

		TextView tv_back = new TextView(context);
		LinearLayout.LayoutParams tv_back_params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		tv_back.setLayoutParams(tv_back_params);
		tv_back.setTextSize(20);
		tv_back.setTextColor(Color.parseColor("#00b4c7"));
		tv_back.setText("返回");
		tv_back.setBackgroundColor(Color.WHITE);
		tv_back.setGravity(Gravity.CENTER);

		tv_back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				dismiss();
			}
		});

		ll_back.addView(v_transparent);
		ll_back.addView(tv_back);
		
		ll_dialog_bottom.addView(tv_title);
		ll_dialog_bottom.addView(tv_title2);
		ll_dialog_bottom.addView(v_line);
		ll_dialog_bottom.addView(ll_lock_add_video);
		ll_dialog_bottom.addView(v_line3);
		ll_dialog_bottom.addView(fl_open_lock);
		ll_dialog_bottom.addView(ll_back);
		linearLayout.addView(ll_dialog_bottom);
		relativeLayout.addView(linearLayout);
		return relativeLayout;
	}
	
	
	float scale;

	*//**
	 * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
	 *//*
	public int dip2px(float dip) {
		if(scale==0){
			scale = context.getResources().getDisplayMetrics().density;
		}
		return (int) (dip * scale + 0.5f);
	}
	*/
	
	private View.OnClickListener openVideoListener=null;
	/**
	 * 设置打开视频监听
	 * @param listener
	 */
	public void setOpenVideoListener(View.OnClickListener listener){
		openVideoListener=listener;
	}
	
	private View.OnClickListener openDoorForpasswordListener=null;
	/**
	 * 设置获取开锁密码监听
	 * @param listener
	 */
	public void setOpenDoorForpasswordListener(View.OnClickListener listener){
		openDoorForpasswordListener=listener;
	}
	
	
	String domain_sn;
	String toSipNumber;
	String path;
	String token;
	String userName;
	
	public void setPath(String path){
		this.path=path;
	}
	
	public void setDomain_sn(String domain_sn) {
		this.domain_sn = domain_sn;
	}

	public void setToSipNumber(String toSipNumber) {
		this.toSipNumber = toSipNumber;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (btn_open != null) {
			if (btn_open.handleActivityEvent(event)) {
				if(!TextUtils.isEmpty(domain_sn)&&!TextUtils.isEmpty(toSipNumber)){
					SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_NAME_KEY, userName);
					SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_DOMAIN_SN_KEY, domain_sn);
					SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_TYPE_KEY, 0);
					HttpUtils.openDoorLock(context,path,token,userName,domain_sn, 0, toSipNumber,null,new HttpRespListener() {
						
						@Override
						public void onSuccess(int code, BaseResult result) {
							ToolsUtil.toast(context, "已发送开锁请求");
						}
						
						@Override
						public void onFail(int code, String msg) {
							if(code==3){
								Intent intent=new Intent();
								intent.setAction(Consts.SAYEE_TOKEN_FAIL_ACTION);
								intent.putExtra(Consts.SAYEE_CALLBACK_CODE, Consts.OPEN_LOCK_CODE);
								intent.putExtra(Consts.SAYEE_ERROR_MSG, Consts.AGAIN_GET_TOKEN_ERROR_MSG);
								context.sendBroadcast(intent);
							}else{
								ToolsUtil.toast(context, msg);
							}
						}
					});
				}
			}
		}
		return super.onTouchEvent(event);
	}

}

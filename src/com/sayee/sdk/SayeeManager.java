package com.sayee.sdk;

import org.linphone.LinphoneManager;
import org.linphone.core.LinphoneCoreException;
import org.linphone.mediastream.Log;

import android.content.Context;
import android.content.Intent;

public class SayeeManager {
	boolean isOpen=false;

	private SayeeManager() {}

	private static SayeeManager instance = null;

	public static SayeeManager getInstance() {
		if (instance == null) {
			synchronized (SayeeManager.class) {
				if (instance == null)
					instance = new SayeeManager();
			}
		}
		return instance;
	}

	public void initalize(Context context) {
		if (context == null)
			try {
				throw new LinphoneCoreException("initalize error,context connot be null");
			} catch (LinphoneCoreException e) {
				e.printStackTrace();
			}
		context.startService(new Intent(context, LinphoneService.class));
		isOpen=true;
	}
	
	public void turnOffCall(Context context) {
		if (context == null){
			Log.e("turn off fail,context cannot be null");
			return;
		}
		if(LinphoneService.isReady()){
			context.stopService(new Intent(context, LinphoneService.class));
		}
		LinphoneManager.destroy();
		isOpen=false;
		System.gc();
	}
	
	public void turnOnCall(Context context){
		if (context == null){
			Log.e("turn on fail,context cannot be null");
			return;
		}
		initalize(context);
	}
	
	
	public boolean isEnableCall(){
		return isOpen;
	}

}

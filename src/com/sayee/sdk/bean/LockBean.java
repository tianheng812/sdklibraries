package com.sayee.sdk.bean;
import java.io.Serializable;

public class LockBean implements Serializable{

	private static final long serialVersionUID = 1212512535L;
	private String lock_name;
	private String domain_sn;
	private String sip_number;
	private String lock_parent_name;
	private String username;

	public String getLock_name() {
		return this.lock_name;
	}

	public void setLock_name(String lock_name) {
		this.lock_name = lock_name;
	}

	public String getDomain_sn() {
		return this.domain_sn;
	}

	public void setDomain_sn(String domain_sn) {
		this.domain_sn = domain_sn;
	}

	public String getSip_number() {
		return this.sip_number;
	}

	public void setSip_number(String sip_number) {
		this.sip_number = sip_number;
	}
	
	public String getLock_parent_name() {
		return lock_parent_name;
	}

	public void setLock_parent_name(String lock_parent_name) {
		this.lock_parent_name = lock_parent_name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public LockBean() {}

	public LockBean(String lock_name, String domain_sn, String sip_number,String lock_parent_name,String username){
		super();
		this.lock_name = lock_name;
		this.domain_sn = domain_sn;
		this.sip_number = sip_number;
		this.lock_parent_name = lock_parent_name;
		this.username = username;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "LockListBean [lock_name=" + lock_name + ", domain_sn="
				+ domain_sn + ", sip_number=" + sip_number
				+ ", lock_parent_name=" + lock_parent_name + ", username="
				+ username + "]";
	}
	
	
	
}
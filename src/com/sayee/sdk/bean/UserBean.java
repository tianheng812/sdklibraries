package com.sayee.sdk.bean;

import java.io.Serializable;

public class UserBean implements Serializable{

	private String user_sip;
    private int fs_port;
    private String user_password;
    private String fs_ip;

    public void setUser_sip(String user_sip) {
        this.user_sip = user_sip;
    }

    public void setFs_port(int fs_port) {
        this.fs_port = fs_port;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public void setFs_ip(String fs_ip) {
        this.fs_ip = fs_ip;
    }

    public String getUser_sip() {
        return user_sip;
    }

    public int getFs_port() {
        return fs_port;
    }

    public String getUser_password() {
        return user_password;
    }

    public String getFs_ip() {
        return fs_ip;
    }

	@Override
	public String toString() {
		return "UserBean [user_sip=" + user_sip + ", fs_port=" + fs_port
				+ ", user_password=" + user_password + ", fs_ip=" + fs_ip + "]";
	}
    
}

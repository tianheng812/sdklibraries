package com.sayee.sdk.bean;

import java.io.Serializable;

/**
 * 获取token成功后，返回的结果集
 */
public class TokenBean implements Serializable{
	
	private String token;
	private long dead_time;
	private int user_id;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	

	public long getDead_time() {
		return dead_time;
	}

	public void setDead_time(long dead_time) {
		this.dead_time = dead_time;
	}
	

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	@Override
	public String toString() {
		return "TokenResult [token=" + token + ", dead_time=" + dead_time
				+ ", user_id=" + user_id + "]";
	}
	
}


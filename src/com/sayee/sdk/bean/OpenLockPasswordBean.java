package com.sayee.sdk.bean;

public class OpenLockPasswordBean {

	private String random_pw;
	private long randomkey_dead_time;
	
	public String getRandom_pw() {
		return random_pw;
	}
	public void setRandom_pw(String random_pw) {
		this.random_pw = random_pw;
	}
	public long getRandomkey_dead_time() {
		return randomkey_dead_time;
	}
	public void setRandomkey_dead_time(long randomkey_dead_time) {
		this.randomkey_dead_time = randomkey_dead_time;
	}
	@Override
	public String toString() {
		return "OpenLockBean [random_pw=" + random_pw
				+ ", randomkey_dead_time=" + randomkey_dead_time + "]";
	}
	
	

}

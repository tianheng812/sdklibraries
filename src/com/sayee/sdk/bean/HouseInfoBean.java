package com.sayee.sdk.bean;

import java.util.List;

public class HouseInfoBean {
	
	private String sip_host_addr;
	private String sip_host_port;
	private List<HouseBean> sipInfoList;
	//private List<String> tagList;
	private String registrationTimeout;
	private String transport;
	private String iosIsShow;
	
	public String getSip_host_addr() {
		return sip_host_addr;
	}
	public void setSip_host_addr(String sip_host_addr) {
		this.sip_host_addr = sip_host_addr;
	}
	public String getSip_host_port() {
		return sip_host_port;
	}
	public void setSip_host_port(String sip_host_port) {
		this.sip_host_port = sip_host_port;
	}
	public List<HouseBean> getSipInfoList() {
		return sipInfoList;
	}
	public void setSipInfoList(List<HouseBean> sipInfoList) {
		this.sipInfoList = sipInfoList;
	}
	public String getRegistrationTimeout() {
		return registrationTimeout;
	}
	public void setRegistrationTimeout(String registrationTimeout) {
		this.registrationTimeout = registrationTimeout;
	}
	public String getTransport() {
		return transport;
	}
	public void setTransport(String transport) {
		this.transport = transport;
	}
	public String getIosIsShow() {
		return iosIsShow;
	}
	public void setIosIsShow(String iosIsShow) {
		this.iosIsShow = iosIsShow;
	}
	@Override
	public String toString() {
		return "HouseInfoBean [sip_host_addr=" + sip_host_addr
				+ ", sip_host_port=" + sip_host_port + ", sipInfoList="
				+ sipInfoList + ", registrationTimeout=" + registrationTimeout
				+ ", transport=" + transport + ", iosIsShow=" + iosIsShow + "]";
	}
	
	
	
	

}

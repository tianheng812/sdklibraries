package com.sayee.sdk.bean;

public class ChildAccountBean {
	
	private boolean is_called_number;
	private String mobile_phone;
	private String username;
	private String alias;

	public void setIs_called_number(boolean is_called_number) {
		this.is_called_number = is_called_number;
	}

	public void setMobile_phone(String mobile_phone) {
		this.mobile_phone = mobile_phone;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public boolean isIs_called_number() {
		return is_called_number;
	}

	public String getMobile_phone() {
		return mobile_phone;
	}

	public String getUsername() {
		return username;
	}

	public String getAlias() {
		return alias;
	}

	@Override
	public String toString() {
		return "ChildAccountBean [is_called_number=" + is_called_number
				+ ", mobile_phone=" + mobile_phone + ", username=" + username
				+ ", alias=" + alias + "]";
	}

}

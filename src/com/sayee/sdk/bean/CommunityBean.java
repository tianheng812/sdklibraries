package com.sayee.sdk.bean;

import java.io.Serializable;

public class CommunityBean implements Serializable{

	private int fport;
	private int fuser_id;
	private String fip;
	private String fneib_name;
	private String id;
	private String ffs_ip;
	private int ffs_port;
	public int getFport() {
		return fport;
	}
	public void setFport(int fport) {
		this.fport = fport;
	}
	public int getFuser_id() {
		return fuser_id;
	}
	public void setFuser_id(int fuser_id) {
		this.fuser_id = fuser_id;
	}
	public String getFip() {
		return fip;
	}
	public void setFip(String fip) {
		this.fip = fip;
	}
	public String getFneib_name() {
		return fneib_name;
	}
	public void setFneib_name(String fneib_name) {
		this.fneib_name = fneib_name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFfs_ip() {
		return ffs_ip;
	}
	public void setFfs_ip(String ffs_ip) {
		this.ffs_ip = ffs_ip;
	}
	public int getFfs_port() {
		return ffs_port;
	}
	public void setFfs_port(int ffs_port) {
		this.ffs_port = ffs_port;
	}
	@Override
	public String toString() {
		return "HouseBean [fport=" + fport + ", fuser_id=" + fuser_id
				+ ", fip=" + fip + ", fneib_name=" + fneib_name + ", id=" + id
				+ ", ffs_ip=" + ffs_ip + ", ffs_port=" + ffs_port + "]";
	}
	
	

}

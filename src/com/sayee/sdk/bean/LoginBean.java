package com.sayee.sdk.bean;

import java.io.Serializable;


public class LoginBean implements Serializable{
	
	private static final long serialVersionUID = 1866669700L;
	private String username;
	private String random_num;
	private String token;
	private String user_id;
	private String user_password;
	private String user_sip;
	private boolean register_yet;
	private String gestire_pwd;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getRandom_num() {
		return random_num;
	}
	public void setRandom_num(String random_num) {
		this.random_num = random_num;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public boolean isRegister_yet() {
		return register_yet;
	}
	public void setRegister_yet(boolean register_yet) {
		this.register_yet = register_yet;
	}
	public String getGestire_pwd() {
		return this.gestire_pwd;
	}

	public void setGestire_pwd(String gestire_pwd) {
		this.gestire_pwd = gestire_pwd;
	}
	
	public String getUser_sip() {
		return user_sip;
	}
	public void setUser_sip(String user_sip) {
		this.user_sip = user_sip;
	}
	@Override
	public String toString() {
		return "LoginBean [username=" + username + ", random_num=" + random_num
				+ ", token=" + token + ", user_id=" + user_id
				+ ", user_password=" + user_password + ", user_sip=" + user_sip
				+ ", register_yet=" + register_yet + ", gestire_pwd="
				+ gestire_pwd + "]";
	}
	
}


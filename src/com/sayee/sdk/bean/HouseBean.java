package com.sayee.sdk.bean;

public class HouseBean {

	private String house_number;
	private boolean is_owner;
	private int disturbing;//是否设置免打扰
	private String house_id;
	private String house_address;
	private boolean isShow;//是否显示

	public void setHouse_number(String house_number) {
		this.house_number = house_number;
	}

	public void setIs_owner(boolean is_owner) {
		this.is_owner = is_owner;
	}

	public void setDisturbing(int disturbing) {
		this.disturbing = disturbing;
	}

	public void setHouse_id(String house_id) {
		this.house_id = house_id;
	}

	public void setHouse_address(String house_address) {
		this.house_address = house_address;
	}

	public String getHouse_number() {
		return house_number;
	}

	public boolean isIs_owner() {
		return is_owner;
	}

	public int getDisturbing() {
		return disturbing;
	}

	public String getHouse_id() {
		return house_id;
	}

	public String getHouse_address() {
		return house_address;
	}

	public boolean isShow() {
		return isShow;
	}

	public void setIsShow(boolean isShow) {
		this.isShow = isShow;
	}

}

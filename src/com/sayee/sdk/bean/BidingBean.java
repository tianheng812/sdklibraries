package com.sayee.sdk.bean;

import java.io.Serializable;

/**
 * 绑定小区时，返回的集合
 *
 */
public class BidingBean implements Serializable{

	/*"neibor_id": {//该社区的基本信息，原来登陆接口返回的neibor_id_list不再返回，该处返回其中一个社区的信息（只有在can_bindding为true才不为空，否则为空）
	            "neighborhoods_id": "8a29ed6753404ef4015340de506a0004",
	            "fremark": "碧桂园",
	            "faddress": "广州市",
	            "fneibname": "碧桂园",
	            "department_id": "40288167557b22f301557b27dcac0001"
	        },
	        "can_binding": true//判断用户是否可以绑定该社区，true为可以，false为不可以
	 */
	
	private boolean can_binding;
	private NeiborIdBean neibor_id  ;
	public boolean getCan_binding() {
		return this.can_binding;
	}

	public NeiborIdBean getNeibor_id() {
		return this.neibor_id;
	}


	public void setNeibor_id(NeiborIdBean neibor_id) {
		this.neibor_id = neibor_id;
	}
	
	

	public void setCan_binding(boolean can_binding) {
		this.can_binding = can_binding;
	}

	public String toString() {
		return "CanbindBean [neibor_id = " + neibor_id + ", can_binding = " + can_binding + "]";

	}
}

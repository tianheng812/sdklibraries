package com.sayee.sdk.utils;

import java.util.Stack;

import android.app.Activity;

public class ActivitysManager {

	private static Stack<Activity> activityStack;
	private static ActivitysManager instance;

	private ActivitysManager() {
	}

	public static ActivitysManager getActivityManager() {
		if (instance == null) {
			instance = new ActivitysManager();
		}
		return instance;
	}

	public void popActivity() {
		if(activityStack==null) return;
		Activity activity = activityStack.lastElement();
		if (activity != null) {
			activity.finish();
			if(activityStack!=null) {
				activityStack.remove(activity);
			}
			activity = null;
		}
	}

	public void popActivity(Activity activity) {
		if (activity != null) {
			activity.finish();
			if(activityStack!=null) {
				activityStack.remove(activity);
			}
			activity = null;
		}
	}

	public Activity currentActivity() {
		if (activityStack==null||activityStack.size() == 0) {
			return null;
		}
		Activity activity = activityStack.lastElement();
		return activity;
	}

	public void pushActivity(Activity activity) {
		if (activityStack == null) {
			activityStack = new Stack<Activity>();
		}
		activityStack.add(activity);
	}

	public void popAllActivityExceptOne(Class cls) {
		while (true) {
			Activity activity = currentActivity();
			if (activity == null) {
				break;
			}
			if (activity.getClass().equals(cls)) {
				break;
			}
			popActivity(activity);
		}
	}

	public void exitApp() {
		while (true) {
			Activity activity = currentActivity();
			if (activity == null) {
				break;
			}
			popActivity(activity);
		}
	}
}

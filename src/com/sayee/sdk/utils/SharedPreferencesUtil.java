package com.sayee.sdk.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferencesUtil {

	// 存储的sharedpreferences文件名
	private static final String FILE_NAME = "sayee_save_file_name";
	public static final String SAYEE_TWO_URL_KEY = "sayee_tow_url_key";
	public static final String SAYEE_USER_NAME_KEY = "sayee_user_name_key";
	public static final String SAYEE_USER_PASSWORD_KEY = "sayee_user_password_key";
	public static final String SAYEE_USER_TOKEN_KEY = "sayee_user_token_key";
	public static final String SAYEE_USER_IP_KEY = "sayee_user_ip_key";
	public static final String SAYEE_USER_SIP_NAME_KEY = "sayee_user_sip_name_key";
	public static final String SAYEE_USER_SIP_PASSWORD_KEY = "sayee_user_sip_password_key";
	public static final String SAYEE_USER_SIP_DOMAIN_KEY = "sayee_user_sip_domain_key";
	public static final String SAYEE_USER_SIP_PORT_KEY = "sayee_user_sip_port_key";
	public static final String SAYEE_USER_SIP_TIME_KEY = "sayee_user_sip_time_key";
	public static final String SAYEE_USER_NEIGBOR_ID_KEY = "sayee_user_neigbor_id_key";
	public static final String SAYEE_USER_NEIGBOR_NAME_KEY = "sayee_user_neigbor_name_key";
	public static final String SAYEE_SIP_IS_CALL_KEY = "sayee_sip_is_call_key";
	public static final String SAYEE_DOMAIN_SN_KEY = "sayee_domain_sn_key";
	public static final String SAYEE_TYPE_KEY = "sayee_type_key";
	public static final String SAYEE_TO_SIP_NUMBER_KEY = "sayee_to_sip_number_key";
	public static final String SAYEE_DEAL_TIME_KEY = "sayee_deal_time";
	public static final String SAYEE_NEIBOR_ID_KEY = "sayee_neibor_id_key";
	public static final String SAYEE_IS_DISTURBING_KEY = "sayee_is_disturbing_key";// 免打扰
	public static final String SAYEE_HOUSE_ID_KEY = "sayee_house_id_key";// 房屋id key
	public static final String SAYEE_POSITION_KEY = "sayee_position_key";// 设置免打扰的位置key
	public static final String SAYEE_ADD_ACCOUNT_NUMBER_KEY = "sayee_add_account_number_key";// 添加子账号的key
	public static final String SAYEE_ADD_ACCOUNT_ALIAS_KEY = "sayee_add_account_alias_key";// 添加子账号的别名key
	public static final String SAYEE_IS_ADD_ACCOUNT_KEY = "sayee_is_add_account_key";// 是否添加子账号的key
	public static final String SAYEE_IS_CALLED_NUMBER_KEY = "sayee_is_called_number_key";// 是否是紧急被叫号key
	public static final String SAYEE_CALLED_NUMBER_KEY = "sayee_called_number_key";// 紧急被叫号key
	public static final String SAYEE_CALLED_NUMBER_IS_MY_KEY = "sayee_called_number_is_my_key";// 紧急被叫号是不是自己 key
	public static final String SAYEE_IS_CALLED_KEY = "sayee_is_called_key";// 是不是紧急被叫号 key
	public static final String SAYEE_USER_ID_KEY="sayee_user_id_key";//user_id key
	public static final String SAYEE_NEI_NAME_KEY="sayee_nei_name_key";//nei_name key
	public static final String SAYEE_IN_PATH_KEY="sayee_in_path_key";//切换社区时，选中的社区的path  key
	public static final String SAYEE_IP_ID_KEY="sayee_ip_id_key";//切换社区时，选中的社区的id  key
	public static final String SAYEE_LAST_SAVE_PAHT_KEY="sayee_last_save_paht_key";//最近一次保存的ip
	
	/**
	 * 当视频来时，是否允许响铃
	 */
	public static final String SAYEE_ALLOW_RINGING_KEY = "sayee_allow_ringing_key";
	/**
	 * 当视频来时，是否允许震动
	 */
	public static final String SAYEE_ALLOW_VIBRATE_KEY = "sayee_allow_vibrate_key";

	private static SharedPreferences sharedPreferences = null;

	public static SharedPreferences sharedPreferences(Context context) {
		if (sharedPreferences == null) {
			sharedPreferences = context.getSharedPreferences(FILE_NAME,
					Context.MODE_PRIVATE);
		}
		return sharedPreferences;
	}

	/**
	 * 保存数据到文件
	 * 
	 * @param context
	 * @param key
	 * @param data
	 */
	public static void saveData(Context context, String key, Object data) {
		if (data == null)
			return;
		String type = data.getClass().getSimpleName();
		SharedPreferences sharedPreferences = sharedPreferences(context);
		Editor editor = sharedPreferences.edit();

		if ("Integer".equals(type)) {
			editor.putInt(key, (Integer) data);
		} else if ("Boolean".equals(type)) {
			editor.putBoolean(key, (Boolean) data);
		} else if ("String".equals(type)) {
			editor.putString(key, (String) data);
		} else if ("Float".equals(type)) {
			editor.putFloat(key, (Float) data);
		} else if ("Long".equals(type)) {
			editor.putLong(key, (Long) data);
		}

		editor.commit();
	}

	/**
	 * 从文件中读取数据
	 * 
	 * @param context
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static Object getData(Context context, String key, Object defValue) {
		if (key == null)
			return null;
		String type = defValue.getClass().getSimpleName();
		SharedPreferences sharedPreferences = sharedPreferences(context);
		// defValue为为默认值，如果当前获取不到数据就返回它
		if ("Integer".equals(type)) {
			return sharedPreferences.getInt(key, (Integer) defValue);
		} else if ("Boolean".equals(type)) {
			return sharedPreferences.getBoolean(key, (Boolean) defValue);
		} else if ("String".equals(type)) {
			return sharedPreferences.getString(key, (String) defValue);
		} else if ("Float".equals(type)) {
			return sharedPreferences.getFloat(key, (Float) defValue);
		} else if ("Long".equals(type)) {
			return sharedPreferences.getLong(key, (Long) defValue);
		}

		return null;
	}

	public static String getTwoUrl(Context context) {
		SharedPreferences sharedPreferences = sharedPreferences(context);
		return sharedPreferences.getString(SAYEE_TWO_URL_KEY, "");
	}

	/**
	 * 从文件中读取数据
	 * 
	 * @param context
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static String getDataString(Context context, String key) {
		if (key == null)
			return null;
		SharedPreferences sharedPreferences = sharedPreferences(context);
		// defValue为为默认值，如果当前获取不到数据就返回它
		return sharedPreferences.getString(key, "");
	}

	/**
	 * 读取TOKEN
	 * 
	 * @return
	 */
	public static String getToken(Context context) {
		return sharedPreferences(context).getString(SAYEE_USER_TOKEN_KEY, "");
	}

	/**
	 * 读取用戶名
	 * 
	 * @return
	 */
	public static String getUserName(Context context) {
		return sharedPreferences(context).getString(SAYEE_USER_NAME_KEY, "");
	}

	/**
	 * 读取用戶名
	 * 
	 * @return
	 */
	public static String getUserID(Context context) {
		return sharedPreferences(context).getString(SAYEE_USER_IP_KEY, "");
	}

	/**
	 * 获取用户sip账号
	 * 
	 * @param context
	 * @return
	 */
	public static String getUserSipName(Context context) {
		return sharedPreferences(context)
				.getString(SAYEE_USER_SIP_NAME_KEY, "");
	}

	/**
	 * 获取社区id
	 * 
	 * @param context
	 * @return
	 */
	public static String getUserNeigborId(Context context) {
		return sharedPreferences(context).getString(SAYEE_USER_NEIGBOR_ID_KEY,
				"");
	}

	/**
	 * 获取用户password
	 * 
	 * @param context
	 * @return
	 */
	public static String getUserPassword(Context context) {
		return sharedPreferences(context)
				.getString(SAYEE_USER_PASSWORD_KEY, "");
	}

	/**
	 * 获取响铃状态
	 * 
	 * @param context
	 * @return true 表示允许响铃
	 */
	public static boolean isAllowRinging(Context context) {
		if (context == null)
			return true;
		return sharedPreferences(context).getBoolean(SAYEE_ALLOW_RINGING_KEY,
				true);
	}

	/**
	 * 设置是是否允许响铃
	 * 
	 * @param isAllowRinging
	 */
	public static void setAllowRinging(Context context, boolean isAllowRinging) {
		SharedPreferencesUtil.saveData(context,
				SharedPreferencesUtil.SAYEE_ALLOW_RINGING_KEY, isAllowRinging);
	}

	/**
	 * 获取震动状态
	 * 
	 * @param context
	 * @return true 表示允许震动
	 */
	public static boolean isAllowVibrate(Context context) {
		if (context == null)
			return true;
		return sharedPreferences(context).getBoolean(SAYEE_ALLOW_VIBRATE_KEY,
				true);
	}

	/**
	 * 设置是是否允许震动
	 * 
	 * @param isAllowVibrate
	 */
	public static void setAllowVibrate(Context context, boolean isAllowVibrate) {
		SharedPreferencesUtil.saveData(context,
				SharedPreferencesUtil.SAYEE_ALLOW_VIBRATE_KEY, isAllowVibrate);
	}

	public static String getUrl(Context context) {
		if (context == null)
			return "";
		SharedPreferences sharedPreferences = sharedPreferences(context);
		return sharedPreferences.getString(SAYEE_TWO_URL_KEY, "")
				+ sharedPreferences.getString(SAYEE_USER_NAME_KEY, "")
				+ sharedPreferences.getString(SAYEE_USER_NEIGBOR_ID_KEY, "");
	}

	/**
	 * 获取用户sip账号
	 * 
	 * @param context
	 * @return
	 */
	public static String getUserSipDomin(Context context) {
		return sharedPreferences(context).getString(SAYEE_USER_SIP_DOMAIN_KEY,
				"");
	}

	/**
	 * token过期时间
	 * 
	 * @param context
	 * @return
	 */
	public static long getDealTime(Context context) {
		return sharedPreferences(context).getLong(SAYEE_DEAL_TIME_KEY, 0l);
	}

	/**
	 * 是否是主叫
	 * 
	 * @param context
	 * @return
	 */
	public static boolean getSipIsCall(Context context) {
		return sharedPreferences(context).getBoolean(SAYEE_SIP_IS_CALL_KEY,
				false);
	}

	/**
	 * 门口机的域
	 * 
	 * @param context
	 * @return
	 */
	public static String getDomainSn(Context context) {
		return sharedPreferences(context).getString(SAYEE_DOMAIN_SN_KEY, "");
	}

	/**
	 * 开锁类型
	 * 
	 * @param context
	 * @return
	 */
	public static int getType(Context context) {
		return sharedPreferences(context).getInt(SAYEE_TYPE_KEY, 0);
	}

	/**
	 * 门开机的sip账号
	 * 
	 * @param context
	 * @return
	 */
	public static String getToSipNumber(Context context) {
		return sharedPreferences(context)
				.getString(SAYEE_TO_SIP_NUMBER_KEY, "");
	}

	/**
	 * 是否免打扰
	 * 
	 * @param context
	 * @return
	 */
	public static int getDisturbing(Context context) {
		return sharedPreferences(context).getInt(SAYEE_IS_DISTURBING_KEY, 0);
	}

	/**
	 * 获取houseId
	 * 
	 * @param context
	 * @return
	 */
	public static String getHouseId(Context context) {
		return sharedPreferences(context).getString(SAYEE_HOUSE_ID_KEY, "");
	}
	
	
	/**
	 * 获取设置免打扰的position
	 * @param context
	 * @return
	 */
	public static int getPostion(Context context) {
		return sharedPreferences(context).getInt(SAYEE_POSITION_KEY, -1);
	}
	
	
	/**
	 * 获取user_id
	 * @param context
	 * @return
	 */
	public static String getUesrId(Context context){
		return sharedPreferences(context).getString(SAYEE_USER_ID_KEY, "");
	}
	

}

package com.sayee.sdk.utils;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.linphone.LinphoneManager;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.mediastream.Log;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.sayee.sdk.Consts;
import com.sayee.sdk.HttpRespListener;
import com.sayee.sdk.LinphoneService;
import com.sayee.sdk.SayeeManager;
import com.sayee.sdk.activity.ChildAccountAddActivity;
import com.sayee.sdk.activity.ChildAccountEditActivity;
import com.sayee.sdk.activity.ChildAccountManageActivity;
import com.sayee.sdk.activity.HouseManageActivity;
import com.sayee.sdk.activity.LockListActivity;
import com.sayee.sdk.bean.HouseBean;
import com.sayee.sdk.bean.HouseInfoBean;
import com.sayee.sdk.bean.OpenLockPasswordBean;
import com.sayee.sdk.result.BaseResult;
import com.sayee.sdk.result.ChildAccountResult;
import com.sayee.sdk.result.HouseResult;
import com.sayee.sdk.result.OpenLockPasswordResult;

public class ToolsUtil {

	private ToolsUtil() {
	}

	/**
	 * MD5加密字符串
	 * 
	 * @param s
	 *            要加密的字符串
	 * @return 返回加密后的结果
	 */
	public final static String MD5(String s) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			byte[] btInput = s.getBytes();
			// 获得MD5摘要算法的 MessageDigest 对象
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			// 使用指定的字节更新摘要
			mdInst.update(btInput);
			// 获得密文
			byte[] md = mdInst.digest();
			// 把密文转换成十六进制的字符串形式
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获取设备uuid
	 * 
	 * @param context
	 * @return
	 */
	public final static String getUuid(Context context) {
		if (context == null)
			return null;
		return Settings.Secure.getString(context.getContentResolver(),
				Settings.Secure.ANDROID_ID);
	}

	/**
	 * 跳转到一键开门界面
	 * 
	 * @param context
	 *            上下文
	 * @param headpParams
	 *            请求头参数
	 * @param params
	 *            参数
	 * @param listener
	 *            请求完成回调
	 */
	public static void getToken(final Context context, String key,
			String userName, String app_id, String oldToken,
			final HttpRespListener listener) {
		HttpUtils.getToken(context, key, userName, app_id, oldToken, listener);
	}

	/**
	 * 跳转到一键开门界面
	 * 
	 * @param context
	 *            上下文
	 * @param headpParams
	 *            请求头参数
	 * @param params
	 *            参数
	 * @param listener
	 *            请求完成回调
	 */
	public static void goToOpenDoor(final Context context,
			Map<String, String> headpParams, Map<String, String> params,
			final HttpRespListener listener) {
		HttpUtils.goToOpenDoor(context, headpParams, params, listener);
	}

	/**
	 * 设置视频来时是否允许响铃
	 * 
	 * @param isAllowRinging
	 */
	public static void setAllowRinging(Context context, boolean isAllowRinging) {
		SharedPreferencesUtil.setAllowRinging(context, isAllowRinging);
	}

	/**
	 * 设置是是否允许震动
	 * 
	 * @param isAllowVibrate
	 */
	public static void setAllowVibrate(Context context, boolean isAllowVibrate) {
		SharedPreferencesUtil.setAllowVibrate(context, isAllowVibrate);
	}

	/**
	 * 呼叫门口机
	 * 
	 * @param context
	 *            上下文
	 * @param toSip
	 *            拨打门口机的sip账号
	 * @param intent
	 *            启动Activity的intent
	 */
	public static void outgoingCall(Context context, String toSip, Intent intent) {
		if (TextUtils.isEmpty(toSip)) {
			Log.e("toSip is null");
			return;
		}

		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		if (lc != null) {
			try {
				if (!LinphoneManager.getInstance()
						.acceptCallIfIncomingPending()) {
					LinphoneManager.getInstance().newOutgoingCall(toSip, "");
					if (context != null && intent != null) {
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						context.startActivity(intent);
					}
				}
			} catch (LinphoneCoreException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 挂断视频通话
	 */
	public static void hangUp() {
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		if (lc != null) {
			LinphoneCall currentCall = lc.getCurrentCall();
			if (currentCall != null) {
				lc.terminateCall(currentCall);
			} else if (lc.isInConference()) {
				lc.terminateConference();
			} else {
				lc.terminateAllCalls();
			}
		}
	}

	/**
	 * 从assets中获取Bitmap对象
	 * 
	 * @param context
	 * @param fileName
	 * @return
	 */
	public static Bitmap getBitmap4Assets(Context context, String fileName) {
		InputStream inStream = context.getClass().getClassLoader()
				.getResourceAsStream("assets/" + fileName);
		return BitmapFactory.decodeStream(inStream);
	}

	/**
	 * 从assets中获取Drawable对象
	 * 
	 * @param context
	 * @param fileName
	 * @return
	 */
	public static Drawable getDrawable4Assets(Context context, String fileName) {
		InputStream inStream = context.getClass().getClassLoader().getResourceAsStream("assets/" + fileName);
		return new BitmapDrawable(BitmapFactory.decodeStream(inStream));
	}

	/**
	 * 根据资源的名字获取其ID值
	 */
	public static int getIdByName(Context context, String className, String name) {
		String packageName = context.getPackageName();
		Class r = null;
		int id = 0;
		try {
			r = Class.forName(packageName + ".R");

			Class[] classes = r.getClasses();
			Class desireClass = null;

			for (int i = 0; i < classes.length; ++i) {
				if (classes[i].getName().split("\\$")[1].equals(className)) {
					desireClass = classes[i];
					break;
				}
			}

			if (desireClass != null)
				id = desireClass.getField(name).getInt(desireClass);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}

		return id;
	}
	

	/**
	 * Toast
	 * 
	 * @param context
	 * @param msg
	 */
	private static Toast toast = null;
	public static void toast(Context context, String msg) {
		if (context != null) {
			//Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
			if (toast == null) {
	            toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
	        } else {
	            toast.setText(msg);
	        }
	        toast.show();
		}
		  
	}

	/**
	 * 检测网络是否可用
	 * 
	 * @return
	 */
	public static boolean isNetworkConnected(Context context) {
		if (context == null)
			return false;
		try {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo ni = cm.getActiveNetworkInfo();
			return ni != null && ni.isConnectedOrConnecting();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static Timer timer = null;
	/**
	 * 刷新注册
	 * 
	 * @param context
	 */
	public static void refreshRegisters(final Context context) {
		if (context == null)
			return;
		if (isNetworkConnected(context)) {
			if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null) {
				 Log.i("sip刷新註冊");
				LinphoneManager.getLc().refreshRegisters();
			} else {
				System.gc();
				if (LinphoneService.isReady()) {
					Log.i("关闭，重新注册");
					SayeeManager.getInstance().turnOffCall(context);
					if (timer == null) {
						timer = new Timer();
					} else {
						timer.cancel();
						timer = new Timer();
					}
					timer.schedule(new TimerTask() {
						@Override
						public void run() {
							if(isRegistration()) return;
							SayeeManager.getInstance().turnOnCall(context);
							Log.i("重新注册---- on");
							int i = 0;
							while (!LinphoneService.isReady()) {
								try {
									Thread.sleep(30);
									i++;
									if (i == 200) {
										Log.i("重新注册----break");
										break;
									}
									Log.i("重新注册---- sleep");
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
							Log.i("重新注册---- end");
							if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null)
								LinphoneManager.getLc().refreshRegisters();
						}
					}, 1500);
				} else {
					Log.i("重新注册");
					SayeeManager.getInstance().turnOnCall(context);
				}
			}
		}
	}

	/**
	 * 判断是否在线
	 * 
	 * @return
	 */
	public static boolean isRegistration() {
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		if (lc != null) {
			LinphoneProxyConfig config = lc.getDefaultProxyConfig();
			if (config != null) {
				return config.isRegistered();
			}
		}
		return false;
	}
	
	
	
	
	/**
	 * token重新获取后，回调接口
	 */
	public static void callback(final Context context,int code,String newToken,long time){
		
		if(!TextUtils.isEmpty(newToken)){
			SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_TOKEN_KEY, newToken);
		}
		if(time!=0){
			SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_DEAL_TIME_KEY, time);
		}
		String path=SharedPreferencesUtil.getTwoUrl(context);
		if(TextUtils.isEmpty(path)) return;
		String userName=SharedPreferencesUtil.getUserName(context);//获取用户名
		if(TextUtils.isEmpty(userName)) return;
		
		final Map<String, String> headParams = new HashMap<String, String>();
		headParams.put("token", newToken);
		headParams.put("username", userName);
		
		switch (code) {
		case Consts.OPEN_LOCK_CODE://一键开锁
			String domain_sn=SharedPreferencesUtil.getDomainSn(context);
			int type=SharedPreferencesUtil.getType(context);
			String toSipUri=SharedPreferencesUtil.getToSipNumber(context);
			HttpUtils.openDoorLock(context,path,newToken, userName,domain_sn,type,toSipUri,null,new HttpRespListener() {
				@Override
				public void onSuccess(int code, BaseResult result) {
					ToolsUtil.toast(context, "已发送开锁请求");
				}
				
				@Override
				public void onFail(int code, String msg) {
					ToolsUtil.toast(context, msg);
				}
			});
			break;
		case Consts.PASSWORD_OPEN_LOCK_CODE://获取密码
			String sip_number=SharedPreferencesUtil.getToSipNumber(context);
			HttpUtils.getOpenLockPassword(path, newToken, userName, sip_number, new HttpRespListener() {
				@Override
				public void onSuccess(int code, BaseResult result) {
					OpenLockPasswordBean bean = ((OpenLockPasswordResult) result).getResult();
					Intent intent = new Intent();
					intent.setAction(Consts.SAYEE_RANDOM_CODE_ACTION);
					intent.putExtra(Consts.SAYEE_RANDOM_PASSWORD,bean.getRandom_pw());
					intent.putExtra(Consts.SAYEE_RANDOM_PASSWORD_DEADLINE,bean.getRandomkey_dead_time());
					context.sendBroadcast(intent);
				}
				
				@Override
				public void onFail(int code, String msg) {
					ToolsUtil.toast(context, "获取新token"+msg);
				}
			});
			break;
		case Consts.HOUSE_MESSAGE_CODE://房产信息
			String neigbor_id=SharedPreferencesUtil.getUserNeigborId(context);
			
			final Map<String, String> params = new HashMap<String, String>();
			params.put("username", userName);
			params.put("neigbor_id", neigbor_id);
			params.put("type", "1");
			HttpUtils.getHouseMessage(context, path, headParams, params,new HttpRespListener() {
						@Override
						public void onSuccess(int code, BaseResult result) {
							Log.i("HOUSE_MESSAGE_CODE   ---------  成功");
							HouseManageActivity.updateAdapter(result);
						}

						@Override
						public void onFail(int code, String msg) {
							Log.i("HOUSE_MESSAGE_CODE    --------- 失败");
							ToolsUtil.toast(context, msg);
						}
					});
			break;
		case Consts.DISTURBING_CODE://设置免打扰
			final Map<String, String> params1= new HashMap<String, String>();
			params1.put("house_id",  SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_HOUSE_ID_KEY));
			params1.put("is_disturbing", SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_IS_DISTURBING_KEY));
			
			HttpUtils.setCalledNumber(context, path, headParams, params1,
					new HttpRespListener() {

						@Override
						public void onSuccess(int code, BaseResult result) {
							HouseManageActivity.updateDisturbing(context,SharedPreferencesUtil.getPostion(context));
							Log.i("设置免打扰   ---------  成功");
						}

						@Override
						public void onFail(int code, String msg) {
							Log.i("设置免打扰   ---------  失败");
							ToolsUtil.toast(context, msg);
						}
					});
			break;
		case Consts.ADD_SUBACCOUNT_CODE://添加子账号
			final Map<String, String> params2= new HashMap<String, String>();
			params2.put("username", userName);
			params2.put("house_id",  SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_HOUSE_ID_KEY));
			params2.put("sub_account", SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_ADD_ACCOUNT_NUMBER_KEY));
			params2.put("is_add", "true");
			params2.put("alias", SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_ADD_ACCOUNT_ALIAS_KEY));
			
			HttpUtils.setSubAccount(context, path, headParams, params2,new HttpRespListener() {
				@Override
				public void onSuccess(int code, BaseResult result) {
					Log.i("添加子账号   ---------  成功");
					ToolsUtil.toast(context, "添加成功");
					if(ActivitysManager.getActivityManager().currentActivity()!=null){
						Intent intent=new Intent();
						intent.putExtra("isFresh", true);
						ActivitysManager.getActivityManager().currentActivity().setResult(Activity.RESULT_OK,intent);
						ActivitysManager.getActivityManager().popActivity();
					}
				}
	
				@Override
				public void onFail(int code, String msg) {
					Log.i("添加子账号   ---------  失败");
				  ToolsUtil.toast(context, msg);
				}
			});
			break;
			
		case Consts.DELETE_SUBACCOUNT_CODE://删除子账号
		case Consts.EDIT_SUBACCOUNT_CODE://修改备注成功

			final Map<String, String> params3 = new HashMap<String, String>();
			params3.put("username", userName);
			params3.put("house_id", SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_HOUSE_ID_KEY));
			params3.put("sub_account", SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_ADD_ACCOUNT_NUMBER_KEY));
			params3.put("alias", SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_ADD_ACCOUNT_ALIAS_KEY));
			final boolean isDelete=(Boolean) SharedPreferencesUtil.getData(context,  SharedPreferencesUtil.SAYEE_IS_ADD_ACCOUNT_KEY, false);
			if(isDelete){
				params3.put("is_add", "false");
			}else{
				params3.put("is_add", "true");
			}
			
			boolean isCall=(Boolean) SharedPreferencesUtil.getData(context,  SharedPreferencesUtil.SAYEE_IS_CALLED_NUMBER_KEY, false);
			params3.put("is_called_number", isCall+"");

			HttpUtils.setSubAccount(context, path, headParams, params3,new HttpRespListener() {
						@Override
						public void onSuccess(int code, BaseResult result) {
							if(isDelete){
								ToolsUtil.toast(context,"删除成功");
							}else{
								ToolsUtil.toast(context,"修改备注成功");
							}
							if(ActivitysManager.getActivityManager().currentActivity()!=null){
								ActivitysManager.getActivityManager().currentActivity().setResult(Activity.RESULT_OK);
								ActivitysManager.getActivityManager().popActivity();
							}
							Log.i("删除子账号   修改备注成功  ---------  成功");
						}

						@Override
						public void onFail(int code, String msg) {
							ToolsUtil.toast(context, msg);
							Log.i("删除子账号   修改备注成功   ---------  失败");
						}
					});
		break;
		case Consts.CHILD_ACCOUNTS_CODE://获取子账号管理
			final Map<String, String> params4 = new HashMap<String, String>();
			params4.put("username", userName);
			params4.put("house_id", SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_HOUSE_ID_KEY));
			HttpUtils.getSubAccount(context, path, headParams, params4,new HttpRespListener() {
				@Override
				public void onSuccess(int code, BaseResult result) {
					ChildAccountManageActivity.updateAdapter(result);
					Log.i("获取子账号管理   ---------  成功");
				}

				@Override
				public void onFail(int code, String msg) {
					ToolsUtil.toast(context, msg);
					Log.i("获取子账号管理   ---------  失败");
				}
			});
			break;
		case Consts.SET_CALL_ACCOUNT_CODE://设置紧急被叫号
			setCallAccount(context, path, userName, headParams);
			break;
		case Consts.IS_CAN_BINDING_CODE://绑定社区
			
			Map<String,String> params5=new HashMap<String, String>();
			final String inPath=SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_IN_PATH_KEY);
			final String nei_name=SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_NEI_NAME_KEY);
			final String ip_id=SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_IP_ID_KEY);
			String user_id=SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_USER_ID_KEY);
			
			params5.put("username", userName);
			params5.put("nei_name", nei_name);
			params5.put("user_id", user_id);
			
			HttpUtils.getIscanBind(context,inPath,headParams, params5, new HttpRespListener() {
				@Override
				public void onSuccess(int code, BaseResult result) {
					Log.i("绑定社区   ---------  成功");
					//LockListActivity.canBindSuccess(context,inPath, nei_name, ip_id, result);
				}
				
				@Override
				public void onFail(int code, String msg) {
					Log.i("绑定社区   ---------  失败");
					ToolsUtil.toast(context, msg);
				}
			});
			break;
			
		}
		
	}

	private static void setCallAccount(final Context context, final String path,final String userName, final Map<String, String> headParams) {
		final Map<String, String> params5 = new HashMap<String, String>();
		params5.put("username", userName);
		params5.put("house_id", SharedPreferencesUtil.getDataString(context, SharedPreferencesUtil.SAYEE_HOUSE_ID_KEY));
		final boolean isMy=(Boolean) SharedPreferencesUtil.getData(context, SharedPreferencesUtil.SAYEE_CALLED_NUMBER_IS_MY_KEY, false);
		final boolean isCalled=(Boolean) SharedPreferencesUtil.getData(context, SharedPreferencesUtil.SAYEE_IS_CALLED_KEY, false);
		String number=(String) SharedPreferencesUtil.getData(context, SharedPreferencesUtil.SAYEE_CALLED_NUMBER_KEY, userName);
		
		if(isMy){
			params5.put("called_number", userName);
		}else{
			params5.put("called_number", number);
		}

		HttpUtils.setCalledNumber(context, path, headParams, params5,new HttpRespListener() {
					@Override
					public void onSuccess(int code, BaseResult result) {
						Log.i("  设置紧急被叫号    ------- 成功");
						if (isCalled) {
							if(!isMy)
								ToolsUtil.toast(context,"设置紧急被叫号成功");
							ChildAccountEditActivity.callBackUpdate(true);
						} else {
							if(!isMy){
								SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_CALLED_NUMBER_IS_MY_KEY, true);
								setCallAccount(context, path, userName,headParams);
								ToolsUtil.toast(context,"取消紧急被叫号成功");
							}
							ChildAccountEditActivity.callBackUpdate(false);
						}
					}

					@Override
					public void onFail(int code, String msg) {
						Log.i("  设置紧急被叫号    ------- 失败");
						ToolsUtil.toast(context, msg);
					}
				});
	}
	
	
	/**
	 *截取后两位字符
	 * @param name
	 * @return
	 */
	public static String getEndTwoLength(String name){
		if(!TextUtils.isEmpty(name))
			return name.length() > 2 ? name.substring(name.length() - 2, name.length()) : name;
		return "";
	}
	
	
	/**
	 * 判断是否是有效的手机号码
	 * @param strPhone
	 * @return
	 */
	public static boolean checkPhone(String strPhone){
		Pattern pPhone = Pattern.compile("^1[3|4|5|7|8|][0-9]{9}$");
        Matcher mPhone = pPhone.matcher(strPhone);
        return mPhone.matches();
	}

	
	/**
	 * 获取attrs类型
	 * @param context
	 * @param name
	 * @return
	 */
	public static int[] getStyleableIntArray(Context context, String name) {
        try {
            Field[] fields = Class.forName(context.getPackageName() + ".R$styleable").getFields();//.与$ difference,$表示R的子类
            for (Field field : fields) {
                if (field.getName().equals(name)) {
                    int ret[] = (int[]) field.get(null);
                    return ret;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }
	
	
}

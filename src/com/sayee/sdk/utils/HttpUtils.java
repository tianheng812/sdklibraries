package com.sayee.sdk.utils;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences.AccountBuilder;
import org.linphone.core.LinphoneAddress.TransportType;
import org.linphone.core.LinphoneAuthInfo;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatMessage.State;
import org.linphone.core.LinphoneChatMessage.StateListener;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.mediastream.Log;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.sayee.sdk.Consts;
import com.sayee.sdk.HttpRespListener;
import com.sayee.sdk.LinphoneService;
import com.sayee.sdk.SayeeManager;
import com.sayee.sdk.activity.LockListActivity;
import com.sayee.sdk.bean.BidingBean;
import com.sayee.sdk.bean.NeiborBean;
import com.sayee.sdk.bean.NeiborIdBean;
import com.sayee.sdk.bean.UserBean;
import com.sayee.sdk.result.BaseResult;
import com.sayee.sdk.result.BidingResult;
import com.sayee.sdk.result.ChildAccountResult;
import com.sayee.sdk.result.CommunityListResult;
import com.sayee.sdk.result.HouseResult;
import com.sayee.sdk.result.LockListResult;
import com.sayee.sdk.result.NeiborResult;
import com.sayee.sdk.result.OpenLockPasswordResult;
import com.sayee.sdk.result.TokenResult;

@SuppressWarnings("deprecation")
public class HttpUtils {

	public static String baseUrl = "https://api.sayee.cn:28084";
	public static String GET_TOKEN_URL = baseUrl + "/sdk/get_ylb_token.json";//获取token
	public static String GET_NEIBOR_ID_URL = baseUrl + "/fir_platform/get_neibor_and_user_msg.json";
	public static String GET_COMMNITY_LIST_URL = baseUrl + "/users/get_nei_ip_list.json";//获取社区列表
	public static String POST_SAVE_NEI_BINDING_USER_URL = baseUrl + "/users/save_nei_binding_user.json";//保存用户绑定小区信息
	
	
	public static void changeUrl(){
		GET_TOKEN_URL = baseUrl + "/sdk/get_ylb_token.json";
	    GET_NEIBOR_ID_URL = baseUrl + "/fir_platform/get_neibor_and_user_msg.json";
	    GET_COMMNITY_LIST_URL = baseUrl + "/users/get_nei_ip_list.json";//获取社区列表
		POST_SAVE_NEI_BINDING_USER_URL = baseUrl + "/users/save_nei_binding_user.json";//保存用户绑定小区信息
	}

	private HttpUtils() {}
	
	/**
	 * 获取token 
	 * @param oldToken 旧的token
	 * @param listener 请求回调 
	 */
	public static void getToken(Context context,String userName,String oldToken,final HttpRespListener listener){
		getToken(context,null,userName,null,oldToken,listener);
	}
	

	/**
	 * 获取token
	 * 
	 * @param key
	 *            密钥
	 * @param userName
	 *            用户名
	 * @param app_id
	 *            app_id
	 * @param listener
	 *            请求完成的回调
	 */
	public static void getToken(final Context context,String key, String userName, String app_id,String oldToken,
			final HttpRespListener listener) {
		final Map<String, String> params = new HashMap<String, String>();
		if(TextUtils.isEmpty(oldToken)){
			params.put("key", key);
			params.put("username", userName);
			params.put("app_id", app_id);
		}else{
			params.put("username", userName);
			params.put("token", oldToken);
		}

		try {
			HttpURLConnectionUtil.doGet(GET_TOKEN_URL, null, params,
					new Handler() {
						@Override
						public void dispatchMessage(Message msg) {
							if (msg.what == 200) {
								try {
									if (msg.obj == null)
										return;
									JSONObject object = new JSONObject(((BaseResult) msg.obj).getMsg());
									int code = object.getInt("code");
									if (code == 0) {
										JSONObject resultJsonObject = object.getJSONObject("result");
										String token = resultJsonObject.getString("token");
										long dead_time=resultJsonObject.getLong("dead_time");
										//String myMsg = object.getString("msg");
										if (listener != null) {
											/*TokenResult tokenResult = new TokenResult();
											tokenResult.setCode(code);
											tokenResult.setMsg(myMsg);
											tokenResult.setToken(token);
											tokenResult.setDead_time(dead_time);*/
											TokenResult tokenResult = null;
											try {
												tokenResult = JsonParserUtil.parseJson2Object(TokenResult.class, object);
											} catch (Exception e) {
												e.printStackTrace();
											}
											listener.onSuccess(code,tokenResult);
											SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_TOKEN_KEY, token);
											SharedPreferencesUtil.saveData(context, Consts.SAYEE_DEAL_TIME, dead_time);
										}
									} else {
										if (listener != null) {
											String myMsg = object.getString("msg");
											listener.onFail(code, myMsg);
										}
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							} else {
								if (listener != null && msg.obj != null) {
									listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
								}
							}
						}
					});
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	
	/**
	 * 跳转到一键开门界面
	 * @param context  上下文
	 * @param headpParams 请求头参数
	 * @param params 参数
	 * @param listener 请求完成回调
	 */
	public static void goToOpenDoor(final Context context,Map<String,String> headpParams, Map<String,String> params,final HttpRespListener listener){
		if(context==null) {
			Log.e("context cannot be null");
			return;
		}
		
		if(headpParams==null){
			Log.e("headpParams cannot be null");
			return;
		}
		
		if(params==null){
			Log.e("params cannot be null");
			return;
		}
		
		final String token=headpParams.get("token");
		final String userName=headpParams.get("username");
		final String dealTime=headpParams.get("dealtime");
		final String user_id=params.get("user_id");
		
		if(TextUtils.isEmpty(token)){
			Log.e("token cannot be null");
			return;
		}
		if(TextUtils.isEmpty(userName)){
			Log.e("username cannot be null");
			return;
		}
		if(TextUtils.isEmpty(dealTime)){
			Log.e("dealtime cannot be null");
			return;
		}
		
		try {
			HttpURLConnectionUtil.doGet(GET_NEIBOR_ID_URL, headpParams, params, new Handler(){
				@Override
				public void dispatchMessage(Message msg) {
					if (msg.what == 200){
						try {
							if(msg.obj==null) return;
							JSONObject object = new JSONObject(((BaseResult) msg.obj).getMsg());
							int code =object.getInt("code");
							if(code==0){
								try {
									NeiborResult neiborResult = JsonParserUtil.parseJson2Object(NeiborResult.class, object);
									if(listener!=null){
										listener.onSuccess(code, neiborResult);
										
										if(neiborResult==null) return;
										final NeiborBean neiborBean=neiborResult.getResult();
										if(neiborBean==null) return;
										long mdealTime=0;
										try{
											mdealTime=Long.parseLong(dealTime);
										}catch(Exception e){
											e.printStackTrace();
										}
										
										String path="https://"+neiborBean.getFip()+":"+neiborBean.getFport();
										final String outTime=neiborBean.getRegistrationTimeout();
										Intent intent=new Intent(context,LockListActivity.class);
										intent.putExtra(LockListActivity.PATH, path);
										intent.putExtra(LockListActivity.TOKEN, token);
										intent.putExtra(LockListActivity.USER_NAME, userName);
										intent.putExtra(LockListActivity.NEIGBOR_ID,neiborBean.getNeibor_id());
										intent.putExtra(LockListActivity.NEIGBOR_NAME,neiborBean.getFneib_name());
										intent.putExtra(LockListActivity.DEAL_TIME, mdealTime);
										intent.putExtra(LockListActivity.USER_ID, user_id);
										intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										context.startActivity(intent);
										
										UserBean userBean=neiborBean.getUser_msg();
										if(userBean==null) return;
										final String sip_number=userBean.getUser_sip();
										final String sip_password=userBean.getUser_password();
										final String sip_domin=userBean.getFs_ip();
										final int sip_port=userBean.getFs_port();
										
										SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_TWO_URL_KEY, path);
										SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_SIP_DOMAIN_KEY, sip_domin);
										SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_NAME_KEY, userName);
										SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_TOKEN_KEY, token);
										SharedPreferencesUtil.saveData(context, Consts.SAYEE_DEAL_TIME, mdealTime);
										SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_ID_KEY, user_id);
										
										SayeeManager.getInstance().turnOnCall(context);
										 new Thread(){
									            @Override
									            public void run() {
									                while(!LinphoneService.isReady()){
									                    try {
									                        Thread.sleep(30);
									                    } catch (InterruptedException e) {
									                        e.printStackTrace();
									                    }
									                }
									                String sipDomin=sip_domin;
									                int sipPort=sip_port;
									                if(TextUtils.isEmpty(sipDomin)){
									                	sipDomin=neiborBean.getFfs_ip();
													}
													
													if(sipPort==0){
														sipPort=neiborBean.getFfs_port();
													}
									                
									                sipRegister(neiborBean,sip_number,sip_password,sip_domin, sip_port,outTime);
									            }
												
										 }.start();
										
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}else{
								if(listener!=null){
									String myMsg=object.getString("msg");
									listener.onFail(code, myMsg);
								}
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}else{
						if(listener!=null&&msg.obj!=null){
							listener.onFail(msg.what, ((BaseResult) msg.obj).getMsg());	
						}
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}

	/**
	 * 跳转到一键开门界面
	 * @param context 上下文
	 * @param token   令牌
	 * @param dealTime  过期时间
	 * @param userName  用户名
	 * @param neibor_flag 社区标识
	 * @param listener  请求完成回调
	 */
	public static void goToOpenDoor(final Context context,final String token,final long dealTime,final String userName,String neibor_flag,final HttpRespListener listener){
		if(context==null) {
			Log.e("context cannot be null");
			return;
		}
		final Map<String, String> headpParams = new HashMap<String, String>();
		headpParams.put("token",token);
		headpParams.put("username", userName);
		final Map<String, String> params = new HashMap<String, String>();
		params.put("username", userName);
		params.put("neibor_flag", neibor_flag);
		
		try {
			HttpURLConnectionUtil.doGet(GET_NEIBOR_ID_URL, headpParams, params, new Handler(){
				@Override
				public void dispatchMessage(Message msg) {
					if (msg.what == 200){
						try {
							if(msg.obj==null) return;
							JSONObject object = new JSONObject(((BaseResult) msg.obj).getMsg());
							int code =object.getInt("code");
							if(code==0){
								try {
									NeiborResult neiborResult = JsonParserUtil.parseJson2Object(NeiborResult.class, object);
									if(listener!=null){
										listener.onSuccess(code, neiborResult);
										
										if(neiborResult==null) return;
										final NeiborBean neiborBean=neiborResult.getResult();
										if(neiborBean==null) return;
										
										final String outTime=neiborBean.getRegistrationTimeout();
										String path="https://"+neiborBean.getFip()+":"+neiborBean.getFport();
										Intent intent=new Intent(context,LockListActivity.class);
										intent.putExtra(LockListActivity.PATH, path);
										intent.putExtra(LockListActivity.TOKEN, token);
										intent.putExtra(LockListActivity.USER_NAME, userName);
										intent.putExtra(LockListActivity.NEIGBOR_ID,neiborBean.getNeibor_id());
										intent.putExtra(LockListActivity.NEIGBOR_NAME,neiborBean.getFneib_name());
										intent.putExtra(LockListActivity.DEAL_TIME, dealTime);
										intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										context.startActivity(intent);
										
										UserBean userBean=neiborBean.getUser_msg();
										if(userBean==null) return;
										final String sip_number=userBean.getUser_sip();
										final String sip_password=userBean.getUser_password();
										final String sip_domin=neiborBean.getFfs_ip();
										final int sip_port=neiborBean.getFfs_port();
										
										SharedPreferencesUtil.saveData(context, Consts.SAYEE_PATH, path);
										SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_NEIGBOR_ID_KEY, neiborBean.getNeibor_id());
										SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_SIP_DOMAIN_KEY, sip_domin);
										SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_NAME_KEY, userName);
										SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_TOKEN_KEY, token);
										SharedPreferencesUtil.saveData(context, Consts.SAYEE_DEAL_TIME, dealTime);
										 
										SayeeManager.getInstance().turnOnCall(context);
										new Thread(){
									            @Override
									            public void run() {
									                while(!LinphoneService.isReady()){
									                    try {
									                        Thread.sleep(30);
									                    } catch (InterruptedException e) {
									                        e.printStackTrace();
									                    }
									                }
									                String sipDomin=sip_domin;
									                int sipPort=sip_port;
									                if(TextUtils.isEmpty(sipDomin)){
									                	sipDomin=neiborBean.getFfs_ip();
													}
													
													if(sipPort==0){
														sipPort=neiborBean.getFfs_port();
													}
									                
									                sipRegister(neiborBean,sip_number,sip_password,sip_domin, sip_port,outTime);
									            }
												
										 }.start();
										
										
									}
								} catch (Exception e) {
									e.printStackTrace();
									if(listener!=null){
										listener.onFail(code, "数据解析错误");
									}
								}
							}else{
								if(listener!=null){
									String myMsg=object.getString("msg");
									listener.onFail(code, myMsg);
								}
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}else{
						if(listener!=null&&msg.obj!=null){
							listener.onFail(msg.what, ((BaseResult) msg.obj).getMsg());	
						}
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}

	

	/**
	 * 获取锁列表
	 * 
	 * @param context
	 */
	public static void getLockList(String path, String token, String userName,String neigbor_id, final HttpRespListener listener) {
		final Map<String, String> headParams = new HashMap<String, String>();
		headParams.put("username", userName);
		headParams.put("token", token);
		final Map<String, String> params = new HashMap<String, String>();
		params.put("username", userName);
		params.put("neigbor_id", neigbor_id);

		try {
			HttpURLConnectionUtil.doGet(path + "/config/my_lock_list.json",headParams, params, new Handler() {
						@Override
						public void dispatchMessage(Message msg) {
							if (msg.what == 200) {
								try {
									JSONObject objectJson = new JSONObject(((BaseResult) msg.obj).getMsg());
									int code = objectJson.getInt("code");
									if (code == 0) {
										if (listener != null) {
											JSONObject resultJson = objectJson.getJSONObject("result");
											LockListResult result = JsonParserUtil.parseJson2Object(LockListResult.class,resultJson);
											result.setMsg(objectJson.getString("msg"));
											listener.onSuccess(code, result);
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								if (listener != null) {
									listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
								}
							}
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
		
	
	/**
	 * 开锁
	 * 
	 * @param userName
	 *            用户名
	 * @param token
	 *            令牌 token
	 * @param domain_sn
	 *            门口机关联域的sn
	 * @param type
	 *            开锁类型 0：直接开锁 1：通话中开锁
	 */
	private static long firstTime = 0;
	private static int count = 0;
	public static void openDoorLock(Context context, String path, String token,String userName, String domain_sn, int type, String toSipUri,String toSipDomin,
			final HttpRespListener listener) {
		
		 long secondTime = System.currentTimeMillis();
         if (secondTime - firstTime >0 &&secondTime - firstTime <= 7000) { // 如果两次按键时间间隔大于2秒，则不退出
        	 count++;
        	 if(count>1){
        		 ToolsUtil.toast(context, "您操作过快，请稍后再试！");
        	 }
        	 return;
         }else{
        	 count=0;
         }
		
		if(TextUtils.isEmpty(toSipDomin))
			toSipDomin = SharedPreferencesUtil.getUserSipDomin(context);
		HashMap<String, String> headParams = new HashMap<String, String>();
		headParams.put("username", userName);
		headParams.put("token", token);

		long time = System.currentTimeMillis();
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("username", userName);
		params.put("domain_sn", domain_sn);
		params.put("time", time + "");
		params.put("type", type + "");

		String sipUri = "sip:" + toSipUri + "@" + toSipDomin;
		String mess = "{\"ver\":\"1.0\",\"typ\":\"req\",\"cmd\":\"0610\",\"tgt\":\""
				+ domain_sn
				+ "\",\"cnt\":{\"username\":\""
				+ userName
				+ "\",\"type\":\"" + type + "\",\"time\":\"" + time + "\"}}";
		// 发送消息
		if(!TextUtils.isEmpty(toSipDomin))
			sendTextMessage(sipUri, mess);

		if (TextUtils.isEmpty(token) || TextUtils.isEmpty(userName)||TextUtils.isEmpty(domain_sn)) {
			return;
		}

		try {
			HttpURLConnectionUtil.doPost(context, path + "/device/remote_unlock.json", headParams, params, new Handler() {
						@Override
						public void dispatchMessage(Message msg) {
							if (msg.what == 200) {
								try {
									JSONObject object = new JSONObject(((BaseResult) msg.obj).getMsg());
									int code = object.getInt("code");
									if (code == 0) {
										 firstTime = System.currentTimeMillis();
										if (listener != null) {
											BaseResult result = new BaseResult();
											result.setCode(code);
											result.setMsg(object.getString("msg"));
											listener.onSuccess(code, result);
										}
									} else {
										if (listener != null) {
											listener.onFail(code,object.getString("msg"));
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								if (listener != null) {
									listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
								}
							}
						}
					}, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取开锁的密码
	 * 
	 * @param context
	 * @param listener
	 */
	public static void getOpenLockPassword(String path, String token,String userName, String sip_number, final HttpRespListener listener) {
		HashMap<String, String> headParams = new HashMap<String, String>();
		headParams.put("username", userName);
		headParams.put("token", token);

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("username", userName);
		params.put("sip_number", sip_number);
		try {
			HttpURLConnectionUtil.doGet(path + "/users/random_password.json",headParams, params, new Handler() {
						@Override
						public void dispatchMessage(Message msg) {
							if (msg.what == 200) {
								try {
									String resultJson = ((BaseResult) msg.obj).getMsg();
									JSONObject object = new JSONObject(resultJson);
									int code = object.getInt("code");
									if (code == 0) {
										if (listener != null) {
											BaseResult result = JsonParserUtil.parseJson2Object(OpenLockPasswordResult.class,resultJson);
											result.setCode(code);
											listener.onSuccess(code, result);
										}
									}else{
										if (listener != null) {
											BaseResult result = JsonParserUtil.parseJson2Object(OpenLockPasswordResult.class,resultJson);
											listener.onFail(code,result.getMsg());
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								if (listener != null) {
									listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
								}
							}
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 发送信息
	 * 
	 * @param sipUri
	 *            sip账号
	 * @param content
	 *            要发的内容
	 */
	private static void sendTextMessage(String sipUri, String content) {
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		LinphoneChatRoom chatRoom = null;
		if (lc != null) {
			chatRoom = lc.getOrCreateChatRoom(sipUri);
		}
		boolean isNetworkReachable = lc == null ? false : lc.isNetworkReachable();
		if ((chatRoom != null) && (content != null) && (content.length() > 0)
				&& isNetworkReachable) {
			LinphoneChatMessage localLinphoneChatMessage = chatRoom
					.createLinphoneChatMessage(content);
			chatRoom.sendMessage(localLinphoneChatMessage, new StateListener() {

				@Override
				public void onLinphoneChatMessageStateChanged(LinphoneChatMessage chatMessage, State status) {
				}
			});
			Log.i("Sent message current status: "+ localLinphoneChatMessage.getStatus());
		} else if (!isNetworkReachable) {
			Log.e("没有网络，请稍后再试");
		}
	}

	
	
	private static void sipRegister(NeiborBean neiborBean,String sip_number,String sip_password,String sip_domin,int sip_port,String timeOut) {
		//创建sip用户
		 LinphoneCore linphoneCore=LinphoneManager.getLcIfManagerNotDestroyedOrNull();
			if(linphoneCore!=null){
				LinphoneAuthInfo[] authInfosList=linphoneCore.getAuthInfosList();
					if(linphoneCore!=null&&authInfosList!=null&&authInfosList.length>0){
						linphoneCore.clearProxyConfigs();
						linphoneCore.clearAuthInfos();
					}
					Log.i("-----创建----");
					AccountBuilder builder = new AccountBuilder(linphoneCore);
					builder.setUsername(sip_number);
					builder.setPassword(sip_password);
					if("tcp".equalsIgnoreCase(neiborBean.getTransport())){
						builder.setTransport(TransportType.LinphoneTransportTcp);
					}else if("udp".equalsIgnoreCase(neiborBean.getTransport())){
						builder.setTransport(TransportType.LinphoneTransportUdp);
					}else if("tls".equalsIgnoreCase(neiborBean.getTransport())){
						builder.setTransport(TransportType.LinphoneTransportTls);
					}else{
						builder.setTransport(TransportType.LinphoneTransportTcp);
					}
					builder.setDomain(sip_domin);
					builder.setProxy(sip_domin + ":"+ sip_port);
					builder.setExpires(timeOut);
					try {
						builder.saveNewAccount();
					} catch (LinphoneCoreException e) {
						e.printStackTrace();
					}
			}
	}
	
	
	
	
	/**
	 * 获取房产信息
	 * @param context
	 * @param path
	 * @param headParams
	 * @param params
	 * @param listener
	 */
	public static void getHouseMessage(final Context context,String path,Map<String,String> headParams, Map<String,String> params,final HttpRespListener listener){
		try {
			HttpURLConnectionUtil.doGet(path + "/config/my_config_info.json",headParams, params, new Handler() {
						@Override
						public void dispatchMessage(Message msg) {
							if (msg.what == 200) {
								try {
									String resultJson = ((BaseResult) msg.obj).getMsg();
									if(!TextUtils.isEmpty(resultJson)){
										resultJson=resultJson.replace("\\", "");
										resultJson=resultJson.replace("\"{", "{");
										resultJson=resultJson.replace("}\"", "}");
										JSONObject object = new JSONObject(resultJson);
										int code = object.getInt("code");
										if (code == 0) {
											if (listener != null) {
												BaseResult result = JsonParserUtil.parseJson2Object(HouseResult.class,resultJson);
												result.setCode(code);
												listener.onSuccess(code, result);
											}
										}else{
											if (listener != null) {
												BaseResult result = JsonParserUtil.parseJson2Object(BaseResult.class,resultJson);
												listener.onFail(code,result.getMsg());
											}
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								if (listener != null) {
									listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
								}
							}
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 获取子账号列表
	 * @param context
	 * @param path
	 * @param headParams
	 * @param params
	 * @param listener
	 */
	public static void getSubAccount(final Context context,String path,Map<String,String> headParams, Map<String,String> params,final HttpRespListener listener){
		try {
			HttpURLConnectionUtil.doPost(context,path + "/config/get_house_subaccount.json",headParams, params, new Handler() {
				@Override
				public void dispatchMessage(Message msg) {
					if (msg.what == 200) {
						try {
							String resultJson = ((BaseResult) msg.obj).getMsg();
							JSONObject object = new JSONObject(resultJson);
							int code = object.getInt("code");
							if (code == 0) {
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(ChildAccountResult.class,resultJson);
									result.setCode(code);
									listener.onSuccess(code, result);
								}
							}else{
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(BaseResult.class,resultJson);
									listener.onFail(code,result.getMsg());
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
							if (listener != null) {
								listener.onFail(9,"数据解析异常");
							}
						}
					} else {
						if (listener != null) {
							listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
						}
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 添加或者删除子账号
	 * @param context
	 * @param path
	 * @param headParams
	 * @param params
	 * @param listener
	 */
	public static void setSubAccount(final Context context,String path,Map<String,String> headParams, Map<String,String> params,final HttpRespListener listener){
		try {
			HttpURLConnectionUtil.doPost(context,path + "/config/set_house_subaccount.json",headParams, params, new Handler() {
				@Override
				public void dispatchMessage(Message msg) {
					if (msg.what == 200) {
						try {
							String resultJson = ((BaseResult) msg.obj).getMsg();
							JSONObject object = new JSONObject(resultJson);
							int code = object.getInt("code");
							if (code == 0) {
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(BaseResult.class,resultJson);
									result.setCode(code);
									listener.onSuccess(code, result);
								}
							}else{
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(BaseResult.class,resultJson);
									listener.onFail(code,result.getMsg());
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
							if (listener != null) {
								listener.onFail(9,"数据解析异常");
							}
						}
					} else {
						if (listener != null) {
							listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
						}
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	/*
	 * 设定房屋被叫号码
	 */
	public static void setCalledNumber(final Context context,String path,Map<String,String> headParams, Map<String,String> params,final HttpRespListener listener){
		try {
			HttpURLConnectionUtil.doPost(context,path + "/config/set_called_number.json",headParams, params, new Handler() {
				@Override
				public void dispatchMessage(Message msg) {
					if (msg.what == 200) {
						try {
							String resultJson = ((BaseResult) msg.obj).getMsg();
							JSONObject object = new JSONObject(resultJson);
							int code = object.getInt("code");
							if (code == 0) {
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(BaseResult.class,resultJson);
									result.setCode(code);
									listener.onSuccess(code, result);
								}
							}else{
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(BaseResult.class,resultJson);
									listener.onFail(code,result.getMsg());
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
							if (listener != null) {
								listener.onFail(9,"数据解析异常");
							}
						}
					} else {
						if (listener != null) {
							listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
						}
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 用户设置房间免打扰
	 */
	public static void setDisturbing(final Context context,String path,Map<String,String> headParams, Map<String,String> params,final HttpRespListener listener){
		try {
			HttpURLConnectionUtil.doGet(context,path + "/users/update_of_disturbing.json",headParams, params, new Handler() {
				@Override
				public void dispatchMessage(Message msg) {
					if (msg.what == 200) {
						try {
							String resultJson = ((BaseResult) msg.obj).getMsg();
							JSONObject object = new JSONObject(resultJson);
							int code = object.getInt("code");
							if (code == 0) {
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(BaseResult.class,resultJson);
									result.setCode(code);
									listener.onSuccess(code, result);
								}
							}else{
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(BaseResult.class,resultJson);
									listener.onFail(code,result.getMsg());
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
							if (listener != null) {
								listener.onFail(9,"数据解析异常");
							}
						}
					} else {
						if (listener != null) {
							listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
						}
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 登陆后获取社区列表以及ip
	 * 
	 */
	public static void getNeiIpList(final Context context,Map<String,String> headParams, Map<String,String> params,final HttpRespListener listener){
		try {
			HttpURLConnectionUtil.doGet(context,GET_COMMNITY_LIST_URL,headParams, params, new Handler() {
				@Override
				public void dispatchMessage(Message msg) {
					if (msg.what == 200) {
						try {
							String resultJson = ((BaseResult) msg.obj).getMsg();
							JSONObject object = new JSONObject(resultJson);
							int code = object.getInt("code");
							if (code == 0) {
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(CommunityListResult.class,resultJson);
									result.setCode(code);
									listener.onSuccess(code, result);
								}
							}else{
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(BaseResult.class,resultJson);
									listener.onFail(code,result.getMsg());
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
							if (listener != null) {
								listener.onFail(9,"数据解析异常");
							}
						}
					} else {
						if (listener != null) {
							listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
						}
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 判断用户是否可以绑定该小区
	 * @param context
	 * @param path
	 * @param headParams
	 * @param params
	 * @param listener
	 */
	public  static void getIscanBind(final Context context,final String path,Map<String,String> headParams, Map<String,String> params,final HttpRespListener listener){
		try {
			HttpURLConnectionUtil.doGet(context,path+"/users/is_can_binding.json",headParams, params, new Handler() {
				@Override
				public void dispatchMessage(Message msg) {
					if (msg.what == 200) {
						try {
							String resultJson = ((BaseResult) msg.obj).getMsg();
							JSONObject object = new JSONObject(resultJson);
							int code = object.getInt("code");
							if (code == 0) {
								if (listener != null) {
									BidingResult result = JsonParserUtil.parseJson2Object(BidingResult.class,resultJson);
									result.setCode(code);
									if(result!=null){
										BidingBean bidingBean=result.getResult();
										if(bidingBean!=null){
											NeiborIdBean neidorIdBean=bidingBean.getNeibor_id();
											if(neidorIdBean!=null){
												SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_NEIGBOR_ID_KEY, neidorIdBean.getNeighborhoods_id());
												SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_USER_NEIGBOR_NAME_KEY, neidorIdBean.getFneibname());
											}
										}
									}
									SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_LAST_SAVE_PAHT_KEY, path);
									SharedPreferencesUtil.saveData(context, SharedPreferencesUtil.SAYEE_TWO_URL_KEY, path);
									listener.onSuccess(code, result);
								}
							}else{
								if (listener != null) {
									BaseResult result = JsonParserUtil.parseJson2Object(BaseResult.class,resultJson);
									listener.onFail(code,result.getMsg());
								}
							}
						} catch (Exception e) {
							if(e!=null && "Value  at neibor_id of type java.lang.String cannot be converted to JSONObject".equals(e.getMessage())){
								if (listener != null) {
									BidingResult result = new BidingResult();
									BidingBean bean=new BidingBean();
									bean.setCan_binding(false);
									result.setResult(bean);
									listener.onSuccess(0,result);
								}
							}else{
								e.printStackTrace();
								if (listener != null) {
									listener.onFail(9,"数据解析异常");
								}
							}
						}
					} else {
						if (listener != null) {
							listener.onFail(msg.what,((BaseResult) msg.obj).getMsg());
						}
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 保存用户绑定小区信息
	 * @param context
	 * @param headParams
	 * @param params
	 * @param listener
	 */
	public static void postSaveUserInfo(final Context context,Map<String,String> headParams, Map<String,String> params){
		try {
			HttpURLConnectionUtil.doPost(context,POST_SAVE_NEI_BINDING_USER_URL,headParams, params,null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}

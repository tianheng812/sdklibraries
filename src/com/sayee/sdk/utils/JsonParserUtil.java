package com.sayee.sdk.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

/**
 * 利用反射解析JSON对象到具体类
 */
public class JsonParserUtil {
	private JsonParserUtil() {
	}

	/**
	 * 解析JSON字符串到具体类
	 * 
	 * @param clazz
	 *            和JSON对象对应的类的Class，必须拥有setXxx()函数，其中xxx为属性
	 * @param json
	 *            被解析的JSON字符串
	 * @return 返回传入的Object对象实例
	 * @throws ClassNotFoundException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws SecurityException
	 * @throws InstantiationException
	 * @throws JSONException
	 */
	public static <T> T parseJson2Object(Class<T> clazz, String json)
			throws ClassNotFoundException, SecurityException,
			IllegalAccessException, InvocationTargetException,
			InstantiationException, JSONException {

		JSONObject jsonObject = new JSONObject(json);
		return parseJson2Object(clazz, jsonObject);
	}

	/**
	 * 解析JSONObject对象到具体类，递归算法
	 * 
	 * @param clazz
	 *            和JSON对象对应的类的Class，必须拥有setXxx()函数，其中xxx为属性
	 * @param jsonObject
	 *            被解析的JSON对象
	 * @return 返回传入的Object对象实例
	 * @throws ClassNotFoundException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws SecurityException
	 * @throws InstantiationException
	 * @throws JSONException
	 */
	public static <T> T parseJson2Object(Class<T> clazz, JSONObject jsonObject)
			throws ClassNotFoundException, SecurityException,
			IllegalAccessException, InvocationTargetException,
			InstantiationException, JSONException {

		// 获取clazz的实例
		T obj = clazz.newInstance();
		// 获取属性列表
		Field[] fields = clazz.getDeclaredFields();
		// 遍历每个属性，如果为基本类型和String则直接赋值，如果为List则得到每个Item添加后再赋值，如果是其它类则得到类的实例后赋值
		for (Field field : fields) {
			// 设置属性可操作
			field.setAccessible(true);
			// 获取字段类型
			Class<?> typeClazz = field.getType();
			// 是否基础变量
			if (typeClazz.isPrimitive()) {
				setProperty(obj, field, jsonObject.opt(field.getName()));
			} else {
				// 得到类型实例
				String typeObj = typeClazz.getSimpleName();
				// 是否为List
				if ("List".equals(typeObj)) {
					// 得到类型的结构，如:java.util.ArrayList<com.xxx.xxx>
					Type type = field.getGenericType();
					ParameterizedType pt = (ParameterizedType) type;
					// 获得List元素类型
					Class<?> dataClass = (Class<?>) pt.getActualTypeArguments()[0];
					if(jsonObject!=null&&jsonObject.toString().contains(field.getName())){
						// 得到List的JSONArray数组
						JSONArray jArray = jsonObject.getJSONArray(field.getName());
						List<Object> list = new ArrayList<Object>();
						// 将每个元素的实例类加入到类型的实例中
						for (int i = 0; i < jArray.length(); i++) {
							// 对于数组，递归调用解析子元素
							list.add(parseJson2Object(dataClass, jsonObject
									.getJSONArray(field.getName()).getJSONObject(i)));
						}
						setProperty(obj, field, list);
					}
				}
				// 是否为String
				else if ("String".equals(typeObj)) {
					setProperty(obj, field, jsonObject.opt(field.getName()));
				} else if ("Boolean".equals(typeObj)) {
					setProperty(obj, field, jsonObject.opt(field.getName()));
				} else if ("Long".equals(typeObj)) {
					setProperty(obj, field, jsonObject.opt(field.getName()));
				} else if ("Integer".equals(typeObj)) {
					setProperty(obj, field, jsonObject.opt(field.getName()));
				} else if ("Short".equals(typeObj)) {
					setProperty(obj, field, jsonObject.opt(field.getName()));
				} else if ("Byte".equals(typeObj)) {
					setProperty(obj, field, jsonObject.opt(field.getName()));
				} else if ("Double".equals(typeObj)) {
					setProperty(obj, field, jsonObject.opt(field.getName()));
				}
				// 是否为其它对象
				else {
					if(!TextUtils.isEmpty(typeObj)){
						// 递归解析对象
						setProperty(obj,field,parseJson2Object(typeClazz,jsonObject.getJSONObject(field.getName())));
					}
				}
			}
		}

		return obj;
	}

	/**
	 * 设置实例类的属性
	 * 
	 * @param obj
	 *            要被赋值的实例类
	 * @param field
	 *            要被赋值的属性
	 * @param valueObj
	 *            要被赋值的属性的值
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private static void setProperty(Object obj, Field field, Object valueObj)
			throws SecurityException, IllegalAccessException,
			InvocationTargetException {
		try {
			if ("serialVersionUID".equals(field.getName())) {
				return;
			}
			Class<?> clazz = obj.getClass();
			// 获取类的setXxx方法，xxx为属性
			Method method = clazz.getDeclaredMethod(
					"set"+ field.getName().substring(0, 1).toUpperCase(Locale.getDefault())
							+ field.getName().substring(1), field.getType());
			// 设置set方法可访问
			method.setAccessible(true);
			// 获取类型
			String fieldType = field.getType().toString();
			if(valueObj==null){
				return;
			}
			// 调用set方法为类的实例赋值
			if ("Integer".equals(fieldType.toString())
					|| "int".equals(fieldType.toString())) {
				if (valueObj!=null&&!valueObj.equals("")&&!"null".equalsIgnoreCase(valueObj.toString())) {
					Integer intval = Integer.parseInt(String.valueOf(valueObj));
					method.invoke(obj, intval);
				} else {
					method.invoke(obj, 0);
				}
			} else if ("Long".equalsIgnoreCase(fieldType)) {
				if(valueObj!=null){
					Long temp = Long.parseLong(valueObj.toString());
					method.invoke(obj, temp);
				}
			} else if ("Short".equalsIgnoreCase(fieldType)) {
				if(valueObj!=null){
					Short temp = Short.parseShort(valueObj.toString());
					method.invoke(obj, temp);
				}
			} else if ("Boolean".equalsIgnoreCase(fieldType)) {
				if(valueObj!=null){
					Boolean temp = Boolean.parseBoolean(valueObj.toString());
					method.invoke(obj, temp);
				}
			} else if ("Double".equalsIgnoreCase(fieldType)) {
				if(valueObj!=null){
					Double temp = Double.parseDouble(valueObj.toString());
					method.invoke(obj, temp);
				}
			} else if ("char".equalsIgnoreCase(fieldType)) {
				if(valueObj!=null){
					char temp = valueObj.toString().charAt(0);
					method.invoke(obj, temp);
				}
			} else {
				if (valueObj!=null&&valueObj instanceof Integer) {
					valueObj = String.valueOf(valueObj);
				}
				if(valueObj!=null)
					method.invoke(obj, valueObj);
			}
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}
}

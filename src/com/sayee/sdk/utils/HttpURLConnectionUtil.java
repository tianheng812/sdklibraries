package com.sayee.sdk.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import org.linphone.mediastream.Log;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.sayee.sdk.result.BaseResult;

/**
 * HTTP工具类 网络连接操作必须在子线程中进行，否则会阻塞主线程引起异常
 */
public class HttpURLConnectionUtil {
	/**
	 * 连接超时时间 30秒
	 */
	public static final int CONNECT_TIMEOUT = 30000;
	/**
	 * 读取超时时间
	 */
	public static final int READ_TIMEOUT = 60000;

	private HttpURLConnectionUtil() {
	}

	/**
	 * get 提交
	 * 
	 * @param path
	 *            提交路径
	 * @param params
	 *            参数：键值对
	 * @return 字符串
	 * @throws Exception
	 */
	public static void doGet(String path, Map<String, String> headerParams,
			Map<String, String> params, Handler handler) throws Exception {
		doGet(null, path, headerParams, params, "UTF-8", handler, false);
	}
	
	
	/**
	 * get 提交
	 * 
	 * @param path
	 *            提交路径
	 * @param params
	 *            参数：键值对
	 * @return 字符串
	 * @throws Exception
	 */
	public static void doGet(Context context,String path, Map<String, String> headerParams,
			Map<String, String> params, Handler handler) throws Exception {
		doGet(context, path, headerParams, params, "UTF-8", handler, false);
	}

	/**
	 * get 提交
	 * 
	 * @param path
	 *            提交路径
	 * @param params
	 *            参数：键值对
	 * @return 字符串
	 * @throws Exception
	 */
	public static void doGet(Context context, String path,
			Map<String, String> headerParams, Map<String, String> params,
			Handler handler, boolean isLoop) throws Exception {
		doGet(context, path, headerParams, params, "UTF-8", handler, isLoop);
	}

	/**
	 * post请求
	 * 
	 * @param path
	 * @param params
	 * @param encode
	 * @return
	 * @throws Exception
	 */
	public static void doGet(final Context context, final String path,
			final Map<String, String> headParams,
			final Map<String, String> params, final String encode,
			final Handler mHandler, final boolean isLoop) throws Exception {

		new Thread() {
			public void run() {
				String returnValue = null;
				if (TextUtils.isEmpty(path)) {
					if (mHandler != null) {
						Message mess = mHandler.obtainMessage();
						BaseResult baseBean = new BaseResult();
						baseBean.setMsg("请求 url 不能为空");
						mess.what = 3;
						mess.obj = baseBean;
						mHandler.sendMessage(mess);
					}
					return;
				}
				StringBuilder urlPath = new StringBuilder(path);
				urlPath.append('?');
				// 添加参数
				if (params != null && params.size() > 0) {
					for (Map.Entry<String, String> entry : params.entrySet()) {
						if(entry!=null&&!TextUtils.isEmpty(entry.getKey())&&!TextUtils.isEmpty(entry.getKey()))
						try {
							urlPath.append(entry.getKey())
									.append('=')
									.append(URLEncoder.encode(entry.getValue(),encode)).append('&');
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
					}
					if(TextUtils.isEmpty(urlPath))
						urlPath.deleteCharAt(urlPath.length() - 1);
				}

				if (!TextUtils.isEmpty(urlPath))
					Log.i("url:   " + urlPath.toString());
				URL url = null;
				HttpURLConnection connection = null;
				try {
					url = new URL(urlPath.toString());
					connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("GET");
					connection.setRequestProperty("Accept", "*/*");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				connection.setConnectTimeout(CONNECT_TIMEOUT);
				connection.setReadTimeout(READ_TIMEOUT);

				if (headParams != null) {
					for (String key : headParams.keySet()) {
						connection.setRequestProperty(key, headParams.get(key));
						Log.i("key= " + key + " and value= "
								+ headParams.get(key));
					}
				}

				try {
					if (connection.getResponseCode() == 200) {
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						InputStream is = null;
						try {
							is = connection.getInputStream();
							byte[] buf = new byte[1024 * 10];
							int len = 0;
							while ((len = is.read(buf)) > 0) {
								baos.write(buf, 0, len);
							}
							is.close();
							returnValue = new String(baos.toByteArray(), encode);
						} catch (IOException e) {
							e.printStackTrace();
						} finally {
							if (is != null) {
								is.close();
								is = null;
							}
						}

						Log.i("result:    " + returnValue);
						if (mHandler != null) {
							Message mess = mHandler.obtainMessage();
							BaseResult baseBean = new BaseResult();
							baseBean.setMsg(returnValue);
							mess.what = connection.getResponseCode();
							mess.obj = baseBean;
							mHandler.sendMessage(mess);
						}
					} else {
						if (mHandler != null) {
							Message mess = mHandler.obtainMessage();
							BaseResult baseBean = new BaseResult();
							int code = connection.getResponseCode();
							if (code == 404) {
								baseBean.setMsg("404  接口不存在");
							} else {
								baseBean.setMsg("服务器错误");
							}
							mess.what = connection.getResponseCode();
							mess.obj = baseBean;
							mHandler.sendMessage(mess);
						}
					}
				} catch (IOException e) {
					if (mHandler != null) {
						Message mess = mHandler.obtainMessage();
						BaseResult baseBean = new BaseResult();
						baseBean.setMsg("请连接网络");
						mess.what = 202;
						mess.obj = baseBean;
						mHandler.sendMessage(mess);
					}
				}
			};
		}.start();

	}
	
	public static void doPost(Context context, String path,Map<String, String> headParams, Map<String, String> params,Handler mHandler) throws Exception {
		doPost(context, path, headParams, params, "UTF-8", mHandler, false);
	}


	public static void doPost(Context context, String path,
			Map<String, String> headParams, Map<String, String> params,
			Handler mHandler, boolean isLoop) throws Exception {
		doPost(context, path, headParams, params, "UTF-8", mHandler, isLoop);
	}

	public static void doPost(final Context context, final String path,
			final Map<String, String> headParams,
			final Map<String, String> params, final String encode,
			final Handler mHandler, final boolean isLoop) throws Exception {

		new Thread() {
			public void run() {
				String returnValue = null;
				StringBuilder body = new StringBuilder();
				if (TextUtils.isEmpty(path)) {
					if (mHandler != null) {
						Message mess = mHandler.obtainMessage();
						BaseResult baseBean = new BaseResult();
						baseBean.setMsg("请求 url 不能为空");
						mess.what = 3;
						mess.obj = baseBean;
						mHandler.sendMessage(mess);
					}
					return;
				}

				if (params != null && params.size() > 0) {
					for (Map.Entry<String, String> entry : params.entrySet()) {
						try {
							body.append(entry.getKey())
									.append('=')
									.append(URLEncoder.encode(entry.getValue(),
											encode)).append('&');
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
					}

					body.deleteCharAt(body.length() - 1);
				}

				byte[] bodyContent = body.toString().getBytes();

				URL url = null;
				try {
					url = new URL(path);
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				}
				
				Log.i(" path ---- "+path);
				
				HttpURLConnection connection = null;
				try {
					connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("POST");
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				connection.setConnectTimeout(CONNECT_TIMEOUT);
				connection.setReadTimeout(READ_TIMEOUT);
				// setDoOutput(true);以后就可以使用conn.getOutputStream().write()
				// get请求用不到conn.getOutputStream()，因为参数直接追加在地址后面，因此默认是false。
				connection.setDoOutput(true);
				// 设置头信息的，比如格式，UA等，不设置自然有默认的，一般的请求倒不需要去设置，可以去看看android里的DefaultHttpClient里也有设置头信息的
				connection.setRequestProperty("Content-Type",
						"application/x-www-form-urlencoded");
				connection.setRequestProperty("Content-Length",
						bodyContent.length + "");
				if (headParams != null) {
					for (String key : headParams.keySet()) {
						connection.setRequestProperty(key, headParams.get(key));
						System.out.println("key= " + key + " and value= "
								+ headParams.get(key));
					}

				}

				OutputStream os = null;
				try {
					os = connection.getOutputStream();
					os.write(bodyContent);
					os.flush();
				} catch (IOException e) {
					if (mHandler != null) {
						Message mess = mHandler.obtainMessage();
						BaseResult baseBean = new BaseResult();
						baseBean.setMsg("请连接网络");
						mess.what = 202;
						mess.obj = baseBean;
						mHandler.sendMessage(mess);
					}
				} finally {
					if (os != null) {
						try {
							os.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
						os = null;
					}
				}

				try {
					if (connection.getResponseCode() == 200) {
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						InputStream is = null;
						try {
							is = connection.getInputStream();
							byte[] buf = new byte[1024 * 10];
							int len = 0;
							while ((len = is.read(buf)) > 0) {
								baos.write(buf, 0, len);
							}
							is.close();
							returnValue = new String(baos.toByteArray(), encode);
						} catch (IOException e) {
							e.printStackTrace();
						} finally {
							if (is != null) {
								is.close();
								is = null;
							}
						}
						Log.i("post result:    " + returnValue);

						if (mHandler != null) {
							Message mess = mHandler.obtainMessage();
							BaseResult baseBean = new BaseResult();
							baseBean.setMsg(returnValue);
							mess.what = connection.getResponseCode();
							mess.obj = baseBean;
							mHandler.sendMessage(mess);
						}
					} else {
						Log.e(returnValue);
						if (mHandler != null) {
							Message mess = mHandler.obtainMessage();
							BaseResult baseBean = new BaseResult();
							baseBean.setMsg("服务器错误");
							mess.what = connection.getResponseCode();
							mess.obj = baseBean;
							mHandler.sendMessage(mess);
						}
					}
				} catch (IOException e) {
					if (mHandler != null) {
						Message mess = mHandler.obtainMessage();
						BaseResult baseBean = new BaseResult();
						baseBean.setMsg("请连接网络");
						mess.what = 500;
						mess.obj = baseBean;
						mHandler.sendMessage(mess);
					}
				}
			};
		}.start();

	}

	/**
	 * Post 上传文件
	 * 
	 * @param path
	 * @param params
	 * @param files
	 * @return
	 * @throws IOException
	 */
	public static String doPost(String path, Map<String, String> params,
			Map<String, File> files) throws Exception {
		return doPost(path, params, files, "UTF-8");
	}

	public static String doPost(String path, Map<String, String> params,
			Map<String, File> files, String encode) throws Exception {
		// UUID是指在一台机器上生成的数字，它保证对在同一时空中的所有机器都是唯一的。
		// UUID由三部分组成 1）当前日期和时间，
		// （2）时钟序列。（3）全局唯一的IEEE机器识别号，如果有网卡，从网卡MAC地址获得，没有网卡以其他方式获得。
		String BOUNDARY = java.util.UUID.randomUUID().toString();
		String PREFIX = "--", LINEND = "\r\n";
		String MULTIPART_FROM_DATA = "multipart/form-data";
		String CHARSET = encode;
		URL uri = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
		conn.setConnectTimeout(CONNECT_TIMEOUT);
		conn.setReadTimeout(READ_TIMEOUT);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setUseCaches(false);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("connection", "keep-alive");
		conn.setRequestProperty("Charsert", CHARSET);
		// multipart/form-data的请求头必须包含一个特殊的头信息：Content-Type，
		// 且其值也必须规定为multipart/form-data，
		// 同时还需要规定一个内容分割符boundary用于分割请求体中的多个post的内容，
		// 如文件内容和文本内容自然需要分割开来，不然接收方就无法正常解析和还原这个文件了。
		conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA
				+ ";boundary=" + BOUNDARY);
		StringBuilder sb = new StringBuilder();

		if (params != null)
			for (Map.Entry<String, String> entry : params.entrySet()) {
				sb.append(PREFIX);
				sb.append(BOUNDARY);
				sb.append(LINEND);
				sb.append("Content-Disposition: form-data; name=\""
						+ entry.getKey() + "\"" + LINEND);
				sb.append("Content-Type: text/plain; charset=" + CHARSET
						+ LINEND);
				sb.append("Content-Transfer-Encoding: 8bit" + LINEND);
				sb.append(LINEND);
				sb.append(entry.getValue());
				sb.append(LINEND);
			}

		DataOutputStream outStream = new DataOutputStream(
				conn.getOutputStream());
		outStream.write(sb.toString().getBytes());
		if (files != null)
			for (Map.Entry<String, File> file : files.entrySet()) {
				StringBuilder sb1 = new StringBuilder();
				sb1.append(PREFIX);
				sb1.append(BOUNDARY);
				sb1.append(LINEND);
				sb1.append("Content-Disposition: form-data; name=\""
						+ file.getKey() + "\"; filename=\""
						+ file.getValue().getName() + "\"" + LINEND);
				sb1.append("Content-Type: application/octet-stream; charset="
						+ CHARSET + LINEND);
				sb1.append(LINEND);
				outStream.write(sb1.toString().getBytes());
				InputStream is = new FileInputStream(file.getValue());
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = is.read(buffer)) != -1) {
					outStream.write(buffer, 0, len);
				}
				is.close();
				outStream.write(LINEND.getBytes());
			}
		byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
		outStream.write(end_data);
		outStream.flush();
		int res = conn.getResponseCode();

		String data = "";
		if (res == 200) {
			InputStream in = conn.getInputStream();
			InputStreamReader isReader = new InputStreamReader(in);
			BufferedReader bufReader = new BufferedReader(isReader);
			String line = null;
			while ((line = bufReader.readLine()) != null) {
				data += line;
			}
			bufReader.close();
		} else {
			throw new Exception("服务器响应错误" + res);
		}

		outStream.close();
		conn.disconnect();
		return data;
	}

	/**
	 * 下载文件
	 * 
	 * @param urlStr
	 * @param file
	 * @return
	 */
	public static boolean downLoadFile(String urlStr, File file) {
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}

		boolean flag = true;
		InputStream inputStream = null;
		OutputStream outputStream = null;

		try {
			URL url = new URL(urlStr);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setConnectTimeout(CONNECT_TIMEOUT);
			connection.setReadTimeout(READ_TIMEOUT);

			inputStream = connection.getInputStream();
			outputStream = new FileOutputStream(file);

			byte buf[] = new byte[1024];
			int len = 0;

			while ((len = inputStream.read(buf)) > 0) {
				outputStream.write(buf, 0, len);
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
			flag = false;

		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
					inputStream = null;
				}
				if (outputStream != null) {
					outputStream.close();
					outputStream = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				flag = false;
			}
		}

		if (flag == false && file != null && file.exists()) {// 如果下载不成功，则把失败的文件删除
			file.delete();
		}
		return flag;
	}

}
package com.sayee.sdk;

import static android.content.Intent.ACTION_MAIN;

import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.LinphoneSimpleListener;
import org.linphone.LinphoneUtils;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.mediastream.Log;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.TextUtils;

import com.sayee.sdk.activity.IncomingCallActivity;
import com.sayee.sdk.utils.SharedPreferencesUtil;
import com.sayee.sdk.utils.ToolsUtil;

public class LinphoneService extends Service implements
		LinphoneSimpleListener.LinphoneServiceListener {
	public static final int IC_LEVEL_OFFLINE = 3;
	public static final int IC_LEVEL_ORANGE = 0;
	private static LinphoneService instance;
	private static PendingIntent mkeepAlivePendingIntent;

	public static LinphoneService instance() {
		if (isReady())
			return instance;
		return null;
	}

	public static boolean isReady() {
		return (instance != null);
	}

	public IBinder onBind(Intent paramIntent) {
		return null;
	}

	public void onCallStateChanged(LinphoneCall call,
			LinphoneCall.State paramState, String paramString) {

		if (instance == null) {
			startService(new Intent(ACTION_MAIN).setClass(this,
					LinphoneService.class));
			return;
		}

		if (paramState == State.IncomingReceived) {
			if (!LinphoneUtils.isHasPermission(getApplicationContext())
					&& LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null
					&& !LinphoneManager.getLc().isIncall()) {
				try {
					ToolsUtil.toast(this, "由于没有录音权限，已自动帮您挂断来电，请打开手机设置的"
							+ LinphoneUtils.getAppName(getApplicationContext())
							+ "的录音权限");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (SharedPreferencesUtil.getSipIsCall(getApplicationContext())) {
				SharedPreferencesUtil.saveData(getApplicationContext(), SharedPreferencesUtil.SAYEE_SIP_IS_CALL_KEY,false);
				Intent intent = new Intent(this, IncomingCallActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			} else {
				if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null) {
					LinphoneManager.getLc().terminateCall(call);
				}
			}
			return;
		}

		if (paramState == LinphoneCall.State.CallUpdatedByRemote) {
			boolean remoteVideo = call.getRemoteParams().getVideoEnabled();
			boolean localVideo = call.getCurrentParamsCopy().getVideoEnabled();
			boolean autoAcceptCameraPolicy = LinphonePreferences.instance()
					.shouldAutomaticallyAcceptVideoRequests();
			if (remoteVideo && !localVideo && !autoAcceptCameraPolicy
					&& !LinphoneManager.getLc().isInConference()) {
				try {
					LinphoneManager.getLc().deferCallUpdate(call);
				} catch (LinphoneCoreException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void onCreate() {
		super.onCreate();
		instance = this;
		LinphoneManager.createAndStart(this, this);
		Intent localIntent2 = new Intent(this, KeepAliveHandler.class);
		PendingIntent mkeepAlivePendingIntent = PendingIntent.getBroadcast(
				this, 0, localIntent2, PendingIntent.FLAG_ONE_SHOT);
		((AlarmManager) getSystemService("alarm")).setRepeating(2,
				1000000L + SystemClock.elapsedRealtime(), 1000000L,
				mkeepAlivePendingIntent);
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	public void onDestroy() {
		instance = null;
		LinphoneManager.destroy();
		if (mkeepAlivePendingIntent != null)
			((AlarmManager) getSystemService("alarm"))
					.cancel(mkeepAlivePendingIntent);
		super.onDestroy();
	}

	public void onDisplayStatus(String paramString) {
	}

	public void onGlobalStateChanged(LinphoneCore.GlobalState paramGlobalState,
			String paramString) {
	}

	public void onRegistrationStateChanged(LinphoneProxyConfig config,
			LinphoneCore.RegistrationState state, String paramString) {
	}

	public void tryingNewOutgoingCallButAlreadyInCall() {
	}

	public void tryingNewOutgoingCallButCannotGetCallParameters() {
	}

	public void tryingNewOutgoingCallButWrongDestinationAddress() {
	}

	@Override
	public void onCallEncryptionChanged(LinphoneCall paramLinphoneCall,
			boolean paramBoolean, String paramString) {

	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: org.linphone.LinphoneService JD-Core Version: 0.6.2
 */
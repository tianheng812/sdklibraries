package com.sayee.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import org.linphone.LinphoneManager;
import org.linphone.mediastream.Log;

public class KeepAliveReceiver extends BroadcastReceiver {
	Handler mHandler=null;
	Context context=null;
	
	public void onReceive(Context context, Intent intent) {
		if (!LinphoneService.isReady()) {
			Log.i("Keep alive broadcast received while Linphone service not ready" );
			return;
		}
		if(LinphoneManager.isInstanciated()&&LinphoneManager.getLcIfManagerNotDestroyedOrNull()!=null){
			if (intent.getAction().equalsIgnoreCase(Intent.ACTION_SCREEN_ON)) {
				LinphoneManager.getLc().enableKeepAlive(true);
			} else if (intent.getAction().equalsIgnoreCase(Intent.ACTION_SCREEN_OFF)) {
				LinphoneManager.getLc().enableKeepAlive(false);
			}
		}
	}
	
}


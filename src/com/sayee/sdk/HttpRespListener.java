package com.sayee.sdk;

import com.sayee.sdk.result.BaseResult;

public interface HttpRespListener {
	/**
	 * 请求成功回调
	 * @param code
	 * @param result
	 */
	public void onSuccess(int code,BaseResult result);
	
	/**
	 * 请求失败回调
	 * @param code
	 * @param msg
	 */
	public void onFail(int code,String msg);
	
}


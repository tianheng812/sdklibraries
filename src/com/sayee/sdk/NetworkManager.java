package com.sayee.sdk;

import org.linphone.LinphoneManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkManager extends BroadcastReceiver
{
  public void onReceive(Context context, Intent intent)
  {
    ConnectivityManager localConnectivityManager = (ConnectivityManager)context.getSystemService("connectivity");
    if(intent!=null){
	    boolean localBoolean = intent.getBooleanExtra("noConnectivity", false);
	    if (LinphoneManager.isInstanciated()){
	        LinphoneManager.getInstance().connectivityChanged(localConnectivityManager, localBoolean);
	    }else{
	    	if(localConnectivityManager!=null){
	    		NetworkInfo eventInfo = localConnectivityManager.getActiveNetworkInfo();
				if (eventInfo != null && (eventInfo.getState() == NetworkInfo.State.CONNECTED)) {
					SayeeManager.getInstance().turnOnCall(context);
					int i=0;
					while (!LinphoneService.isReady()) {
						try {
							Thread.sleep(30);
							i++;
							if(i==150){
								System.gc();
								break;
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
	    	}
	    }
    }
  }
}


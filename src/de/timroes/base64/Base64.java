package de.timroes.base64;

import java.util.HashMap;

public class Base64
{
  private static final char[] code = "=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();
  private static final HashMap<Character, Byte> map = new HashMap();

  static
  {
    for (int i = 0; i < code.length; i++)
      map.put(Character.valueOf(code[i]), Byte.valueOf((byte)i));
  }

  public static byte[] decode(String paramString)
  {
    String str = paramString.replaceAll("\\r|\\n", "");
    if (str.length() % 4 != 0)
      throw new IllegalArgumentException("The length of the input string must be a multiple of four.");
    if (!str.matches("^[A-Za-z0-9+/]*[=]{0,3}$"))
      throw new IllegalArgumentException("The argument contains illegal characters.");
    byte[] arrayOfByte1 = new byte[3 * str.length() / 4];
    char[] arrayOfChar = str.toCharArray();
    int i = 0;
    int j = 0;
    while (j < arrayOfChar.length)
    {
      int k = -1 + ((Byte)map.get(Character.valueOf(arrayOfChar[j]))).byteValue();
      int m = -1 + ((Byte)map.get(Character.valueOf(arrayOfChar[(j + 1)]))).byteValue();
      int n = -1 + ((Byte)map.get(Character.valueOf(arrayOfChar[(j + 2)]))).byteValue();
      int i1 = -1 + ((Byte)map.get(Character.valueOf(arrayOfChar[(j + 3)]))).byteValue();
      int i2 = i + 1;
      arrayOfByte1[i] = ((byte)(k << 2 | m >>> 4));
      int i3 = i2 + 1;
      arrayOfByte1[i2] = ((byte)((m & 0xF) << 4 | n >>> 2));
      int i4 = i3 + 1;
      arrayOfByte1[i3] = ((byte)((n & 0x3) << 6 | i1 & 0x3F));
      j += 4;
      i = i4;
    }
    if (str.endsWith("="))
    {
      byte[] arrayOfByte2 = new byte[arrayOfByte1.length - (str.length() - str.indexOf("="))];
      System.arraycopy(arrayOfByte1, 0, arrayOfByte2, 0, arrayOfByte2.length);
      return arrayOfByte2;
    }
    return arrayOfByte1;
  }

  public static String decodeAsString(String paramString)
  {
    return new String(decode(paramString));
  }

  public static String encode(String paramString)
  {
    return encode(paramString.getBytes());
  }

  public static String encode(byte[] paramArrayOfByte)
  {
    StringBuilder localStringBuilder = new StringBuilder(4 * ((2 + paramArrayOfByte.length) / 3));
    byte[] arrayOfByte = encodeAsBytes(paramArrayOfByte);
    for (int i = 0; i < arrayOfByte.length; i++)
    {
      localStringBuilder.append(code[(1 + arrayOfByte[i])]);
      if (i % 72 == 71)
        localStringBuilder.append("\n");
    }
    return localStringBuilder.toString();
  }

  public static String encode(Byte[] paramArrayOfByte)
  {
    byte[] arrayOfByte = new byte[paramArrayOfByte.length];
    for (int i = 0; i < arrayOfByte.length; i++)
      arrayOfByte[i] = paramArrayOfByte[i].byteValue();
    return encode(arrayOfByte);
  }

  public static byte[] encodeAsBytes(String paramString)
  {
    return encodeAsBytes(paramString.getBytes());
  }

  public static byte[] encodeAsBytes(byte[] paramArrayOfByte)
  {
    byte[] arrayOfByte1 = new byte[4 * ((2 + paramArrayOfByte.length) / 3)];
    byte[] arrayOfByte2 = new byte[3 * ((2 + paramArrayOfByte.length) / 3)];
    System.arraycopy(paramArrayOfByte, 0, arrayOfByte2, 0, paramArrayOfByte.length);
    int i = 0;
    for (int j = 0; j < arrayOfByte2.length; j += 3)
    {
      int m = i + 1;
      arrayOfByte1[i] = ((byte)((0xFF & arrayOfByte2[j]) >>> 2));
      int n = m + 1;
      arrayOfByte1[m] = ((byte)((0x3 & arrayOfByte2[j]) << 4 | (0xFF & arrayOfByte2[(j + 1)]) >>> 4));
      int i1 = n + 1;
      arrayOfByte1[n] = ((byte)((0xF & arrayOfByte2[(j + 1)]) << 2 | (0xFF & arrayOfByte2[(j + 2)]) >>> 6));
      i = i1 + 1;
      arrayOfByte1[i1] = ((byte)(0x3F & arrayOfByte2[(j + 2)]));
    }
    for (int k = arrayOfByte2.length - paramArrayOfByte.length; k > 0; k--)
      arrayOfByte1[(arrayOfByte1.length - k)] = -1;
    return arrayOfByte1;
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.base64.Base64
 * JD-Core Version:    0.6.2
 */
package de.timroes.axmlrpc;

import de.timroes.axmlrpc.serializer.SerializerHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class XMLRPCClient {
	static final String CONTENT_LENGTH = "Content-Length";
	static final String CONTENT_TYPE = "Content-Type";
	private static final String DEFAULT_USER_AGENT = "aXMLRPC";
	static final String FAULT = "fault";
	public static final int FLAGS_8BYTE_INT = 2;
	public static final int FLAGS_APACHE_WS = 776;
	public static final int FLAGS_DEFAULT_TYPE_STRING = 256;
	public static final int FLAGS_ENABLE_COOKIES = 4;
	public static final int FLAGS_FORWARD = 32;
	public static final int FLAGS_IGNORE_NAMESPACES = 512;
	public static final int FLAGS_IGNORE_STATUSCODE = 16;
	public static final int FLAGS_NIL = 8;
	public static final int FLAGS_NONE = 0;
	public static final int FLAGS_SSL_IGNORE_ERRORS = 192;
	public static final int FLAGS_SSL_IGNORE_INVALID_CERT = 128;
	public static final int FLAGS_SSL_IGNORE_INVALID_HOST = 64;
	public static final int FLAGS_STRICT = 1;
	static final String HOST = "Host";
	static final String HTTP_POST = "POST";
	static final String METHOD_CALL = "methodCall";
	static final String METHOD_NAME = "methodName";
	static final String METHOD_RESPONSE = "methodResponse";
	static final String PARAM = "param";
	static final String PARAMS = "params";
	static final String STRUCT_MEMBER = "member";
	static final String TYPE_XML = "text/xml";
	static final String USER_AGENT = "User-Agent";
	public static final String VALUE = "value";
	private AuthenticationManager authManager;
	private Map<Long, Caller> backgroundCalls = new HashMap();
	private CookieManager cookieManager;
	private int flags;
	private String forcedCN;
	private Map<String, String> httpParameters = new HashMap();
	private ResponseParser responseParser;
	private TrustManager[] trustAllManagers;
	private URL url;

	public XMLRPCClient(URL paramURL) {
		this(paramURL, DEFAULT_USER_AGENT, 0);
	}

	public XMLRPCClient(URL paramURL, int paramInt) {
		this(paramURL, DEFAULT_USER_AGENT, paramInt);
	}

	public XMLRPCClient(URL paramURL, String paramString) {
		this(paramURL, paramString, 0);
	}

	public XMLRPCClient(URL paramURL, String paramString, int paramInt) {
		SerializerHandler.initialize(paramInt);
		this.url = paramURL;
		this.flags = paramInt;
		this.responseParser = new ResponseParser();
		this.cookieManager = new CookieManager(paramInt);
		this.authManager = new AuthenticationManager();
		this.httpParameters.put("Content-Type", "text/xml");
		this.httpParameters.put("User-Agent", paramString);
	}

	private Call createCall(String paramString, Object[] paramArrayOfObject) {
		if ((isFlagSet(1)) && (!paramString.matches("^[A-Za-z0-9\\._:/]*$")))
			throw new XMLRPCRuntimeException(
					"Method name must only contain A-Z a-z . : _ / ");
		return new Call(paramString, paramArrayOfObject);
	}

	private boolean isFlagSet(int paramInt) {
		return (paramInt & this.flags) != 0;
	}

	public Object call(String paramString, Object[] paramArrayOfObject)
			throws XMLRPCException {
		return new Caller().call(paramString, paramArrayOfObject);
	}

	public long callAsync(XMLRPCCallback paramXMLRPCCallback,
			String paramString, Object[] paramArrayOfObject) {
		long l = System.currentTimeMillis();
		new Caller(paramXMLRPCCallback, l, paramString, paramArrayOfObject)
				.start();
		return l;
	}

	public void cancel(long paramLong) {
		Caller localCaller = (Caller) this.backgroundCalls.get(Long
				.valueOf(paramLong));
		if (localCaller == null)
			return;
		localCaller.cancel();
		try {
			localCaller.join();
			return;
		} catch (InterruptedException localInterruptedException) {
		}
	}

	public void clearCookies() {
		this.cookieManager.clearCookies();
	}

	public void clearLoginData() {
		this.authManager.clearAuthData();
	}

	public URL getURL() {
		return this.url;
	}

	public void setCustomHttpHeader(String paramString1, String paramString2) {
		if (("Content-Type".equals(paramString1))
				|| ("Host".equals(paramString1))
				|| ("Content-Length".equals(paramString1)))
			throw new XMLRPCRuntimeException(
					"You cannot modify the Host, Content-Type or Content-Length header.");
		this.httpParameters.put(paramString1, paramString2);
	}

	public void setForcedCN(String paramString) {
		this.forcedCN = paramString;
	}

	public void setLoginData(String paramString1, String paramString2) {
		this.authManager.setAuthData(paramString1, paramString2);
	}

	public void setUserAgentString(String paramString) {
		this.httpParameters.put("User-Agent", paramString);
	}

	private class Caller extends Thread {
		private volatile boolean canceled;
		private HttpURLConnection http;
		private XMLRPCCallback listener;
		private String methodName;
		private Object[] params;
		private long threadId;

		public Caller() {
		}

		public Caller(XMLRPCCallback paramLong, long threadId,String paramArrayOfObject, Object[] a) {
			this.listener = paramLong;
			this.threadId = threadId;
			this.methodName = paramArrayOfObject;
			this.params = a;
		}

		private final String extractCN(String paramString)
				throws SSLPeerUnverifiedException {
			int i = paramString.indexOf("CN=");
			if (i == -1)
				throw new SSLPeerUnverifiedException("cannot extract CN from "
						+ paramString);
			int j = paramString.indexOf(',', i);
			if (j == -1)
				return paramString.substring(i + 3).trim();
			return paramString.substring(i + 3, j).trim();
		}

		private HttpURLConnection verifyConnection(URLConnection conn)
				throws XMLRPCException {
			if (!(conn instanceof HttpURLConnection)) {
				throw new IllegalArgumentException(
						"The URL is not valid for a http connection.");
			}

			// Validate the connection if its an SSL connection
			if (conn instanceof HttpsURLConnection) {

				HttpsURLConnection h = (HttpsURLConnection) conn;

				// Don't check, that URL matches the certificate.
				if (isFlagSet(FLAGS_SSL_IGNORE_INVALID_HOST)) {
					h.setHostnameVerifier(new HostnameVerifier() {
						public boolean verify(String host, SSLSession ssl) {
							return true;
						}
					});
				}

				// Check with a forced domain name
				if (forcedCN != null) {
					h.setHostnameVerifier(new HostnameVerifier() {
						public boolean verify(String host, SSLSession ssl) {
							try {
								String principal = ssl.getPeerPrincipal()
										.getName();
								String cnPattern = extractCN(principal)
										.replace("*", "[\\\\*a-z0-9.]*");
								return forcedCN.matches(cnPattern);
							} catch (SSLPeerUnverifiedException e) {
								e.printStackTrace();
							}
							return false;
						}
					});
				}

				// Don't validate the certificate if flag is set.
				if (isFlagSet(FLAGS_SSL_IGNORE_INVALID_CERT)) {

					// Initialize tolerant TrustManager
					if (trustAllManagers == null) {
						trustAllManagers = new TrustManager[] { new X509TrustManager() {

							public void checkClientTrusted(
									X509Certificate[] xcs, String string)
									throws CertificateException {
							}

							public void checkServerTrusted(
									X509Certificate[] xcs, String string)
									throws CertificateException {
							}

							public X509Certificate[] getAcceptedIssuers() {
								return null;
							}
						} };
					}

					// Associate the TrustManager with TLS and SSL connections.
					try {

						String[] sslContexts = new String[] { "TLS", "SSL" };

						for (String ctx : sslContexts) {
							SSLContext sc = SSLContext.getInstance(ctx);
							sc.init(null, trustAllManagers, new SecureRandom());
							h.setSSLSocketFactory(sc.getSocketFactory());
						}

					} catch (Exception ex) {
						throw new XMLRPCException(ex);
					}

				}

				return h;

			}

			return (HttpURLConnection) conn;

		}

		public Object call(String methodName, Object[] params)
				throws XMLRPCException {

			try {

				Call c = createCall(methodName, params);

				URLConnection conn = url.openConnection();

				http = verifyConnection(conn);
				http.setInstanceFollowRedirects(false);
				http.setRequestMethod(HTTP_POST);
				http.setDoOutput(true);
				http.setDoInput(true);

				// Set the request parameters
				for (Map.Entry<String, String> param : httpParameters
						.entrySet()) {
					http.setRequestProperty(param.getKey(), param.getValue());
				}

				authManager.setAuthentication(http);
				cookieManager.setCookies(http);

				OutputStreamWriter stream = new OutputStreamWriter(
						http.getOutputStream());
				stream.write(c.getXML());
				stream.flush();
				stream.close();

				// Try to get the status code from the connection
				int statusCode;
				try {
					statusCode = http.getResponseCode();
				} catch (IOException ex) {
					// Due to a bug on android, the getResponseCode()-method
					// will
					// fail the first time, with a IOException, when 401 or 403
					// has been returned.
					// The second time it should success. If it fail the second
					// time again
					// the normal exceptipon handling can take care of this,
					// since
					// it is a real error.
					statusCode = http.getResponseCode();
				}

				InputStream istream;

				// If status code was 401 or 403 throw exception or if
				// appropriate
				// flag is set, ignore error code.
				if (statusCode == HttpURLConnection.HTTP_FORBIDDEN
						|| statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

					if (isFlagSet(FLAGS_IGNORE_STATUSCODE)) {
						// getInputStream will fail if server returned above
						// error code, use getErrorStream instead
						istream = http.getErrorStream();
					} else {
						throw new XMLRPCException("Invalid status code '"
								+ statusCode + "' returned from server.");
					}

				} else {
					istream = http.getInputStream();
				}

				// If status code is 301 Moved Permanently or 302 Found ...
				if (statusCode == HttpURLConnection.HTTP_MOVED_PERM
						|| statusCode == HttpURLConnection.HTTP_MOVED_TEMP) {
					// ... do either a foward
					if (isFlagSet(FLAGS_FORWARD)) {
						boolean temporaryForward = (statusCode == HttpURLConnection.HTTP_MOVED_TEMP);

						// Get new location from header field.
						String newLocation = http.getHeaderField("Location");
						// Try getting header in lower case, if no header has
						// been found
						if (newLocation == null || newLocation.length() <= 0)
							newLocation = http.getHeaderField("location");

						// Set new location, disconnect current connection and
						// request to new location.
						URL oldURL = url;
						url = new URL(newLocation);
						http.disconnect();
						Object forwardedResult = call(methodName, params);

						// In case of temporary forward, restore original URL
						// again for next call.
						if (temporaryForward) {
							url = oldURL;
						}

						return forwardedResult;

					} else {
						// ... or throw an exception
						throw new XMLRPCException(
								"The server responded with a http 301 or 302 status "
										+ "code, but forwarding has not been enabled (FLAGS_FORWARD).");

					}
				}

				if (!isFlagSet(FLAGS_IGNORE_STATUSCODE)
						&& statusCode != HttpURLConnection.HTTP_OK) {
					throw new XMLRPCException(
							"The status code of the http response must be 200.");
				}

				// Check for strict parameters
				if (isFlagSet(FLAGS_STRICT)) {
					if (!http.getContentType().startsWith(TYPE_XML)) {
						throw new XMLRPCException(
								"The Content-Type of the response must be text/xml.");
					}
				}

				cookieManager.readCookies(http);

				return responseParser.parse(istream);

			} catch (IOException ex) {
				// If the thread has been canceled this exception will be
				// thrown.
				// So only throw an exception if the thread hasnt been canceled
				// or if the thred has not been started in background.
				if (!canceled || threadId <= 0) {
					throw new XMLRPCException(ex);
				} else {
					throw new CancelException();
				}
			}

		}

		public void cancel() {
			this.canceled = true;
			this.http.disconnect();
		}

		public void run() {
			if (this.listener == null)
				return;
			try {
				XMLRPCClient.this.backgroundCalls.put(
						Long.valueOf(this.threadId), this);
				Object localObject2 = call(this.methodName, this.params);
				this.listener.onResponse(this.threadId, localObject2);
				return;
			} catch (XMLRPCClient.CancelException localCancelException) {
			} catch (XMLRPCServerException localXMLRPCServerException) {
				this.listener.onServerError(this.threadId,
						localXMLRPCServerException);
				return;
			} catch (XMLRPCException localXMLRPCException) {
				this.listener.onError(this.threadId, localXMLRPCException);
				return;
			} finally {
				XMLRPCClient.this.backgroundCalls.remove(Long
						.valueOf(this.threadId));
			}
		}
	}

	private class CancelException extends RuntimeException {
		private CancelException() {
		}
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: de.timroes.axmlrpc.XMLRPCClient JD-Core Version: 0.6.2
 */
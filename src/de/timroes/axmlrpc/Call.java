package de.timroes.axmlrpc;

import de.timroes.axmlrpc.serializer.SerializerHandler;
import de.timroes.axmlrpc.xmlcreator.SimpleXMLCreator;
import de.timroes.axmlrpc.xmlcreator.XmlElement;

public class Call
{
  private String method;
  private Object[] params;

  public Call(String paramString)
  {
    this(paramString, null);
  }

  public Call(String paramString, Object[] paramArrayOfObject)
  {
    this.method = paramString;
    this.params = paramArrayOfObject;
  }

  private XmlElement getXMLParam(Object paramObject)
    throws XMLRPCException
  {
    XmlElement localXmlElement1 = new XmlElement("param");
    XmlElement localXmlElement2 = new XmlElement("value");
    localXmlElement1.addChildren(localXmlElement2);
    localXmlElement2.addChildren(SerializerHandler.getDefault().serialize(paramObject));
    return localXmlElement1;
  }

  public String getXML()
    throws XMLRPCException
  {
    SimpleXMLCreator localSimpleXMLCreator = new SimpleXMLCreator();
    XmlElement localXmlElement1 = new XmlElement("methodCall");
    localSimpleXMLCreator.setRootElement(localXmlElement1);
    XmlElement localXmlElement2 = new XmlElement("methodName");
    localXmlElement2.setContent(this.method);
    localXmlElement1.addChildren(localXmlElement2);
    if ((this.params != null) && (this.params.length > 0))
    {
      XmlElement localXmlElement3 = new XmlElement("params");
      localXmlElement1.addChildren(localXmlElement3);
      Object[] arrayOfObject = this.params;
      int i = arrayOfObject.length;
      for (int j = 0; j < i; j++)
        localXmlElement3.addChildren(getXMLParam(arrayOfObject[j]));
    }
    return localSimpleXMLCreator.toString();
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.Call
 * JD-Core Version:    0.6.2
 */
package de.timroes.axmlrpc;

import de.timroes.axmlrpc.xmlcreator.XmlElement;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLUtil {
	public static Element getOnlyChildElement(NodeList list)
			throws XMLRPCException {
		Element e = null;
		Node n;
		for (int i = 0; i < list.getLength(); i++) {
			n = list.item(i);
			// Strip only whitespace text elements and comments
			if ((n.getNodeType() == Node.TEXT_NODE && n.getNodeValue().trim()
					.length() <= 0)
					|| n.getNodeType() == Node.COMMENT_NODE)
				continue;

			// Check if there is anything else than an element node.
			if (n.getNodeType() != Node.ELEMENT_NODE) {
				throw new XMLRPCException("Only element nodes allowed.");
			}

			// If there was already an element, throw exception.
			if (e != null) {
				throw new XMLRPCException("Element has more than one children.");
			}

			e = (Element) n;

		}

		return e;
	}

	public static String getOnlyTextContent(NodeList list)
			throws XMLRPCException {
		StringBuilder builder = new StringBuilder();
		Node n;

		for (int i = 0; i < list.getLength(); i++) {
			n = list.item(i);

			// Skip comments inside text tag.
			if (n.getNodeType() == Node.COMMENT_NODE) {
				continue;
			}

			if (n.getNodeType() != Node.TEXT_NODE) {
				throw new XMLRPCException(
						"Element must contain only text elements.");
			}

			builder.append(n.getNodeValue());

		}

		return builder.toString();
	}

	public static boolean hasChildElement(NodeList paramNodeList) {
		for (int i = 0; i < paramNodeList.getLength(); i++)
			if (paramNodeList.item(i).getNodeType() == 1)
				return true;
		return false;
	}

	public static XmlElement makeXmlTag(String paramString1, String paramString2) {
		XmlElement localXmlElement = new XmlElement(paramString1);
		localXmlElement.setContent(paramString2);
		return localXmlElement;
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: de.timroes.axmlrpc.XMLUtil JD-Core Version: 0.6.2
 */
package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import org.w3c.dom.Element;

public class IntSerializer
  implements Serializer
{
  public Object deserialize(Element paramElement)
    throws XMLRPCException
  {
    return Integer.valueOf(Integer.parseInt(XMLUtil.getOnlyTextContent(paramElement.getChildNodes())));
  }

  public XmlElement serialize(Object paramObject)
  {
    return XMLUtil.makeXmlTag("int", paramObject.toString());
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.serializer.IntSerializer
 * JD-Core Version:    0.6.2
 */
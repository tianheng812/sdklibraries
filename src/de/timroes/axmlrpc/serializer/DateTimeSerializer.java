package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.w3c.dom.Element;

public class DateTimeSerializer
  implements Serializer
{
  private static final String DATETIME_FORMAT = "yyyyMMdd'T'HH:mm:ss";
  private static final SimpleDateFormat DATE_FORMATER = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");

  public Object deserialize(Element paramElement)
    throws XMLRPCException
  {
    try
    {
      Date localDate = DATE_FORMATER.parse(XMLUtil.getOnlyTextContent(paramElement.getChildNodes()));
      return localDate;
    }
    catch (ParseException localParseException)
    {
      throw new XMLRPCException("Unable to parse given date.", localParseException);
    }
  }

  public XmlElement serialize(Object paramObject)
  {
    return XMLUtil.makeXmlTag("dateTime.iso8601", DATE_FORMATER.format(paramObject));
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.serializer.DateTimeSerializer
 * JD-Core Version:    0.6.2
 */
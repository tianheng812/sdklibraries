package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import org.w3c.dom.Element;

public abstract interface Serializer
{
  public abstract Object deserialize(Element paramElement)
    throws XMLRPCException;

  public abstract XmlElement serialize(Object paramObject);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.serializer.Serializer
 * JD-Core Version:    0.6.2
 */
package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import java.text.DecimalFormat;
import org.w3c.dom.Element;

public class DoubleSerializer
  implements Serializer
{
  public Object deserialize(Element paramElement)
    throws XMLRPCException
  {
    return Double.valueOf(Double.parseDouble(XMLUtil.getOnlyTextContent(paramElement.getChildNodes())));
  }

  public XmlElement serialize(Object paramObject)
  {
    return XMLUtil.makeXmlTag("double", new DecimalFormat("#0.0#").format(((Double)paramObject).doubleValue()));
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.serializer.DoubleSerializer
 * JD-Core Version:    0.6.2
 */
package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import org.w3c.dom.Element;

public class BooleanSerializer
  implements Serializer
{
  public Object deserialize(Element paramElement)
    throws XMLRPCException
  {
    if (XMLUtil.getOnlyTextContent(paramElement.getChildNodes()).equals("1"))
      return Boolean.TRUE;
    return Boolean.FALSE;
  }

  public XmlElement serialize(Object paramObject)
  {
    if (((Boolean)paramObject).booleanValue() == true);
    for (String str = "1"; ; str = "0")
      return XMLUtil.makeXmlTag("boolean", str);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.serializer.BooleanSerializer
 * JD-Core Version:    0.6.2
 */
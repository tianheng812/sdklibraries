package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLRPCRuntimeException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ArraySerializer implements Serializer {
	private static final String ARRAY_DATA = "data";
	private static final String ARRAY_VALUE = "value";

	public Object deserialize(Element paramElement) throws XMLRPCException {
		ArrayList localArrayList = new ArrayList();
		Element data = XMLUtil
				.getOnlyChildElement(paramElement.getChildNodes());
		if (!ARRAY_DATA.equals(data.getNodeName()))
			throw new XMLRPCException("The array must contain one data tag.");
		Node value;
		for (int i = 0; i < data.getChildNodes().getLength(); i++) {

			value = data.getChildNodes().item(i);

			// Strip only whitespace text elements and comments
			if (value == null
					|| (value.getNodeType() == Node.TEXT_NODE && value
							.getNodeValue().trim().length() <= 0)
					|| value.getNodeType() == Node.COMMENT_NODE)
				continue;

			if (value.getNodeType() != Node.ELEMENT_NODE) {
				throw new XMLRPCException("Wrong element inside of array.");
			}

			localArrayList.add(SerializerHandler.getDefault().deserialize(
					(Element) value));

		}
		return localArrayList.toArray();
	}

	public XmlElement serialize(Object paramObject) {
		Object[] arrayOfObject = (Object[]) paramObject;
		XmlElement localXmlElement1 = new XmlElement("array");
		XmlElement localXmlElement2 = new XmlElement("data");
		localXmlElement1.addChildren(localXmlElement2);
		try {
			int i = arrayOfObject.length;
			for (int j = 0; j < i; j++) {
				Object localObject = arrayOfObject[j];
				XmlElement localXmlElement3 = new XmlElement("value");
				localXmlElement3.addChildren(SerializerHandler.getDefault()
						.serialize(localObject));
				localXmlElement2.addChildren(localXmlElement3);
			}
		} catch (XMLRPCException localXMLRPCException) {
			throw new XMLRPCRuntimeException(localXMLRPCException);
		}
		return localXmlElement1;
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: de.timroes.axmlrpc.serializer.ArraySerializer JD-Core
 * Version: 0.6.2
 */
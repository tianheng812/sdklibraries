package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import org.w3c.dom.Element;

class LongSerializer
  implements Serializer
{
  public Object deserialize(Element paramElement)
    throws XMLRPCException
  {
    return Long.valueOf(Long.parseLong(XMLUtil.getOnlyTextContent(paramElement.getChildNodes())));
  }

  public XmlElement serialize(Object paramObject)
  {
    return XMLUtil.makeXmlTag("i8", ((Long)paramObject).toString());
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.serializer.LongSerializer
 * JD-Core Version:    0.6.2
 */
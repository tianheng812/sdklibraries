package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import de.timroes.base64.Base64;
import org.w3c.dom.Element;

public class Base64Serializer
  implements Serializer
{
  public Object deserialize(Element paramElement)
    throws XMLRPCException
  {
    return Base64.decode(XMLUtil.getOnlyTextContent(paramElement.getChildNodes()));
  }

  public XmlElement serialize(Object paramObject)
  {
    return XMLUtil.makeXmlTag("base64", Base64.encode((Byte[])paramObject));
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.serializer.Base64Serializer
 * JD-Core Version:    0.6.2
 */
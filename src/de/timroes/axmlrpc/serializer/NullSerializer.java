package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import org.w3c.dom.Element;

public class NullSerializer
  implements Serializer
{
  public Object deserialize(Element paramElement)
    throws XMLRPCException
  {
    return null;
  }

  public XmlElement serialize(Object paramObject)
  {
    return new XmlElement("nil");
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.serializer.NullSerializer
 * JD-Core Version:    0.6.2
 */
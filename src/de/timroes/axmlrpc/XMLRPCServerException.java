package de.timroes.axmlrpc;

public class XMLRPCServerException extends XMLRPCException
{
  private int errornr;

  public XMLRPCServerException(String paramString, int paramInt)
  {
    super(paramString);
    this.errornr = paramInt;
  }

  public int getErrorNr()
  {
    return this.errornr;
  }

  public String getMessage()
  {
    return super.getMessage() + " [" + this.errornr + "]";
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.XMLRPCServerException
 * JD-Core Version:    0.6.2
 */
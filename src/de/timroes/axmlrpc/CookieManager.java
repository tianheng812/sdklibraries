package de.timroes.axmlrpc;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class CookieManager {
	private static final String COOKIE = "Cookie";
	private static final String SET_COOKIE = "Set-Cookie";
	private Map<String, String> cookies = new HashMap();
	private int flags;

	public CookieManager(int paramInt) {
		this.flags = paramInt;
	}

	public void clearCookies() {
		this.cookies.clear();
	}

	public void readCookies(HttpURLConnection http) {
		// Only save cookies if FLAGS_ENABLE_COOKIES has been set.
		if ((flags & XMLRPCClient.FLAGS_ENABLE_COOKIES) == 0)
			return;

		String cookie, key;
		String[] split;

		// Extract every Set-Cookie field and put the cookie to the cookies map.
		for (int i = 0; i < http.getHeaderFields().size(); i++) {
			key = http.getHeaderFieldKey(i);
			if (key != null
					&& SET_COOKIE.toLowerCase().equals(key.toLowerCase())) {
				cookie = http.getHeaderField(i).split(";")[0];
				split = cookie.split("=");
				if (split.length >= 2)
					cookies.put(split[0], split[1]);
			}
		}
	}

	public void setCookies(HttpURLConnection paramHttpURLConnection) {
		if ((0x4 & this.flags) == 0)
			return;
		String str = "";
		Iterator localIterator = this.cookies.entrySet().iterator();
		while (localIterator.hasNext()) {
			Map.Entry localEntry = (Map.Entry) localIterator.next();
			str = str + (String) localEntry.getKey() + "="
					+ (String) localEntry.getValue() + "; ";
		}
		paramHttpURLConnection.setRequestProperty(COOKIE, str);
	}
}

/*
 * Location: F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name: de.timroes.axmlrpc.CookieManager JD-Core Version: 0.6.2
 */
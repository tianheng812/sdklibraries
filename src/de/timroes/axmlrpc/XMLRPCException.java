package de.timroes.axmlrpc;

public class XMLRPCException extends Exception
{
  public XMLRPCException()
  {
  }

  public XMLRPCException(Exception paramException)
  {
    super(paramException);
  }

  public XMLRPCException(String paramString)
  {
    super(paramString);
  }

  public XMLRPCException(String paramString, Exception paramException)
  {
    super(paramString, paramException);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.XMLRPCException
 * JD-Core Version:    0.6.2
 */
package de.timroes.axmlrpc.xmlcreator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class XmlElement
{
  private List<XmlElement> children = new ArrayList<XmlElement>();
  private String content;
  private String name;

  public XmlElement(String paramString)
  {
    this.name = paramString;
  }

  public void addChildren(XmlElement paramXmlElement)
  {
    this.children.add(paramXmlElement);
  }

  public void setContent(String paramString)
  {
    this.content = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((this.content != null) && (this.content.length() > 0))
    {
      localStringBuilder.append("\n<").append(this.name).append(">").append(this.content).append("</").append(this.name).append(">\n");
      return localStringBuilder.toString();
    }
    if (this.children.size() > 0)
    {
      localStringBuilder.append("\n<").append(this.name).append(">");
      Iterator<XmlElement> localIterator = this.children.iterator();
      while (localIterator.hasNext())
        localStringBuilder.append(((XmlElement)localIterator.next()).toString());
      localStringBuilder.append("</").append(this.name).append(">\n");
      return localStringBuilder.toString();
    }
    localStringBuilder.append("\n<").append(this.name).append("/>\n");
    return localStringBuilder.toString();
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.xmlcreator.XmlElement
 * JD-Core Version:    0.6.2
 */
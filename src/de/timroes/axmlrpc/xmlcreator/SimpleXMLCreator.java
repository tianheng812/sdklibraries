package de.timroes.axmlrpc.xmlcreator;

public class SimpleXMLCreator
{
  private XmlElement root;

  public void setRootElement(XmlElement paramXmlElement)
  {
    this.root = paramXmlElement;
  }

  public String toString()
  {
    return "<?xml version=\"1.0\"?>\n" + this.root.toString();
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.xmlcreator.SimpleXMLCreator
 * JD-Core Version:    0.6.2
 */
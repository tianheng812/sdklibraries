package de.timroes.axmlrpc;

public class XMLRPCRuntimeException extends RuntimeException
{
  public XMLRPCRuntimeException(Exception paramException)
  {
    super(paramException);
  }

  public XMLRPCRuntimeException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.XMLRPCRuntimeException
 * JD-Core Version:    0.6.2
 */
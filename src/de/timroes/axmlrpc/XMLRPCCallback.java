package de.timroes.axmlrpc;

public abstract interface XMLRPCCallback
{
  public abstract void onError(long paramLong, XMLRPCException paramXMLRPCException);

  public abstract void onResponse(long paramLong, Object paramObject);

  public abstract void onServerError(long paramLong, XMLRPCServerException paramXMLRPCServerException);
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.XMLRPCCallback
 * JD-Core Version:    0.6.2
 */
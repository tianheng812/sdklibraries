package de.timroes.axmlrpc;

import de.timroes.base64.Base64;
import java.net.HttpURLConnection;

public class AuthenticationManager
{
  private String pass;
  private String user;

  public void clearAuthData()
  {
    this.user = null;
    this.pass = null;
  }

  public void setAuthData(String paramString1, String paramString2)
  {
    this.user = paramString1;
    this.pass = paramString2;
  }

  public void setAuthentication(HttpURLConnection paramHttpURLConnection)
  {
    if ((this.user == null) || (this.pass == null) || (this.user.length() <= 0) || (this.pass.length() <= 0))
      return;
    String str = Base64.encode(this.user + ":" + this.pass);
    paramHttpURLConnection.addRequestProperty("Authorization", "Basic " + str);
  }
}

/* Location:           F:\DevTools\android\apktool\dex2jar-0.0.9.15\classes_dex2jar.jar
 * Qualified Name:     de.timroes.axmlrpc.AuthenticationManager
 * JD-Core Version:    0.6.2
 */